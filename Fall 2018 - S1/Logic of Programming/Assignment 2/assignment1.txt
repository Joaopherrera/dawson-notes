import java.util.Scanner;
public class GradeCalculator
{
  public static void main(String[] args)
  {
   Scanner reader = new Scanner(System.in);
   
    //Lines to get input for the user for assignments //
    System.out.println("Grade for assignment 1: ");
    double asst1 = reader.nextDouble();   
    System.out.println("Grade for assignment 2: ");
    double asst2 = reader.nextDouble();
    System.out.println("Grade for assignment 3: ");
    double asst3 = reader.nextDouble();
    System.out.println("Grade for assignment 4: ");
    double asst4 = reader.nextDouble();
       
    //Lines to get input from the user for the Quizes//
    System.out.println("Grade for Quiz 1: ");
    double quiz1 = reader.nextDouble();
    System.out.println("Grade for Quiz 2: ");
    double quiz2 = reader.nextDouble();
    System.out.println("Grade for Quiz 3: ");
    double quiz3 = reader.nextDouble();        
       
    //Lines to get input from the user for the Midterm test//
    System.out.println("Grade for Midterm Exam: ");
    double midtermE = reader.nextDouble();
    
    //Lines to get input from the user for the Final test//
    System.out.println("Grade for the Final Exam: ");
    double finalE = reader.nextDouble();
    
    //Validate grades//
    if ((validate(asst1)) && (validate(asst2)) && (validate(asst3)) && (validate(asst4)) && (validate(quiz1)) && (validate(quiz2)) && (validate(quiz3)) && (validate(midtermE)) && (validate(finalE)))
    {
      //Keep going with the program.//
    }
    else
    {
      System.out.println("One of the grades is invalid.");
      return;
    }
    //Averages//
    double asstAvg = calcAsstAvg(asst1, asst2, asst3, asst4); // Callig method to calculate the average for the assignment.// 
    double quizAvg = calcQuizAvg(quiz1, quiz2, quiz3);//Calling method to calculate average for the quizes//
    double examsAvg = calcExamsAvg(quizAvg, midtermE, finalE);//Calling method to calculate the average to all exams//
    
    double totalAvg = calcOverallAvg(asstAvg, quizAvg, midtermE, finalE); //Calling method to calculate the overall average of grades//
    
    //Print statements fot the summary//
    System.out.println("Average Assignment: " + asstAvg + "%");
    System.out.println("Average quiz: " + quizAvg + "%");
    System.out.println("Midterm: " + midtermE + "%");
    System.out.println("Final: " + finalE + "%");

    //Validate to see if you passed or not//
    if (asstAvg < 50 || examsAvg < 48)
    {
      if (totalAvg > 50)
      {
        System.out.println("Overall Average: 50%");
      }
      else 
      {
        System.out.println("Overall Average: " + totalAvg + "%");
      }
      System.out.println("You Failed!!!");
    }
    else
    {
      System.out.println("Overall Average: " + totalAvg + "%");
      System.out.println("You Passed!!!");
    }
  }
 
  
  //Methods//
  public static boolean validate(double grade)//Method to validate the range of the grades//
  {
    if (grade >= 0 && grade <= 100)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  //Method to calculate the assingment avegare//
  public static double calcAsstAvg(double asst1, double asst2, double asst3, double asst4)
  {
    return (asst1 + asst2 + asst3 + asst4)/4;
  }
  //Method to caculate the quiz average//
  public static double calcQuizAvg(double quiz1, double quiz2, double quiz3)
  {
    return (quiz1 + quiz2 + quiz3)/3;
  }
  //Method to calculate the overall average of the grades//
  public static double calcOverallAvg(double asstAvg, double quizAvg, double midtermE, double finalE)
  {
    return ((asstAvg * 0.20) + (quizAvg * 0.10) + (midtermE * .25) + (finalE * .45));
  }
  //Method to calculate the average of all the examinations(quizezs, midterm and final)//
  public static double calcExamsAvg(double quizAvg, double midtermE, double finalE)
  {
    return ((quizAvg * .10) + (midtermE * .25) + (finalE * .45));
  }
}