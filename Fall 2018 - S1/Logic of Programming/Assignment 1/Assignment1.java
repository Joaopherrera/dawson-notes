public class Assignment1
{
  public static void main(String[] args)
  {
    java.util.Scanner reader = new java.util.Scanner(System.in);
    
    System.out.println("Enter your number:");
    int number = reader.nextInt();
    
    //Slipt into four digigts.
    int numA = number / 1000;
    int numberM = number % 1000;
    int numB = numberM / 100;
    numberM = numberM % 100;
    int numC = numberM / 10;
    numberM = numberM % 10;
    int numD = numberM;
        
    //Adding +6 to each digit.
    numA = numA + 6;
    numB = numB + 6;
    numC = numC + 6;
    numD = numD + 6;
    
    //Remainder for each number divided by 10.
    numA = numA % 10;
    numB = numB % 10;
    numC = numC % 10;
    numD = numD % 10;
    
    //Creating new varialbes to swap A and D.
    int numA1 = numD;
    int numD1 = numA;
    
    //Putting all four digits together.
    numA1 = numA1 * 1000;
    numB = numB * 100;
    numC = numC * 10;
    
    int numberE = numA1 + numB + numC + numD1;
    
    //Output
    
    System.out.println("Original number is: " + number);
    System.out.println("Encrypted number is: " + numberE);
    System.out.println("Encryption by: Joao Pedro M. Herrera.");    
  }
}