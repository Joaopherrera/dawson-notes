import java.util.*;
public class HangmanGame{
  
  public static void main(String[] args)
  {
   Scanner reader = new Scanner(System.in);
   
   System.out.println("Enter your word:");
   String word = reader.next();
   
   runGame(word);
  }
  
  //Method to see if the letter guessed is one of the letters inthe word//
  public static int isLetterInWord (String word, char c)
  {
    int count = 0;
    while (count <= 3)
    {
      if (toUpperCase(word.charAt(count)) == c)
      {
        return count;
      }
      count++;
    }
    return -1;
  }
  
  //Method to convert the letters to upper case//
  public static char toUpperCase (char c)
  {
    if (c >= 'a' && c <= 'z')
    {
      c = (char)(c - 32);
    }
    return c; 
  }
  
  //Method to run the game itself//
  public static void runGame (String word)
  {
    Scanner reader = new Scanner(System.in);
       
    boolean pos0 = false;
    boolean pos1 = false;
    boolean pos2 = false;
    boolean pos3 = false;
    int misses = 0;
    
    while ((pos0 && pos1 && pos2 && pos3) == false && misses < 6)
    {
      System.out.println("Enter your guess:");
      char c = (toUpperCase(reader.next().charAt(0)));
      
      if (isLetterInWord(word, c) == 0)
      {
        pos0 = true;
      }
      else if (isLetterInWord(word, c) == 1)
      {
        pos1 = true;
      }
      else if (isLetterInWord(word, c) == 2)
      {
        pos2 = true;
      }
      else if (isLetterInWord(word, c) == 3)
      {
        pos3 = true;
      }
      else if(isLetterInWord(word, c) == -1)
      {
        misses++;
      }
      
      printHanging(misses);
      printWord(word, pos0, pos1, pos2, pos3);
    
    }
   }
    
  //printHanging takes as input the number of misses the player has made,
  //and prints out the hangman corresponding to the number of misses they have made
  //printHanging does not return any values
  public static void printHanging(int misses)
  {
    //Initially, no part of the hangman should be drawn, leave blank spaces for each
    String head = " ", body = " ", larm = " ", rarm = " ", lleg = " ", rleg = " ";
    switch (misses)
    {
      default: head = "0"; //After making 6 or more misses, we add on the head - the player has lost
      case 5: rleg = "\\"; //After making 5 or more misses, we draw the right leg
      case 4: lleg = "/";  //After making 4 or more misses, we draw the left leg
      case 3: rarm = "\\"; //After making 3 or more misses, we draw the right arm
      case 2: larm = "/";  //After making 2 or more misses, we draw the left arm
      case 1: body = "|";  //After making 1 or more misses, we draw the left arm
      case 0: ;            //With 0 misses, nothing should be drawn
    }    
    //Print statement which draws the hangman
    System.out.println("___________\n" +
                       "|         |\n" +
                       "|         " + head +"\n" +
                       "|        " + larm + body + rarm + "\n" +
                       "|       " + larm + " " + body + " " + rarm + "\n" +
                       "|        " + lleg + " " + rleg + "\n" +
                       "|       " + lleg + "   " + rleg + "\n" +
                       "|__________\n");
   }
  
  
  
  //printWord prints the 4 letter word based on which letters have been correctly guessed.
  //It takes as input the String corresponding to the word being guessed,
  //as well as a boolean value for each letter in the word - representing whether or not that
  //particular letter has been guessed yet.
  public static void printWord(String word, boolean pos0, boolean pos1, boolean pos2, boolean pos3)
  {
    //if letter at position 0 has been guessed, print the character at position 0, otherwise print _
    if(pos0)
    {
      System.out.print(" " + word.charAt(0) + " ");
    }
    else
    {
      System.out.print(" _ ");
    }
    //if letter at position 1 has been guessed, print the character at position 1, otherwise print _
    if(pos1)
    {
      System.out.print(" " + word.charAt(1) + " ");
    }
    else
    {
      System.out.print(" _ ");
    }
    //if letter at position 2 has been guessed, print the character at position 2, otherwise print _
    if(pos2)
    {
      System.out.print(" " + word.charAt(2) + " ");
    }
    else
    {
      System.out.print(" _ ");
    }
    
    //if letter at position 3 has been guessed, print the character at position 3, otherwise print _
    if (pos3)
    {
      System.out.println(" " + word.charAt(3) + " ");
    }
    else
    {
      System.out.println(" _ ");
    }   
  }  
}