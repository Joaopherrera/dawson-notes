package ca.joaopmh.finalexam;

import androidx.appcompat.app.AppCompatActivity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    private final String TAG = "Testing";
    private ListView listView;
    private EditText userInput;
    private String input = "Name";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = this.getPreferences(Context.MODE_PRIVATE);
        input = sharedPreferences.getString("searchBarString", "Name");

        listView = (ListView) findViewById(R.id.listView);
        userInput = (EditText) findViewById(R.id.editTextTextPersonName);
        userInput.setText(input);

        getAllContacts();
    }


    private void getAllContacts()
    {
        List<String> result = new ArrayList<>();
        //Setting the projection
        String[] projections =
                {
                        ContactsContract.Contacts.DISPLAY_NAME
                };

        //Querying the results
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, projections, null, null, null);

        //Getting the info from the cursor
        while (cursor.moveToNext())
        {
            result.add(cursor.getString(0));
            Log.i(TAG, cursor.getString(0));
        }

        //Populating the adapter and the list view
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, result);
        listView.setAdapter(arrayAdapter);

        //Creating the event listener for each entry in the list view
        listView.setOnItemClickListener((arg0, v, position, arg3) ->
        {
            String selectedContacts = result.get(position);
            Toast.makeText(getApplicationContext(), "You should call "+ selectedContacts, Toast.LENGTH_SHORT).show();
        });

        cursor.close();
    }


    public void getAllContacts(String input)
    {
        List<String> result = new ArrayList<>();

        //Setting the projection
        String[] projections =
                {
                        ContactsContract.Contacts.DISPLAY_NAME
                };

        //Querying the results
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, projections, null, null, null);

        //Getting the info from the cursor
        while (cursor.moveToNext())
        {
            result.add(cursor.getString(0));
            Log.i(TAG, cursor.getString(0));
        }

        //Filtering the results based on the input
        List<String> filteredList = filterList(input, result);

        //Populating the adapter and the list view
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, filteredList);
        listView.setAdapter(arrayAdapter);

        //Creating the event listener for each entry in the list view
        listView.setOnItemClickListener((arg0, v, position, arg3) ->
        {
            String selectedContacts = filteredList.get(position);
            Toast.makeText(getApplicationContext(), "You should call "+ selectedContacts, Toast.LENGTH_SHORT).show();
        });
        cursor.close();
    }


    private List<String> filterList(String input, List<String> result)
    {
        List<String> filteredList = new ArrayList<>();
        //Going thru the result and selecting the names that contains the string
        for (String name : result)
        {
            if (name.contains(input)) { filteredList.add(name); }
        }
        return filteredList;
    }

    public void onClick(View view) 
    {
        input = userInput.getText().toString();
        
        getAllContacts(input);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //Creating the About menu and setting the event listener to display the toast
        MenuItem about = menu.add("About");
        about.setOnMenuItemClickListener(menuItem ->
        {
            Toast.makeText(getApplicationContext(), "This application was made by me, Joao.", Toast.LENGTH_LONG).show();
            return false;
        });
        return true;
    }


    @Override
    protected void onStop()
    {
        super.onStop();
        //Creating the shared preference object to save the string
        SharedPreferences sharedPreferences = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("searchBarString", input);
        editor.apply();
    }
}