package ca.joaopmh.bmi_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Student ID:1835031
 */
public class MainActivity extends AppCompatActivity
{
    private EditText inputWeight;
    private EditText inputHeight;
    private TextView resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Creating the objects
        inputWeight = (EditText) findViewById(R.id.inputWeight);
        inputHeight = (EditText) findViewById(R.id.inputHeight);
        resultText = (TextView) findViewById(R.id.resultText);
    }

    //Method to run onClick, verifies the input and if they are good, call calculateBMI
    public void calculate(View view)
    {
        double height = Double.parseDouble(inputHeight.getText().toString());
        double weight = Double.parseDouble(inputWeight.getText().toString());

        if (weight > 10 && weight < 300)
        {
            if (height > 0.2 && height < 2.2)
            {
                calculateBMI(height, weight);
            }
            else { resultText.setText("Please enter a height between 0.2m and 2.2m"); }
        }
        else { resultText.setText("Please enter a weight between 10Kg and 300Kg"); }
    }

    //Method to calculate the BMI
    public void calculateBMI(double height, double weight)
    {
        double bmi = weight / Math.pow(height,2);

        if (bmi < 18.5)
        {
            resultText.setText("Your Bmi: " + bmi + ". Risk of developing problems such as nutritional deficiency and osteoporosis");
        }
        else if (bmi > 18.5 && bmi < 23)
        {
            resultText.setText("Your Bmi: " + bmi + ". Low Risk (healthy range)");
        }
        else if (bmi > 23 && bmi < 27.5)
        {
            resultText.setText("Your Bmi: " + bmi + ". Moderate risk of developing heart disease, high blood pressure, stroke, diabetes");
        }
        else
        {
            resultText.setText("Your Bmi: " + bmi + ". High risk of developing heart disease, high blood pressure, stroke, diabetes");
        }
    }
}