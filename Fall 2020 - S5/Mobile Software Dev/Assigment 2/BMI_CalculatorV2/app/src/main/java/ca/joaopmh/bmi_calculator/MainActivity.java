package ca.joaopmh.bmi_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Student ID:1835031
 */
public class MainActivity extends AppCompatActivity
{
    private EditText inputWeight;
    private EditText inputHeight;
    private TextView resultText;
    private TextView bmiResultText;
    private ImageView imageView;
    private TextView textViewInputHeight;
    private TextView textViewInputWeight;
    private Button systemBtn;
    private boolean isMetric = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Creating the objects
        inputWeight = (EditText) findViewById(R.id.inputWeight);
        inputHeight = (EditText) findViewById(R.id.inputHeight);
        resultText = (TextView) findViewById(R.id.resultText);
        bmiResultText = (TextView) findViewById(R.id.bmiResultText);
        imageView = (ImageView) findViewById(R.id.imageView);
        textViewInputHeight = (TextView) findViewById(R.id.textViewInputHeight);
        textViewInputWeight = (TextView) findViewById(R.id.textViewInputWeight);
        systemBtn = (Button) findViewById(R.id.systemBtn);

        //Setting the texts to metric
        imperialOrMetricSystem();
    }

    //Method to set the texts according to the isMetric value
    private void imperialOrMetricSystem()
    {
        if (!isMetric)
        {
            textViewInputHeight.setText(getResources().getText(R.string.heightInputFeet));
            textViewInputWeight.setText(getResources().getText(R.string.weightInputPound));
            systemBtn.setText(getResources().getText(R.string.systemBtnMetric));
        }
        else
        {
            textViewInputHeight.setText(getResources().getText(R.string.heightInput));
            textViewInputWeight.setText(getResources().getText(R.string.weightInput));
            systemBtn.setText(getResources().getText(R.string.systemBtnImperial));
        }
    }

    //Method to run onClick, verifies the input and if they are good, call calculateBMI
    public void calculate(View view)
    {
        //If input fields are empty, don't do anything, if not continue
        if (!(inputHeight.getText().toString().isEmpty() || inputWeight.getText().toString().isEmpty()))
        {
            double height = Double.parseDouble(inputHeight.getText().toString());
            double weight = Double.parseDouble(inputWeight.getText().toString());

            //Checking if its metric or not and do the according validation
            if (isMetric)
            {
                System.out.println("**************");

                if (weight > 10 && weight < 300)
                {
                    if (height > 0.2 && height < 2.2)
                    {
                        calculateBMI(height, weight);
                    }
                    else { resultText.setText(getResources().getText(R.string.invalidHeightError)); }
                }
                else { resultText.setText(getResources().getText(R.string.invalidWeightError)); }
            }
            else
            {
                if (weight > 22.0462 && weight < 661.387)
                {
                    if (height > 7.874 && height < 86.6142)
                    {
                        calculateBMI(height, weight);
                    }
                    else { resultText.setText(getResources().getText(R.string.invalidHeightError)); }
                }
                else { resultText.setText(getResources().getText(R.string.invalidWeightError)); }
            }
        }
    }

    //Method to calculate the BMI
    public void calculateBMI(double height, double weight)
    {
        double bmi = 0.0;
        //Checking if isMetric is true or not to use the appropriate formula
        if (isMetric)
        {
            bmi = weight / Math.pow(height,2);
        }
        else
        {
            bmi = weight / Math.pow(height,2) * 703;
        }

        bmiResultText.setText(String.valueOf(bmi));

        //Under
        if (bmi < 18.5)
        {
            imageView.setImageResource(R.drawable.under);
            resultText.setText(getResources().getText(R.string.lowBMI));
        }
        //Normal
        else if (bmi > 18.5 && bmi < 23)
        {
            imageView.setImageResource(R.drawable.normal);
            resultText.setText(getResources().getText(R.string.healthyBMI));
        }
        //over
        else if (bmi > 23 && bmi < 27.5)
        {
            imageView.setImageResource(R.drawable.over);
            resultText.setText(getResources().getText(R.string.okBMI));
        }
        //Obese
        else
        {
            imageView.setImageResource(R.drawable.obese);
            resultText.setText(getResources().getText(R.string.highBMI));
        }
    }

    //Method to call the intent and open the browser
    public void intentOpenLink(View view)
    {
        String url = "http://en.wikipedia.org/wiki/Body_mass_index";

        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    //OnClick event for the systemBtn to toggle between metric and imperial
    public void onSystemBtnClicked(View view)
    {
        if (isMetric) { isMetric = false; }
        else { isMetric = true; }

        imperialOrMetricSystem();
    }
}