package ca.joaopmh.fahrenheittocelsius_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private EditText input;
    private TextView answer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.input);
        answer = (TextView) findViewById(R.id.answer);
    }

    public void calculate(View view)
    {
        double tempF = Double.parseDouble(input.getText().toString());

        Double tempC = ((tempF - 32) * 5) / 9;

        answer.setText(tempC.toString());
    }
}