/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.echoclientjavafxgui;

import inet.ipaddr.IPAddressString;
import inet.ipaddr.IPAddress;
/**
 *
 * @author Carlton Davis
 */
public class MyUtility {
    
    public MyUtility() {
    }
    
    public int IP_checker(String hostName) { 
        int return_value=0;
        if(hostName.equals("localhost")) {
            return_value = 1;
        }
        else {
            IPAddressString str = new IPAddressString(hostName);
            IPAddress addr = str.getAddress();
            if(addr != null) {
                return_value = 1;
            }
        }
        return return_value;
    }
}
