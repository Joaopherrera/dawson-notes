import java.net.*;  // for Socket, ServerSocket, and InetAddress
import java.io.*;   // for IOException and Input/OutputStream
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Random;

public class TCPEchoServerEgCode {
    private static final int BUFSIZE = 28;   // Size of receive buffer

    public static void main(String[] args) throws IOException {

        if (args.length != 1) {
            // Test for correct # of args
            throw new IllegalArgumentException("Parameter(s): <Port>");
        }

        int servPort = Integer.parseInt(args[0]);

        // Create a server socket to accept client connection requests
        ServerSocket servSock = new ServerSocket(servPort);

        int recvMsgSize;   // Size of received message
        byte[] receiveBuf = new byte[BUFSIZE];  // Receive buffer

        while (true) {
            // Run forever, accepting and servicing connections
            Socket clntSock = servSock.accept();     // Get client connection

            SocketAddress clientAddress = clntSock.getRemoteSocketAddress();
            System.out.println("Handling client at " + clientAddress);

            InputStream in = clntSock.getInputStream();
            OutputStream out = clntSock.getOutputStream();

            // Receive until client closes connection, indicated by -1 return
            while ((recvMsgSize = in.read(receiveBuf)) != -1) {
                System.out.println(receiveBuf[1]);

                if (receiveBuf[0] == 1)
                {
                    out.write(perfomCalculation(receiveBuf), 0, recvMsgSize);
                }
                else
                {
                    out.write(receiveBuf, 0, recvMsgSize);
                }

            }
            clntSock.close();
        }

    }

    private static byte[] perfomCalculation(byte[] inputArray)
    {
        Random random = new Random();
        int salt = random.nextInt(10 + 1) + 1;

        double number1 = 0;
        double number2 = 0;

        try
        {
            number1 = Double.parseDouble(Arrays.toString(new byte[]{inputArray[1], inputArray[2], inputArray[3], inputArray[4], inputArray[5], inputArray[6], inputArray[7]}));

            number2 = Double.parseDouble(Arrays.toString(new byte[]{inputArray[1], inputArray[2], inputArray[3], inputArray[4], inputArray[5], inputArray[6], inputArray[7]}));
        }
        catch (NumberFormatException e)
        {
            System.out.println("yeah it should throw the error since i didnt have time to finish this long question.");
        }

        double summedNum = number1 + number2;

        double result = summedNum + salt;

        byte[] resultByteArr = DoubleToBytes.doubleToByteArray(result);
        byte[] saltByteArr = DoubleToBytes.doubleToByteArray(salt);
        byte[] number1Byte = DoubleToBytes.doubleToByteArray(number1);
        //byte[] number2Byte = DoubleToBytes.doubleToByteArray(number2);

        //find a way to return this
        byte[] returnArray = new byte[26];

        returnArray[0] = 1;

        for (int i = 1; i < 8; i++) {
            returnArray[i] = number1Byte[i - 1];
        }

        for (int i = 9; i < 16; i++) {
            //returnArray[i] = number2Byte[i - 9];
        }

        for (int i = 17; i < 24; i++) {
            returnArray[i] = resultByteArr[i - 17];
        }

        returnArray[25] = Integer.valueOf(salt).byteValue();

        return returnArray;
    }
}