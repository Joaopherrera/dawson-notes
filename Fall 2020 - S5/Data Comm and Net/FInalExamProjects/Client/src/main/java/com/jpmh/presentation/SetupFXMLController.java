/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jpmh.presentation;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import com.jpmh.EchoClientFXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import utils.MyUtility;

/**
 *
 * @author Carlton Davis
 */
public class SetupFXMLController
{

    private Socket socket; // Client socket
    private DataInputStream in; // Socket input stream
    private OutputStream out; // Socket output stream

    @FXML
    private TextField textField1, textField2;

    @FXML
    private TextArea textArea1, textArea2;

    private String server;
    private int servPort;
    @FXML
    private Button myButton;
    private FXMLLoader echoClientGuiLoader;
    private ClientFXMLController controller;
    private BorderPane root;
    
    //Instantiate MyUtility class
    MyUtility myUtility = new MyUtility();
    @FXML
    private GridPane gridPane;
    
    //Event handler to get server name
    @FXML
    private void getTextField1Text(KeyEvent event)
    {
        if(event.getCode().equals(KeyCode.ENTER))
        {
            server = textField1.getText();
            if(myUtility.IP_checker(server) != 1)
            {
                textArea1.appendText("ERROR: "+ server + " isn't valid IP address." +"\n");
            }
            else
            {
                textArea1.appendText("Server name verified" +"\n");  
            }
            textField1.clear();
        }
    }
     
     //Event handler to get the port where the server runs
    @FXML
    private void getTextField2Text(KeyEvent event)
    {
        if(event.getCode().equals(KeyCode.ENTER))
        {
            try
            {
                servPort = Integer.parseInt(textField2.getText().trim());
                textArea2.appendText("Your entered: " + servPort +"\n");
            }
            catch (NumberFormatException nfe)
            {
                textArea2.appendText("ERROR: "+ textField2.getText() + " isn't valid port." +"\n");
            }
            textField2.clear();
        }
    }
    
    
    //Event handler to handle the clicking of the button
    @FXML
    private void buttonClicked(ActionEvent eventu) throws IOException
    {
        //Load the FXML file for the client GUI
        echoClientGuiLoader = new FXMLLoader(getClass().getResource("/fxml/clientFXML.fxml"));
        try
        {
           root = (BorderPane)echoClientGuiLoader.load();
        }
        catch (IOException exception)
        {
            System.out.println("ERROR: Problem loading clientFXML.fxml");
            throw new RuntimeException(exception);
        }
        //Get a handle to the controller for the client GUI
        controller = (ClientFXMLController)echoClientGuiLoader.getController();
        //Use the handle to the controller to pass the server and port to the controller
        controller.setServerNameAndPort(server, servPort);
        
        //Open a socket to the server
        Socket socket = controller.openSocket(server, servPort);
        //Pass the handle to the opened socket to the controller
        controller.setSocket(socket);
        Scene scene = new Scene(root);
        EchoClientFXML.theStage.setScene(scene);
        EchoClientFXML.theStage.setTitle("Echo Client GUI");
        EchoClientFXML.theStage.show();
    }
}
