/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jpmh.presentation;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Random;

import com.jpmh.EchoClientFXML;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Carlton Davis
 */
public class ClientFXMLController
{
    @FXML
    private TextField textField;

    @FXML
    private TextArea textArea;
    
    private Socket socket; // Client socket
    private DataInputStream in; // Socket input stream
    private OutputStream out; // Socket output stream
    private Stage theStage;
    private String serverName;
    private int servPort;
    
    /*The initialize method of a controller gets called after the initialization
      of the controller completes.
    */
    public void initialize()
    {
        //Event handler for close request of the stage
        EchoClientFXML.theStage.setOnCloseRequest(new EventHandler<WindowEvent>()
        {
                @Override
                public void handle(WindowEvent t)
                {
                    try
                    {
                      socket.close();
                    }
                    catch (Exception exception)
                    {
                    }
                      System.exit(0);
                }
        });
    }
    
    //Method to pass the server and the port to the controller
    public void setServerNameAndPort(String server, int port)
    {
        serverName = server;
        servPort = port;
    }
    
    //Mether to pass the handle to the open socket to the controller
    public void setSocket(Socket theSocket) {
        this.socket = theSocket;
    }
    
    //Event handler to get the text to be sent to the server
    @FXML
    private void getTextFieldText(KeyEvent event)
    {
        if(event.getCode().equals(KeyCode.ENTER))
        {
            try
            {
                sendAndReceiveData();
            }
            catch
            (IOException ex)
            {
                textArea.appendText("ERROR: Problem sending text to server");
            }
            textField.clear();
        } 
    }
  
    //Method to open a socket to the server
    public Socket openSocket(String serverName, int port) throws IOException
    {
        try
        {
            //Create socket and fetch I/O streams
            socket = new Socket(serverName, servPort);
        }
        catch (IOException e)
        {
            System.out.println("ERROR: Problem opening socket");
        }
        return socket;
    }
    
    //Method to send and receive data to and from the server
    public void sendAndReceiveData() throws IOException
    {
        in = new DataInputStream(socket.getInputStream());
        out = socket.getOutputStream();

        String inputMessage = textField.getText();

        byte[] byteBuffer;

        if (inputValidation(inputMessage))
        {
            //Call the method to parse the message and return the byte array to be displayed
            byteBuffer = parseMessage(inputMessage);
        }
        else
        {
            byteBuffer = "Invalid Format String.".getBytes();
        }


        //Code to send text to server and receivce reply
        try
        {
            out.write(byteBuffer);
            in.readFully(byteBuffer);
            textArea.appendText(new String(byteBuffer) + "\n");
        }
        catch (IOException e)
        {
            textArea.appendText("ERROR\n");
        }
    }

    private boolean inputValidation(String input)
    {
        String beginMessage = input.charAt(0) + "" + input.charAt(1) + "";

        if (beginMessage.equals("1:"))
        {
            String actualMessage = input.substring(2);
            String trimmedMessage = actualMessage.trim();

            String[] messageSplited = trimmedMessage.split(" ");

            Double number1 = null;
            Double number2 = null;

            for (int i = 0; i < messageSplited.length; i++)
            {
                try
                {
                    double parsedNumber = Double.parseDouble(messageSplited[i]);
                    if (number1 == null)
                    {
                        number1 = parsedNumber;
                    }
                    else if (number2 == null)
                    {
                        number2 = parsedNumber;
                        return true;
                    }
                }
                catch (NumberFormatException e) { System.out.println("Not a double"); }
            }
        }
        else if (beginMessage.equals("0:"))
        {
            return true;
        }
        return false;
    }

    private byte[] parseMessage(String message)
    {
        System.out.println("Entering parseMessage");
        System.out.println("Incoming message: " + message);
        String[] messageSplited;

        if (message.charAt(0) == '0')
        {
            String actualMessage = message.substring(2);
            System.out.println(actualMessage);
            String trimmedMessage = actualMessage.trim();

            return trimmedMessage.getBytes();
        }
        else if (message.charAt(0) == '1')
        {
            byte[] returnArray = new byte[26];

            returnArray[0] = 1;

            String actualMessage = message.substring(2);
            String trimmedMessage = actualMessage.trim();

            messageSplited = trimmedMessage.split(" ");

            Double number1 = null;
            Double number2 = null;

            for (int i = 0; i < messageSplited.length; i++)
            {
                try
                {
                    double parsedNumber = Double.parseDouble(messageSplited[i]);
                    if (number1 == null) { number1 = parsedNumber; }
                    else if (number2 == null) { number2 = parsedNumber; }
                }
                catch (NumberFormatException e) { System.out.println("Not a double"); }
            }

            System.out.println("Double1: " + number1 + " Double2: " + number2);

            byte[] number1Byte = com.jpmh.DoubleToBytes.doubleToByteArray(number1);
            byte[] number2Byte = com.jpmh.DoubleToBytes.doubleToByteArray(number2);

            for (int i = 1; i < 8; i++)
            {
                returnArray[i] = number1Byte[i - 1];
            }

            for (int i = 9; i < 16; i++)
            {
                returnArray[i] = number2Byte[i - 9];
            }

            return returnArray;
        }
        //Never reached
        return new byte[0];
    }
}
