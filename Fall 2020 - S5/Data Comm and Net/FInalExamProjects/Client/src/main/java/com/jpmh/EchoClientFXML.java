package com.jpmh;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import javafx.scene.layout.GridPane;

/*
 *
 * @author Carlton Davis
 */

public class EchoClientFXML extends Application
{
    public static Stage theStage; //Allows the stage be easily accessible

    @Override
    public void start(Stage theStage) throws IOException {
        this.theStage = theStage;
        //Loads the FXML file for the setup GUI
        FXMLLoader guiSetupLoader = new FXMLLoader(getClass().getResource("/fxml/setupFXML.fxml"));
        GridPane root = (GridPane)guiSetupLoader.load();
        //Create a for the GUI
        Scene scene = new Scene(root, 600, 200);
        //Add the scene to the stage
        theStage.setScene(scene); 
        theStage.setTitle("Setup GUI");
        theStage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }

}