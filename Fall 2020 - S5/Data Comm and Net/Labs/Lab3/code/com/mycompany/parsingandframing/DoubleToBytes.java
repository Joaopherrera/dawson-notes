/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.parsingandframing;

/**
 * Converting a double to an eight-byte array and back to a double.
 *
 * @author Alex Simonelis
 * @version October, 2011
 * @author cdavis
 * Modified by Carlton Davis, November 2020
 */

/**
 *
 * @author cdavis
 * Modified by Carlton Davis, November 2020
 */

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.util.Scanner;


public class DoubleToBytes
{

    public static void main(String[] args)
    {
        DoubleToBytes myObject = new DoubleToBytes();
        double d = myObject.getDouble();

        System.out.println("Input double: " + d);
        byte[] myByteArray = myObject.doubleToByteArray(d);
        double outputValue = myObject.byteArrayToDouble(myByteArray);
        System.out.println("Output value: " + outputValue);
    }

    public double getDouble()
    {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Enter a double : ");
        String input = myScanner.nextLine();  // Read user input
        Scanner newScanner = new Scanner(input);
        int flag = 0;
        while (flag == 0)
        {
            if (newScanner.hasNextDouble())
            {
                flag = 1;
            }
            else
            {
                System.out.println(input + " is not a double. Enter a double : ");
                input = myScanner.nextLine();
                newScanner = new Scanner(input);
                if (newScanner.hasNextDouble())
                {
                    flag = 1;
                }
            }
        }
        return Double.parseDouble(input);
    }

    public byte[] doubleToByteArray(double numDouble)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try
        {
            dataOutputStream.writeDouble(numDouble);
            dataOutputStream.flush();
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
        return byteArrayOutputStream.toByteArray();
    }

    public double byteArrayToDouble(byte[] arrayByte)
    {
        double numDouble;
        long numLong;

        numLong = (((long) arrayByte[0] & 0x00000000000000FFL) << 56) | (((long) arrayByte[1] & 0x00000000000000FFL) << 48) |
                (((long) arrayByte[2] & 0x00000000000000FFL) << 40) | (((long) arrayByte[3] & 0x00000000000000FFL) << 32) |
                (((long) arrayByte[4] & 0x00000000000000FFL) << 24) | (((long) arrayByte[5] & 0x00000000000000FFL) << 16) |
                (((long) arrayByte[6] & 0x00000000000000FFL) << 8) | ((long) arrayByte[7] & 0x00000000000000FFL);

        numDouble = Double.longBitsToDouble(numLong);

        return numDouble;
    }

}

/*
Question 1:
    Since Java runs its code in the JVM, it doesnt rely on the internal CPU architecture to decide if it will use little endian or big endian.

Question 2:
    Its like splitting and isolating the current part of the byte array so it can constructs the full byte array based on the other split parts for each position

OUTPUT SAMPLE
    Enter a double :
    543.6
    Input double: 543.6
    Output value: 543.6
 */
