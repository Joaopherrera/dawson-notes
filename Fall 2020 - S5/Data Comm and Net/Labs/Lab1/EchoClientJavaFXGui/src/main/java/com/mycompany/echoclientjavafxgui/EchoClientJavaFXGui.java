/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.echoclientjavafxgui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.io.OutputStream;
import java.net.Socket;
import javafx.scene.control.Button;
//import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;

/**
 *
 * @author Carlton Davis
 */
public class EchoClientJavaFXGui extends Application {

    private TextField textField, textField1, textField2;
    private TextArea textArea, textArea1, textArea2;
    private BorderPane borderPane, borderPane1;
    private Scene scene, scene1;
    private GridPane grid;
    private Button myButton;

    private String server;
    private int servPort;

    @Override
    public void start(Stage primaryStage) {
    primaryStage.setTitle("JavaFX Echo Client GUI");  
    
    //Scene for geting server IP and port
    grid = new GridPane();
    //grid.setHgap(10);
    //grid.setVgap(10);
    //grid.setPadding(new Insets(0, 10, 0, 10));

    textField1= new TextField();
    textField1.setPromptText("Enter the server's IP address");
    grid.add(textField1, 1, 0);
    
    textField2= new TextField();
    textField2.setPromptText("Enter the port");
    grid.add(textField2, 2, 0);
    
    textArea1 = new TextArea();
    textArea1.setPromptText("This is your imput for the server's IP:");
    textArea1.setEditable(false);
    grid.add(textArea1, 1, 1);
    
    textArea2 = new TextArea();
    textArea2.setPromptText("This is your imput for the port:");
    textArea2.setEditable(false); 
    grid.add(textArea2, 2, 1);
    
    myButton = new Button("Click to continue");
    grid.add(myButton, 1, 2);
    
    //Style GridPane
    grid.setStyle("-fx-border-color: blue");
    grid.setStyle("-fx-background-color: AliceBlue");
    
    //Creating a scene object 
    scene1 = new Scene(grid, 600, 200);
    primaryStage.setScene(scene1);
    
    //Displaying the contents of the stage 
    primaryStage.show();
    
    //Instantiate MyUtility class
    MyUtility myUtility = new MyUtility();
    
    //Setup event handle for textField1
    textField1.setOnKeyPressed(new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            if(event.getCode().equals(KeyCode.ENTER)) { 
                server = textField1.getText();
                    if(myUtility.IP_checker(server) != 1){
                        textArea1.appendText("ERROR: "+ server + " isn't valid IP address." +"\n");
                    }
                    else {
                        textArea1.appendText("Server name verified" +"\n");             
                    }
                    textField1.clear();
            }
        }
               
    });
    
    //Setup event handle for textField2
    textField2.setOnKeyPressed(new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            if(event.getCode().equals(KeyCode.ENTER)) { 
                try {
                    servPort = Integer.parseInt(textField2.getText().trim());
                    textArea2.appendText("Your entered: " + servPort +"\n");
                }
                catch (NumberFormatException nfe) {
                    textArea2.appendText("ERROR: "+ textField2.getText() + " isn't valid port." +"\n");
                }
                textField1.clear();
            }
        }
    });

   //Set up event handle for mouse click
    myButton.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent event) {
        textField = new TextField();
        textArea = new TextArea();
        borderPane = new BorderPane();
        textField.setPromptText("Enter text to send to server");
        textArea.setPromptText("Response from the server");
        textArea.setEditable(false);
        borderPane.setTop(textArea);
        borderPane.setBottom(textField);

        //Creating a scene object 
        scene = new Scene(borderPane);
        
        //Add scene to stage
        primaryStage.setScene(scene);

        //Displaying the contents of the stage 
        primaryStage.show();
        
        final Socket socket; // Client socket
        final DataInputStream in; // Socket input stream
        final OutputStream out; // Socket output stream

        try {
            //Create socket and fetch I/O streams
            socket = new Socket(server, servPort);

            in = new DataInputStream(socket.getInputStream());
            out = socket.getOutputStream();

            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    if(event.getCode().equals(KeyCode.ENTER)) {
                        byte[] byteBuffer = textField.getText().getBytes();
                        textField.clear();
                        //Code to send text to server and receivce reply
                        try {
                            out.write(byteBuffer);
                            in.readFully(byteBuffer);
                            textArea.appendText(new String(byteBuffer) + "\n");
                        } catch (IOException e) {
                            textArea.appendText("ERROR\n");
                        }
                    }
                }
            });

            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    try {
                      socket.close();
                    } catch (Exception exception) {
                    }
                      System.exit(0);
                }
            });

        }  catch (SocketException e) {
            textArea.appendText(e.toString() + "\n");
        }

        catch (IOException e) {
                    textArea.appendText(e.toString() + "\n");
        }
        }
    });
  }

  public static void main(String[] args) {
      launch(args);
  }
}