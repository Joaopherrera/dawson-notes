module com.joaopmh.lab2 {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.joaopmh.lab2 to javafx.fxml;
    exports com.joaopmh.lab2;
}
