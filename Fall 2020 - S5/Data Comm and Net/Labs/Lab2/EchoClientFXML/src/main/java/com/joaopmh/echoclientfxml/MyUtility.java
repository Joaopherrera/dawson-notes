/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.joaopmh.echoclientfxml;

import inet.ipaddr.IPAddressString;
import inet.ipaddr.IPAddress;
/**
 *
 * @author Carlton Davis
 */
public class MyUtility {

    public MyUtility() {
    }
    
    /*Method to check if an IP address is valid. It accepts IPv4, IPv6 addresses
      as well as "localhost".
    */ 
    public int IP_checker(String hostName) {
        int return_value=0;
        if(hostName.equalsIgnoreCase("localhost")) {
            return_value = 1;
        }
        else {
            IPAddressString str = new IPAddressString(hostName);
            IPAddress addr = str.getAddress();
            if(addr != null) {
                return_value = 1;
            }
        }
        return return_value;
    }
}
