module com.joaopmh.echoclientfxml {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.joaopmh.echoclientfxml to javafx.fxml;
    exports com.joaopmh.echoclientfxml;
    requires inet.ipaddr;
    requires javafx.base;
    requires javafx.graphics;
}
