/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.joaopmh.echoclientfxml;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Carlton Davis
 */
public class ClientFXMLController {
    @FXML
    private TextField textField;

    @FXML
    private TextArea textArea;
    
    private Socket socket; // Client socket
    private DataInputStream in; // Socket input stream
    private OutputStream out; // Socket output stream
    private Stage theStage;
    private String serverName;
    private int servPort;
    
    /*The initialize method of a controller gets called after the initialization
      of the controller completes.
    */
    public void initialize() {
        //Event handler for close request of the stage
        EchoClientFXML.theStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    try {
                      socket.close();
                    } catch (Exception exception) {
                    }
                      System.exit(0);
                }
        });
    }
    
    //Method to pass the server and the port to the controller
    public void setServerNameAndPort(String server, int port) {
        serverName = server;
        servPort = port;
    }
    
    //Mether to pass the handle to the open socket to the controller
    public void setSocket(Socket theSocket) {
        this.socket = theSocket;
    }
    
    //Event handler to get the text to be sent to the server
    @FXML
    private void getTextFieldText(KeyEvent event) {
        if(event.getCode().equals(KeyCode.ENTER)) { 
            try {
                sendAndReceiveData();
                } catch (IOException ex) {
                    textArea.appendText("ERROR: Problem sending text to server"); 
                }
            textField.clear();
        } 
    }
  
    //Method to open a socket to the server
    public Socket openSocket(String serverName, int port) throws IOException {
        try {
            //Create socket and fetch I/O streams
            socket = new Socket(serverName, servPort);
        } catch (IOException e) {
            System.out.println("ERROR: Problem opening socket");
        }
        return socket;
    }
    
    //Method to send and receive data to and from the server
    public void sendAndReceiveData() throws IOException {
        in = new DataInputStream(socket.getInputStream());
        out = socket.getOutputStream();
        byte[] byteBuffer = textField.getText().getBytes();
        //Code to send text to server and receivce reply
        try {
            out.write(byteBuffer);
            in.readFully(byteBuffer);
            textArea.appendText(new String(byteBuffer) + "\n");
            } catch (IOException e) {
                textArea.appendText("ERROR\n");
            }
    }

}