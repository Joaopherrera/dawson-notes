import java.net.*;   // for Socket
import java.io.*;    // for IOException and Input/OutputStream

class EchoProtocol2 implements Runnable
{
  static public final int BUFSIZE = 32;   		// Size (in bytes) of I/O buffer

  private Socket clntSock;  					// Connection socket

  public EchoProtocol2(Socket clntSock)
  {
    this.clntSock = clntSock;
  }

  public void run()
  {
    try
    {
      // Get the input and output I/O streams from socket
      InputStream in = clntSock.getInputStream();
      OutputStream out = clntSock.getOutputStream();

      int recvMsgSize;                        	// Size of received message
      int totalBytesEchoed = 0;               	// Bytes received from client
      byte[] echoBuffer = new byte[BUFSIZE];  	// Receive Buffer
      // Receive until client closes connection, indicated by -1
      while ((recvMsgSize = in.read(echoBuffer)) != -1)
      {
        out.write(echoBuffer, 0, recvMsgSize);
        totalBytesEchoed += recvMsgSize;
      }

    } catch (IOException e)
    {
      System.out.println("Exception = " +  e.getMessage());
    }

    try  										// Close socket
    {
      clntSock.close();
    } catch (IOException e)
    {
      System.out.println("Exception = " +  e.getMessage());
    }
  }

}
