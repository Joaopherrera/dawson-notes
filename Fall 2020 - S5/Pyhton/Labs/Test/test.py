import pandas as panda
import seaborn as sns

titanic = panda.read_csv('titanic.csv')  # load data set


def siblingsAndSpousesOverSurvived():
    sns.barplot(x='Siblings/Spouses Aboard', y='Survived', data=titanic)

def survivorAgainstFare():
    ranges = [0, 10, 30, 50, 100, 200, 300, titanic["Fare"].max()]

    # create the 9 bins
    cut = panda.cut(titanic.Fare, ranges)

    # group survivors by bins
    groupby = titanic["Survived"].groupby(cut)

    # count the number of survivors in eachbin
    cou = groupby.sum()

    cou.plot.bar()


siblingsAndSpousesOverSurvived()
survivorAgainstFare()
