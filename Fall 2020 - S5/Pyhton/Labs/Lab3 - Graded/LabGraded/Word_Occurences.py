class WordOccurrence:
    def __init__(self):
        pass

    def mostMostImportantWord(self):
        try:
            with open('lab1.txt') as file:
                read_data = file.read()
        except FileNotFoundError as fnf_error:
            print(fnf_error)

        temp = read_data.replace("\n", "")
        temp = temp.replace(",", "")
        temp = temp.replace("!", "")
        temp = temp.replace(".", "")
        temp = temp.replace("'", "")
        temp = temp.split(" ")

        thisDict = {}

        for word in temp:
            if word in thisDict:
                thisDict[word] = thisDict[word] + 1
            else:
                thisDict[word] = 1

        sortedDict = sorted(thisDict, key=thisDict.get, reverse=True)


        for i in range(10):
           print("Word Number " + str(i+1) + ": " + sortedDict[i] + " - " + str(thisDict[sortedDict[i]]))


wordOccurrence = WordOccurrence()

wordOccurrence.mostMostImportantWord()
