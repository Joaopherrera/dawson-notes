class Palindrome:
    def __init__(self):
        pass

    def generate(self):
        inputWord = str(input("Enter your word: "))
        reverseString = ""

        for i in range(len(inputWord)):
            reverseString = reverseString + inputWord[(len(inputWord) - 1) - i]

        inputWord = inputWord + reverseString

        print(inputWord)

        return inputWord

palindrome = Palindrome()
palindrome.generate()