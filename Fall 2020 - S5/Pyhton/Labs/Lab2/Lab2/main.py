import math

# ---> Exercise 1 <--- #
print("-------- Exercise 1 --------")
# Input for the user enter the temperature in C
tempC = 0
# tempC = float(input("Please enter the temperature in Celsius: "))

# Formula to convert from C to F
tempF = (tempC * (9 / 5)) + 32

print(f"Temperature in Celsius: {tempC}, Temperature in Fahrenheit: {tempF}")

# ---> Exercise 2 <--- #
print()
print("-------- Exercise 2 --------")
# Input from the user to get the height and distance
height = float(input("Please enter the height of the pole:"))
distance = float(input("Please enter the distance from the beam:"))

# Formula to find the length of the beam
length = math.sqrt(height ** 2 + distance ** 2)

print(f"Length required: {length}")

# ---> Exercise 3 <--- #
print()
print("-------- Exercise 3 --------")
# TODO

# ---> Exercise 4 <--- #
print()
print("-------- Exercise 4 --------")

# Taking input from the user
num = int(input("Enter your number: "))
power = 0

while 1 < 2:
    if 7 ** power < num:
        power += 1
    else:
        print(7 ** power)
        break

# ---> Exercise 5 <--- #
print()
print("-------- Exercise 5 --------")
# TODO

# ---> Exercise 6 <--- #
print()
print("-------- Exercise 6 --------")
# TODO

# ---> Exercise 7 <--- #
print()
print("-------- Exercise 7 --------")
# TODO

# ---> Exercise 8 <--- #
print()
print("-------- Exercise 8 --------")
# TODO

# ---> Exercise 9 <--- #
print()
print("-------- Exercise 9 --------")
# TODO
