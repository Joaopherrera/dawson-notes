import pandas as panda
import seaborn as sns

# Retrieve the data from the csv file
titanic = panda.read_csv('titanic.csv')  # load data set

# Function to show the plot which will display the survival rate if you had siblings or spouses aboard
def siblingsAndSpousesOverSurvived():
    sns.barplot(x='Siblings/Spouses Aboard', y='Survived', data=titanic)

# Function to show the survivors against the fare they paid
def survivorAgainstFare():
    # Setting the ranges
    ranges = [0, 10, 30, 50, 100, 200, 300, titanic["Fare"].max()]
    # create the bins for the fares
    cut = panda.cut(titanic.Fare, ranges)
    # group survivors by fare
    groupby = titanic["Survived"].groupby(cut)
    # counting the number of survivors in each bin
    cou = groupby.sum()
    cou.plot.bar()

# Calling the two functions
siblingsAndSpousesOverSurvived()
survivorAgainstFare()
