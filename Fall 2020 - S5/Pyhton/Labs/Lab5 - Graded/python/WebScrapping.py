# Import statements
from bs4 import BeautifulSoup
import requests

# URL of the website we want to scrap
url = "https://www.monster.ca/jobs/search/?where=Chicoutimi__2C-Quebec"

req = requests.get(url)
# Once teh req is done, parse the result into html text
soup = BeautifulSoup(req.text, "html.parser")

# get the SearchResults div
results = soup.find(id='SearchResults')

# get the individual elements for each job post
job_elems = results.find_all('section', class_='card-content')

# Loop to go over all the elements and print the information
for job_elem in job_elems:
    title_elem = job_elem.find('h2', class_='title')
    company_elem = job_elem.find('div', class_='company')
    location_elem = job_elem.find('div', class_='location')

    if None in (title_elem, company_elem, location_elem):
        continue
    print("----------------------")
    print(title_elem.text.strip())
    print(company_elem.text.strip())
    print(location_elem.text.strip())