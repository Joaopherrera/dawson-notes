import mysql.connector

# Creating the connection with the db with the credentials
mydb = mysql.connector.connect(
    host="localhost",
    user="jpmh",
    password="Nh3206ea?",
    database="testdb"
)

mycursor = mydb.cursor(buffered=True)
# Dropping all the tables if they exist
mycursor.execute("DROP TABLE IF EXISTS books")
mycursor.execute("DROP TABLE IF EXISTS authors")

# Create statements to create the Books and Author tables
mycursor.execute("CREATE TABLE books(ISBN INT PRIMARY KEY, title VARCHAR(255), author_id INT, cost FLOAT, publishingDate DATE)")
mycursor.execute("CREATE TABLE authors(author_id INT PRIMARY KEY, firstName VARCHAR(255), lastName VARCHAR(255))")

# Block to insert 10 sample authors into the author table
insertAuthor = "INSERT INTO authors(author_id, firstName, lastName) VALUES (%s, %s, %s)"
val = [(1, 'Peter', 'Johson'),
       (2, 'Amy', 'Peterson'),
       (3, 'Hannah', 'Banana'),
       (4, 'John', 'Peterson'),
       (5, 'Max', 'Maximilan'),
       (6, 'Bob', 'Boberson'),
       (7, 'Rose', 'Rosemary'),
       (8, 'Jake', 'Jakeson'),
       (9, 'Richard', 'Nickson'),
       (10, 'Josh', 'Josh')]
mycursor.executemany(insertAuthor, val)

# Block to insert 10 sample books into the book table
insertBooks = "INSERT INTO books(ISBN, title, author_id, cost, publishingDate) VALUES (%s, %s, %s, %s, %s)"
val = [('111', 'The Book 1', 1, 30, '2008-11-11'),
       ('222', 'The Book 2', 2, 10, '2002-08-07'),
       ('333', 'The Book 3', 3, 40, '2012-03-04'),
       ('444', 'The Book 4', 4, 5, '2007-04-23'),
       ('555', 'The Book 5', 5, 21, '2013-07-06'),
       ('666', 'The Book 6', 6, 78, '2006-05-09'),
       ('777', 'The Book 7', 7, 12, '2003-05-31'),
       ('888', 'The Book 8', 8, 21, '2018-10-29'),
       ('999', 'The Book 9', 9, 10, '2020-08-04'),
       ('123', 'The Book 10', 10, 35, '2002-06-05')]
mycursor.executemany(insertBooks, val)

# Statement to retrieve all the books from the book table
mycursor.execute("SELECT * FROM books")
myresult = mycursor.fetchall()
print('---- BOOK TABLE ----')
for x in myresult:
    print(x)
print('--------------------')

# Statement to retrieve the last name of the author of the book "The Book 3"
mycursor.execute(
    "SELECT lastName FROM authors INNER JOIN books on authors.author_id = books.author_id WHERE books.title = 'The Book 3' ")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)
print('--------------------')

# Statement to retrieve the list of books of the authors which name starts with letter J
mycursor.execute(
    "SELECT * FROM books INNER JOIN authors ON books.author_id = authors.author_id WHERE authors.firstName LIKE 'J%'")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)
print('--------------------')

# Statement to calculate the average cost for the books each authors has published
mycursor.execute(
    "SELECT AVG(cost), author_id, firstName, lastName FROM books INNER JOIN authors using(author_id) GROUP BY author_id")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)
print('--------------------')

# Statement to replace the name of author 10 by the title of book isbn 111
mycursor.execute("UPDATE authors SET firstName = (SELECT title FROM books WHERE ISBN = 111) WHERE author_id = 10")
mycursor.execute("SELECT firstName FROM authors WHERE author_id = 10")
myresult = mycursor.fetchall()
print(myresult)
print('--------------------')
