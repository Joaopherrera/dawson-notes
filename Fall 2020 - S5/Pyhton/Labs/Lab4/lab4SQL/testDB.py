import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="jpmh",
    password="Nh3206ea?",
    database="testdb"
)

mycursor = mydb.cursor(buffered=True)

# mycursor.execute("DROP TABLE customers")

# mycursor.execute("CREATE TABLE customers (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255),
# address VARCHAR(255), age INT, amount FLOAT)")

sql = "INSERT INTO customers (name, address, age, amount) VALUES (%s, %s, %s, %s)"
val = [('Peter', 'Lowstreet 4', 40, 3000),
       ('Amy', 'Applest 652', 23, 228739),
       ('Hannah', 'Mountain 21', 12, 123455),
       ('John', 'Baker St 12', 38, 1274231),
       ('Max', 'Nowhere 123', 31, 1376345),
       ('Bob', 'HighStreet 321', 28, 78659435),
       ('Rose', 'LakeRoad 099', 75, 123531),
       ('Jake', 'Bla 1', 2, 21),
       ('Richard', 'A 123', 30, 1084),
       ('Josh', 'asmdalskd 76', 52, 35265)]
mycursor.executemany(sql, val)

# Statement to retrieve all the data from the table for testing purpose
mycursor.execute("SELECT * FROM customers")
myresult = mycursor.fetchall()
print('database content ')
for x in myresult:
    print(x)

# Statement to retrieve all the customer's that has the letter 'e' in their name
mycursor.execute("SELECT name FROM customers WHERE name LIKE '%e%'")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)

# Statement to calculate the average of money for people above 40
mycursor.execute("SELECT avg(amount) FROM customers WHERE age >= 40")
myresult = mycursor.fetchall()
print(myresult)
