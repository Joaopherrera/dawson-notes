# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 18:36:59 2020

@author: laurent
"""

import pandas as pd

import matplotlib.pyplot as plt

air_quality = pd.read_csv("air_quality_no2.csv", index_col=0, parse_dates=True)
print(air_quality.head())

plt.rcParams["font.size"] = "20"

# air_quality["station_B"].plot(figsize=(30, 10))

# air_quality.plot.scatter(x="station_B",y="station_C", alpha=0.5, figsize=(20, 20))

# separate plots 

axs = air_quality.plot.area(figsize=(30, 10), subplots=True)

# create and save customized chart
fig, axs = plt.subplots(figsize=(30, 10))
air_quality.plot.area(ax=axs)
axs.set_xlabel("Date")
axs.set_ylabel("NO$_2$ concentration")

# fig.savefig("no2_concentrations.png")

# air_quality["london_mg_per_cubic"] = air_quality["station_london"] * 1.882

# print the avarage of the 3 stations 
print("average of the 3 stations ")
print(air_quality.describe())

id = air_quality.idxmax()

print('date for maximum pollution for ', air_quality.columns[0], ' is equal to ', id[0])
print('date for maximum pollution for ', air_quality.columns[1], ' is equal to ', id[1])
print('date for maximum pollution for ', air_quality.columns[2], ' is equal to ', id[2])

avg = air_quality.mean()
print('maximunm No2 for ', air_quality.columns[0], 'is equal to', avg[0], '%')
print('maximunm No2 for ', air_quality.columns[1], 'is equal to', avg[1], '%')
print('maximunm No2 for ', air_quality.columns[2], 'is equal to', avg[2], '%')
# print(avg.pivot_table())

# avg.plot()

# plt.bar( avg[1], avg[0])

# fig = plt.figure(1, figsize=(9, 6))
# ax = fig.add_subplot(111)    
# axs.boxplot(air_quality, showmeans=True)

# plt.show()
