import mysql.connector

# Creating the connection with the db with the credentials
mydb = mysql.connector.connect(
    host="localhost",
    user="jpmh",
    password="Nh3206ea?",
    database="testdb"
)

mycursor = mydb.cursor(buffered=True)
# Dropping all the tables if they exist
mycursor.execute("DROP TABLE IF EXISTS books")
mycursor.execute("DROP TABLE IF EXISTS authors")

# Create statements to create the Books and Author tables
mycursor.execute("CREATE TABLE books(ISBN INT PRIMARY KEY, title VARCHAR(255), author_id INT, cost FLOAT, publishingDate DATE, category VARCHAR(30)")
mycursor.execute("CREATE TABLE authors1835031(author_id INT PRIMARY KEY, firstName VARCHAR(255), lastName VARCHAR(255))")

# Block to insert 10 sample authors into the author table
insertAuthor = "INSERT INTO authors(author_id, firstName, lastName) VALUES (%s, %s, %s)"
val = [(1, 'Peter', 'Johson'),
       (2, 'Amy', 'Peterson'),
       (3, 'Hannah', 'Banana'),
       (4, 'John', 'Peterson'),
       (5, 'Max', 'Maximilan'),
       (6, 'Bob', 'Boberson'),
       (7, 'Rose', 'Rosemary'),
       (8, 'Jake', 'Jakeson'),
       (9, 'Richard', 'Nickson'),
       (10, 'Josh', 'Josh')]
mycursor.executemany(insertAuthor, val)

# Block to insert 10 sample books into the book table
insertBooks = "INSERT INTO books(ISBN, title, author_id, cost, publishingDate, category) VALUES (%s, %s, %s, %s, %s)"
val = [('111', 'The Book 1', 1, 30, '2008-11-11', 'novel'),
       ('222', 'The Book 2', 2, 10, '2002-08-07', 'action'),
       ('333', 'The Book 3', 3, 40, '2012-03-04', 'sci-fi'),
       ('444', 'The Book 4', 4, 5, '2007-04-23', 'drama'),
       ('555', 'The Book 5', 5, 21, '2013-07-06', 'comic book'),
       ('666', 'The Book 6', 6, 78, '2006-05-09', 'novel'),
       ('777', 'The Book 7', 7, 12, '2003-05-31', 'sci-fi'),
       ('888', 'The Book 8', 8, 21, '2018-10-29', 'action'),
       ('999', 'The Book 9', 9, 10, '2020-08-04', 'novel'),
       ('123', 'The Book 10', 10, 35, '2002-06-05', 'drama')]
mycursor.executemany(insertBooks, val)

# 2 - Printing the average cost of all books.
mycursor.execute("SELECT AVG(cost) from books1835031")
average = mycursor.fetchall()
print('the average cost of all books is equals to: $', average)

# 3 - Print the title and the price of all the books which cost is smaller than the average cost
mycursor.execute("SELECT title, cost FROM books1835031 WHERE cost < (SELECT AVG(cost))")
result = mycursor.fetchall()
for x in result:
    print('the book: ', x[0], 'has a cost of $', x[1])

# 4 - Reduce the price of ALL books by 10%
mycursor.execute("UPDATE books1835031 SET cost = (cost * 0.9)")
mycursor.execute("SELECT title, cost FROM books1835031")
result = mycursor.fetchall()
for x in result:
    print('the book: ', x[0], ' now has a cost of $', x[1])

# 5 - Ask the user to change the author from a specified book
try:
    titleInput = str(input("enter the title of the book to change the author: "))
    authorInput = str(input("enter the name of the author to assign to this book: "))

except ValueError as error:
    print('Please enter a valid input.')

query = "UPDATE books1835031 SET author_id = (SELECT author_id FROM authors1835031 WHERE firstName = %s) WHERE title = %s"
val = (authorInput, titleInput)
mycursor.execute(query, val)

print('the book ', titleInput, ' has now the author ', authorInput)