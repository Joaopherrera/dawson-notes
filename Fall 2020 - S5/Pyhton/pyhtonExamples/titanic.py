# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 17:09:01 2020

@author: laurent
"""

import pandas as pd
import matplotlib.pyplot as plt

titanic = pd.read_csv("titanic.csv")

print(titanic.head())

print("data types ")
print(titanic.dtypes)

titanic.to_excel('titanicSaved.xlsx', sheet_name='passengers', index=False)

# titanicIn = pd.read_excel('titanicSaved.xlsx', sheet_name='passengers')

# print( 'titanic info ')
# print ( titanic.info())

age = titanic["Age"]
print("age description:")
print(age.describe())

print(titanic.head())
age_sex = titanic[["Age", "Sex"]]
print(age_sex.head())
# age_sex["Age"].plot(figsize=(30, 10))

print("age sex  description:")
print(age_sex.describe())

above_35 = titanic[titanic["Age"] > 35]
print(above_35.head())

class_23 = titanic[titanic["Pclass"].isin([2, 3])]

class_23 = titanic[(titanic["Pclass"] == 2) | (titanic["Pclass"] == 3)]
print(class_23.head())

# useless
age_no_na = titanic[titanic["Age"].notna()]
print(age_no_na.shape)

adult_names = titanic.loc[titanic["Age"] > 35, "Name"]
# print ( adult_names.head() )
print(adult_names.shape)

sel = titanic.iloc[9:25, 2:5]
print(sel)

# describe specific data 
titanic[["Age", "Fare"]].describe()

titanic.agg({'Age': ['min', 'max', 'median', 'skew'], 'Fare': ['min', 'max', 'median', 'mean']})

print(titanic[["Sex", "Age"]].groupby("Sex").mean())

print(titanic.groupby("Sex").mean())

print(titanic.groupby(["Sex", "Pclass"])["Fare"].mean())

print(titanic["Pclass"].value_counts(sort=False))
pd.set_option('display.max_columns', 500)
print(titanic.sort_values(by="Age").head())
print(titanic.sort_values(by="Age")[["Age", "Name", "Survived"]])
