# -*- coding: utf-8 -*-
"""
Created on Sun Apr  5 13:39:35 2020

@author: laurent
"""

import pandas as pd
import matplotlib.pyplot as p

    

#url = 'https://docs.google.com/spreadsheets/d/1D6okqtBS3S2NRC7GFVHzaZ67DuTw7LX49-fqSLwJyeo/export?format=xlsx'

url = "Public_COVID-19_Canada_Orig.xlsx"
covid19 = pd.read_excel(url, skiprows=3,usecols=[0,1,2,3,4,5,7])


print ( ' number of case  ')
print( covid19["province"].value_counts(sort=False))


ax = covid19["province"].value_counts(sort=False).plot(kind = 'bar',figsize=(30, 10) )
ax.set_yscale('log')
p.show()


