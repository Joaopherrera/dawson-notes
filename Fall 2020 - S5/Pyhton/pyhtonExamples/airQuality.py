# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 18:36:59 2020

@author: laurent
"""

import pandas as pd

import matplotlib.pyplot as plt

air_quality = pd.read_csv("air_quality_no2.csv",index_col=0, parse_dates=True)
print (air_quality.head() )
air_quality["station_paris"].plot(figsize=(30, 10))

air_quality.plot.scatter(x="station_london",y="station_paris", alpha=0.5, figsize=(20, 20))

# separate plots 

axs = air_quality.plot.area(figsize=(30, 10), subplots=True)

#create and save customized chart
fig, axs = plt.subplots(figsize=(30, 10))
air_quality.plot.area(ax=axs)
axs.set_ylabel("NO$_2$ concentration")

#fig.savefig("no2_concentrations.png")

air_quality["london_mg_per_cubic"] = air_quality["station_london"] * 1.882