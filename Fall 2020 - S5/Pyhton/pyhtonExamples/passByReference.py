# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 11:43:51 2020

@author: laurent
"""
x = 8 
print('id value ', id(x))

def cube(val):
    print('cube id ', id(val))
    return(val ** 3)

print ( 'cube value ', cube(x))
print ( 'id after cube ', id (x))

def square(val):
    print('val   id before  ', id(val))
    val = val ** 2
    print('val  id after ', id(val))
    return val


print( 'square of ', x, 'equals', square(x))