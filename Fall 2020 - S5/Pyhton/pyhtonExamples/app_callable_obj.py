# -------------------------------------------
# Python's bundled WSGI server
from wsgiref.simple_server import make_server

# from app_callableobj_1fn import my_app_obj_as_fn
# from app_callableobj_2class import MY_APP_OBJ_AS_CLASS
# from app_callableobj_3instance  import  instance
# from app_callableobj_4environ import display_environ_items
from cookieFunction import my_app_set_cookie

# The server side or gateway invokes the application callable
# once for each request it receives from an HTTP client
# Instantiate the server
httpd = make_server(
    '',  # The host name
    8033,  # A port number where to wait for the request
    # my_app_obj_as_fn # The app obj name, in this case a function
    # MY_APP_OBJ_AS_CLASS # The app obj name, in this case a class
    # display_environ_items # test for environ dictionary items
    my_app_set_cookie
)

# Wait for a single request, serve it and quit
# httpd.handle_request()
print(" serving port 8033...\n visit http://127.0.0.1:8033\n to kill enter ctrl c ")

httpd.serve_forever()
