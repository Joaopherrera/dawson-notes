# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 10:46:13 2020

@author: laurent
"""

import pandas as pd
import matplotlib.pyplot as plt

    
url = "covid_testing.csv"
covid19 = pd.read_csv(url)
print ( covid19)

# creating a new column
covid19["percentage"] = 100*covid19["cumulative_testing"]/covid19["population"]
print( 'dataframe ')
print ( covid19)

#plot the different percentage
plt.figure ( figsize = (30,10))
plt.bar(covid19["province"] , covid19["percentage"] )

