# this is a callable object (application side)
response_body = 'TESTING 2. \nWelcome to the world of wsgi!!!\n \
We are using a class as a callable app object'


# we can use a class as a callabale application objects
class MY_APP_OBJ_AS_CLASS:
    # if creating an  *instances* of 'AppClass' as application
    # objects instead, we need  a '__call__' method
    def __init__(self, environ, start_response):
        self.environ = environ
        self.start_response = start_response

    def __iter__(self):
        status = '200 OK'
        response_headers = [('Content-type', 'text/plain')]
        self.start_response(status, response_headers)
        # transform the text / string int bytes
        # can use encode or bytes functions
        # output=str.encode(response_body, 'utf-8')
        output = bytes(response_body, 'utf-8')
        yield output
# yield not return as
# yield will iterate over the element list one at a time
