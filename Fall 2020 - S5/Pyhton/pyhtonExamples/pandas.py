# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 16:44:30 2020

@author: laurent
"""

import pandas as pd
df = pd.DataFrame({ "Name": ["Braund, Mr. Owen Harris", "Allen, Mr. William Henry", "Bonnell, Miss. Elizabeth"], 
                   "Age": [22, 35, 58], 
                   "Sex": ["male", "male", "female"]}) 
print (df)

age = df["Age"]
print (age)

ages = pd.Series([22, 35, 78], name="Age")


print ( "max of age" , age. max())

print ( "max of ages" , ages. max())

print( " ages description ", ages.describe())

ages.to_excel('ages.xlsx', sheet_name='ages', index=False)
