def calculatePoints(word):
    upperWord = word.upper()
    for l in word:
        if l.isnumeric():
            result = "is a number"
        else:
            return 'Invalid String'

    points = 0
    for letter in word:
        if letter == 'E' or 'A' or 'I' or 'N' or 'O' or 'R' or 'S' or 'T' or 'U' or 'L':
            points += 1
        elif letter == 'D' or 'M' or 'G':
            points += 2
        elif letter == 'B' or 'C' or 'P':
            points += 3
        elif letter == 'F' or 'H' or 'V':
            points += 4
        elif letter == 'J' or 'Q':
            points += 8
        elif letter == 'K' or 'W' or 'X' or 'Y' or 'Z':
            points += 10

    return points


print(calculatePoints('banana'))