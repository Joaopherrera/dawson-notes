# this is a callable object (application side)

# we can use a function as a callabale application objects
# we will display all the keys and values in the environ dictionary

def display_environ_items(environ, start_response_fn):
    """Simplest possible application object"""
    status = '200 OK'
    response_headers = [('Content-type', 'text/plain')]
    start_response_fn(status, response_headers)
    # we return a list (or any iterable for effciciency sake)

    response_body = get_items_dictionnary(environ)
    return [bytes(response_body, 'utf-8')]


def get_items_dictionnary(my_dictionary):
    out = ['k:v->{0}:{1}'.format(k, v) for k, v in sorted(my_dictionary.items())]
    out = '\n'.join(out)
    return out
