class Car:


    wheels = 0


    def __init__(self, color, model, year):

        self.color = color

        self.model = model

        self.year = year
		
        self._cupholders = 6
        
        self.__engines  = 6
		
		
my_car = Car("blue", "Ford", 1972)

# first way to prtotect direct access 
print(f"It has {my_car._cupholders} cupholders.")

#even more complex way .... 
print(f"It has {my_car._Car__engines} engines ")	