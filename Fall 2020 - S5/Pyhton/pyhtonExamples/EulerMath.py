# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 15:07:34 2019

@author: laurent
"""


## 
#main entry 
import math as m 
currEst = 1.0
prevEst = 0.0
accuracy = 0.00001
counter = 1

while ( m.fabs(currEst - prevEst) > accuracy):
    prevEst = currEst
    currEst = currEst + 1/m.factorial(counter)
    counter = counter + 1
    #print( 'currEst ' ,currEst )
    
print ( 'Euler s number is equal to ', currEst )  
print(' square root is equal to ' , m.sqrt(currEst))
print(' square  is equal to ' , m.pow(currEst,2))



