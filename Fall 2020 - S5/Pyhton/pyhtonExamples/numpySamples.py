# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 15:34:39 2020

@author: laurent
"""

import numpy as np 

print('enter the number of elements in the array ' )
N   = int (input())


# create a 1d array of 20 elements with zeroes 
zeroArray = np.zeros(N)


print ('zeroArray contains')
print(zeroArray)

# create a 1d array of 20 elements from 1 to 19
array = np.arange(N) 
print ( 'array wit increasing numbers contains') 
print ( array) 


# create a 1d array of 20 elements from 1 to 19
emptyArray = np.empty(N) 
print ( 'empty array contains  ') 
print ( emptyArray) 


print('change the values using a for loop' ) 
#change the data value 
for i in range (1,N):
    array[i] = array[i]*2 
    
print ( 'tes')  
for elem in array:
    print ( elem)    
    
print('new value inside array')    
print (array)
    
print('change the values using a while  loop' ) 

j = 0 
while j < len(array):
    array[j] = array[j]*2
    j += 1
    
print( array)    

# Delete element at index position 2
arr = np.delete(array, 2)
print ( 'after deleting element 2')    
print( arr)


# Append a single element at the end of Numpy Array
newArr = np.append(arr, 88)
print ( 'after apending  element 88')    
print( newArr)

na = np.insert(newArr,1,5)
print ( 'after insertin element 5 at position 1 ')    
print( na)

fullArray = np.full(10,3)
print ( 'fullArray')
print ( fullArray)

oneArray = np.ones(10)
print('oneArray')
print ( oneArray)

#changing 1 element 
oneArray[5] = 4.5 
print('oneArray changed ')
print ( oneArray)

# shallow copy 
newArray = oneArray 

newArray[0] =100 

print('oneArray')
print(oneArray)
print('newArray')
print(newArray)

#deep copy 
deepCopy = oneArray.copy()
deepCopy[0] =200
print('deepCopy')
print(deepCopy)

print('oneArray')
print(oneArray)



# creating an array with fixed data 
fixedArray  = np.array([10, 11, 12, 13])
print( 'fixedArray')
print( fixedArray)

#find the max value in an array 
print('newArray max value is equal to ', np.max(newArray))

#finding the max manually 
max = -100 
for i in range(0,len(newArray)):
    if newArray[i] > max:
    	max = newArray[i]
print( 'max value ', max )



