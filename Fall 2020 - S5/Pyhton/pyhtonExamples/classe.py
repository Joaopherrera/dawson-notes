# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 17:31:37 2019

@author: laurent
"""

class Rectangle:
  def __init__(self, length, width):
    self.length = length
    self.width = width
    self.area = width*length

  def surface(self): #returns the area
   return  self.area
    
  def surfaceComputed(self):  #computes the area
   return ( self.length*self.width)

re = Rectangle(12, 36)

print ( "the area of the rect is  ", re.area) 
print ( "the surface  of the rect is  ", re.surface()) 
print ( "the computed surface  of the rect is  ", re.surfaceComputed()) 


