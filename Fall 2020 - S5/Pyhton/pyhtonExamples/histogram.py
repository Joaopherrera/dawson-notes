# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 11:22:04 2019

@author: laurent
"""

def histogram(s):
    d = dict() #create an empty dictionary
    for c in s: #loop through all characters of the input 
        if c not in d: #if not in the dict, add it 
            d[c] = 1
        else:          # else increase the number of occurences
            d[c] += 1
    return d             # return the dict


h = histogram('brontosaurus') #call the function with a string
print (h)                    # print the dict