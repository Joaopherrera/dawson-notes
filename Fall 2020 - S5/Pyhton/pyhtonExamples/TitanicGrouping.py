# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 11:05:11 2020

@author: laurent
"""

import pandas as pd

titanic = pd.read_csv("titanic.csv")

ranges = [0, 10, 20, 30, 40, 50, 60, 70, titanic["Age"].max()]

age = titanic["Age"]
print("age description:")
print(age.describe())

# create the 9 bins 
cut = pd.cut(titanic.Age, ranges)

# group survivors by bins
groupby = titanic["Survived"].groupby(cut)

# count the number of survivors in eachbin
cou = groupby.sum()

survivors = titanic["Survived"].groupby(pd.cut(titanic.Age, ranges)).sum()

print(titanic["Survived"].groupby(pd.cut(titanic.Age, ranges)).sum())

survivors.plot.bar()

pclass = titanic['Pclass'].value_counts()

print(titanic['Pclass'].value_counts())

pclass.plot.bar()
