# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 17:30:12 2020

@author: laurent
"""

import datetime # we will use this for date objects

class Personne:
  def __init__(self, name, age):
    self.name = name
    self.age = age



class Person:

    def __init__(self, name, surname, birthdate, address, telephone, email):
        self.name = name
        self.surname = surname
        self.birthdate = birthdate
        self.address = address
        self.telephone = telephone
        self.email = email

    def age(self):
        today = datetime.date.today()
        age = today.year - self.birthdate.year

        if today < datetime.date(today.year, self.birthdate.month, self.birthdate.day):
            age -= 1

        return age



p1 = Personne("John", 36)

print('p1.name ', p1.name)
print('p1 age ', p1.age)
print('p1.surname',p1.surname)

p2 = Person('jane', 'doe')

person = Person(
    "Jane",
    "Doe",
    datetime.date(1992, 3, 12), # year, month, day
    "No. 12 Short Street, Greenville",
    "555 456 0987",
    "jane.doe@example.com"
)

print( 'printing now some person data' )

print(person.name)
print(person.email)
print(person.surname)
print(person.age())


