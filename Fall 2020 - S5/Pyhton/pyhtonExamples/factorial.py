# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 15:07:34 2019

@author: laurent
"""

# compute the Euler number 

print('enter the number to compute the factorial  ', end = ' ' )
num    = int(input())
counter = 1
prod = 1 
while ( counter <= num ):
    prod = prod * counter 
    counter = counter + 1 
    #print ( 'eulers s loop ', prod, 'counter ' , counter )    
    
print ( 'factorial of ', num, ' is equal to ', prod )    