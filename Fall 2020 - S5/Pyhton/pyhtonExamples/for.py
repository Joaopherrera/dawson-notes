# -*- coding: utf-8 -*-
"""
Created on Thu Nov 28 17:32:08 2019

@author: laurent

"""
for x in range(0, 3):
    print ("We're on time %d" % (x))
    
string = "Hello World"
for x in string:
    print (x)
    
print ( ' next for loop ' )     
collection = ['hey', 5, 'd']
for x in collection:
    print (x)

print ( 'nested for loop computing  ' )     
for x in range(1, 5):
    for y in range(1, 4):
        print ('%1.2f * %1.2f = %.3f' % (x, y, 1.23*x*y))        
   

# Prints out only odd numbers - 1,3,5,7,9
print ( 'odd  for loop   ' )      
for x in range(10):
    # Check if x is even
    if x % 2 != 0:    
      print(x)    