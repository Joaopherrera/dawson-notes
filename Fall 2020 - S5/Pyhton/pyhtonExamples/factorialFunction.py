# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 15:07:34 2019

@author: laurent
"""

# compute the factorial  
def fact(num):
    counter = 1
    prod = 1 
    while ( counter <= num ):
        prod = prod * counter 
        counter = counter + 1 
        #print ( 'eulers s loop ', prod, 'counter ' , counter )    
        
    print ( 'factorial of ', num, ' is equal to ', prod )    
    return ( prod)

## 
#main entry 

currEst = 1.0
prevEst = 0.0
accuracy = 0.00001
counter = 1

while ( abs(currEst - prevEst) > accuracy):
    prevEst = currEst
    currEst = currEst + 1/fact(counter)
    counter = counter + 1
    #print( 'currEst ' ,currEst )
    
print ( 'Euler s number is equal to ', currEst )  

#passed = False





