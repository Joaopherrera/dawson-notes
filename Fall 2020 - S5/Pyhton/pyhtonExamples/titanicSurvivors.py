# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 17:09:01 2020

@author: laurent
"""

import pandas as pd 
import matplotlib.pyplot as plt

# data visualization
import seaborn as sns

#Survived,Pclass,Name,Sex,Age,Siblings/Spouses Aboard,Parents/Children Aboard,Fare
data = pd.read_csv('titanic.csv')  # load data set


print( 'Survival distribution among classes ') 
print(data.pivot_table(index=['Pclass','Survived']))

# df = data.filter 
print ( data['Pclass'].value_counts())


print('df types', data.info())
# x = data.iloc[:, 1]  # survived 


#from https://towardsdatascience.com/predicting-the-survival-of-titanic-passengers-30870ccc7e8

#correlation between pclass & survival
sns.barplot(x='Pclass', y='Survived', data=data)


#aggregate the survival rate vs the male/female feature and age 

# define plot labels 
survived = 'survived'
not_survived = 'not survived'

#create the number of subplots : 1 row, 2 plots, with the proper size 
fig, axes = plt.subplots(nrows=1, ncols=2,figsize=(10, 4))

#create 2 data frames : for women and men 
women = data[data['Sex']=='female']
men = data[data['Sex']=='male']

#create 2 distribution plot , removing the data without age, creating 2 different bins
ax = sns.distplot(women[women['Survived']==1].Age.dropna(), bins=18, label = survived, ax = axes[0], kde =False)
ax = sns.distplot(women[women['Survived']==0].Age.dropna(), bins=40, label = not_survived, ax = axes[0], kde =False)
#create a legend 
ax.legend()
#set the tile 
ax.set_title('Female')

#do the same for male
ax = sns.distplot(men[men['Survived']==1].Age.dropna(), bins=18, label = survived, ax = axes[1], kde = False)
ax = sns.distplot(men[men['Survived']==0].Age.dropna(), bins=40, label = not_survived, ax = axes[1], kde = False)
ax.legend()
ax.set_title('Male')


# # drawing showing the age vs pclass & survival : 6 plots drawn 
grid = sns.FacetGrid(data, col='Survived', row='Pclass', height=2.2, aspect=1.6)
grid.map(plt.hist, 'Age', alpha=.5, bins=20)
grid.add_legend();

