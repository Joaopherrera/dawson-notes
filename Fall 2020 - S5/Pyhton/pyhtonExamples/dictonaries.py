# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 14:11:02 2019

@author: lruhlmann
"""
#creation 
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
print(thisdict)

#accessing the content
x = thisdict["model"]
print( " x value is ", x)

#changing the content 
thisdict["year"] = 2018

print( "the year is now ", thisdict["year"])

#looping through the keys dictionary 
for x in thisdict:
  print(x) 
  
# looping through the values   
for x in thisdict.values():
  print(x)   

#looping through both   
for x, y in thisdict.items():
  print(x, y)   
  
if "model" in thisdict:
  print("Yes, 'model' is one of the keys in the thisdict dictionary") 
  
#adding datA
thisdict["color"] = "red"
print(thisdict)

#removing items 
del thisdict["model"]
print(thisdict) 

#copy a dictionary 
mydict = thisdict.copy()
print("mydict is equal to ", thisdict) 


#nesting dictionaies 
child1 = {
  "name" : "Emil",
  "year" : 2004
}
child2 = {
  "name" : "Tobias",
  "year" : 2007
}
child3 = {
  "name" : "Linus",
  "year" : 2011
}

myfamily = {
  "child1" : child1,
  "child2" : child2,
  "child3" : child3
} 

print ( " my family is equal to ", myfamily)


  