# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 17:31:37 2019

@author: laurent
"""

#define the Rectangle class
class Rectangle:
  length = 0
  width = 0   
  def __init__(self, length, width):
    self.length = length
    self.width = width 
          

  def surface(self): #returns the surface
   return  self.length * self.width
    
  def perimeter(self):  #computes the perimeter
   return ( 2*(self.length+self.width))

#Define the Square class 
class Square:
  edge = 0 
    
  def __init__(self):
     self.edge = 10
        
  def surface(self): #returns the surface
     return  self.edge * self.edge
    
  def perimeter(self):  #computes the perimeter
   return ( 4*self.edge)    



#this is the main program 
        
# create a Rectangle Object     
rectangleObject  = Rectangle( 12, 36)

 
print ( "the surface  of the rect is  ", rectangleObject.surface()) 
print ( "the computed area  of the rect is  ", rectangleObject.perimeter()) 


#tNow ceate a Square object 
squareObejct  = Square()

print ( "the surface  of the square  is  ", squareObejct.surface()) 

print ( "the area  of the square  is  ", squareObejct.perimeter()) 



