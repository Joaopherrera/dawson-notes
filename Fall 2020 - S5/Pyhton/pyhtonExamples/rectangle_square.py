# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 17:31:37 2019

@author: laurent
"""

#define the Rectangle class
class Rectangle:
  length = 0
  width = 0   
  def __init__(self, l, w):
    self.length = l
    self.width = w
          

  def surface(self): #returns the surface
   return  self.length * self.width
    
  def perimeter(self):  #computes the perimeter
   return ( 2*(self.length+self.width))

#Define the Square class 
class Square:
      edge = 0 
    
      def __init__(self):
       self.edge = 10
        
      def surface(self): #returns the surface
        return  self.edge * self.edge
    
      def perimeter(self):  #computes the perimeter
       return ( 4*self.edge)   
   
      def setEdgeLength(self,e):
        self.edge = e 

     


#this is the main program 
        
# create a Rectangle Object     
rectangleObject  = Rectangle( 12, 36)


print ( "the surface  of the rect is  ", rectangleObject.surface()) 
print ( "the perimeter of the rect is  ", rectangleObject.perimeter()) 


#tNow ceate a Square object 
squareObejct  = Square()

print ( "the surface  of the square  is  ", squareObejct.surface()) 
print ( "the perimeter  of the square  is  ", squareObejct.perimeter()) 

#create a house floor plan 

kitchen = Rectangle( 10 ,20) 
bedroom = Rectangle ( 20 , 40 )
washroom = Square()
den = Square()
#print th total area of the house 
print ( 'total area of the appartment is equal to: ', kitchen.surface() + bedroom.surface() + washroom.surface() + den.surface())


print ('current washroom area ', washroom.surface())

#change the washroom edge length
washroom.setEdgeLength(20) 
print ('new washroom area ', washroom.surface())

print('washroom edge length ', washroom.edge)




