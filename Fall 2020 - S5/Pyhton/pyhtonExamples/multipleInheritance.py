# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 16:40:40 2020

@author: laurent
"""
class Vehicle:
    def __init__(self, color, model):
        self.color = color
        self.model = model

class Device:
    def __init__(self):
        self._voltage = 12

class Car(Vehicle, Device):
    def __init__(self, color, model, year):
        Vehicle.__init__(self, color, model)
        Device.__init__(self)
        self.year = year

    #@property
    def voltage(self):
        return self._voltage

    #@voltage.setter
    def voltage(self, volts):
        print("Warning: this can cause problems!")
        self._voltage = volts

    #@voltage.deleter
    def voltage(self):
        print("Warning: the radio will stop working!")
        del self._voltage


my_car = Car("yellow", "beetle", 1969)

print(f"My car is {my_car.color}")

print(f"My car uses {my_car.voltage} volts")