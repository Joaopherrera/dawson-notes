# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 14:51:24 2020

@author: lruhlmann
"""

import numpy as np 
grades = np.zeros(5)

for i in range( 0,5):
    print('Enter grade ', i+1)
    grades[i] = float( input())
    
print (' min = ', np.amax(grades), ' min= ', np.amin(grades), 'averag = ' , np.average(grades), 'std dev = ', np.std(grades))