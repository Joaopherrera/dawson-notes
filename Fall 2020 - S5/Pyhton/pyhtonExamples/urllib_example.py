# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 16:42:01 2020

@author: laurent
"""
from urllib.request import urlretrieve
from urllib.parse import quote
from urllib.parse import urlencode
from wsgiref import simple_server, util
import webbrowser

# thing = urlretrieve("https://www.duckduckgo.com/?q=Stanford University")

thing = urlretrieve("https://www.duckduckgo.com/?q=Stanford%20University")
print(thing)

# quote example
qstr = quote("Stanford University")
thing = urlretrieve("https://www.duckduckgo.com/?q=" + qstr)
print(thing)

# encode example
mydict = {'q': 'whee! Stanford!!!', 'something': 'else'}
qstr = urlencode(mydict)
print(qstr)

# str resolves to: 'q=whee%21+Stanford%21%21%21&something=else'
thing = urlretrieve("https://www.duckduckgo.com/?" + qstr)
print(thing[0])

# open the html code
webbrowser.open(thing[0])
