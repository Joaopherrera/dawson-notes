# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 17:22:09 2020

@author: laurent
If specified, default should be a function that gets called for 
objects that can’t otherwise be serialized. It should return a JSON 
encodable version of the object or raise a TypeError. If not specified, 
TypeError is raised.

"""

import json

def encode_complex(z):
    if isinstance(z, complex):
        return (z.real, z.imag)
    else:
        type_name = z.__class__.__name__
        raise TypeError(f"Object of type '{type_name}' is not JSON serializable")




z = 3 + 8j

comp = complex(3, 8)

#create a tuple for the dumps statement 
tu = (comp.real, comp.imag)
okComp = json.dumps( tu )

print ( z.real)
print ( z.imag)

#use a function for dumps call
okDumps = json.dumps(9 + 5j, default=encode_complex)

print ( okDumps)