# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 11:17:52 2019

@author: laurent
"""
#create a set 
thisset = {"apple", "banana", "cherry"}

print(thisset) 

#print the items 
for x in thisset:
  print(x)

#check existence   
print("banana" in thisset) 
print("bleuet" in thisset) 

#add 1 element 
thisset.add("bleuet ")
print(thisset) 

#add many elements 
thisset.update(["orange", "mango", "grapes"])
print(thisset) 

#size of the set 
print("size of the set is ", len(thisset)) 


#removing 
thisset.remove("banana")
print(thisset) 

#joining 2 sets 
set1 = {"a", "b" , "c"}
set2 = {1, 2, 3}

set3 = set1.union(set2)
print("set3 ", set3)

#updating a set 
set1.update(set2)
print("updated set 1 ", set1) 