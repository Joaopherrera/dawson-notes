# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 10:48:03 2019

@author: laurent
"""

thislist = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]
# print the 1st elemeent
print(thislist[1])
# prints the whole list
print(thislist)
# print the last element
print(thislist[-1])

# change an item
thislist[1] = "blackcurrant"
print(thislist)

# loop through the list
for x in thislist:
    print(x)

# check the existence
if "apple" in thislist:
    print("Yes, 'apple' is in the fruits list")

print("the list lenght is ", len(thislist))

# append an item
thislist.append("orange")
print(thislist)

# insert at a specific location
thislist.insert(1, "bleuet")
print(thislist)

# remove an item
thislist.remove("cherry")
print(thislist)

# delete a specific item by its index
del thislist[0]
print(thislist)

# copy a list
mylist = thislist.copy()
print("mylist contains ")
print(mylist)

# joining 2 lists
list1 = ["a", "b", "c"]
list2 = [1, 2, 3]

list3 = list1 + list2
print("joined list")
print(list3)

list1.extend(list2)
print("extended  list1")
print(list1)
