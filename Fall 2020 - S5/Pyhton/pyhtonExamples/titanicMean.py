# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 15:07:52 2020

@author: lruhlmann
"""

# Program to extract number 
# of rows using Python 
import csv
  

# file without header 
file_to_open='titanicData.csv'

finaList= list()
totalFare1= 0.0
totalFare2= 0.0
totalFare3= 0.0
fare1Num = 0
fare2Num = 0
fare3Num = 0


totalAge = 0

with open(file_to_open, 'r') as csv_file:
#     this_csv_reader = csv.reader(this_csv_file, delimiter=",")
#     header = next(this_csv_reader)
#     print(header)   

    #read it in a list 
    csv_reader = csv.reader(csv_file, delimiter=",")   
    for line in  csv_reader:
        #print(line)
        finaList.append(line)
       
    # #read it in a dictionary 
    # csv_reader = csv.DictReader(csv_file)
    # for row in csv_reader:
    #     print(row)

    
# compute the sum of fares by class  and the sum of all ages 
for lin in   finaList:
    totalAge += float(lin[4])    

    if ( lin[1] == '1'):
        fare1Num +=1 
        totalFare1 += float(lin[7])
    elif  ( lin[1] == '2'):
        fare2Num +=1 
        totalFare2 += float(lin[7])
    else :
        fare3Num +=1 
        totalFare3 += float(lin[7])
    
#prints the results 
print ( 'average age = ', totalAge/len(finaList))    
print ( 'average fare for class 1  = ', totalFare1/fare1Num)    
print ( 'average fare for class 2  = ', totalFare2/fare2Num )   
print ( 'average fare for class 3  = ', totalFare3/fare3Num)    
     