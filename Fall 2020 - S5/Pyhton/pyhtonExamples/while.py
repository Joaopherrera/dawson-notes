# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 14:09:04 2019

@author: laurent
"""

import math as m 

print('enter the number you want to compute the sum  ', end = ' ' )
n   = int (input())

sum = 0
ctr = 0 
while ( ctr <= n ):
    sum = sum + ctr
    ctr = ctr + 1
    
print ( 'the sum of ', n, ' numbers is equal to ', sum )  

x = 20
while(x > 0):
   print(x , ' is greater than 0')
   x = x -1 

print ( 'final value of x is equal to  ', x )  

ctr = 10   
sum = 0 
while ( sum  < 200   ):
    sum = sum + ctr 
    print ( 'sum is equal to ', sum)
    
print ( 'final value of sum is equal to ', sum)  
    
    
#using the break statement   
count = 0
while True:
    print('inside the while lopp , count == ' , count)
    count += 1
    if count >= 5:
        break
    