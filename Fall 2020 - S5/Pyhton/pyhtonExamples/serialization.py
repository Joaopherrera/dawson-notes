# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 16:44:41 2020

@author: laurent
"""

data = {
    "president": {
        "name": "Zaphod Beeblebrox",
        "species": "Betelgeusian",
        "male": True,
    }
}

import json

json_data = json.dumps(data, indent=2)  # serialize
restored_data = json.loads(json_data)  # deserialize

print('json_data', json_data)
print('restored_data', restored_data)
