# this is a callable object (application side)

response_body = 'Test 1. \nWelcome to the world of wsgi!!!\n \
Here we are using a function callabale object '


# we could declare response_body as
# response_body=b'test 1. \nWelcome to the world of wsgi!!!\n'
# notice the b before the string is there to translation the text
# to bytes (http deals with bytes)
# we prefer to use the byte(...) function instead, see line 20


# we can use a function as a callable application objects
def my_app_obj_as_fn(environ, start_response_fn):
    """Simplest possible application object"""
    status = '200 OK'
    response_headers = [('Content-type', 'text/plain')]
    start_response_fn(status, response_headers)
    # we return a list (or any iterable for efficiency sake)

    return [bytes(response_body, 'utf-8')]
