# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 15:16:50 2020

@author: laurent
"""

import json

class Elf:
    def __init__(self, level, ability_scores=None):
        self.level = level
        self.ability_scores = {
            "str": 11, "dex": 12, "con": 10,
            "int": 16, "wis": 14, "cha": 13
        } if ability_scores is None else ability_scores
        self.hp = 10 + self.ability_scores["con"]


elf = Elf(level=4)
print ( ' elf level ', elf.level)


#tuple creation 
tu = (elf.ability_scores,elf.level)

#elf serialization 
jsRes = json.dumps(tu)
print ( jsRes)

#write the tuple to a file : serialization 
fileName = "elf.json"
with open(fileName, "w") as write_file:
    json.dump(tu, write_file)
    
print('file' , fileName, ' has been written sucessfully ')   

#load the file back 
with open(fileName, "r") as read_file:
    inData = json.load(read_file)


#deserialzie 
back = json.loads( jsRes)

#increment the level 
back[1] += 2

listData = list(inData)
listData[1] += 2
#listData[0].dex = 200


#recreate an elf now 
elf2 = Elf( back[1], back[0])
elf3 = Elf( listData[1], listData[0])

print ( ' elf2 level ', elf2.level)
print ( ' elf3 level ', elf3.level, elf3.ability_scores)



