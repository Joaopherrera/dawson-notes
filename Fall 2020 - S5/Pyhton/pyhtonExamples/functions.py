# -*- coding: utf-8 -*-
"""
Created on Thu Nov 28 11:14:39 2019

@author: laurent
"""

def my_function():
  print("Hello from a function")
  
def mult(a,b):
    var = a * b 
    return var 

def listFunction(food):
  for x in food:
    print(x)
    
my_function()

res = mult( 10,34 )
print ( 'call to mult function with 10, 34 paramters ', res )


fruits = ["apple", "banana", "cherry"]

listFunction(fruits)