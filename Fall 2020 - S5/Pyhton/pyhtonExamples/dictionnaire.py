# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 16:39:28 2020

@author: laurent
"""
import matplotlib.pyplot as plt

def dictionnaire (str):
    dic = dict() #create an empty dictionary
    for c in str: #loop through all characters of the input 
        if c not in dic: #if not in the dict, add it 
            dic[c] = 1
        else:    # else increase the number of occurences
            dic[c] += 1
    return  dic            # return the dict


# main entry 
dictio = dictionnaire ('ceci est une phrase complexe')
print(dictio)

#graphics 
plt.bar(range(len(dictio)), list(dictio.values()), align='center')
plt.xticks(range(len(dictio)), list(dictio.keys()))

plt.show()