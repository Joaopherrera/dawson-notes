# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 11:43:00 2020

@author: laurent
"""

import pandas as pd
titanic = pd.read_csv("titanic.csv") #reads a csv file 


df = pd.DataFrame({ 
"Name": ["Braund, Mr. Owen Harris", "Allen, Mr. William Henry", "Bonnell, Miss. Elizabeth"], 
 "Age": [22, 35, 58], 
  "Sex": ["male", "male", "female"]}) 
print (df)

ages = pd.Series([22, 35, 78], name="Age") 
print ( ages)

titanic = pd.read_csv("titanic.csv") #reads a csv file 
print ( titanic.head()) 
print(titanic.dtypes)

age = titanic["Age"] 

age_sex = titanic[["Age", "Sex"]]
print(age_sex.head())
print(age_sex)

print(titanic[["Sex", "Age"]].groupby("Sex").mean())

print(titanic.groupby("Sex").mean())



