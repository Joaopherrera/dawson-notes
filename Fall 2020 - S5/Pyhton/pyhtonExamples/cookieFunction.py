import datetime
import os


def set_cookie_header(name, value, days=365):
    dtime = datetime.datetime.now() + datetime.timedelta(days=days)
    fdtime = dtime.strftime('%a, %d %b %Y %H:%M:%S GMT')
    secs = days * 24 * 3600
    return ('Set-Cookie', '{}={}; Expires={}; Max-Age={}; HttpOnly=True; Path=/'.format(name, value, fdtime, secs))


# HttpOnly=True; (default False)  is for serevr script, if you try this
# with client side script (like JS), it wont work ....(proof cookie set from server)
# students must be aware of the flexible formtting output str.format(...) check w3school for god examples

def my_app_set_cookie(environ, start_response):
    content_type = 'text/html'
    cookie1 = set_cookie_header('prof', 'mr.laurent', -1)  # sets the age to <0 to remove the cookie
    cookie2 = set_cookie_header('DMR_TOKEN', 'DMR_TOKEN', -1)
    headers = [('Content-Type', content_type), cookie1, cookie2]

    start_response('200 OK', headers)
    # getting environ dictionary, it contains HTTP_COOKIE ctrl-f to check on cookies
    response_body = get_items_dictionnary(environ)
    return [bytes(response_body, 'utf-8')]


def get_items_dictionnary(my_dictionary):
    out = ['k:v->{0}:{1}'.format(k, v) for k, v in sorted(my_dictionary.items())]
    out = '\n'.join(out)
    return out
