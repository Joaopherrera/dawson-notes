#! /usr/bin/env python

# Python's bundled WSGI server
from wsgiref.simple_server import make_server


def application(environ, start_response):
    status = '200 OK'
    response_headers = [('Content-Type', 'text/html; charset=utf-8')]
    start_response(status, response_headers)

    return [b'<strong>Hello World I just created a very nice  web app </strong>']


with make_server('', 8050, application) as server:
    print("serving port 8000...\n visit http://127.0.0.1:8000\n to kill enter ctrl c ")
    server.serve_forever()
