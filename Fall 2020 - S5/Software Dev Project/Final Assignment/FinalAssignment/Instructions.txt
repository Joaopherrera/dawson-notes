How to use these files:
Step 1:
Create a package for the custom exception classes in your project:
DivisionByZero.java
InvalidInputQueueString.java
NonBinaryExpression.java
NonMatchingParenthesis.java
You will need to update the package statement that is currently commented out.
You must use these named exceptions.

Step 2:
Create a package in src/test/java for the unit test classes:
DivisionByZeroTest.java
InvalidInputQueueStringTest.java
NonBinaryExpressionTest.java
NonMatchingParenthesisTest.java
ResultParserWithParenthesisTest.java
You will need to update the package statement that is currently commented out.
There are four imports for the custom exception classes that are currently commented out. You will need to uncomment them and enter the package that matches yours.

Step 3:
So that the test classes work with your code you must:
Name your class ResultParserWithParenthesis.java
You must two methods with the following return value and signature:
    public Queue<String> convertInfixToPostfix(Queue<String> infixQueue) throws NonMatchingParenthesis, InvalidInputQueueString, NonBinaryExpression {
and
    public String evaluatePostfix(Queue<String> postfixQueue) throws DivisionByZero {

You are free to add any additional methods you require.

Grading:
If your code will pass all the tests you will receive 15/15 unless I determine that the code you submitted was not your own work.






