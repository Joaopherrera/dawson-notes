package com.exam.finalexam517.beans;

import com.google.protobuf.DoubleValue;
import javafx.beans.property.*;

public class CarBean
{
    private IntegerProperty id;
    private StringProperty car;
    private DoubleProperty gasConsumption;
    private DoubleProperty horsePower;
    private DoubleProperty weightKG;
    private IntegerProperty model_year;
    private StringProperty country;

    public CarBean(int id, String car, double gasConsumption, double horsePower, double weightKG, int model_year, String country)
    {
        this.id = new SimpleIntegerProperty(id);
        this.car = new SimpleStringProperty(car);
        this.gasConsumption = new SimpleDoubleProperty(gasConsumption);
        this.horsePower = new SimpleDoubleProperty(horsePower);
        this.weightKG = new SimpleDoubleProperty(weightKG);
        this.model_year = new SimpleIntegerProperty(model_year);
        this.country = new SimpleStringProperty(country);
    }

    /**
     * Empty Constructor
     */
    public CarBean()
    {
        this.id = new SimpleIntegerProperty(-1);
        this.car = new SimpleStringProperty("");
        this.gasConsumption = new SimpleDoubleProperty(0);
        this.horsePower = new SimpleDoubleProperty(0);
        this.weightKG = new SimpleDoubleProperty(0);
        this.model_year = new SimpleIntegerProperty(0);
        this.country = new SimpleStringProperty("");
    }

    public int getId() { return id.get(); }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getCar() {
        return car.get();
    }

    public StringProperty carProperty() {
        return car;
    }

    public void setCar(String car) {
        this.car.set(car);
    }

    public double getGasConsumption() {
        return gasConsumption.get();
    }

    public DoubleProperty gasConsumptionProperty() {
        return gasConsumption;
    }

    public void setGasConsumption(double gasConsumption) {
        this.gasConsumption.set(gasConsumption);
    }

    public double getHorsePower() {
        return horsePower.get();
    }

    public DoubleProperty horsePowerProperty() {
        return horsePower;
    }

    public void setHorsePower(double horsePower) {
        this.horsePower.set(horsePower);
    }

    public double getWeightKG() {
        return weightKG.get();
    }

    public DoubleProperty weightKGProperty() {
        return weightKG;
    }

    public void setWeightKG(double weightKG) {
        this.weightKG.set(weightKG);
    }

    public int getModel_year() {
        return model_year.get();
    }

    public IntegerProperty model_yearProperty() {
        return model_year;
    }

    public void setModel_year(int model_year) {
        this.model_year.set(model_year);
    }

    public String getCountry() {
        return country.get();
    }

    public StringProperty countryProperty() {
        return country;
    }

    public void setCountry(String country) {
        this.country.set(country);
    }
}
