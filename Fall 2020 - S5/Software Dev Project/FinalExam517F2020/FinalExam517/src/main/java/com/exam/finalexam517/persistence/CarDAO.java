package com.exam.finalexam517.persistence;

import com.exam.finalexam517.beans.CarBean;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.*;

/**
 * DAO class to access all the information from the db
 */
public class CarDAO
{
    private final static Logger LOG = LoggerFactory.getLogger(CarDAO.class);
    //Connection information
    private final String url = "jdbc:mysql://localhost:3306/AUTOMOBILE?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "driver";
    private final String password = "kfstandard";

    /**
     * Method to retrieve all the items from the db to display in the table
     *
     * @return The observable list with all the cars
     * @throws SQLException
     */
    public ObservableList<CarBean> findAll() throws SQLException {

        ObservableList<CarBean> allCars = FXCollections.observableArrayList();

        String selectQuery = "SELECT ID, CAR, GAS_CONSUMPTION, HORSEPOWER, WEIGHT_KG, MODEL_YEAR, COUNTRY FROM CARS";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement pStatement = connection.prepareStatement(selectQuery);
             ResultSet resultSet = pStatement.executeQuery())
        {
            while (resultSet.next())
            {
                allCars.add(createBoundFishData(resultSet, new CarBean()));
            }
        }
        LOG.info("# of records found : " + allCars.size());
        return allCars;
    }

    /**
     * MEthod to retrieve all the cars based on the filtered
     *
     * @param filter The number for the gas comsumption.
     * @return The list with all the cars that meet the category.
     * @throws SQLException
     */
    public ObservableList<CarBean> findAllFiltered(int filter) throws SQLException {

        ObservableList<CarBean> allCars = FXCollections.observableArrayList();

        String selectQuery = "SELECT ID, CAR, GAS_CONSUMPTION, HORSEPOWER, WEIGHT_KG, MODEL_YEAR, COUNTRY FROM CARS WHERE GAS_CONSUMPTION <= ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement pStatement = connection.prepareStatement(selectQuery))
        {
            pStatement.setInt(1, filter);
            ResultSet resultSet = pStatement.executeQuery();

            while (resultSet.next())
            {
                allCars.add(createBoundFishData(resultSet, new CarBean()));
            }
        }
        LOG.info("# of records found : " + allCars.size());
        return allCars;
    }

    /**
     * Method to find the next car based on the current car id
     *
     * @param carBean the current car
     * @return the next car
     * @throws SQLException
     */
    public CarBean findNextByID(CarBean carBean) throws SQLException {

        String selectQuery = "SELECT ID, CAR, GAS_CONSUMPTION, HORSEPOWER, WEIGHT_KG, MODEL_YEAR, COUNTRY FROM CARS WHERE ID = (SELECT MIN(ID) from CARS WHERE ID > ?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {

            pStatement.setInt(1, carBean.getId());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundFishData(resultSet, carBean);
                }
            }
        }
        LOG.info("Found " + carBean.getId());
        return carBean;
    }

    /**
     * Method to find the previous car based on the current car id
     * @param carBean the current car
     * @return the previous car
     * @throws SQLException
     */
    public CarBean findPrevByID(CarBean carBean) throws SQLException {

        String selectQuery = "SELECT ID, CAR, GAS_CONSUMPTION, HORSEPOWER, WEIGHT_KG, MODEL_YEAR, COUNTRY FROM CARS WHERE ID = (SELECT MAX(ID) from CARS WHERE ID < ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
             // You must use PreparedStatements to guard against SQL
             // Injection
             PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, carBean.getId());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundFishData(resultSet, carBean);
                }
            }
        }
        LOG.info("Found " + carBean.getId());
        return carBean;
    }

    /**
     * Method to save or create car into the db
     * DISCLAIMER: NOT TESTED DUE TO TIME CONSTRAINTS
     * @param carBean the bean with the updated info to be put into the db
     * @return the number of rows affected
     * @throws SQLException
     */
    public int saveCar(CarBean carBean) throws SQLException
    {
        int records;
        if (carBean.getId() == -1)
        {
            records = createCar(carBean);
        }
        else
        {
            records = updateCar(carBean);
        }
        return records;
    }

    /**
     * Method to add a car into the db
     * DISCLAIMER: NOT TESTED DUE TO TIME CONSTRAINTS
     * @param carBean the bean holding all the info to create the car
     * @return the nbumber of rows affected
     * @throws SQLException
     */
    private int createCar(CarBean carBean) throws SQLException
    {
        int records;
        String query = "INSERT INTO CARS (CAR, GAS_CONSUMPTION, HORSEPOWER, WEIGHT_KG, MODEL_YEAR, COUNTRY) values (?,?,?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             // You must use PreparedStatements to guard against SQL
             // Injection
             PreparedStatement pStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);)
        {
            pStatement.setString(1, carBean.getCar());
            pStatement.setDouble(2, carBean.getGasConsumption());
            pStatement.setDouble(3, carBean.getHorsePower());
            pStatement.setDouble(4, carBean.getWeightKG());
            pStatement.setDouble(5, carBean.getModel_year());
            pStatement.setString(6, carBean.getCountry());
            records = pStatement.executeUpdate();

            // Retrieve the primary key integer generated for this record
            try (ResultSet rs = pStatement.getGeneratedKeys();)
            {
                int recordNum = -1;
                if (rs.next())
                {
                    recordNum = rs.getInt(1);
                }
                carBean.setId(recordNum);
            }
        }
        return records;
    }

    /**
     * Method to update the info of a given car
     * DISCLAIMER: NOT TESTED DUE TO TIME CONSTRAINTS
     * @param carBean the car we want to update the info
     * @return the number of rows affected
     * @throws SQLException
     */
    private int updateCar(CarBean carBean) throws SQLException
    {
        int records;
        String query = "UPDATE CARS SET CAR=?, GAS_CONSUMPTION=?, HORSEPOWER=?, WEIGHT_KG=?, MODEL_YEAR=?, COUNTRY=? WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             // You must use PreparedStatements to guard against SQL
             // Injection
             PreparedStatement pStatement = connection.prepareStatement(query);)
        {
            pStatement.setString(1, carBean.getCar());
            pStatement.setDouble(2, carBean.getGasConsumption());
            pStatement.setDouble(3, carBean.getHorsePower());
            pStatement.setDouble(4, carBean.getWeightKG());
            pStatement.setDouble(5, carBean.getModel_year());
            pStatement.setString(6, carBean.getCountry());
            pStatement.setInt(7, carBean.getId());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    /**
     * Method to delete a car
     * DISCLAIMER: NOT TESTED DUE TO TIME CONSTRAINTS
     * @param carBean the car we want to delete from the db
     * @return the number of rows affected
     * @throws SQLException
     */
    public int deleteCar(CarBean carBean) throws SQLException
    {
        int records;
        String query = "DELETE FROM CARS WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement pStatement = connection.prepareStatement(query);)
        {
            pStatement.setInt(1, carBean.getId());
            records = pStatement.executeUpdate();
        }
        LOG.info("Number of records deleted: " + records);
        return records;
    }

    /**
     * Method to make car beans out of result set coming from the db
     * @param resultSet the info comming from the db
     * @param carBean the empty car bean that we want to populate with info from the db
     * @return the filled up car bean
     * @throws SQLException
     */
    private CarBean createBoundFishData(ResultSet resultSet, CarBean carBean) throws SQLException
    {
        carBean.setId(resultSet.getInt("ID"));
        carBean.setCar(resultSet.getString("CAR"));
        carBean.setGasConsumption(resultSet.getDouble("GAS_CONSUMPTION"));
        carBean.setHorsePower(resultSet.getDouble("HORSEPOWER"));
        carBean.setWeightKG(resultSet.getDouble("WEIGHT_KG"));
        carBean.setModel_year(resultSet.getInt("MODEL_YEAR"));
        carBean.setCountry(resultSet.getString("COUNTRY"));
        return carBean;
    }
}
