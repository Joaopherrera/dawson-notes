package com.exam.finalexam517.presentation;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import com.exam.finalexam517.beans.CarBean;
import com.exam.finalexam517.persistence.CarDAO;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainLayoutController
{
    private final static Logger LOG = LoggerFactory.getLogger(MainLayoutController.class);

    private CarBean carBean;
    private CarDAO carDAO;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML
    private Button displayReportBtn;

    @FXML
    private TextField carNameInput;

    @FXML
    private TextField gasInput;

    @FXML
    private TextField hpInput;

    @FXML
    private TextField weightInput;

    @FXML
    private TextField modeYearInput;

    @FXML
    private TextField countryInput;

    @FXML
    private TextField filterInput;

    @FXML
    private TableView<CarBean> carTable;

    @FXML // fx:id="idColumn"
    private TableColumn<CarBean, Number> idColumn;
    @FXML // fx:id="carColumn"
    private TableColumn<CarBean, String> carColumn;
    @FXML // fx:id="gasColumn"
    private TableColumn<CarBean, Number> gasColumn;
    @FXML // fx:id="hpColumn"
    private TableColumn<CarBean, Number> hpColumn;
    @FXML // fx:id="weightColumn"
    private TableColumn<CarBean, Number> weightColumn;
    @FXML // fx:id="modelColumn"
    private TableColumn<CarBean, Number> modelColumn;
    @FXML // fx:id="countryColumn"
    private TableColumn<CarBean, String> countryColumn;


    @FXML
    void initialize()
    {
        // Connects the property in the FishData object to the column in the table
        idColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        carColumn.setCellValueFactory(cellData -> cellData.getValue().carProperty());
        gasColumn.setCellValueFactory(cellData -> cellData.getValue().gasConsumptionProperty());
        hpColumn.setCellValueFactory(cellData -> cellData.getValue().horsePowerProperty());
        weightColumn.setCellValueFactory(cellData -> cellData.getValue().weightKGProperty());
        modelColumn.setCellValueFactory(cellData -> cellData.getValue().model_yearProperty());
        countryColumn.setCellValueFactory(cellData -> cellData.getValue().countryProperty());
    }


    /**
     * Method to bind all the properties to the fieds
     */
    private void doBindings()
    {
        // Two way binding
        Bindings.bindBidirectional(carNameInput.textProperty(), carBean.carProperty());
        Bindings.bindBidirectional(gasInput.textProperty(), carBean.gasConsumptionProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(hpInput.textProperty(), carBean.horsePowerProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(weightInput.textProperty(), carBean.weightKGProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(modeYearInput.textProperty(), carBean.model_yearProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(countryInput.textProperty(), carBean.countryProperty());
    }

    /**
     * Setter for the DAO and Bean
     * @param carBean
     * @param carDAO
     */
    public void setCarDAOData(CarBean carBean, CarDAO carDAO)
    {
        this.carBean = carBean;
        //Call binding method once the bean is passed.
        doBindings();
        try
        {
            this.carDAO = carDAO;
            carDAO.findNextByID(carBean);
        }
        catch (SQLException ex)
        {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }


    //****************************************//
    //********** FORM METHODS ****************//
    //****************************************//

    /**
     * Method to clear the clear the form
     * @param event
     */
    @FXML
    void clearForm(ActionEvent event)
    {
        carBean.setId(-1);
        carBean.setCar("");
        carBean.setGasConsumption(0);
        carBean.setHorsePower(0);
        carBean.setWeightKG(0);
        carBean.setModel_year(0);
        carBean.setCountry("");
    }

    /**
     * Event handler to delete a car
     * @param event
     */
    @FXML
    void deleteForm(ActionEvent event)
    {
        try
        {
            int records = carDAO.deleteCar(carBean);
            LOG.info("Records deleted = " + records);
            clearForm(event);
        }
        catch (SQLException ex)
        {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Event Handler to go to the next car
     * @param event
     */
    @FXML
    void nextCar(ActionEvent event)
    {
        try
        {
            carDAO.findNextByID(carBean);
        }
        catch (SQLException ex)
        {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Event handler to go to the previous car
     * @param event
     */
    @FXML
    void prevCar(ActionEvent event)
    {
        try
        {
            carDAO.findPrevByID(carBean);
        }
        catch (SQLException ex)
        {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Event handler to save any changes
     * @param event
     */
    @FXML
    void saveForm(ActionEvent event)
    {
        try
        {
            if (validateInputs())
            {
                int records = carDAO.saveCar(carBean);
                LOG.info("Records updated or saved = " + records);
            }
            else
            {
                errorAlert("One or more inputs are invalid. Please fix them.");
            }

        }
        catch (SQLException ex)
        {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Method to validate inputs before letting the user save any changes
     * @return is the inputs are valid or not
     */
    private boolean validateInputs()
    {
        //TODO - Finish this
        String carName = carNameInput.getText();
        double gasConsumption = Double.parseDouble(gasInput.getText());
        double horsePower = Double.parseDouble(hpInput.getText());
        double weight = Double.parseDouble(weightInput.getText());
        int modelYear = Integer.parseInt(modeYearInput.getText());
        String country = countryInput.getText();



        if (carName.length() > 40)
        {}
        else if (country.length() > 20)
        {

        }

        //Its only returning true bc i didnt have time to finish.
        return true;
    }



    //****************************************//
    //********** TABLE METHODS ***************//
    //****************************************//

    /**
     * Method to display all the cars on the table
     */
    public void displayTable()
    {
        try
        {
            carTable.setItems(carDAO.findAll());
        }
        catch (SQLException e)
        {
            errorAlert(e.getMessage());
        }
    }


    /**
     * Event handler to display the cars based on the filter input
     * @param event
     */
    @FXML
    void filterCars(ActionEvent event)
    {
        int filter = Integer.parseInt(filterInput.getText());

        try
        {
            carTable.setItems(carDAO.findAllFiltered(filter));
        }
        catch (SQLException e)
        {
            errorAlert(e.getMessage());
        }
    }



    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ErrorDAOTitle"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ErrorDAOText"));
        dialog.setContentText(msg);
        dialog.show();
    }

}
