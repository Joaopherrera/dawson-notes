package com.exam.finalexam517;

import com.exam.finalexam517.beans.CarBean;
import com.exam.finalexam517.persistence.CarDAO;
import com.exam.finalexam517.presentation.MainLayoutController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author
 */
public class MainAppFX extends Application
{
    private final static Logger LOG = LoggerFactory.getLogger(MainAppFX.class);

    private CarBean carBean = new CarBean();
    private CarDAO carDAO = new CarDAO();

    public static void main(String[] args)
    {
        launch(args);
        System.exit(0);
    }

    @Override
    public void start(Stage primaryStage)
    {
        Parent layout = null;

        Locale currentLocale = new Locale("en", "CA");
        try
        {
            //Setting the location of the FXML and loading it
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("MessageBundle"));
            loader.setLocation(MainAppFX.class.getResource("/fxml/MainLayout.fxml"));
            layout = loader.load();
            //Getting the controller, setting the bean and dao
            MainLayoutController mainLayoutController = loader.getController();
            mainLayoutController.setCarDAOData(carBean, carDAO);
            //displaying the table
            mainLayoutController.displayTable();
            Scene scene = new Scene(layout);
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (IOException e)
        {
            LOG.error(null, e);
            Platform.exit();
        }
        LOG.info("Program Started");
    }
}
