package com.javabycomparison.analysis;

import java.util.Collections;
import java.util.Objects;

//Favor Enums Over Integer Constants
enum Language
{
    JAVA("Java"), PYTHON("Python"), OTHER("Other");

    final String languageName;
    
    Language(String languageName)
    {
        this.languageName = languageName;
    }
}

public class ResultDataPrinter {

    public String print(ResultData data) {
        Objects.requireNonNull(data);
        
        String language = data.type.name();
        
        return data.name
                + "\t"
                + language
                + "\t"
                + data.L
                + "\t"
                + data.LOC
                + "\t"
                + data.commentLOC
                + "\t"
                + data.numMethod
                + "\t"
                + data.nImports;
    }

    public String printFileName(ResultData data, int length) {
        Objects.requireNonNull(data);
        return String.join("", Collections.nCopies(Math.max(length - data.name.length(), 0), " "))
                + data.name;
    }

    public String printLanguage(ResultData data, int length) {
        Objects.requireNonNull(data);
        String language = data.type.name();
        return String.join("", Collections.nCopies(Math.max(length - language.length(), 0), " "))
                + language;
    }

    public String printLineOfCodes(ResultData data, int length) {
        Objects.requireNonNull(data);
        return String.join("", Collections.nCopies(Math.max(length - String.valueOf(data.LOC).length(), 0), " "))
                + data.LOC;
    }

    public String printCommentLineOfCodes(ResultData data, int length) {
        Objects.requireNonNull(data);
        return String.join("", Collections.nCopies(Math.max(length - String.valueOf(data.commentLOC).length(), 0), " "))
                + data.commentLOC;
    }

    public String printNumMethodLineOfCodes(ResultData data, int length) {
        Objects.requireNonNull(data);
        return String.join("", Collections.nCopies(Math.max(length - String.valueOf(data.numMethod).length(), 0), " "))
                + data.numMethod;
    }

    public String printNImportsLineOfCodes(ResultData data, int length) {
        Objects.requireNonNull(data);
        return String.join("", Collections.nCopies(Math.max(length - String.valueOf(data.nImports).length(), 0), " "))
                + data.nImports;
    }
}
