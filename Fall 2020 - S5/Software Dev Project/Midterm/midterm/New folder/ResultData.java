package com.javabycomparison.analysis;

import java.util.StringJoiner;


public class ResultData {

    public Language type;
    public String name;
    public int L;
    public int LOC;
    public int commentLOC;
    public int numMethod;
    public int nImports;

    /**
     * 
     * 
     * @param type
     * @param name
     * @param LOC
     * @param commentLOC
     * @param numMethod
     * @param nImports 
     */
    public ResultData(Language type, String name, int LOC, int commentLOC, int numMethod, int nImports) {
        this.type = type;
        this.name = name.replaceAll("\\\\", "/");
        this.LOC = LOC;
        this.commentLOC = commentLOC;
        this.numMethod = numMethod;
        this.nImports = nImports;
    }

    //Remove Commented-Out Code
    public ResultData() {
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (this == o) {
            return true;
        }

        ResultData resultData = (ResultData) o;
        return type == resultData.type
                && L == resultData.L
                && LOC == resultData.LOC
                && commentLOC == resultData.commentLOC
                && numMethod == resultData.numMethod
                && nImports == resultData.nImports
                && name.equals(resultData.name);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ResultData.class.getSimpleName() + "[", "]")
                .add("type=" + type.name())
                .add("name='" + name + "'")
                .add("L=" + L)
                .add("LOC=" + LOC)
                .add("commentLOC=" + commentLOC)
                .add("numMethod=" + numMethod)
                .add("nImports=" + nImports)
                .toString();
    }
}
