package com.javabycomparison.search;

import com.javabycomparison.analysis.AnalyzerImpl;
import com.javabycomparison.analysis.JavaAnalyzer;
import com.javabycomparison.analysis.PythonAnalyzer;
import com.javabycomparison.analysis.ResultData;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SearchClient {

    private boolean summary;

    public SearchClient(boolean summary) {
        this.summary = summary;
    }

    public List<ResultData> collectAllFiles(String directoryPath) {
        LinkedList<ResultData> resultsList = new LinkedList<>();
        try {
            for (Path file : Files.walk(Paths.get(directoryPath))
                            .filter(path -> !path.toString().contains(".git"))
                            .filter(
                                    path -> 
                                    {
                                        try 
                                        {
                                            return !Files.isHidden(path);
                                        } 
                                        catch (IOException e) 
                                        {
                                            return false;
                                        }
                                    })
                            .sorted()
                            .collect(Collectors.toList()))
            {
                if (isJavaFile(file)) 
                {
                    if (!summary) 
                    {
                        printToConsole(file, "java");
                    }
                    ResultData resultData = new JavaAnalyzer(file).analyze();
                    resultsList.add(resultData);
                } 
                else if (isPythonFile(file)) 
                {
                    if (!summary) 
                    {
                        printToConsole(file, "python");                    
                    }
                    ResultData resultData = new PythonAnalyzer(file).analyze();
                    resultsList.add(resultData);
                } 
                else
                {
                    if (!Files.isDirectory(file)) 
                    {
                        if (!summary) 
                        {
                            printToConsole(file, "other");
                        }
                        resultsList.add(new AnalyzerImpl(file).analyze());
                    } 
                    else 
                    {
                        if (!summary) 
                        {
                            System.out.println("Skipping directory " + file + ".");
                        }
                    }
                }
            }
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
        return resultsList;
    }

    private boolean isJavaFile(Path file) 
    {
        //Return Boolean Expressions Directly
        return file.toString().matches(".*\\.java");
    }

    private boolean isPythonFile(Path file) 
    {
        //Return Boolean Expressions Directly
        return file.getFileName().toString().matches(".*\\.py");
    }
    
    private void printToConsole(Path file, String type)
    {
        System.out.println(String.format("File %S is a %ss file. It will be analyzed.", file.toString(), type));
    }
}
