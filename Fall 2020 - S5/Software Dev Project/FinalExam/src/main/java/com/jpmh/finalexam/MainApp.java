package com.jpmh.finalexam;

import com.jpmh.presentation.MainLayoutController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class MainApp extends Application
{
    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    //private Bean bean = new Bean();
    //private BlankDAO dao = new BlankDAO();

    public static void main(String[] args)
    {
        launch(args);
        System.exit(0);
    }

    @Override
    public void start(Stage primaryStage)
    {
        Parent layout = null;

        Locale currentLocale = new Locale("en", "CA");
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("MessageBundle"));
            loader.setLocation(MainApp.class.getResource("/fxml/MainLayout.fxml"));
            layout = (BorderPane) loader.load();

            MainLayoutController mainLayoutController = loader.getController();
            //mainLayoutController.setDAOData(bean, blankDAO);

            Scene scene = new Scene(layout);
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (IOException e)
        {
            LOG.error(null, e);
            Platform.exit();
        }
        LOG.info("Program Started");
    }
}
