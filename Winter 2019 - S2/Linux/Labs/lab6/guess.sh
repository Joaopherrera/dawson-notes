#!/bin/bash
# Joao Pedro Morais Herrera
# 03-03-2019
#
# Script to make a guess game with the user

word=banana
counter=1

echo 'Enter your guess: (or "hint" for a hint)'
read guess

while [[ $guess != $word ]]
do
		
	if [[ $guess == "hint" ]]
	then
		echo 'Its an yellow fruit'
		echo 'Enter your guess: (or "hint" for a hint)'
		read guess
	else 
		
		echo 'Enter your guess: (or "hint" for a hint)'
		read guess 
		((counter++))
	fi
done

echo 'You got the word, congratulations.'
echo 'Number of attemps:' $counter

