#!/bin/bash
# Joao Pedro Morais Herrera
# 03-03-2019
#
# Script to display size of files 

if [[ $# -eq 0 ]]
then
	echo 'The script needs at least one command line arguments'
	exit 1
else
	echo $0: Evaluating $# files. 
fi

for files in $@
do 
	if [[  -e $files ]]
	then
		echo $files
		size=$(du -b $files | cut -f 1)
		echo 'Size:' $size 
		sizeKB=$((size/1024))
		echo 'Size(In KB):' $sizeKB
	else
		echo 'Its not a file'
	fi
done
	
