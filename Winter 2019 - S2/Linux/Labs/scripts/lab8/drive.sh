#!/bin/bash
# Joao Pedro Morais Herrera
# 14-03-2019
# 
# Scripts for fonctions lab8

summer ()
{
	if [[ $# -eq 0 ]]
	then
		return 5
	fi

	for number in $@
	do
		sum=$((number+sum))
	done

	return 0
}

factorial ()
{
	if [[ $# -eq 0 ]]
	then
		return 5
	fi

	fac=$1
	number=$(( $1 - 1 ))
	while [[ number -ne 0 ]]
	do 
		fac=$(( number * fac ))
		((number--))
	done

	return 0
}

dirs ()
{
	if [[ $# -eq 0 ]]
	then
		return 5
	fi

	local countE=0
	local countDE=0
	countDIR=0

	for var in $@ 
	do 
		if [[ -d $var ]]
		then 
			((countDIR++))
			((countE++))
		elif [[ -e $var ]]
		then
			((coutnE++))
		else
			((countDE++))
		fi
	done

	echo Counter Exists: $countE
	echo "Counter Doesn't Exists: " $countDE

	return 0
}

echo - TEST OUTPUTS -

echo -e "\n"

echo Output for SUMMER:
summer 1 2 3 4 
echo Supposed to be 0: $?
echo Supposed to be 10: $sum

echo -e "\n"

echo Output for FACTORIAL:

echo 'Test 1(one arg)' 
factorial 5
echo Supposed to be 0: $?
echo Supposed to be 120: $fac

echo 'Test 2(no args)'
factorial
echo Supposed to be 5: $?
echo Supposed to be nothing: $?

echo -e "\n"

echo Output for DIRS:

echo 'Test 1(3 args)'
dirs /etc/passwd ~/lab8 testforfonc
echo Supposed to be 0: $?
echo Supposed to be 1 dir: $countDIR
countDIR=0

echo 'Test 2(0 args)'
dirs
echo Supposed to be 5: $?
echo Supposed to be 0: $countDIR


echo -e "\n"
