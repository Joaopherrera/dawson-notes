#!/bin/bash
# Joao Pedro Morais Herrera
# 03-03-2019
#
# Script to create directories with the cli, date and pid

for args in $@ 
do
	mkdir $args.$(date +%F).$$
done

