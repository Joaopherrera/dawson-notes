#!/bin/bash
# Joao Pedro Morais Herrera
# 28-03-2019
#
# Script for test regex

read -p "ENTER A FILE: " list

if [[ ! -e "$list" ]] 
then
	echo "That's not a file"
	exit 5
else
	for str in $(cat $list)
	do
		echo -- PATTERNS FOR $str --

		echo "Regex for Linux (End)"
		if [[ "$str" =~ Linux$ ]]
		then
			echo Match
		else
			echo No Match
		fi 

		echo "Regex for Linux (Begins)"
		if [[ "$str" =~ ^Linux ]]
		then
			echo Match
		else 	
			echo No Match
		fi

		echo "Regex for string 'unix'"
		if [[ "$str" =~ unix ]]
		then
			echo Match
		else
			echo No Match
		fi

		echo "Regex for string unix mixed with any case"
		if [[ "$str" =~ [uU][Nn][iI][xX] ]]
		then
			echo Match
		else
			echo No Match
		fi

		echo "Regex for begins with an uppercase letter"
		if [[ "$str" =~ ^[[:upper:]]+ ]] 
		then
			echo Match 
		else
			echo No Match
		fi

		echo "Regex for does not contain a number anywhere"
		if [[ "$str" =~ [^[:digit:]]+ ]]
		then
			echo Match
		else
			echo No Match
		fi

		echo "Regex for Windows/windows and Linux/linux"
		if [[ "$str" =~ ([Ww]indows|[Ll]inux) ]]
		then
			echo Match
		else
			echo No Match
		fi
		
		echo "Regex for finds thing with min of 2 and max if 5 gs"
		if [[ "$str" =~ thing{2,5}$ ]]
		then
			echo Match
		else
			echo No Match
		fi

		echo "Regex for not containing any white spaces"
		if [[ "$str" =~ [^[:blank:]] ]]
		then 
			echo Match
		else 
			echo No Match
		fi 

		echo "Regex for Linux(with 0 or more x on the end)" 
		if [[ "$str" =~ Linux* ]]
		then
			echo Match
		else
			echo No Match
		fi

		echo "Regex for word Tux"
		if [[ "$str" =~ [[:space:]]Tux[[:space:]]  ]]
		then
			echo Match
		else 
			echo No Match
		fi 
		
		echo "Regex for any word that ends with nix"
		if [[ "$str" =~ nix$ ]] 
		then 
			echo Match
		else 
			echo No Match
		fi 

	done
fi
