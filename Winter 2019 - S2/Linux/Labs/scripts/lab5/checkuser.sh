#!/bin/bash
# Joao Pedro Morais Herrera 
# 26-02-2019
#
# Script to check for a user in the passaword file and group

echo Enter a userID:
read userid


if grep -q $userid /etc/passwd
then
	echo $userid is in the /etc/passwd file
else
	echo $userid is not in the /etc/passwd file
fi

if grep -q $userid /etc/group
then 
	echo $userid is in the /etc/group
else
	echo $userid is not in the /etc/group
fi

