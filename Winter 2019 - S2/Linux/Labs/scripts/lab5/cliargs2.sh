#!/bin/bash
# Joao Pedro Morais Herrera
# 26-02-2019
#
# Script to ensure that there are 2 command line arguments, if not show an error, if yes it will
# compare the variables and display the largest

if [[ $# -ne 2 ]]
then 
	echo The Scripts needs only 2 args
	exit
fi 
 
if [[ $1 -lt $2 ]]
then
	echo $2 is bigger then $1
else 
	echo $1 is bigger then $2
fi

	
