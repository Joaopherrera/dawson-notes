#!/bin/bash
# Joao Pedro Morais Herrera
# 26-02-2019
#
# Script to print logged users, current work directory, name of the shell, name of the OS and all the# files in the /etc/ directory

 
echo $0: $(whoami)
echo $0: $(pwd)
echo $0: $SHELL
echo $0: $(uname -o)
echo $0: $(ls /etc/*conf)

