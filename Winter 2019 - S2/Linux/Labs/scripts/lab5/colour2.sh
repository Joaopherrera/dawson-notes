#!/bin/bash 
# Joao Pedro Morais Herrera
# 26-02-2019
#
# Compare two Strings colours and display a message 

mycolour=fuschia

if [[ $# -eq 1 ]]
then
	yourcolour=$1

	if [[ $mycolour == $yourcolour ]]
	then
		echo Our favourite colour is $mycolour
	else 
		echo My colour is $mycolour and your colour is $yourcolour
	fi
	exit
else 
	echo Enter your colour:
        read yourcolour 
	
	if [[ $mycolour == $yourcolour ]]
	then
		echo Our favourite colour is $mycolour 
	else 	
		echo My colour is $mycolour and your colour is $yourcolour
	fi
fi



