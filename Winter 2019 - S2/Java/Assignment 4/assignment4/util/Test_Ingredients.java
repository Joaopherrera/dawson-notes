package assignment4.util;
import assignment4.util.*;

public class Test_Ingredients
{
  public static void main(String[] args)
  {
    System.out.println("//-- Test For Ingredients --//");
    System.out.println("Test Case One - String and Double:");
    
    Ingredients test1 = new Ingredients("banana", 2.65);
    System.out.println("Name: " + test1.getName() + " - Cost: " + test1.getCost());
    
    System.out.println("Test Case Two - Nothing:");
    
    Ingredients test2 = new Ingredients("Nothing", 0.0);
    System.out.println("Name: " + test2.getName() + " - Cost: " + test2.getCost());
     
    System.out.println("Test Case Three - String and Int:");
    
    Ingredients test3 = new Ingredients("12", 12);
    System.out.println("Name: " + test3.getName() + " - Cost: " + test3.getCost());
    
  }
}