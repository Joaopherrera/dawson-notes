package assignment4.util;

public class Item
{
  private Ingredients[] material;
  private double cost;
  private String name;
  private double ingrCost;
  
  //Constructor
  public Item(String[] inputLine ,Ingredients[] ingredients)
  {
    this.name = inputLine[0];
    this.cost = Double.parseDouble(inputLine[1]);
    
    material = new Ingredients[inputLine.length - 2];
    
    for(int i = 2; i < inputLine.length; i++)
    {
      for(int j = 0; j < ingredients.length; j++)
      {
        if(inputLine[i].equals(ingredients[j].getName()))
        {
          material[i-2] = ingredients[j];
          ingrCost += ingredients[j].getCost();
        }
      }
    }
  }
  
  
  //Getters
  public String getName()
  {
    return this.name;
  }
  public double getCost()
  {
    return this.cost;
  }
  public Ingredients[] getIngredients()
  {
    return this.material;
  }
  public double getIngCost()
  {
    return this.ingrCost;
  }
}