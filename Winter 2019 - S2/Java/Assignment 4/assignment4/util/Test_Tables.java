package assignment4.util;
import assignment4.util.*;

public class Test_Tables
{
  public static void main(String[] args)
  {
    Tables table = new Tables();
    
    System.out.println("//-- Test For Tables --//");
    System.out.println("DEFAULT STATE");
    System.out.println("Had Paid: " + table.getHadPaid() + " - Payment: " + table.getPaymentMethod() + " - Num of Orders: " + table.getNumOfOrders() + " - Amount: " + table.getOwedAmount());
    
    table.setNumOfOrder(3);
    table.setHadPaid(true);
    table.setPaymentMethod("debit");
    table.setOwedAmount(4.8);
    
    System.out.println("CHANGED STATE");
    System.out.println("Had Paid: " + table.getHadPaid() + " - Payment: " + table.getPaymentMethod() + " - Num of Orders: " + table.getNumOfOrders() + " - Amount: " + table.getOwedAmount());
    
    table.resetTable();
    System.out.println("RESET TABLE");
    System.out.println("Had Paid: " + table.getHadPaid() + " - Payment: " + table.getPaymentMethod() + " - Num of Orders: " + table.getNumOfOrders() + " - Amount: " + table.getOwedAmount());
  }
}