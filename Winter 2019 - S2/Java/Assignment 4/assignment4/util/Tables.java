package assignment4.util;

public class Tables
{
  private Item[] order;
  private int numOfOrders;
  private boolean hadPaid;
  private String paymentMethod;
  private double owedAmount;
  
  public Tables()
  {
    order = new Item[100];
    numOfOrders = 0;
    hadPaid = false;
  }

  
  //Setter
  public void setNumOfOrder(int n)
  {
    this.numOfOrders = n;
  }
  public void setHadPaid(boolean b)
  {
    this.hadPaid = b;
  }
  public void setPaymentMethod(String paymentMethod)
  {
    this.paymentMethod = paymentMethod;
  }
  public void setOwedAmount(double owedAmount)
  {
    this.owedAmount = owedAmount;
  }
  
  
  //Getters
  public boolean getHadPaid()
  {
    return this.hadPaid;
  }
  public String getPaymentMethod()
  {
    return this.paymentMethod;
  }
  public Item[] getOrders()
  {
    return this.order;
  }
  public int getNumOfOrders()
  {
    return this.numOfOrders;
  }
  public double getOwedAmount()
  {
    return this.owedAmount;
  }
}