package assignment4.util;
import assignment4.util.*;

public class Test_Item
{
  public static void main(String[] args)
  {
    String[] arr = {"Tomato Soup","9.5", "Pea", "Carrot", "Tomato"};
    
    Ingredients[] ingArr = new Ingredients[5];
    
    ingArr[0] = new Ingredients("Pea",2.5);
    ingArr[1] = new Ingredients("Carrot",0.12);
    ingArr[2] = new Ingredients("Tomato",0.16);
    ingArr[3] = new Ingredients("Banana",1.0);
    ingArr[4] = new Ingredients("Apple",0.90);
  
    for(int i = 0; i < ingArr.length; i++)
    {
      System.out.println(ingArr[i].getName() + " " + ingArr[i].getCost());
    }
    
    Item test1 = new Item(arr,ingArr);
    
    System.out.println("//-- Test For Item --//");
    System.out.println("Name: " + test1.getName() + " - Cost: " + test1.getCost());
    
    Ingredients[] ingArr2 = test1.getIngredients();
    
    System.out.println("For Ingredients:");
    for(int i = 0; i < ingArr2.length; i++)
    {
      System.out.println("Name: " + ingArr2[i].getName() + " - Cost: " + ingArr2[i].getCost());
    }
  }
}