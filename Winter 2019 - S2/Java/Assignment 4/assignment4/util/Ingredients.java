package assignment4.util;

public class Ingredients
{
  private double cost;
  private String name;
  
  //Constructor
  public Ingredients(String name, double cost)
  {
    this.name = name;
    this.cost = cost;
  }
  
  //Getters
  public String getName()
  {
    return this.name;
  }
  public double getCost()
  {
    return this.cost;
  }
}
