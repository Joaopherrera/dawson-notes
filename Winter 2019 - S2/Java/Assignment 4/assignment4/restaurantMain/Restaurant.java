package assignment4.restaurantMain;
import assignment4.util.*;
import assignment4.restaurant.*;
import java.io.*;

public class Restaurant
{
  public static void main(String[] args) throws FileNotFoundException
  {
    RestaurantApp app = new RestaurantApp();
    app.run();
  }
}