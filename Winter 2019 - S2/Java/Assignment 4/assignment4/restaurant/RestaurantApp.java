package assignment4.restaurant;
//import assignment4.util.*;
import java.util.Scanner;
import java.io.*;

public class RestaurantApp
{
  //-- PROPERTIES --//
  private Tables[] tables;
  private Tables[] paidTables;
  private int totalPaidTables;
  private Item[] menuItems;
  private Ingredients[] ingredients;
  
  
  //Constructor
  public RestaurantApp() throws FileNotFoundException
  {
    //-- CREATING ALL THE INGREDIENTS --//
    try
    {  
      File ingredientsFile = new File("..\\Assignment 4\\ingredients.csv");
      
      Scanner ingredientScan = new Scanner(ingredientsFile);
      
      //Just to count the number of lines
      int ingredientsCount = 0;
      while(ingredientScan.hasNextLine())
      {
        ingredientScan.nextLine();
        ingredientsCount++;
      }
      
      //Initializing the length the array
      ingredients = new Ingredients[ingredientsCount];
      
      ingredientScan = new Scanner(ingredientsFile);
      int ingredientsNum = 0;
      
      //Loop to create the Ingredients array
      while(ingredientScan.hasNextLine())
      {
        String[] inputLine = ingredientScan.nextLine().split(",");
        ingredients[ingredientsNum] = new Ingredients(inputLine[0],Double.parseDouble(inputLine[1]));
        ingredientsNum++;      
      }
    }
    catch(FileNotFoundException e)
    {
      System.out.println("Ingredients Not Found.");
    }
    
    
    try
    {
      //-- CREATING THE ITEM FOR THE MENU --//
      File menuFile = new File("..\\Assignment 4\\menuitems.csv");
      
      Scanner menuItemScan = new Scanner(menuFile);
      
      //Just to count the number of lines
      int menuItemCount = 0;
      while(menuItemScan.hasNextLine())
      {
        menuItemScan.nextLine();
        menuItemCount++;
      }
      
      menuItems = new Item[menuItemCount];
      
      menuItemScan = new Scanner(menuFile);
      int menuItemNum = 0;
      
      //Loop to create the Ingredients array
      while(menuItemScan.hasNextLine())
      {
        String[] inputLine = menuItemScan.nextLine().split(",");
        menuItems[menuItemNum] = new Item(inputLine, ingredients);
        menuItemNum++;
      }
    }
    catch(FileNotFoundException e)
    {
      System.out.println("MenuItem Not Found.");
    }
    
    //-- CREATIONG ALL THE TABLES --//
    tables = new Tables[12];
    for(int i = 0; i < 12; i++)
    {
      tables[i] = new Tables();
    }
    
    paidTables = new Tables[100];
    totalPaidTables = 0;
  }
  
  
  
  //Run the restaurant
  public void run()
  {
    
    //Boolean which tracks if the restaurant has closed.
    boolean closeRestaurant = false;
    
    Scanner keyboard = new Scanner(System.in);
    do
    {
      System.out.println("Please choose from the following options:\n"
                           + "1. New Party\n"
                           + "2. Add Order\n"
                           + "3. Accept Payment\n"
                           + "4. End Night");
      int option = keyboard.nextInt();
      switch(option)
      {
        case 1:
          //ADD NEW PARTY
          boolean condition;
          do
          {
            condition = addParty();          
          }
          while(!(condition));
          break;
          
        case 2:
          //ADD ORDER
          order();
          break;
          
        case 3:
          //ACCEPT PAYMENT
          payment();
          break;
          
        case 4:
          //CLOSE RESTAURANT
          closeRestaurant = closingRestaurant();
          break;
          
        default:
          //If the user enters a value that isn't 1-4, they have done something wrong.
          System.out.println("invalid option - please enter a number 1 - 4");
      }
    }
    while(!closeRestaurant);
    
    //Compute the amount of money the restaurant took in from cash/debit/credit
    //as well as the amount we spent on ingredients before computing total profit.
    double creditTransactions = 0;
    double debitTransactions = 0;
    double cashTransactions = 0;
    double ingredientTotal = 0;
    double paidAmount = 0;
    
    //iterate the the orders of each paid table.
    for(int i = 0; i < totalPaidTables; i++)
    {
      paidAmount = paidTables[i].getOwedAmount();
      String payment = paidTables[i].getPaymentMethod();
      
      //check how the table paid, and add the cost of the meal to the appropriate sum.
      if(payment.equals("debit"))
      {
        debitTransactions += paidAmount;
      }
      else if(payment.equals("credit"))
      {
        creditTransactions += paidAmount;
      }
      else if(payment.equals("cash"))
      {
        cashTransactions += paidAmount;
      }
      else
      {
        System.out.println("Invalid payment method accepted - something went wrong!");
      }
      
      Item[] orders = paidTables[i].getOrders();

      //for each ingredient used to make the item in question, 
      for(int item = 0; item < paidTables[i].getNumOfOrders(); item++)
      {
        ingredientTotal += orders[item].getIngCost();
      }
    }
    
    //Print out the amount made from various transactions, amount paid for ingredients, and total profit made.
    System.out.printf("We made %7.2f from credit.\n",creditTransactions);
    System.out.printf("We made %7.2f from debit.\n",debitTransactions);
    System.out.printf("We made %7.2f from cash.\n",cashTransactions);
    System.out.printf("We paid %7.2f on ingredients.\n",ingredientTotal);
    System.out.printf("We made %7.2f total profit.\n",creditTransactions + debitTransactions + cashTransactions - ingredientTotal);
  }
  
  //CASE 1 - Method to add a new party(new table) on the restaurant
  public boolean addParty()
  {
    Scanner keyboard = new Scanner(System.in);
    
    //Find if a table is available
    int table = -1;
    for(int i = 0; i < tables.length; i++)
    {
      if(tables[i].getNumOfOrders() == 0)
      {
        table = i;
        break;
      }
    }
    
    //if no table was found, print a message indicating this, and break back to the option menu.
    if(table == -1)
    {
      System.out.println("No tables available");
      return false;
    }
    
    int items = 0;        
    //Ask how many items will be ordered until a number > 0 has been entered.
    do
    {
      System.out.println("How many menu items will this table be having?");
      items = keyboard.nextInt();
    }
    while(!(items > 0));
    
    tables[table].setNumOfOrder(items);
    System.out.printf("Table %d has been seated.\n\n", table);
    
    return true;
  }
  
  //CASE 2 - Method to place the orders
  public boolean order()
  {
    Scanner keyboard = new Scanner(System.in);
    
    //Ask which table is paying until a choice between 0 and 11 has been entered
    int orderTable;
    do
    {
      System.out.println("Which table is ordering? Enter a table from 0 to 11.");
      orderTable = keyboard.nextInt();
    }
    while(orderTable < 0 || orderTable > 11);
    
    //if nobody is sitting at that table, break back to the option menu.
    if(tables[orderTable].getNumOfOrders() == 0)
    {
      System.out.println("There is nobody at that table!");
      return false;
    }
    
    
    //if that table has already ordered, break back to the option menu.
    if(tables[orderTable].getOrders()[0] != null)
    {
      System.out.println("That table has already ordered!"); 
      return false;
    }    
    
    //Print out a formatted list of all the items on the menu          
    System.out.println("Choose items from the menu:");
    
    for(int i = 1; i < menuItems.length; i++)
    {
      System.out.printf("%2d. %-15s",i,menuItems[i].getName());
      
      if(i%3 == 0)//Every third item, we print a new line to make the menu more readable.
      {
        System.out.println();
      }
    }
    
    //Ask for each item to be added to the table's list of orders.
    for(int i = 0; i < tables[orderTable].getNumOfOrders() ; i++)
    {
      int orderedItem;
      //Make sure each input menu item corresponds to an item that is actually on the menu.
      do
      {
        System.out.printf("\nEnter the next item to be ordered. Enter a value 1 to %d\n", menuItems.length - 1);
        orderedItem = keyboard.nextInt();
      }
      while(orderedItem < 1 || orderedItem > menuItems.length);
      
      //add each individual item to the list of the table's ordered items.
      (tables[orderTable].getOrders())[i] = menuItems[orderedItem];
    }  
    return true;
  }
  
  //CASE 3 - Method to accpet a payment method and empty the table
  public boolean payment()
  {
    Scanner keyboard = new Scanner(System.in);
    
    //Ask which table is paying until a choice between 0 and 11 has been entered
    int payTable;
    do
    {
      System.out.println("Which table is paying? Enter a table from 0 to 11.");
      payTable = keyboard.nextInt();
    }
    while(payTable < 0 || payTable > 11);
    
    //if nobody is sitting at that table, break back to the option menu.
    if(tables[payTable].getNumOfOrders() == 0)
    {
      System.out.println("There is nobody at that table!");
      return false;
    }
    
    //Item[] temp = tables[payTable].getOrders();
    //if that table has yet to order, break back to the option menu.
    if((tables[payTable].getOrders())[0] == null)
    {
      System.out.println("That table hasn't ordered yet!"); 
      return false;
    }
    
    //Calculate the amount the table owes
    double owedAmount = 0;
    for(int i = 0; i < tables[payTable].getNumOfOrders(); i++)
    {
      Item[] orders = tables[payTable].getOrders();
      //determine the price of each menuItem that is in the list of items ordered by the table and add it.
      owedAmount += orders[i].getCost();
    }
    //print out the owed amount to 2 decimal places.
    System.out.printf("Table %d owes $%.2f\n",payTable,owedAmount);
    tables[payTable].setOwedAmount(owedAmount);
    
    //Ask them whether they are paying via debit credit or cash.
    String payMethod;
    do
    {
      System.out.println("How is this table paying? Enter debit, credit, or cash.");
      payMethod = keyboard.next();
    }
    while(!(payMethod.equals("debit") || payMethod.equals("credit") || payMethod.equals("cash")));
    
    //put the payment method this table used at the payMethod variable.
    tables[payTable].setPaymentMethod(payMethod);

    //Copy the table to the array of paid tables.
    paidTables[totalPaidTables] = tables[payTable];  
    
    //increment the total amount of tables that have paid.
    totalPaidTables++;

    //empty the table from the tables array, allowing for new customers to come in.
    tables[payTable] = new Tables();

    return true;
  }
  
  //CASE 4 - Method to check and close the restaurant
  public boolean closingRestaurant()
  {
    //Print a message indicating the close, and modify closeRestaurant so we break out of the loop.
    System.out.println("Closing Restaurant");
    
    //Check to make sure there are no occupied tables, if a table is occupied print a message.
    for(int i = 0; i < tables.length; i++)
    {
      if(!(tables[i].getNumOfOrders() == 0))
      {
        System.out.printf("Cannot close - Table %d is occupied\n",i);
        return false;
      }
    }
    return true;
  }
}