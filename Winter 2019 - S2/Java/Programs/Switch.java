import java.util.*;
public class Switch
{
  public static void main (String[] args)
  {
    System.out.println("Enter Letter:");
    Scanner reader = new Scanner(System.in);
    String input; // To hold keyboard input
    char choice;  // To store the user's choice
    
// Ask the user to enter A, B, or C.
    input = reader.next();
    choice = input.charAt(0);  // Get the first char
    
// Determine which character the user entered.
    switch (choice)
    {
      case 'A':
        System.out.println("You entered A.");
        break;
      case 'B':
        System.out.println("You entered B.");
        break;
      case 'C':
        System.out.println("You entered C.");
        break;
      default:
        System.out.println("That's not A, B, or C!");
    }
    reader.close();
  }
}