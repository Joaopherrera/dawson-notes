package moreexercises.lab02;
import labexercises.lab02.*;
public class MultipleClasses
{
  public static void main(String[] args)
  {
    int[] a = {1,2,3,4,5,6,7,8,9,10};
    int e = ArrayUtilities.countEvens(a);
    System.out.println(e);
    
    int[] b = ArrayUtilities.returnEvens(a);
    for(int i = 0; i < b.length; i++)
    {
      System.out.print(b[i]);
    }
    System.out.println();
    
    ArrayUtilities hello = new ArrayUtilities();
    hello.hello();
    
  }
}