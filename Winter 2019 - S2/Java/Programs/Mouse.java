package programs;
public class Mouse
{
  private String name;
  private int health;
  
  public Mouse(String name, int n)
  {
    setName(name);
    setHealth(n);
  }
  
  public String getName()
  {
    return this.name;
  }
  public int getHealth()
  {
    return this.health;
  }

  public void setName(String name)
  {
    this.name = name;
  }
  public void setHealth(int health)
  {
    this.health = health;
  }
  
  public void loseHealth()
  {
    health = health - 5;
  }
  
  public void squeal()
  {
    System.out.println(name + ": My health is " + health);
    if(health <= 0)
    {
      System.out.println(name + ": I'm dead");
    }
    else if(health < 10)
    {
      System.out.println(name + ": I'm almost dead");
    }
    else
    {
      System.out.println(name + ": Sqqqqqeeeeeak");
    }
  }  
}