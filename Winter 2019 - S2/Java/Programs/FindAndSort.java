public class FindAndSort
{
  public static void main(String[] args)
  {
    int[] arr = {45,5,20,7,10};
    int min = findMin(arr,0);
    
    for(int i = 0; i < arr.length; i++)
    {
      System.out.print(arr[i]);
    }
    
    selectionSort(arr);
  
    System.out.println();
    
    for(int i = 0; i < arr.length; i++)
    {
      System.out.print(arr[i]);
    }
    
  }
  
  public static int findMin(int[] arr, int start)
  {
    int min = start;
    for(int i = start + 1; i < arr.length; i++)
    {
      if(arr[i] < arr[min])
      {
        min = i;
      }
    }
    return min;
  }
  
  public static void selectionSort(int[] arr)
  {
    for(int i = 0; i < arr.length; i++)
    {
      int min = findMin(arr,i);
      
      int temp = arr[i];
      arr[i] = arr[min];
      arr[min] = temp;
    }
  }
}