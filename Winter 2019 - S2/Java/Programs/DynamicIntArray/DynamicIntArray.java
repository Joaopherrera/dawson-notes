import java.util.Arrays;
public class DynamicIntArray
{
  private int[] intArray;
  private int size;
  //Constructors
  public DynamicIntArray()
  {
    intArray = new int[10];
    size = 0;
  }
  //getter
  public int getSize()
  {
    return this.size;
  }
  //public methods
  public void add(int element)
  {
    if(this.size < this.intArray.length)
    {
      intArray[size] = element;
      size++;
    }
    else
    {
      intArray = resizeArray(1);
      intArray[size] = element;
      size++;
    }
  }
  public void removeAtIndex(int index)
  {
    int[] newArray = new int[intArray.length];
    int counter = 0;
    for(int i = 0;  i<intArray.length; i++)
    {
      if(i != index)
      {
        newArray[counter] = intArray[i];
        counter++;
      }
    }
    intArray = newArray;
    this.size--;
  }
  public int removeLast()
  {
    int last = intArray[size-1];
    removeAtIndex(size-1);
    return last;
  }
  public String toString()
  {
    String builder="{ ";
    for(int i = 0; i<size; i++)
      builder= builder + intArray[i]+" ";
    builder+=" ]";
    return builder;
  }
  //private methods
  private int[] resizeArray(int amount)
  {
    int[] newArray = new int[intArray.length+amount];
    for(int i = 0; i<intArray.length; i++)
    {
      newArray[i] = intArray[i];
    }
    return newArray;
  }
}