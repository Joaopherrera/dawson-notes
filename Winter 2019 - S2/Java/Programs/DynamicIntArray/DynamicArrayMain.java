public class DynamicArrayMain
{
  public static void main(String[] args)
  {
    DynamicIntArray foo = new DynamicIntArray();
    System.out.println("Size at the beginning: "+foo.getSize());
    foo.add(4);
    foo.add(5);
    foo.add(6);
    System.out.println("Size after adding elements: "+foo.getSize());

    System.out.println("Contents of array after adding elements: "+foo.toString());
   
    System.out.println("Last element: "+foo.removeLast());  
    System.out.println("Size after removing last element: "+foo.getSize());
    System.out.println("Contents after removing last element: "+foo.toString());
  }
}