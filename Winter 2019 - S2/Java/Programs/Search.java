public class Search
{
  public static void main(String[] args)
  {
    int[] arr = {1,3,5,7,9,12,13,16,24,56};
    
    
  }
  
  public static int binaryS(int[] arr,int target)
  {
    int start = 0;
    int end = arr.length;
    int i = (start + end) / 2;
    while (start <= end) 
    {
      if (arr[i] < target)
      {
        start = i + 1;
      } 
      else if (arr[i] > target)
      {
        end = i - 1; 
      }
      else
      {
        return i;
      }
      i = (start + end) / 2;
    }
    return -1;
  }

  public static int linearS(int[] arr, int target)
  {
    for(int i = 0; i < arr.length; i++)
    {
      if(arr[i] == target)
      {
        return 1;
      }
    }
    return -1;
  }
}