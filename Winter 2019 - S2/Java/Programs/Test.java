import dawson.util.*;

public class Test
{
  public static void main(String[] args)
  {
    int[] arr = {1,4,6,7,7,13,45};
    System.out.println(arr[find(arr,7)]);
  }

  public static int find(int[] arr, int num)
  {
    int start = 0;
    int end = arr.length;
    int i = (start + end) / 2;
    while (start <= end) 
    {
      if (arr[i] < num)
      {
        start = i + 1;
      } 
      else if (arr[i] > num)
      {
        end = i - 1; 
      }
      else
      {
        return i;
      }
      i = (start + end) / 2;
    }
    return -1;
  }

}