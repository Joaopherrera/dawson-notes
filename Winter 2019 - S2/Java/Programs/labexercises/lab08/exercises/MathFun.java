package lab08.exercises;
import java.util.*;
public class MathFun
{
  public static void main(String[] args)
  {
    Scanner reader = new Scanner(System.in);
    
    System.out.println("Enter numerator:");
    int n1 = reader.nextInt();
    System.out.println("Enter denominator:");
    int n2 = reader.nextInt();
    
    double decimal = toDecimal(n1,n2);
  }
  
  public static double toDecimal(int n1, int n2)
  {
    return (n1/n2);
  }

  
}