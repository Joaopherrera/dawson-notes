package labexercises.lab1;
import java.util.Scanner;
public class RectangleApp {
  public static void main(String[] args) {
    Scanner reader = new Scanner(System.in);
    System.out.println("Enter the width");
    int width = reader.nextInt();
    System.out.println("Enter the height");
    int height = reader.nextInt();
    reader.close();
    System.out.println("The perimeter of the rectangle is " + (width + height + width + height) );
  }
  
}