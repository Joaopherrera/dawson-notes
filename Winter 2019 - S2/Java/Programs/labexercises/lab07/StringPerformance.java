public class StringPerformance
{
  public static void main(String[] args)
  {
    String holder;
    for(int i = 10; i < 1048576; i = (i*2))
    {
      String s = concatenateStringWithString(10);
      System.out.println(s);
      System.out.println("0." +(System.nanoTime()/1000000000) + "s");
      
      String s1 = concatenateStringWithBuilder(10);
      System.out.println(s1);
      System.out.println("0." +(System.nanoTime()/1000000000) + "s");
      
      String s2 = concatenateStringWithStringBuilderKnownSize(10);
      System.out.println(s2);
      System.out.println("0." +(System.nanoTime()/1000000000) + "s");
      
      String s3 = prependStrigWithStringBuilder(10);
      System.out.println(s3);
      System.out.println("0." +(System.nanoTime()/1000000000) + "s");
      
      String s4 = replace('3','1',s2);
      System.out.println(s4);
      System.out.println("0." +(System.nanoTime()/1000000000) + "s");
      
      String s5 = replaceWithStringBuilder('6','1',s1);
      System.out.println(s5);
      System.out.println("0." +(System.nanoTime()/1000000000) + "s");
      
      
    }
  }
  
  public static String concatenateStringWithString(int n)
  {
    String s = "";
    for(int i = 0; i < n; i++)
    {
      s = s + "1";
    }
    return s;
  }
  
  public static String concatenateStringWithBuilder(int n)
  {
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < n; i++)
    {
      sb = sb.append("1");
    }
    return sb.toString();
  }
  
  public static String concatenateStringWithStringBuilderKnownSize(int n)
  {
    StringBuilder sb = new StringBuilder(n);
    for(int i = 0; i < n; i++)
    {
      sb = sb.append("1");
    }
    return sb.toString();
  }
  
  public static String prependStrigWithStringBuilder(int n)
  {
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < n; i++)
    {
      sb = sb.insert(0,"1");
    }
    return sb.toString();
  }
  
  public static String replace(char newChar, char oldChar, String s)
  {
    String newS = "";
    for (int i = 0; i < s.length(); i++) {
      if (s.charAt(i) == oldChar) {
        newS = s + newChar;
      }
      else {
        newS = newS + s.charAt(i);
      }
    }
    return newS;
  }
  
  public static String replaceWithStringBuilder(char newChar, char oldChar, String s)
  {
    StringBuilder sb = new StringBuilder(s.length());
    for (int i = 0; i < s.length(); i++) {
      if (s.charAt(i) == oldChar) 
      {
        sb.append(newChar);
      } 
      else 
      {
        sb.append(s.charAt(i));
      } 
    }
    return sb.toString();
  }
}