public class MatrixFunctions
{
  public static void main(String[] args)
  {
    int[][] matrix1 = {{1,2,3},{4,5},{6}};
    int[][] matrix2 = {{2,2},{0,6},{8,9}};
    int[][] matrix3 = {{42,7},{2,4},{1,9}};
    int[][] matrix4 = {{2,3,4},{5,1,6}};
    
    System.out.println(isMatrix(matrix1));
    System.out.println(isMatrix(matrix2));
    
    printMatrix(matrix2);
    System.out.println();
    printMatrix(matrix3);
    System.out.println();
    
    int[][] newMatrix = sum(matrix2, matrix3);
    printMatrix(newMatrix);
    
    System.out.println();
    
    int[][] trMatrix = trans(matrix4);
    printMatrix(trMatrix);
    
  }
  
  //IsMatrix Method 
  public static boolean isMatrix (int[][] matrix)
  {
    for(int i = 0; i < matrix.length - 1; i++)
    {
      if(!(matrix[i].length == matrix[i+1].length))
      {
        return false;
      }      
    }
    return true;
  }
  
  //Sum Method
  public static int[][] sum (int[][] matrix1, int[][] matrix2)
  {
    int[][] newMatrix = new int[3][2];
    if(isMatrix(matrix1) != isMatrix(matrix2))
    {
      throw new IllegalArgumentException("Something went wrong");
    }
    else
    {
      for(int i = 0; i < matrix1.length-1; i++)
      {
        for(int j = 0; j < matrix1.length-1; j++)
        {
          newMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
        }
      }
    }
    return newMatrix;
  }
  
  //Transpose Matrix Method
  public static int[][] trans (int[][] matrix)
  {
    int[][] trMatrix = new int[matrix[0].length][matrix.length];
    for(int i = 0; i < matrix.length-1; i++)
    {
      for(int j = 0; j < matrix[i].length-1; j++)
      {
        trMatrix[i][j] = matrix[j][i];
      }
    }
    return trMatrix;
  }
  //Print 2D Arrays Method
  public static void printMatrix (int[][] array)
  {
    for(int i = 0; i < array.length; i++)
    {
      for(int j =0; j < array[i].length; j++)
      {
        System.out.print(array[i][j]);
        System.out.print(" , ");
      }
    }
  }
}