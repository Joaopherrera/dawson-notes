package labexercises.lab02;
public class ArrayUtilities
{
  public static int countEvens (int[] a)
  {
    int e = 0;
    for(int i = 0; i < a.length; i++)
    {
      if(a[i] % 2 == 0)
      {
        e++;
      }
    }
    return e;
  }
  
  public static int[] returnEvens (int[] a)
  {
    int e = ArrayUtilities.countEvens(a);
    int[] b = new int[e];
    int evens = 0;
    
    for(int i = 0; i < a.length; i++)
    {
      if(a[i] % 2 == 0)
      {
        b[evens] = a[i];
        evens++;
      }
    }
    return b;
  }

  public void hello()
  {
    System.out.println("Hello World");
  }
}