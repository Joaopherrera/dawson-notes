import java.util.*;
import java.io.*;
import java.math.*;

public class GradeCalculator
{
  public static void main(String[] args)
  {
    //Scanner reader = new Scanner(new File("C:\\Users\\Joao Pedro\\OneDrive - Dawson College\\Courses\\Winter 2019\\Java\\Programs\\labexercises\\lab09\\Input.txt"));
    calculateAverage("C:\\Users\\Joao Pedro\\OneDrive - Dawson College\\Courses\\Winter 2019\\Java\\Programs\\labexercises\\lab09\\Input.txt");
  }
  
  public static void calculateAverage(String filename)
  {
    try
    {
      Scanner reader = new Scanner(new File("C:\\Users\\joaop\\OneDrive - Dawson College\\Courses\\Winter 2019\\Java\\Programs\\labexercises\\lab09\\Input.txt"));
      
      String line1 = reader.nextLine();
      String line2 = reader.nextLine();
      
      String[] strline1 = line1.split(" ");
      String[] strline2 = line2.split(" ");
      
      int grade1 = Integer.parseInt(strline1[1]);
      int grade2 = Integer.parseInt(strline1[2]);
      int grade3 = Integer.parseInt(strline1[3]);
      
      int grade11 = Integer.parseInt(strline2[1]);
      int grade21 = Integer.parseInt(strline2[2]);
      int grade31 = Integer.parseInt(strline2[3]);
      
      double average1 = (((double)grade1 + (double)grade2 + (double)grade3)/3);
      double average2 = (((double)grade11 + (double)grade21 + (double)grade31)/3);
      
      if(average1 >= 60)
      {
        System.out.println(strline1[0] + " " + average1 + " Pass!");
      }
      else
      {
        System.out.println(strline1[0] + " " + average1 + " Fail!");
      }
      
      if(average2 >= 60)
      {
        System.out.println(strline2[0] + " " + average2 + " Pass!");
      }
      else
      {
        System.out.println(strline2[0] + " " + average2 + " Fail!");
      }
    }
    catch(FileNotFoundException e)
    {
      System.out.println("File not found.");
    }
  }
}