package Programs;
public class Car
{
  private String model;
  private int location;
  private int currentSpeed;
  private boolean movingForward;
  private int maxSpeed;
  
  //GET METHODS//
  public String getModel()
  {
    return this.model;
  }
  
  public boolean getDirection()
  {
    return this.movingForward;
  }
  
  public int getLocation()
  {
    return this.location;
  }
  
  //OTHER METHODS//
  public void go()
  {
    this.currentSpeed = maxSpeed;
  }
  
  public void stop()
  {
    this.currentSpeed = 0;
  }
  
  public void turnAround()
  {
    this.movingForward = !(movingForward);
  }
  
  public void toContinue()
  {
    if(movingForward)
    {
      this.location = location + currentSpeed;
    }
    else
    {
      this.location = location - currentSpeed;
    }
  }
  
  public void display()
  {
    if (getDirection())
    {
      System.out.println(model + " Located at " + location + ", facing forward and it's moving at " + currentSpeed + " speed"); 
    }
    else
    {
      System.out.println(model + " Located at " + location + ", facing backward and it's moving at " + currentSpeed + " speed");
    }
  }
  
  public Car(String model, int maxSpeed, int location)
  {
    this.model = model;
    this.maxSpeed = maxSpeed;
    this.location = location;
    this.movingForward = true;
    this.currentSpeed = 0;
    
  }
}