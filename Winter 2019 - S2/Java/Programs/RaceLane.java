import java.util.*;
import Programs.Car;
public class RaceLane
{
  public static void main(String[] args)
  {
    Scanner reader = new Scanner(System.in);
    
    System.out.println("Enter a car model:");
    String modelC1 = reader.nextLine();
    System.out.println("Enter an initial Location:");
    int locationC1 = reader.nextInt();
    System.out.println("Enter the max Speed:");
    int maxSC1 = reader.nextInt();
    
    System.out.println("Enter a car model:");
    String modelC2 = reader.next();
    System.out.println("Enter an initial Location:");
    int locationC2 = reader.nextInt();
    System.out.println("Enter the max Speed:");
    int maxSC2 = reader.nextInt();
    
    Car c1 = new Car(modelC1, maxSC1, locationC1);
    Car c2 = new Car(modelC2, maxSC2, locationC2);
    
    c1.display();
    c2.display();
    
    c2.turnAround();
    
    c1.display();
    c2.display();
    
    c1.go();
    c2.go();
    
    while(c1.getLocation() < c2.getLocation())
    {
      c1.toContinue();
      c2.toContinue();
      c1.display();
      c2.display();
    }
    System.out.println("BOOM!!!");
  }
}