package dawson.gofish;
import dawson.util.*;
import java.util.*;
import java.lang.*;

/**
 * GoFishGame is used to create a game object
 * 
 * @author Joao Pedro Morais Herrera
 * @version 04/30/2019
 */

public class GoFishGame
{
  private Deck deck;
  private Player[] players;
  
  Scanner reader = new Scanner(System.in);
  
  /**
   * Constructor to create the gofish game
   * 
   * 
   *
   * */
  public GoFishGame()
  {
    System.out.println("Enter Number Of Players:");
    int num = reader.nextInt();
    players = new Player[num]; //Creating the player array with the input as the length
    
    for(int i = 0; i < num; i++)
    {
      System.out.println("Enter Name For Player " + (i+1) + ":");
      String name = reader.next();
      players[i] = new Player(name);//Creating each player object with the input name
    }
    deck = new Deck();
    deck.shuffleCards();
    
    for(Player p: players)
    {
      System.out.println(p.getName() + "'s hand.");
      while(p.getSize() <= 6)
      {
        drawCard(p);
      }
    }
  }
  
  /**
   * drawCard method to draw a card to the player's hand
   * 
   * @param player Player 
   * @return boolean 
   *
   * */
  //Draw a card from the deck
  public boolean drawCard(Player player)
  {
    PlayingCard card = deck.deal();
    
    if(player.match(card))
    {
      System.out.println("Match! Value: " + card.getNumber());
      player.addPoint();
      return true;
    }
    System.out.println("Card Drawn: " + card.getNumber());
    player.addCard(card);
    return false;
  }
  
  /**
   * requestCard method to request a card from another player
   * 
   * @param player Player 
   * @return boolean 
   *
   * */
  //Request Card from another player
  public boolean requestCard(Player player) throws CheatingPlayerException 
  {
    if(player.getSize() == 0)
    {
      drawCard(player);
      return false;
    }
    
    System.out.println("Enter player number to ask for a card:");
    int playerNum = reader.nextInt();
    
    System.out.println("Enter card number for request: ");
    int cardNum = reader.nextInt();
    
    PlayingCard temp = new PlayingCard(cardNum);
    
    if(!(player.match(temp))) //Condition to see if the player is requesting a card that he/she has
    {
      throw new CheatingPlayerException("Player is Cheating");
    }
    else if(player == players[playerNum]) //Condition to see if the player is calling the request on himslef
    {
      throw new CheatingPlayerException("Player is Cheating");
    }
    
    if(players[playerNum].match(temp))
    {
      System.out.println("Match!");
      player.addPoint();
      player.printCards();
      return true;
    }
    else
    {
      System.out.println("No Match!");
      player.addCard(temp);
      return false;
    }
  }
  
  /**
   * doTurn method to do the turn dor each player
   * 
   * @param player Player  
   *
   * */
  //DoTurns for the input player
  public void doTurn(Player player) throws CheatingPlayerException
  {
    System.out.println(""); //Blank Line
    
    System.out.println(player.getName() + "'s turn.");
    
    player.printCards();
    
    System.out.println(""); //Blank Line
    
    int flag = 0;
    
    while(flag == 0)
    {
      boolean request;
      do
      {
        System.out.println("Points: " + player.getPoints());
        request = requestCard(player);
      }
      while(request);
      
      player.printCards();
      System.out.println("Points: " + player.getPoints());
      boolean draw = drawCard(player);
      if(!(draw))
      {
        flag = 1;
      }
    }
  }
  
  /**
   * 
   * runGame method to run the game itself
   * 
   *
   *
   * */
  //Run the game itself
  public void runGame() throws CheatingPlayerException
  {
    int flag = 0;
    while(deck.getSize() != 0 && flag != 1)
    {
      for(Player p: players)
      {
        try
        {
          doTurn(p);
        }
        catch(CheatingPlayerException e)
        {
          flag = 1;
          System.out.println(p.getName() + " is a cheater! Everyone else wins!!!");
          break;
        }
      }
    }
    
    if(flag != 1)
    {
      int max = 0;
      for(int i = 0; i < players.length-1; i++)
      {
        if(players[i].getPoints() > players[max].getPoints())
        {
          max = i;
        }
      }
      
      for(int i = 0; i < players.length; i++)
      {
        if(players[i].getPoints() == players[max].getPoints())
        {
          System.out.println("Congratulations " + players[i].getName() + " with " + players[i].getPoints() + " Points.");
        }
      }
      System.out.println("End of the game.");
    } 
  }
}