package dawson.concentration;

public class Player
{
  private String name;
  private int points;
 
  //Constructor
  public Player(String name)
  {
    this.name = name;
    points = 0;
  }
  
  //Getters
  public int getPoints()
  {
    return this.points;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  //Method to add points to the player
  public void addPoint()
  {
    points++;
  }
}