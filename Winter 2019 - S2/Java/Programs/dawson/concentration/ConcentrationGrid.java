package dawson.concentration;
import dawson.util.*;
import dawson.concentration.*;

public class ConcentrationGrid
{
  private PlayingCard[][] grid;
  
  //Constructor
  public ConcentrationGrid(Deck deck)
  {
    grid = new PlayingCard[7][8];
    for(int i = 0; i <= 6; i++)
    {
      for(int j = 0; j <= 7; j++)
      {
        if((i == 0 && j == 0) || (i == 0 && j == 7) || (i == 6 && j == 0) || (i == 6 && j == 7))
        {
          grid[i][j] = null;
        }
        else
        {
          grid[i][j] = deck.deal();
        }
      }
    }
  }
  
  //Print Grid Method to print the start grid/state of the grid
  public void printGrid()
  {
    for(int i = 0; i <= 6; i++)
    {
      for(int j = 0; j <= 7; j++)
      {
        if((i == 0 && j == 0) || (i == 0 && j == 7) || (i == 6 && j == 0) || (i == 6 && j == 7))
        {
          System.out.print("_");
        }
        else if(grid[i][j] == null)
        {
          System.out.print("X");
        }
        else
        {
          System.out.print("#");
        }
      }
      System.out.println();
    }
  }
  
  //Method to print the cards the player choose
  public void printMatch(int x1, int y1, int x2, int y2)
  {
    for(int i = 0; i <= 6; i++)
    {
      for(int j = 0; j <= 7; j++)
      {
        if((i == 0 && j == 0) || (i == 0 && j == 7) || (i == 6 && j == 0) || (i == 6 && j == 7))
        {
          System.out.print("_");
        }
        else if((i == x1 && j == y1))
        {
          System.out.print(grid[x1][y1].getNumber());

        }
        else if((i == x2 && j == y2))
        {
          System.out.print(grid[x2][y2].getNumber());
        }
        else
        {
          System.out.print("#");
        }
      }
      System.out.println();
    }
  }
  
  //Match Method to check if the cards are the same 
  public boolean match(int x1, int y1, int x2, int y2)
  {
    if(x1 == x2 && y1 == y2)
    {
      throw new IllegalArgumentException("The input is invalid");
    }
    else if(grid[x1][y1] == null || grid[x2][y2] == null)
    {
      throw new IllegalArgumentException("The input is invalid");
    }
    
    if(grid[x1][y1].getNumber() == grid[x2][y2].getNumber())
    {
      grid[x1][y1] = null;
      grid[x2][y2] = null;
      return true;
    }
    return false;
  }
}