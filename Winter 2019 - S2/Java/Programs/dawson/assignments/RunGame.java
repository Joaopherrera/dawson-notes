package dawson.assignments;
import dawson.util.*;

/**
 * RunGame is used just to run the other classes and run the program
 * 
 * @author Joao Pedro Morais Herrera
 * @version 02/19/2019
 */

public class RunGame
{
  public static void main(String[] args)
  {
    Deck deck = new Deck();
    deck.shuffle();
    deck.printCards();
  }
}