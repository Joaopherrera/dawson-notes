package dawson.assignments;
import dawson.util.*;
import dawson.gofish.*;

/**
 * GoFishMain is used just to run the other classes and run the program
 * 
 * @author Joao Pedro Morais Herrera
 * @version 04/30/2019
 */


public class GoFishMain
{
  public static void main(String[] args) throws CheatingPlayerException
  {
    GoFishGame game = new GoFishGame(); //Just create the game object
    
    game.runGame();
  }
}
