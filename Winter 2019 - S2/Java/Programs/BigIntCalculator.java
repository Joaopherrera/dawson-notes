import java.math.*;

public class BigIntCalculator
{
  public static void main(String[] args)
  {
    yPowersOfTen(17);
    for(int i = 10; i <= 100; i = i+10)
    {
      fibonnaci(i);
    }
  }
  
  public static void yPowersOfTen(int n)
  {
    BigInteger bInt = new BigInteger("1");
    for(int i = 0; i < n; i++)
    {
      System.out.println(bInt);
      bInt = bInt.multiply(BigInteger.TEN);
    }
  }
  
  public static void fibonnaci(int n)
  {
    BigInteger[] arr = new BigInteger[n];
    
    arr[0] = BigInteger.ONE;
    arr[1] = BigInteger.ONE;
    
    for(int i = 2; i < n; i++)
    {
      arr[i] = arr[i-2].add(arr[i-1]);
    } 
    System.out.println(arr[n-1]);
  }
}