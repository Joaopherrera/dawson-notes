import programs.*;
import java.util.*;
public class AnimalFarm
{
  public static void main (String[] args)
  {
    Scanner reader = new Scanner(System.in);
    
    System.out.println("Please enter the name of your mouse: ");
    String mouseName = reader.next();
    System.out.println("Please enter the health of your mouse: ");
    int mouseHealth = reader.nextInt();
    
    System.out.println("Please enter the name of your dog: ");
    String dogName = reader.next();
    System.out.println("Please enter the age of yout dog: ");
    int dogAge = reader.nextInt();
    
    Mouse m = new Mouse(mouseName, mouseHealth);
    m.squeal();

    
    Dog d = new Dog(dogName, dogAge);
    d.bark();
 
  
    while(m.getHealth() > 0)
    {
      d.attack(m);
      m.squeal();
    }    
  }
}