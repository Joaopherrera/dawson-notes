public class IceCreamShop{
 IceCream[] iceCreams;
 
 public static void main(String[] args){
    IceCream i1 = new IceCream(Size.SMALL, Flavour.VANILLA, Topping.COOKIE);
    IceCream i2 = new IceCream(Size.SMALL, Flavour.VANILLA, Topping.COOKIE);
    IceCream i3 = new IceCream(Size.SMALL, Flavour.VANILLA);
    IceCream i4 = new IceCream(Size.MEDIUM, Flavour.VANILLA, Topping.COOKIE);
    IceCream i5 = new IceCream(Size.SMALL, Flavour.CHOCOLATE, Topping.COOKIE);
    IceCream i6 = new IceCream(Size.SMALL, Flavour.VANILLA);
    IceCream[] ic = {i1,i2,i3,i4,i5,i6};
    IceCreamShop ics = new IceCreamShop(ic);
    System.out.println(ics.countDifferentIceCreams());
    
 }
 
 public IceCreamShop(IceCream[] iceCreams){
  this.iceCreams = iceCreams; 
 }
 
 public int countDifferentIceCreams(){
   int count = 0;
   for(int i = 0; i<iceCreams.length; i++){
     int j = 0;
     for(j = 0; j<i; j++){
       if(iceCreams[i].equals(iceCreams[j])){
         break;
       }
     }
     if(j == i){
       count++;
     }
   }
     return count;
     
   }
   
 }
  
  
