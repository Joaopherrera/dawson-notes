public class IceCream{
 private Size size;
 private Flavour flavour;
 private Topping topping;
 
 public IceCream(Size size, Flavour flavour, Topping topping){
  this.size = size;
  this.flavour = flavour;
  this.topping = topping;   
 }
 
 public IceCream(Size size, Flavour flavour){
   this.size = size;
   this.flavour = flavour;
   this.topping = null;   
 
 }
 
 public Size getSize(){
  return this.size; 
 }
 public Flavour getFlavour(){
   return this.flavour;
 }
 public Topping getTopping(){
  return this.topping; 
 }
 
 public boolean equals(Object o){
   if(!(o instanceof  IceCream)){
     return false;
   }
   IceCream i = (IceCream) o;
   
//You can equate the two IceCream objects by converting them to string and calling the .equalson the 2 strings also
  // String thisIceCream = this.toString();
  // String otherIceCream = o.toString();
  // return thisIceCream.equals(otherIceCream);
   
   //Or you can equate each property
   if(this.size == i.size && this.flavour == i.flavour && this.topping == i.topping){
     return true;
   }
   return false;
   
 }
 
 public String toString(){
   String sizeStr = "";
   String flavourStr = "";
   String toppingStr = "";
   switch(this.size){
     case SMALL:
       sizeStr = "Small"; break;
     case MEDIUM:
       sizeStr = "Medium"; break;
     case LARGE:
       sizeStr = "Large"; break;
   }
   switch(this.flavour){
     case CHOCOLATE:
       flavourStr = "Chocolate"; break;
     case VANILLA:
       flavourStr = "Vanilla"; break;
     case STRAWBERRY:
       flavourStr = "Strawberry"; break;
   }
   if(this.topping == null){
    return sizeStr + " " + flavourStr + " Ice Cream"; 
   }else{
     switch(this.topping){
       case COOKIE:
         toppingStr = "Cookie"; break;
       case SPRINKLE:
         toppingStr = "Sprinkle"; break;
       case PEANUT:
         toppingStr = "Peanut"; break;
     }
   }
   
   return sizeStr + " " + flavourStr + " Ice Cream with " + toppingStr + " topping"; 
 }
   
  
}