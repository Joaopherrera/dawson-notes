public class IceCreamTests{
  public static void main(String[] args){
    testConstructor();
    testAltConstructor();
    testToString();
    testEquals();
   
  }
  
  public static void testEquals(){
    System.out.println("Testing equals...");
    IceCream i1 = new IceCream(Size.SMALL, Flavour.VANILLA, Topping.COOKIE);
    IceCream i2 = new IceCream(Size.SMALL, Flavour.VANILLA, Topping.COOKIE);
    IceCream i3 = new IceCream(Size.SMALL, Flavour.VANILLA);
    if(i1.equals(i2) && !i1.equals(i3) && !i1.equals("hi")){
     System.out.println("PASSED!"); 
    }else{
     System.out.println("FAILED!"); 
    }
  }
  
  public static void testConstructor(){
    System.out.println("Testing constructor and getters..."); 
   //print PASSED! if constructor works, print FAILED! otherwise.
    IceCream testCream = new IceCream(Size.SMALL, Flavour.VANILLA, Topping.COOKIE);
    if(!(testCream.getSize() == Size.SMALL)){
      System.out.println("FAILED!");
    }else if(!(testCream.getFlavour() == Flavour.VANILLA)){
      System.out.println("FAILED!");
    }else if(!(testCream.getTopping() == Topping.COOKIE)){
      System.out.println("FAILED!");
    }else{
      System.out.println("PASSED!");
    }
  }
  
    public static void testAltConstructor(){
      System.out.println("Testing alternative constructor..."); 
   //print PASSED! if constructor works, print FAILED! otherwise.
    IceCream testCream = new IceCream(Size.SMALL, Flavour.VANILLA);
    if(!(testCream.getSize() == Size.SMALL)){
      System.out.println("FAILED!");
    }else if(!(testCream.getFlavour() == Flavour.VANILLA)){
      System.out.println("FAILED!");
    }else if(!(testCream.getTopping() == null)){
      System.out.println("FAILED!");
    }else{
      System.out.println("PASSED!");
    }
  }
    
    public static void testToString(){
      String expect = "Small Vanilla Ice Cream with Cookie topping"; 
      IceCream testCream = new IceCream(Size.SMALL, Flavour.VANILLA, Topping.COOKIE);
      String expect2 = "Small Vanilla Ice Cream"; 
      IceCream testCream2 = new IceCream(Size.SMALL, Flavour.VANILLA);
    
      String s = testCream.toString();
      String s2 = testCream2.toString();
       if(!s.equals(expect)){
         System.out.println("FAILED!");
       }else if (!s2.equals(expect2)){
         System.out.println("FAILED!");
       }else{
         System.out.println("PASSED!"); 
       }
   
      
    }
  
}