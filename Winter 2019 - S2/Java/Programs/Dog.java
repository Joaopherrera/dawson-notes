package programs;
public class Dog
{
  private String name;
  private int age;
  
  public Dog(String name, int age)
  {
    setName(name);
    setAge(age);
  }
  
  public String getName()
  {
    return this.name;
  }
  public int getAge()
  {
    return this.age;
  }

  public void setName(String name)
  {
    this.name = name;
  }
  public void setAge(int age)
  {
    this.age = age;
  }

  public void attack(Mouse m)
  {
    System.out.println(name + ": Grrrrr");
    m.loseHealth();
  }
  
  public void bark()
  {
    System.out.println("Ruf Ruf, My name is " + name + "!"
                      + " Ruff, I'm " + age + " years old. " + " in human years is " + age*7 + " years old.");
  }
}

