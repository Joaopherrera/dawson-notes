package dawson.concentration;
import dawson.concentration.*;
import dawson.util.*;
import java.util.*;

//Class to create the game itself 
public class ConcentrationGame 
{
  private ConcentrationGrid grid;
  private Player[] players;
  Scanner reader = new Scanner(System.in);
  
  //Contructor to create the object for the game
  public ConcentrationGame()
  {
    System.out.println("Enter number of players:"); //Creates the array for players
    int n = reader.nextInt();
    players = new Player[n];
    
    for(int i = 0; i < n; i++)
    {
      System.out.println("Enter name player " + (i+1) + ": ");
      String name = reader.next();
      Player player = new Player(name);
      players[i] = player;
    }
    
    Deck deck = new Deck(); //Creates the deck
    deck.shuffle();
    
    grid = new ConcentrationGrid(deck); // Initialize the grid with the deck as parameter
  }
  
  //Method to DoTurns
  public void doTurns(Player p) throws InterruptedException
  {
    System.out.println("Welcome to your turn, " + p.getName());
    
    grid.printGrid();
    
    //Inputs for the locations on the grid
    System.out.println("Enter first card location(x1):");
    int x1 = reader.nextInt();
    System.out.println("Enter first card location(y1):");
    int y1 = reader.nextInt();
    System.out.println("Enter second card location(x2):");
    int x2 = reader.nextInt();
    System.out.println("Enter second card location(y2):");
    int y2 = reader.nextInt();
    
    grid.printMatch(x1,y1,x2,y2);
    
    if(grid.match(x1,y1,x2,y2))
    {
      p.addPoint();
      System.out.println("Match!");
      Thread.sleep(3000);
    }
    else
    {
      System.out.println("No Match!");
    }
  }
  
  //RunGame Method
  public void runGame() throws InterruptedException
  {
    int points = 0; //Variable to check the condition 
    while(points != 5)
    {
      for(Player p: players)
      {
        doTurns(p);
        if( p.getPoints() == 5)
        {
          points = 5;
          System.out.println("Congratulations " + p.getName() + ", you won!!!");
          break;
        }
      }
    }
  }
}