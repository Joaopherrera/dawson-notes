package dawson.assignments;
import dawson.concentration.*;

public class ConcentrationMain
{
  public static void main (String[] args) throws InterruptedException
  {
    //Create the object game and call the runGame to run the game
    ConcentrationGame game = new ConcentrationGame();
    game.runGame();
  }
}