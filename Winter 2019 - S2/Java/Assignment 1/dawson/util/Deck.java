package dawson.util;
import java.util.*;

/**
 * Class Deck is used to create an array of objetc of type Playing Cards and it has methods to shuffle and print
 * 
 * @author Joao Pedro Morais Herrera
 * @version 02/19/2019
 */

public class Deck
{
  private PlayingCard[] deck;
  /**
   * Constructor for the Playing Card Array
   * 
   * */
  public Deck()
  {
    deck = new PlayingCard[52];
    
    String[] suits = new String[] {"hearts", "spades", "clubs", "diamonds"};
    int position = 0;
    
    for(int suit = 0; suit <= 3; suit++)
    {
      for(int number = 1; number <= 13; number++)
      {
        deck[position] = new PlayingCard(suits[suit],number);
        position++;
      }
    }
  }
  
  /**
   * Shuflle the Cards Array 
   * 
   * 
   *
   * */
  
  public void shuffle()
  {
    PlayingCard swap;
    Random random = new Random();
    int n;
    for(int i = 0; i <51; i++)
    {
      n = random.nextInt(52);
      swap = deck[n];
      deck[i] = swap;
      deck[n] = deck[i];
    }
  }
  
   /**
   * Print the Cards Array 
   * 
   * 
   *
   * */
  public void printCards()
  {
    for(int i = 0; i <= 51; i++)
    {
      deck[i].print();
    }
  }
}