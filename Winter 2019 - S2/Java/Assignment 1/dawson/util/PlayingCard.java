package dawson.util;

/**
 * Playing Card is used to create a Card object that has a String Suit and an int Number
 * 
 * @author Joao Pedro Morais Herrera
 * @version 02/19/2019
 */

public class PlayingCard
{
  private String suit;
  private int number;
  

  /**
   * Constructor to create the card object 
   * 
   * @param suit String for the suit for the card
   * @param number int for the number of the card 
   *
   * */
  
  public PlayingCard(String suit, int number)
  {
    if((number > 13 && number < 0) || ( suit.equals("hearts") && suit.equals("spades") && suit.equals("clubs") && suit.equals("diamonds")))
    {
      throw new IllegalArgumentException("The input is invalid");
    }
    else
    {
      this.suit = suit;
      this.number = number;
    }
  }
  
   /**
   * Get method to get valeus (suit and number) outside of the class
   * 
   * 
   * @return The suit for the card
   *
   * */
  public String getSuit()
  {
    return this.suit;
  }
  public int getNumber()
  {
    return this.number;
  }

  
   /**
   * Print method to print card
   * 
   * 
   *
   **/
  public void print()
  {
    System.out.println( number + " of " + suit);
  }
  
}