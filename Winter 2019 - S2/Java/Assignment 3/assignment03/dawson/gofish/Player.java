package dawson.gofish;
import dawson.util.*;

/**
 * Player is used to create a player object that has a String name.
 * 
 * @author Joao Pedro Morais Herrera
 * @version 04/30/2019
 */

public class Player
{
  private String name;
  private int points;
  private CardList hand;
  
  /**
   * Constructor to create the card object 
   * 
   * @param name String for the name for the player
   *
   * */
  public Player(String name)
  {
    this.name = name;
    this.points = 0;
    hand = new CardList();
  }
  
  //--Getters--// 
  /**
   * Get method to get valeus (name and points) outside of the class
   * 
   * 
   * @return The player's name, points and hand size.
   *
   * */
  //Getter for Name
  public String getName()
  {
    return this.name;
  }
  //Getter for points
  public int getPoints()
  {
    return this.points;
  }
  //Getter for player's hand size
  public int getSize()
  {
    return hand.getSize();
  }
  
  //--Adders--//
  /**
   * Add method to add valeus/objects(points and cards) outside of the class
   * 
   * 
   *
   * */
  //Add for points 
  public void addPoint()
  {
    this.points = this.points + 1;
  }
  //Add cards to the palyer's hand
  public void addCard(PlayingCard card)
  {
    hand.add(card);
  }
  
  /**
   * match method to match if the card is in the player's hand
   * 
   * 
   * @return boolean 
   *
   * */
  //Boolean for match if the card is in the player's hand
  public boolean match(PlayingCard card)
  {
    if(this.hand.find(card) != -1)
    {
      hand.removeAtIndex(hand.find(card));
      return true;
    }
    return false;
  }
  
  /**
   * Print method to print the player's hand
   * 
   *  
   *
   * */
  //Print the player's hand
  public void printCards()
  {
    hand.print();
  }
}