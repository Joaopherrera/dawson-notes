package dawson.util;
import dawson.util.*;
import java.util.*;

/**
 * Class CardList is used to create an array of cards of type Playing Cards
 * 
 * @author Joao Pedro Morais Herrera
 * @version 04/30/2019
 */

public class CardList
{
  private PlayingCard[] cards;
  private int size;
  
    /**
   * Constructor for the CardList object
   * 
   * */
  //Constructor
  public CardList()
  {
    cards = new PlayingCard[10];
    size = 0;
  }
  
   /**
   * resize method to resize the player hand when it's full
   * 
   * 
   * */
  //Resize the hand
  public void resize()
  {
    PlayingCard[] arr = new PlayingCard[size*2];
    for(int i = 0; i < size; i++)
    {
      arr[i] = cards[i];
    }
    cards = arr;
  }
  
   /**
   * add method to add cards to the player hand
   * 
   * @param card PlayingCard
   * 
   * */
  //Add cards to the hand
  public void add(PlayingCard card)
  {
    if(size == cards.length)
    {
      resize();
    }
    cards[size]= card;
    this.size++;
  }
  
     /**
   * getAtIndex method get the card at the given index 
   * 
   * @param i int for the position
   * @return PlayingCard
   * 
   * */
  //Get the card at the given index
  public PlayingCard getAtIndex(int i)
  {
    return cards[i];
  }
  
   /**
   * remove method to remove a the last card  
   * 
   * @return PlayingCard
   * 
   * */
  //Remove last card from the hand
  public PlayingCard remove()
  {
    PlayingCard card  = cards[size-1];
    size--;
    return card;
  }
  
       /**
   * removeAtIndex method remove the card at the given index 
   * 
   * @param i int for the position
   * @return PlayingCard
   * 
   * */
  //Remove card from the given index
  public PlayingCard removeAtIndex(int i)
  {
    PlayingCard card = cards[i];
    for(int j = i; j < size-1; j++)
    {
      cards[j] = cards[j+1];
    }
    size--;
    return card;
  }
  
       /**
   * find method to find the input card
   * 
   * @param card PlayingCard 
   * @return int 
   * 
   * */
  //Find the given card in the hand
  public int find(PlayingCard card)
  {
    sort();
    
    int start = 0;
    int end = this.size-1;
    int i = (start + end) / 2;
    while (start <= end) 
    {
      if (cards[i].compareValue(card) == -1)
      {
        start = i + 1;
      } 
      else if (cards[i].compareValue(card) == 1)
      {
        end = i - 1; 
      }
      else
      {
        return i;
      }
      i = (start + end) / 2;
    }
    return -1;
  }
  
   /**
   * 
   * sort method to sort the player's hand using selection sort 
   * 
   * 
   * */
  //Sort the hand using selection sort
  public void sort()
  {
    for(int i = 0; i < size; i++)
    {
      int min = findMin(cards,i);
      
      PlayingCard temp = cards[i];
      cards[i] = cards[min];
      cards[min] = temp;
    }
  }
  
   /**
   * findMin method to find the minimun value in the array
   * 
   * @param arr PlayingCard[], int i
   * @return int
   * 
   * */
  //(HELPER METHOD FOR THE SORT METHOD)
  public int findMin(PlayingCard[] arr, int start)
  {
    int min = start;
    for(int i = start + 1; i < this.size; i++)
    {
      if(arr[i].getNumber() < arr[min].getNumber())
      {
        min = i;
      }
    }
    return min;
  }
  
   /**
   * 
   * shuffle method to shuffle the cards
   * 
   * 
   * */
  //Shuffle the hand 
  public void shuffle()
  {
    PlayingCard swap;
    Random random = new Random();
    int n;
    for(int i = 0; i <size; i++)
    {
      n = random.nextInt(size);
      swap = cards[n];
      cards[n] = cards[i];
      cards[i] = swap;
    }
  }
  
   /**
   * 
   * getSize method to get hand size 
   * 
   * @return int
   * 
   * */
  //Helper Getter for the size
  public int getSize()
  {
    return this.size;
  }
  
   /**
   * 
   * print method to print the player's hand 
   * 
   * 
   * */
  //Print method 
  public void print()
  {
    for(int i = 0; i < size; i++)
    {
      System.out.print(cards[i].getNumber() + " ");
    }
    System.out.println(" ");
  }
}