package dawson.util;
import java.util.*;

/**
 * Class Deck is used to create an array of objetc of type Playing Cards and it has methods to shuffle and print
 * 
 * @author Joao Pedro Morais Herrera
 * @version 04/30/2019
 */

public class Deck
{
  private CardList deck;
 
  /**
   * Constructor for the Playing Card Array
   * 
   * */
  public Deck()
  {
    deck = new CardList();
    
    String[] suits = new String[] {"hearts", "spades", "clubs", "diamonds"};
    int position = 0;
    
    for(int suit = 0; suit <= 3; suit++)
    {
      for(int number = 1; number <= 13; number++)
      {
        deck.add(new PlayingCard(suits[suit],number));
        position++;
      }
    }
  }
  
  /**
   * Shuflle the Cards Array 
   * 
   * 
   *
   * */
  
  public void shuffleCards()
  {
    deck.shuffle();    
  }
  
   /**
   * Print the Cards Array 
   * 
   * 
   *
   * */
  public void printCards()
  {
    for(int i = 0; i <= 51; i++)
    {
      deck.print();
    }
  }

  //Deal cards method
  public PlayingCard deal()
  {
    PlayingCard lastCard = deck.remove();
    return lastCard;
  }
  
  //Getter to return size
  public int getSize()
  {
    return deck.getSize();
  }
}