DROP TABLE Pet;
DROP TABLE PetOwner;

/* Create a pet table which contains information about pets. It should contain a pet’s
name (up to 30 characters), animal type, and a Boolean value ‘owned’ indicating whether
it has an owner or not, and a pet_id. */
-- Animal Table --
CREATE TABLE Pet(
    name varchar2(30) NOT NULL,
    type varchar2(20) NOT NULL,
    owned varchar2(1),
    pet_id varchar2(10) NOT NULL UNIQUE,
    cost number(6,2)
);

-- Add a default to the Boolean ‘owned’ value so that it defaults to false. --
ALTER TABLE Pet MODIFY owned DEFAULT '0';

-- Insert 3 pets into your table --
INSERT INTO Pet VALUES ('Bob', 'dog', '1', '0001');
INSERT INTO Pet VALUES ('Max', 'cat', '0', '0002');
INSERT INTO Pet VALUES ('Baw', 'cat', '0', '0003');

-- Add a cost column to the pet table to indicate how much the pet costs --
ALTER TABLE Pet ADD (cost number(6,2));

-- Modify the name column to allow names up to 100 characters in length --
ALTER TABLE Pet MODIFY (name varchar2(100));

-- Remove the Boolean ‘owned’ column --
ALTER TABLE Pet DROP COLUMN owned;

-- Insert 3 pets in to your new table --
INSERT INTO Pet VALUES ('Chat', 'dog', '0004', 50000);
INSERT INTO Pet VALUES ('Banana', 'parrot', '0005', 400);
INSERT INTO Pet VALUES ('Mikey', 'mouse', '0006', 10000);

-- Update all records with no cost to cost $50. --
UPDATE Pet SET cost = 50 WHERE cost IS NULL;

-- Delete all records for dogs. --
DELETE FROM Pet WHERE type = 'dog';

-- Create a petowner table which contains information about petowners. --
-- It should contain a petowner’s name (up to 30 characters) and the pet_id of the pet they own. --
CREATE TABLE PetOwner(
    name varchar2(30),
    pet_id varchar2(10)
);

-- Insert 3 new owners and 3 new pets. --
INSERT INTO PetOwner VALUES ('James', '0005');
INSERT INTO PetOwner VALUES ('Big Boy', '0002');
INSERT INTO PetOwner VALUES ('Wesley', '0003');

INSERT INTO Pet VALUES ('Tarruf', 'rat', '0007', 700);
INSERT INTO Pet VALUES ('Mafmuf', 'bear', '0008', 1000);
INSERT INTO Pet VALUES ('Miau', 'mouse', '0009', 50);

-- Update all pets so that they now have an owner --
ALTER TABLE Pet ADD (owner_id varchar2(10));
ALTER TABLE PetOwner ADD (owner_id varchar2(10));

-- Write a query that shows how much each person has spent on pets. --
SELECT PetOwner.name, SUM(Pet.cost) FROM PetOwner
INNER JOIN Pet ON PetOwner.pet_id = Pet.pet_id
GROUP BY PetOwner.name;

-- Write a query that finds the name of the pets owned by the person who owns the most pets --
SELECT p.name FROM Pet p
INNER JOIN PetOwner po ON p.pet_id = po.pet_id
WHERE po.name IN (SELECT po.name FROM Pet p
INNER JOIN PetOwner po ON p.pet_id = po.pet_id
GROUP BY po.name
HAVING COUNT(po.name) IN (SELECT MAX(mycount)
FROM(SELECT COUNT(po.name) mycount FROM Pet p
INNER JOIN PetOwner po ON p.pet_id = po.pet_id
GROUP BY po.name)));
