/** LAB 2 **/
/** 1. List all Author’s first and last names. **/
SELECT Lname, Fname FROM Author;
/** 2. List all the entries in the Customers Table **/
SELECT * FROM Customers;
/** 3. List all Customers first names, last names, and cities. **/
SELECT FirstName, LastName, City FROM Customers;
/** 4. Show all the Customers that live in the city BURBANK**/
SELECT * FROM Customers WHERE City = 'BURBANK';
/** 5. Show all books that cost over 30 dollars.**/
SELECT * FROM Books WHERE Cost > 30;
/** 6. Show all books in the categories business or cooking**/
SELECT * FROM Books WHERE Category = 'BUSINESS' OR Category = 'COOKING';
/** 7. Show all books, which cost less than 40 dollars whose retail is over 55 dollars.**/
SELECT * FROM Books WHERE (Cost < 40) AND (Retail > 55);
/** 8. Show all books, which cost less than 40 dollars whose retail is over 55 dollars in the children’s category. **/
SELECT * FROM Books WHERE (Cost < 40 AND Retail > 55) AND Category = 'CHILDREN';
/** 9. Show all books sorted from lowest cost to highest cost. **/
SELECT * FROM Books ORDER BY Cost ASC;
/** 10. Show all author first and last names under the headings authorsfn and authorsln **/
SELECT Lname AS authorsln, Fname AS authorsfn FROM Author;
/** - Query Problems – Find the following information: **/
/** 1. Show all books whose category starts with C (use like). **/
SELECT * FROM Books WHERE Category LIKE 'C%';
/** 2. List all the categories of books (each should appear only once). **/
SELECT DISTINCT Category FROM Books;
/** 3. Show all Customers sorted alphabetically by last name, then first name.**/
SELECT * FROM Customers ORDER BY LastName, FirstName ASC;
/** 4. Show all customers that live in Burbank, Chicago, or Atlanta. **/
SELECT * FROM Customers WHERE (City = 'BURBANK') OR (City = 'CHICAGO') OR (City = 'ATLANTA');
/** 5. Show the titles of all books in categories that start with a C or an F, which cost more than 30 dollars under the heading ‘Expensive_CF_Titles’, sorted alphabetically. **/
SELECT Title AS Expensive_CF_Titles FROM Books WHERE ((Category LIKE 'C%') OR (Category LIKE 'F%')) AND Cost > 30 ORDER BY Title ASC;

/** -- LAB 3 -- **/
/**1. List all books published by publisher with pubID= 1, OR whose category is computer. Display the publisher id, the title of the book, and the book’s category. The Title and the Category should have only the First Letter in Uppercase. **/
SELECT PubID, SUBSTR(UPPER(Title),0,1) || SUBSTR(LOWER(Title),2) AS Title, INITCAP(Category) FROM Books WHERE PubID = 1 OR Category = 'COMPUTER';
/**2. Display the first Name and the first 4 letters of the Last Name append a dot “.” At the end, for all customers living in CA State. **/
SELECT FirstName, SUBSTR(LastName,0,4) || '.' AS LastName FROM Customers WHERE State = 'CA';
/**3. Display the First Name, Last Name and Address of all customers living in the CA State. Remove the character string “P.O. BOX” from all addresses before displaying the result set. Rename the Address column into “Cust. Address” **/
SELECT FirstName, LastName, REPLACE(Address, 'P.O. BOX') AS "Cust. Address" FROM Customers WHERE State = 'CA';
/**4. Display the title and the retail as “Retail Price” of all books. The retail price should be rounded to the nearest dollar. **/
SELECT Title, ROUND(retail,0) FROM Books;
/**5. Display the list of all books’ title with their current Discount. Use the appropriate function to avoid the NULL value in the discount. If the Discount is Null, replace it by 0. **/
SELECT Title, NVL2(Discount, Discount,0) AS Discount FROM Books;
/**6. Display all book’s title as “Book Title”, add the ‘-=’ in front and behind of each title. The total length 40 on the left of the title and 60 on the right including the symbol. **/
SELECT RPAD(LPAD(Title,40,'-='),60,'=-') FROM Books;
/**7. Display all orders where the ship date is smaller than 1 week after the order date. **/
SELECT * FROM Orders WHERE ((ShipDate - OrderDate)/7) < 1;
/**8. Display the customer number and its state which does not have a ship date. **/
SELECT Customer#, ShipState FROM Orders WHERE ShipDate IS NULL;
/**Queries – These questions will be graded: **/
/**1. Select all customers first Name, Last name and address as “Cust. Address” where the zip code does not contains “P.O. BOX” at the beginning, and add the String “P.O. BOX ” in front of the resulting addresses. **/

/**2. Display the title, retail and discount of all books. If the discount is not Null display the discount rounded to 3 digits after the dot and if the discount is Null display 0 **/

/**3. Display the list of all books title with their current sales price which is (the retail price – the discount) as “Sale Price”. Make sure you address the NULL value issue in the discount calculation. **/

/**4. Search all customers living in the region ‘FL’ and replace FL with Florida in your result set . **/

/**5. Display all authors which Last name contains the string “IN ”. **/

/**6. Display all Customer First Name, Last Name and the first three digits of their zip code AS “extractedZip”. **/
