/*1. Write a query that shows the names of all authors, and shows the name of publishers which
have published them, when applicable*/
SELECT a.lname, a.fname, p.name FROM publisher p 
INNER JOIN books b ON p.pubid = b.pubid
INNER JOIN bookauthor ba ON b.isbn = ba.isbn
INNER JOIN author a ON ba.authorid = a.authorid;

/*2. List the titles of all books that are part of orders, which Sue has assisted with.*/
SELECT b.title, e.name FROM books b
INNER JOIN orderitems oi ON b.isbn = oi.isbn
INNER JOIN orderassist oa ON oa.order# = oi.order#
INNER JOIN employee e ON oa.employeeid = e.employeeid
WHERE e.name = 'Sue';

/*3. Show the titles of books Jake Lucas has ordered, showing each only once.*/
SELECT distinct b.title, c.firstname, c.lastname FROM books b
INNER JOIN orderitems oi ON b.isbn = oi.isbn
INNER JOIN orders o ON oi.order# = o.order#
INNER JOIN customers c ON o.customer# = c.customer#
WHERE c.lastname = 'LUCAS' AND c.firstname = 'JAKE';

/*4. List all employees with a bad salary rating.*/
SELECT e.name,e.employeeid, s.salarytype FROM employee e 
INNER JOIN salary s ON e.salary BETWEEN s.minsalary AND s.maxsalary
WHERE s.salarytype = 'BAD';

/*5. Show a list of customerís names and the titles of books, which the person who referred them has
ordered.*/
SELECT c1.firstname as "customenr" , c1.lastname as "customer" , c2.firstname , c2.lastname  , b.title FROM books b
INNER JOIN orderitems oi ON b.isbn = oi.isbn
INNER JOIN orders o ON oi.order# = o.order#
INNER JOIN customers c1 ON o.customer# = c1.customer#
INNER JOIN customers c2 ON c2.referred = c1.customer#;

/*6. List all employees, and for those who have assisted on an order, list the name of the customer
they assisted.*/
SELECT e.name as "employee" , c.lastname, c.firstname, oa.order# FROM employee e
INNER JOIN orderassist oa ON e.employeeid = oa.employeeid
INNER JOIN orders o ON oa.order# = o.order#
INNER JOIN customers c ON o.customer# = c.customer#;

/*7. List all titles of books in the books table, and if it has been ordered, list the corresponding order
numbers along with the state in which the customer resides.*/
SELECT b.title, o.order#, c.state FROM customers c
INNER JOIN orders o ON c.customer# = o.customer#
INNER JOIN orderitems oi ON o.order# = oi.order#
INNER JOIN books b ON oi.isbn = b.isbn;

/*8. (*) Using set operators, show a list of books written by TAMARA KZOCHSKY which have never
been ordered.*/
(SELECT b.title FROM books b
INNER JOIN bookauthor ba ON b.isbn = ba.isbn
INNER JOIN author a ON ba.authorid = a.authorid
WHERE a.lname = 'KZOCHSKY' AND a.fname = 'TAMARA')
MINUS
(SELECT b.title FROM books b
INNER JOIN orderitems oi ON b.isbn = oi.isbn);

/*9. (*) Using set operators, show a list of books written by SAM SMITH that are also published by
READING MATERIALS INC.*/
(SELECT b.title, a.lname, a.fname FROM books b
INNER JOIN bookauthor ba ON b.isbn = ba.isbn
INNER JOIN author a ON ba.authorid = a.authorid
WHERE a.lname = 'SMITH' AND a.fname = 'SAM')
INTERSECT
(SELECT b.title, a.lname, a.fname FROM author a
INNER JOIN bookauthor ba ON a.authorid = ba.authorid
INNER JOIN books b ON ba.isbn = b.isbn
INNER JOIN publisher p ON b.pubid = p.pubid
WHERE p.name = 'READING MATERIALS INC.');
