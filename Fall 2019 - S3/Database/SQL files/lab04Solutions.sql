/* -- QUERY QUESTIONS -- */

/*11. Write a query that shows the names of all customers and all authors, and shows which
customers ordered books written by each author. */
SELECT Customers.FirstName, Books.Title, Author.Fname FROM Customers
INNER JOIN Orders ON customers.Customer# = Orders.Customer#
INNER JOIN ORDERITEMS ON Orders.Order# = ORDERITEMS.Order#
INNER JOIN Books ON ORDERITEMS.ISBN = Books.ISBN
INNER JOIN BookAuthor ON Books.ISBN = BookAuthor.ISBN
INNER JOIN Author ON BookAuthor.AuthorID = Author.AuthorID;

/*12. List the names of all customers who have placed orders, along with their order number. Next to
the names of the customers, list the name of the manager of the person they have received
assistance from, if such a person exists. */
SELECT DISTINCT Customers.FirstName, Customers.LastName, Orders.Order#, e.MANAGERID FROM Customers
INNER JOIN Orders ON Customers.Customer# = Orders.Customer#
LEFT OUTER JOIN orderassist ON Orders.Order# = orderassist.Order#
LEFT OUTER JOIN Employee e ON orderassist.EMPLOYEEID = e.EMPLOYEEID ORDER BY Orders.Order#;

/*13. For each order number, list which promotional gifts should be applied to that order, if any. */
SELECT DISTINCT Orders.Order#, promotion.Gift FROM Orders
INNER JOIN ORDERITEMS ON Orders.Order# = ORDERITEMS.Order#
INNER JOIN Books ON ORDERITEMS.ISBN = Books.ISBN
INNER JOIN promotion ON Books.retail BETWEEN promotion.Minretail AND promotion.Maxretail;

/*14. Show names of all authors, along with promotional gifts that can be acquired from books
written by that author. */
SELECT Author.Fname, Author.LName, promotion.Gift FROM Author
INNER JOIN BookAuthor ON Author.AuthorID = BookAuthor.AuthorID
INNER JOIN Books ON BookAuthor.ISBN = Books.ISBN
INNER JOIN promotion ON Books.retail BETWEEN promotion.Minretail AND promotion.Maxretail;

/*15. List the names of every manager who has a manager, along with the name of the person they
manage, as we ll as the name of the person who manages them. (2 self joins required!) */
SELECT e1.NAME AS "Employee", e.NAME AS "Middle Manager", e2.NAME AS "Higher Manager" FROM employee e
INNER JOIN Employee e1 ON e.EMPLOYEEID = e1.MANAGERID
INNER JOIN EMPLOYEE e2 ON e.MANAGERID = e2.EMPLOYEEID;