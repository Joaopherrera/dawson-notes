/*-- 2 --*/
SELECT author.FNAME AS "FirstName", author.LNAME AS "LastName" FROM author
INNER JOIN bookauthor ON author.authorid = bookauthor.authorid
INNER JOIN books ON bookauthor.isbn = books.isbn
WHERE books.retail IN
(
SELECT
    CASE WHEN MAX(books.retail) <= 25 THEN NULL ELSE MAX(books.retail) END
FROM books
GROUP BY books.category);

/*-- 3 --*/
SELECT customers.lastname, customers.firstname FROM customers
INNER JOIN orders ON customers.customer# = orders.customer#
INNER JOIN orderitems ON orders.order# = orderitems.order#
WHERE orderitems.isbn IN (
SELECT orderitems.isbn FROM orderitems
WHERE orderitems.order# IN(
SELECT orders.order# FROM orders
WHERE orders.orderdate IN (
SELECT MIN(orders.orderdate) AS  FROM orders)));

/*-- 5 --*/
SELECT publisher.name FROM publisher
INNER JOIN books ON books.pubid = publisher.pubid
WHERE books.title 
IN (
SELECT books.title FROM books
WHERE books.title LIKE '%THE%');

/*-- 6 --*/
DROP VIEW Compaustin;
CREATE VIEW Compaustin AS
SELECT books.title FROM books
INNER JOIN bookauthor ON books.isbn = bookauthor.isbn
INNER JOIN author ON author.authorid = bookauthor.authorid
WHERE author.lname = 'AUSTIN' AND author.fname = 'JAMES' AND books.category = 'COMPUTER';