--------------------------------------------------------
--  File created - Tuesday-October-15-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PETOWNER
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."PETOWNER" 
   (	"PETOWNER_ID" NUMBER(5,0), 
	"NAME" VARCHAR2(30 BYTE), 
	"PET_ID" NUMBER(5,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into SYSTEM.PETOWNER
SET DEFINE OFF;
Insert into SYSTEM.PETOWNER (PETOWNER_ID,NAME,PET_ID) values (1,'paul',1);
Insert into SYSTEM.PETOWNER (PETOWNER_ID,NAME,PET_ID) values (2,'John',1);
Insert into SYSTEM.PETOWNER (PETOWNER_ID,NAME,PET_ID) values (3,'ringo',4);
Insert into SYSTEM.PETOWNER (PETOWNER_ID,NAME,PET_ID) values (4,'georges',2);
--------------------------------------------------------
--  DDL for Index SYS_C008819
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."SYS_C008819" ON "SYSTEM"."PETOWNER" ("PETOWNER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table PETOWNER
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."PETOWNER" ADD PRIMARY KEY ("PETOWNER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PETOWNER
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."PETOWNER" ADD CONSTRAINT "PETOWNER_PET_ID_FK" FOREIGN KEY ("PET_ID")
	  REFERENCES "SYSTEM"."PET" ("PETID") ENABLE;
