DROP TABLE Cars;
DROP TABLE Suppliers;
DROP TABLE Parts;
DROP TABLE Cars_Suppliers_Junction;
DROP TABLE Suppliers_Parts_Junction;

-- Create Tables--
CREATE TABLE Cars (
    car_id varchar2(4),
    model varchar2(10) UNIQUE NOT NULL,
    brand varchar2(10) NOT NULL,
    consumption number(2,1) NOT NULL,
    hp number(4) NOT NULL,
    PRIMARY KEY (car_id)
);

CREATE TABLE Suppliers (
    supplier_id varchar2(4),
    supplier varchar2(20) UNIQUE NOT NULL,
    contact varchar2(10) NOT NULL,
    phone varchar2(12) UNIQUE NOT NULL,
    PRIMARY KEY (supplier_id)
);

CREATE TABLE Parts (
    part_id varchar2(4),
    part varchar2(10) UNIQUE NOT NULL,
    part_type varchar2(10) NOT NULL,
    PRIMARY KEY (part_id)
);

CREATE TABLE Cars_Suppliers_Junction (
    car_id varchar2(4) NOT NULL,
    supplier_id varchar2(4) NOT NULL
    -- junction_id varchar(5) --
);

CREATE TABLE Suppliers_Parts_Junction (
    suppliers_id varchar2(4) NOT NULL,
    part_id varchar2(4) NOT NULL
    -- junction_id varchar(5) --
);

-- Populating all the tables --
-- Cars Table --
INSERT INTO Cars VALUES ('C001', 'Civic', 'Honda', 7.5, 160);
INSERT INTO Cars VALUES ('C002', 'Civic', 'Honda', 7.5, 160);
INSERT INTO Cars VALUES ('C003', 'Fit', 'Honda', 7.0, 140);
INSERT INTO Cars VALUES ('C004', 'Focus', 'Ford', 7.8, 170);
INSERT INTO Cars VALUES ('C005', 'Focus', 'Ford', 7.8, 170);
INSERT INTO Cars VALUES ('C006', 'Fiesta', 'Ford', 6.9, 165);

-- Suppliers Table --
INSERT INTO Suppliers VALUES ('S001', 'PartsCo', 'Emma', '5147134561');
INSERT INTO Suppliers VALUES ('S002', 'Joe Tires', 'Joe', '5149875647');
INSERT INTO Suppliers VALUES ('S003', 'CarStuff', 'Alder', '5149813246');

-- Parts Table --
INSERT INTO Parts VALUES ('P001', 'Muf1', 'Muffler');
INSERT INTO Parts VALUES ('P002', 'Brk01', 'Brake');
INSERT INTO Parts VALUES ('P003', 'JTire', 'Tire');
INSERT INTO Parts VALUES ('P004', 'Wp06', 'WWiper');
INSERT INTO Parts VALUES ('P005', 'BrkAX', 'Brake');

-- Cars_Suppliers_Junction Table --
INSERT INTO Cars_Suppliers_Junction VALUES ('C001', 'S001');
INSERT INTO Cars_Suppliers_Junction VALUES ('C001', 'S002');
INSERT INTO Cars_Suppliers_Junction VALUES ('C002', 'S003');
INSERT INTO Cars_Suppliers_Junction VALUES ('C003', 'S001');
INSERT INTO Cars_Suppliers_Junction VALUES ('C003', 'S002');
INSERT INTO Cars_Suppliers_Junction VALUES ('C004', 'S002');

-- Suppliers_Parts_Junction Table --
INSERT INTO Suppliers_Parts_Junction VALUES ('S001', 'P001');
INSERT INTO Suppliers_Parts_Junction VALUES ('S001', 'P002');
INSERT INTO Suppliers_Parts_Junction VALUES ('S001', 'P004');
INSERT INTO Suppliers_Parts_Junction VALUES ('S001', 'P005');
INSERT INTO Suppliers_Parts_Junction VALUES ('S002', 'P003');
INSERT INTO Suppliers_Parts_Junction VALUES ('S002', 'P004');
INSERT INTO Suppliers_Parts_Junction VALUES ('S003', 'P001');
INSERT INTO Suppliers_Parts_Junction VALUES ('S003', 'P003');

-- QUERIES --
-- 1. Which cars does Emma supply parts for? --
SELECT model FROM cars
inner join Cars_Suppliers_Junction CSJ on Cars.car_id = CSJ.car_id
inner join Suppliers on CSJ.supplier_id  = Suppliers.supplier_id
Where Suppliers.contact = 'Emma';

-- 2. What phone number should you call to get JTires for a Civic? --
SELECT DISTINCT Suppliers.phone FROM Suppliers
inner join Cars_Suppliers_Junction CSJ on Suppliers.supplier_id = CSJ.supplier_id
inner join Cars on Cars.car_id = CSJ.car_id
inner join Suppliers_Parts_Junction SPJ on Suppliers.supplier_id = SPJ.suppliers_id
inner join Parts on Parts.part_id = SPJ.part_id
WHERE Cars.model = 'Civic' AND Parts.part = 'JTire';

-- 3. Which cars does CarStuff provide mufflers for? --
SELECT Cars.model FROM Suppliers
inner join Cars_Suppliers_Junction CSJ on CSJ.supplier_id = Suppliers.supplier_id
inner join Cars on Cars.car_id = CSJ.car_id
inner join Suppliers_Parts_Junction SPJ on Suppliers.supplier_id = SPJ.suppliers_id
inner join Parts on Parts.part_id = SPJ.part_id
WHERE Parts.part_type = 'Muffler' AND Suppliers.supplier = 'CarStuff';