/* -- QUERY QUESTIONS -- */

/* Display customers first Name, Last name and address as “Cust. Address” and add “P.O. BOX ” in front of the resulting addresses */
SELECT FirstName, LastName,'P.O BOX ' || Address AS "Cust. Address" FROM Customers WHERE Address NOT LIKE 'P.O. BOX%';

/* Display title, retail and discount of all books. If the discount is Null display 0 */
SELECT Title, Retail, NVL2(Discount, Discount, 0) AS Discount FROM Books;

/* Display all books title with their current sales price(the retail price – the discount) as “Sale Price”m*/
SELECT Title, (Retail - NVL2(Discount,Discount, 0)) AS "Sales Price" FROM Books;

/* Display all customers living in the region ‘FL’ and replace by Florida */
SELECT LastName, FirstName, Address, City, REPLACE(State,'FL','Florida'), Zip, Referred, Region, Email FROM Customers WHERE (State = 'FL');

/* Display all authors which Last name contains string “IN ”. */
SELECT * FROM Author WHERE Lname LIKE '%IN%';

/* Display all Customer First Name, Last Name and the first three digits of their zip code AS “extractedZip”. */
SELECT FirstName, LastName, SUBSTR(Zip, 1, 3) AS "ExtractedZip" FROM Customers;

/* -- Query Exercises -- */

/* 1. List all books published by publisher with pubID= 1, OR whose category is computer. */
SELECT PubID, SUBSTR(UPPER(Title),0,1) || SUBSTR(LOWER(Title),2) AS Title, INITCAP(Category) AS Category FROM Books WHERE PubID = 1 OR (Category ='COMPUTER');

/* 2. Display the first Name and the first 4 letters of the Last Name append a dot “.” At the end, for all customers living in CA State. */
SELECT FirstName, SUBSTR(LastName, 0,4) || '.' AS LastName FROM Customers WHERE (State = 'CA');

/* 3. Display the First Name, Last Name and Address of all customers living in the CA State. Remove the character string “P.O. BOX” from all addresses before displaying the result set. Rename the Address column into “Cust. Address” */
SELECT FirstName, LastName, REPLACE(Address,'P.O. BOX ') AS "Cust Address" FROM Customers WHERE (State = 'CA');

/* 4. Display the title and the retail as “Retail Price” of all books. The retail price should be rounded to the nearest dollar. */
SELECT Title, ROUND(Retail,0) AS "Retail Price" FROM Books;

/* 5. Display the list of all books’ title with their current Discount. Use the appropriate function to avoid the NULL value in the discount. If the Discount is Null, replace it by 0. */
SELECT Title, NVL2(Discount, Discount, 0) AS Discount FROM Books;

/*6. Display all book’s title as “Book Title”, add the ‘-=’ in front and behind of each title. The total length 40 on the left of the title and 60 on the right including the symbol. */
SELECT RPAD(LPAD(Title,40,'-='),60,'-=') AS "Book Title" FROM Books;

/* 7. Display all orders where the ship date is smaller than 1 week after the order date. */
SELECT * FROM Orders WHERE ((ShipDate - OrderDate)/7 < 1 );

/* 8. Display the customer number and its state which does not have a ship date. */
SELECT Customer#, ShipState FROM Orders WHERE ShipDate IS NULL;
