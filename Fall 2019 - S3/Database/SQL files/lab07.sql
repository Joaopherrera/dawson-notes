DROP TABLE gym;
DROP TABLE sales;
DROP TABLE employees;
DROP TABLE instructor;
DROP TABLE gym_activity;
DROP TABLE gym_member;


CREATE TABLE gym (
gym_id varchar2(5),
gym_location varchar2(4) UNIQUE NOT NULL,
phone_number varchar2(10) NOT NULL,
email varchar2(100) NOT NULL,
PRIMARY KEY (gym_id)
);

CREATE TABLE sales (
employee_id varchar2(7) NOT NULL,
member_id varchar2(5) UNIQUE NOT NULL,
join date NOT NULL,
paid number(10), 
gym_id varchar2(5) NOT NULL
);

CREATE TABLE employees (
employee_id varchar2(7),
name varchar2(40) NOT NULL,
gym_id varchar2(5) NOT NULL,
PRIMARY KEY (employee_id)
);

CREATE TABLE instructor (
instructor_id varchar2(5),
name varchar2(40) NOT NULL,
employee_id varchar2(7) UNIQUE NOT NULL,
PRIMARY KEY (instructor_id)
);

CREATE TABLE gym_activity (
name varchar2(40) NOT NULL,
activity_id varchar2(5),
instructor_id varchar2(7) UNIQUE NOT NULL,
room varchar2(4) NOT NULL,
PRIMARY KEY (activity_id)
);

CREATE TABLE gym_member (
member_id varchar2(5),
name varchar2(40) NOT NULL,
address varchar2(40),
join date NOT NULL,
gym_id varchar2(5) NOT NULL,
membership_level varchar2(10) NOT NULL,
activity_id varchar2(5) NOT NULL,
active varchar2(1) NOT NULL,
PRIMARY KEY (member_id)
);


--Populating all the tables --
-- Gym Tables --
INSERT INTO gym VALUES('G0001', 'b001', '5145545444', 'gymbuilding1@email.com');
INSERT INTO gym VALUES('G0002', 'b002', '5149870987', 'gymbuilding2@email.com');
INSERT INTO gym VALUES('G0003', 'b003', '5221056890', 'gymbuilding3@email.com');
INSERT INTO gym VALUES('G0004', 'b004', '5141111151', 'gymbuilding4@email.com');

-- Sales Table -- 
INSERT INTO sales VALUES('E0001', 'M0001', '21-MAR-00', 1000, 'G0001');
INSERT INTO sales VALUES('E0002', 'M0002', '05-JUL-10', 500, 'G0002');
INSERT INTO sales VALUES('E0003', 'M0003', '07-JUN-05', 1250, 'G0003');
INSERT INTO sales VALUES('E0002', 'M0004', '03-DEC-09', 3000, 'G0001');

-- Employees Table -- 
INSERT INTO employees VALUES('E0001', 'John Smith', 'G0001');
INSERT INTO employees VALUES('E0002', 'Samantha Will', 'G0003');
INSERT INTO employees VALUES('E0003', 'Kyle Hardin', 'G0002');
INSERT INTO employees VALUES('E0004', 'Donovan Small', 'G0001');

-- Instructor --
INSERT INTO instructor VALUES('I0001', 'Dwayne The Rock Johnson', 'E0003');
INSERT INTO instructor VALUES('I0002', 'Jackie Chan', 'E0004');
INSERT INTO instructor VALUES('I0003', 'Keanu Reeves', 'E0005');
INSERT INTO instructor VALUES('I0004', 'Sylvester Stallone', 'E0006');

-- Gym Activity --
INSERT INTO gym_activity VALUES('Martial Arts', 'A0001', 'I0002', '1F21');
INSERT INTO gym_activity VALUES('Bodybuilding', 'A0002', 'I0001', '2F17');
INSERT INTO gym_activity VALUES('Becoming breathtaking', 'A0003', 'I0003', '3H21');
INSERT INTO gym_activity VALUES('Surviving into the Forest ', 'A0004', 'I0004', '1D01');

-- Gym Members --
INSERT INTO gym_member VALUES('M0001', 'Dawood Stokes', '202 Mason Street', '10-JAN-99', 'G0001', 'master', 'A0001', '1');
INSERT INTO gym_member VALUES('M0002', 'Alina Briggs', '100 Noname Street', '30-MAR-05', 'G0001', 'peasant', 'A0002', '1');
INSERT INTO gym_member VALUES('M0003', 'Joss Hart', '4431 Somewhere Av.', '04-JUN-10', 'G0002', 'king', 'A0002', '0');
INSERT INTO gym_member VALUES('M0004', 'Elvis Robinson', '408 Boul Maisonneuve', '09-DEC-09', 'G0001', 'god', 'A0001', '1');

/* This is to show every person in a specific gym */
SELECT member.name from gym_member member inner join gym gy
on member.gym_id = gy.gym_id
WHERE gy.gym_id = 'G0001';

/* find all of the people who have been served by ... */
SELECT member.name from gym_member member
inner join sales
on member.member_id = sales.member_id
inner join employees emp 
on emp.employee_id = sales.employee_id
WHERE emp.name = 'John Smith';


/* Show people employees who are also instructors */
SELECT emp.name FROM employees emp 
inner join instructor ins
on ins.employee_id = emp.employee_id ;


/* show all inactive members in a specific gym  */
SELECT member.name FROM gym_member member 
inner join gym gy
on member.gym_id = gy.gym_id
WHERE member.active = '0';

/* show all active members in a specific gym  */
SELECT member.name FROM gym_member member 
inner join gym gy
on member.gym_id = gy.gym_id
WHERE member.active = '1';