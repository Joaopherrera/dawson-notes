/* -- Lab 05 -- */
/* Graded Questions */

/* 1- For each course, show the title of the course and the number of students who have
taken the course or are currently enrolled. */
SELECT uni_course.title, COUNT(uni_enrollment.stud_id) FROM uni_course
JOIN uni_enrollment ON uni_course.course_id = uni_enrollment.course_id
GROUP BY uni_course.title;

/* 2- Show title of courses whose class average is over 80 or which are taught
by lecturers in the computer science department. */
SELECT uni_course.title FROM uni_department
INNER JOIN uni_course ON uni_course.dept_id = uni_department.dept_id
INNER JOIN UNI_ENROLLMENT ON UNI_COURSE.COURSE_ID = UNI_ENROLLMENT.COURSE_ID
GROUP BY uni_course.title, UNI_DEPARTMENT.NAME
HAVING (AVG(UNI_ENROLLMENT.GRADE) > 80) OR UNI_DEPARTMENT.NAME = 'Computer Science';

/* 3- Show the name and age (in years) of each student with an average of over 80. */
SELECT uni_student.fullname, ((MONTHS_BETWEEN(CURRENT_DATE, DOB))/12) FROM uni_student
INNER JOIN uni_enrollment ON uni_enrollment.stud_id = uni_student.stud_id
GROUP BY uni_student.fullname, ((MONTHS_BETWEEN(CURRENT_DATE, DOB))/12)
HAVING AVG(uni_enrollment.grade) > 80;

/* 4- Show the names of students that have taken courses in the history department
and names of lecturers in the history department. (In one column) */
(SELECT uni_student.fullname AS "History" FROM uni_student
INNER JOIN uni_enrollment ON UNI_STUDENT.STUD_ID = UNI_ENROLLMENT.STUD_ID
INNER JOIN uni_course ON UNI_ENROLLMENT.COURSE_ID = UNI_COURSE.COURSE_ID
INNER JOIN UNI_DEPARTMENT ON UNI_COURSE.DEPT_ID = UNI_DEPARTMENT.DEPT_ID
GROUP BY uni_student.fullname, uni_department.NAME
HAVING UNI_DEPARTMENT.NAME LIKE 'History')
UNION
(SELECT UNI_LECTURER.FULLNAME AS "Hisroty"  FROM UNI_LECTURER
INNER JOIN UNI_DEPARTMENT ON UNI_LECTURER.DEPT_ID = UNI_DEPARTMENT.DEPT_ID
GROUP BY UNI_LECTURER.FULLNAME, UNI_DEPARTMENT.NAME
HAVING UNI_DEPARTMENT.NAME LIKE 'History');

/* 5- List the names of students who got above the class average in Canadian History',
or who got below the class average in ‘Bio-Informatics’ */
SELECT DISTINCT s.fullname FROM uni_student s
JOIN uni_enrollment esinglestudent ON s.stud_id = esinglestudent.stud_id
JOIN uni_course c ON esinglestudent.course_id = c.course_id
JOIN uni_enrollment egroup ON egroup.course_id = c.course_id
GROUP BY egroup.grade, s.fullname, c.title, esinglestudent.grade
HAVING (c.Title = 'Canadian History' AND AVG(egroup.grade) < esinglestudent.grade)
UNION
SELECT DISTINCT s.fullname FROM uni_student s
JOIN uni_enrollment esinglestudent ON s.stud_id = esinglestudent.stud_id
JOIN uni_course c ON esinglestudent.course_id = c.course_id
JOIN uni_enrollment egroup ON egroup.course_id = c.course_id
JOIN uni_lecturer l ON l.lec_id = c.lec_id
GROUP BY egroup.grade, s.fullname, c.title, esinglestudent.grade
HAVING (c.Title = 'Bio Informatics' AND AVG(egroup.grade) > esinglestudent.grade);