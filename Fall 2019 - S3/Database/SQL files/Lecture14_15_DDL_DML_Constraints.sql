DROP TABLE uni_course CASCADE CONSTRAINTS; 
DROP TABLE uni_department CASCADE CONSTRAINTS;
DROP TABLE uni_enrollment CASCADE CONSTRAINTS;
DROP TABLE uni_lecturer CASCADE CONSTRAINTS;
DROP TABLE uni_student CASCADE CONSTRAINTS;

--Remove  these tables  from the recycle bin forever . 
PURGE TABLE uni_course ;
PURGE TABLE uni_department ;
PURGE TABLE uni_enrollment ;
PURGE TABLE uni_lecturer;
PURGE TABLE uni_student ;
-- verify the content of  recycle bin
SELECT object_name, original_name, createtime FROM USER_RECYCLEBIN;  


-- tables creation 

-- Table: uni_course
CREATE TABLE uni_course (
    course_id varchar2(8)  NOT NULL,
    title varchar2(100)  NULL,
    dept_id varchar2(8)  NOT NULL,
    lec_id varchar2(20)  NOT NULL,
    CONSTRAINT course_id PRIMARY KEY (course_id)  --primary key definition at table-level 
) ;

-- Table: uni_department
CREATE TABLE uni_department (
    dept_id varchar2(8)  PRIMARY KEY, --primary key definition at column level 
    name varchar2(20)  NULL,
    budget number(10,2)  NOT NULL
   
) ;

-- Table: uni_enrollment
CREATE TABLE uni_enrollment (
    stud_id varchar2(8)  NOT NULL,
    course_id varchar2(8)  NOT NULL,
    grade number(3,0) NULL,
    CONSTRAINT uni_enrollment_pk PRIMARY KEY (stud_id,course_id) -- composite primary key always define as table-level constraint
) ;

-- Table: uni_lecturer
CREATE TABLE uni_lecturer (
    lec_id varchar2(20)  NOT NULL,
    fullname varchar2(20)  NOT NULL,
    salary number(10,2)  NOT NULL,
    dept_id varchar2(8)  NOT NULL,
    CONSTRAINT uni_lecturer_pk PRIMARY KEY (lec_id)
) ;

-- Table: uni_student
CREATE TABLE uni_student (
    stud_id varchar2(8)  NOT NULL,
    fullname varchar2(20)  NOT NULL,
    DOB date  NULL,
    email varchar2(20) NULL,
    CONSTRAINT stud_id PRIMARY KEY (stud_id)
) ;


-- foreign keys definition at table-level
-- Reference name: Course_Department_fk (table: uni_course)
ALTER TABLE uni_course ADD CONSTRAINT Course_Department_fk
    FOREIGN KEY (dept_id)
    REFERENCES uni_department (dept_id);

-- Reference name: Course_Lecturer_fk (table: uni_course)
ALTER TABLE uni_course ADD CONSTRAINT Course_Lecturer_fk
    FOREIGN KEY (lec_id)
    REFERENCES uni_lecturer (lec_id);

-- Reference name: Lecturer_Department_fk (table: uni_lecturer)
ALTER TABLE uni_lecturer ADD CONSTRAINT Lecturer_Department_fk
    FOREIGN KEY (dept_id)
    REFERENCES uni_department (dept_id);

-- Reference name: Enrollment_Course_fk (table: uni_enrollment)
ALTER TABLE uni_enrollment ADD CONSTRAINT Enrollment_Course_fk
    FOREIGN KEY (course_id)
    REFERENCES uni_course (course_id);

-- Reference name: Enrollment_Student_fk (table: uni_enrollment)
ALTER TABLE uni_enrollment ADD CONSTRAINT Enrollment_Student_fk
    FOREIGN KEY (stud_id)
    REFERENCES uni_student (stud_id);

-- End of table creation (DDL Statements).

--DML Statements : Fill tables with data, query the table ...

INSERT INTO uni_student VALUES ('0001','Joe Benson','01-APR-92','joe@email.com');
INSERT INTO uni_student VALUES ('0002','Jane Benson','06-APR-94','jan@email.com');
INSERT INTO uni_student VALUES ('0003','Den Merkon','02-OCT-93',NULL);
INSERT INTO uni_student VALUES ('0004','Blort Vergul','28-FEB-95','blor@email.com');
INSERT INTO uni_student VALUES ('0005','Shane Quesna',NULL,'shane@email.com');
INSERT INTO uni_student VALUES ('0006','Emily Dubella','08-JAN-94',NULL);
INSERT INTO uni_student VALUES ('0007','Robert Robertson','12-JUL-93','rob@email.com');

INSERT INTO uni_department VALUES ('001','Biology',120000);
INSERT INTO uni_department VALUES ('002','Computer Science',140000);
INSERT INTO uni_department VALUES ('003','History',90000);
INSERT INTO uni_department VALUES ('004','English',80000);

INSERT INTO uni_lecturer VALUES ('0001','Mort Mortenson',40000,'001');
INSERT INTO uni_lecturer VALUES ('0002','Ted Bronson',40000,'001');
INSERT INTO uni_lecturer VALUES ('0003','Jackie Brown',50000,'001');
INSERT INTO uni_lecturer VALUES ('0004','Sophie Sophiston',60000,'002');
INSERT INTO uni_lecturer VALUES ('0005','Paul Dente',40000,'002');
INSERT INTO uni_lecturer VALUES ('0006','Edgar Roe',40000,'003');
INSERT INTO uni_lecturer VALUES ('0007','Sarah Orb',30000,'003');
INSERT INTO uni_lecturer VALUES ('0008','Shane Delben',30000,'003');
INSERT INTO uni_lecturer VALUES ('0009','Bert Lebre',20000,'004');

INSERT INTO uni_course VALUES ('0001','Intro Bio','001','0001');
INSERT INTO uni_course VALUES ('0002','Advanced Bio','001','0001');
INSERT INTO uni_course VALUES ('0003','Topics in Bio','001','0002');
INSERT INTO uni_course VALUES ('0004','Animals and Stuff','001','0003');
INSERT INTO uni_course VALUES ('0005','Plants and Stuff','001','0003');
INSERT INTO uni_course VALUES ('0006','Intro Programming','002','0004');
INSERT INTO uni_course VALUES ('0007','Advanced Programming','002','0004');
INSERT INTO uni_course VALUES ('0008','Bio-Informatics','002','0002');
INSERT INTO uni_course VALUES ('0009','Databases','002','0005');
INSERT INTO uni_course VALUES ('0010','Artificial Intelligence','002','0005');
INSERT INTO uni_course VALUES ('0011','Canadian History','003','0006');
INSERT INTO uni_course VALUES ('0012','Neoliberalism and You','003','0006');
INSERT INTO uni_course VALUES ('0013','Neoliberalism and Modern Society','003','0007');
INSERT INTO uni_course VALUES ('0014','Neoliberalism and the Future','003','0008');
INSERT INTO uni_course VALUES ('0015','Ancient Civilzations','003','0008');
INSERT INTO uni_course VALUES ('0016','Blaaaaah','004','0009');
INSERT INTO uni_course VALUES ('0017','Renaissance Literature','004','0007');

INSERT INTO uni_enrollment VALUES ('0001','0011',85);
INSERT INTO uni_enrollment VALUES ('0001','0012',80);
INSERT INTO uni_enrollment VALUES ('0001','0013',NULL);
INSERT INTO uni_enrollment VALUES ('0001','0015',NULL);
INSERT INTO uni_enrollment VALUES ('0001','0008',99);
INSERT INTO uni_enrollment VALUES ('0002','0017',70);
INSERT INTO uni_enrollment VALUES ('0002','0011',65);
INSERT INTO uni_enrollment VALUES ('0002','0001',NULL);
INSERT INTO uni_enrollment VALUES ('0002','0015',70);
INSERT INTO uni_enrollment VALUES ('0003','0006',70);
INSERT INTO uni_enrollment VALUES ('0003','0005',50);
INSERT INTO uni_enrollment VALUES ('0003','0007',45);
INSERT INTO uni_enrollment VALUES ('0003','0008',NULL);
INSERT INTO uni_enrollment VALUES ('0004','0001',80);
INSERT INTO uni_enrollment VALUES ('0004','0002',70);
INSERT INTO uni_enrollment VALUES ('0004','0003',0);
INSERT INTO uni_enrollment VALUES ('0004','0004',10);
INSERT INTO uni_enrollment VALUES ('0004','0005',20);
INSERT INTO uni_enrollment VALUES ('0004','0015',NULL);
INSERT INTO uni_enrollment VALUES ('0005','0006',NULL);
INSERT INTO uni_enrollment VALUES ('0005','0007',NULL);
INSERT INTO uni_enrollment VALUES ('0005','0012',NULL);
INSERT INTO uni_enrollment VALUES ('0007','0011',99);
INSERT INTO uni_enrollment VALUES ('0007','0010',80);
INSERT INTO uni_enrollment VALUES ('0007','0009',85);
INSERT INTO uni_enrollment VALUES ('0007','0008',80);


--Some queries
--Insert a Math department 
INSERT INTO uni_department VALUES ('005','Math',80000);
--update the  department, set the budget of the English department to 100 000
UPDATE  uni_department  SET budget = 100000 WHERE name='History';

--Display the name, email and DOB of all students enrolled in the university 

SELECT stud.fullname, NVL(stud.email,'No Email'), NVL(stud.DOB, to_date(CURRENT_DATE)) AS "Date Of Birth"  from uni_student stud INNER JOIN uni_enrollment ue USING(stud_id);

--More queries :
-- show the names of student enrolled in the  History department
-- show the name of the lecturer who has the less courses to teach (0 or 1 course)
--  the math department  has no lecturer  add the previous lecturer to the math and 1 additional lecturer of your choice .Add 2 courses to each lecturer of the Math department. 
-- Show the names of students that have taken courses in the history department and names of lecturers in the history department.


