/* -- QUERY EXERCISE -- */
/* 1. List all Author’s first and last names (14) */
SELECT Fname, Lname FROM Author;

/* 2. List all the entries in the Customers Table (20) */
SELECT * FROM Customers;

/* 3. List all Customers first names, last names, and cities. (20) */
SELECT FirstName, LastName, City FROM Customers;

/* 4. Show all the Customers that live in the city BURBANK (2) */
SELECT * FROM Customers WHERE City = 'BURBANK';

/* 5. Show all books that cost over 30 dollars. (5) */
SELECT * FROM Books WHERE Cost > 30;

/* 6. Show all books in the categories business or cooking (3) */
SELECT * FROM Books WHERE Category IN ('BUSINESS', 'COOKING');

/* 7. Show all books, which cost less than 40 dollars whose retail is over 55 dollars. (2) */
SELECT * FROM Books WHERE (Cost < 40 AND Retail > 55);

/* 8. Show all books, which cost less than 40 dollars whose retail is over 55 dollars in the children category. (1) */
SELECT * FROM Books WHERE ((Cost < 40 AND Retail > 55) AND Category = 'CHILDREN');

/* 9. Show all books sorted from lowest cost to highest cost. (14) */
SELECT * FROM Books ORDER BY Cost ASC;

/* 10. Show all author first and last names under the headings authorsfn and authorsln (14) */
SELECT Lname AS authorsfn, Fname AS authorsln FROM Author;

/* -- QUERY PROBLEMS -- */
/* List all books with category starts with C using like */
SELECT * FROM Books WHERE Category LIKE 'C%';

/* List all the categories of books only once */
SELECT DISTINCT Category FROM Books;

/* Show all Customers sorted alphabetically by last name, then first name. */
SELECT * FROM Customers ORDER BY LastName, FirstName ASC;

/* Show all customers that live in Burbank, Chicago, or Atlanta. */
SELECT * FROM Customers WHERE (City = 'BURBANK' OR City = 'CHICAGO' OR City = 'ATLANTA');

/* Show the titles of all books in categories that start with a C or an F, which cost more than 30 dollars under the heading ‘Expensive_CF_Titles’, sorted alphabetically. */
SELECT Title AS Expensive_CF_Titles FROM Books WHERE (((Category LIKE 'C%') OR (Category LIKE 'F%')) AND (Cost > 30)) ORDER BY Title ASC;