﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BMI_App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_Main_Click(object sender, RoutedEventArgs e)
        {
            //Creating variables to store the data
            String sWeight = textBox_Weight.Text;
            String sHeight = textBox_Height.Text;
           
            //Converting the String to Double to do the computation
            double weightValue = Convert.ToDouble(sWeight);
            double heightValue = Convert.ToDouble(sHeight);
            
            double resultBMI = 0.0;
            
            //If statements to verify which option (Metric or Imperial) the user choosed
            if (radioButton_Imperial.IsChecked == true)
            {
                if (weightValue > 661.387 || weightValue < 22.0462)
                {
                    MessageBox.Show("Weight Invalid.");
                }
                else if (heightValue > 86.6142 || heightValue < 7.87402)
                {
                    MessageBox.Show("Height Invalid.");
                }

                resultBMI = (weightValue / (heightValue * heightValue)) * 703;
            }
            else if (radioButton_Metric.IsChecked == true)
            {
                if (weightValue > 300 || weightValue < 10)
                {
                    MessageBox.Show("Weight Invalid.");
                }
                else if (heightValue > 2.2 || heightValue < 0.2)
                {
                    MessageBox.Show("Height Invalid.");
                }

                resultBMI = (weightValue / (heightValue * heightValue));
            }

            lbl_bmiNumber.Content = resultBMI;
            
            //Setting/reseting all the images to hidden 
            image_under.Visibility = Visibility.Hidden;
            image_normal.Visibility = Visibility.Hidden;
            image_over.Visibility = Visibility.Hidden;
            image_obese.Visibility = Visibility.Hidden;
            
            //If statements to categorize the result of the BMI
            if (resultBMI <= 18.5)
            {
                if ((String)btn_ChangeLanguage.Content == "FR")
                {
                    lbl_rankResult.Content = "Risk of developing problems such as nutritional deficiency and osteoporosis";
                }
                else if ((String)btn_ChangeLanguage.Content == "ENG")
                {
                    lbl_rankResult.Content = "Risque de développer des problèmes tels qu'une carence nutritionnelle et l'ostéoporose";
                }

                image_under.Visibility = Visibility.Visible;
            }
            else if (resultBMI > 18.5 && resultBMI <= 23)
            {
                if ((String)btn_ChangeLanguage.Content == "FR")
                {
                    lbl_rankResult.Content = "Low Risk (healthy range)";
                }
                else if ((String)btn_ChangeLanguage.Content == "ENG")
                {
                    lbl_rankResult.Content = "Risque faible";
                }

                image_normal.Visibility = Visibility.Visible;
            }
            else if (resultBMI > 23 && resultBMI <= 27.5)
            {
                if ((String)btn_ChangeLanguage.Content == "FR")
                {
                    lbl_rankResult.Content = "Moderate risk of developing heart disease, high blood pressure, stroke, diabetes";
                }
                else if ((String)btn_ChangeLanguage.Content == "ENG")
                {
                    lbl_rankResult.Content = "Risque modéré de développer une maladie cardiaque, une hypertension artérielle, un accident vasculaire cérébral, le diabète";
                }

                image_over.Visibility = Visibility.Visible;
            }
            else if(resultBMI > 27.5)
            {
                if ((String)btn_ChangeLanguage.Content == "FR")
                {
                    lbl_rankResult.Content = "High risk of developing heart disease, high blood pressure, stroke, diabetes";
                }
                else if ((String)btn_ChangeLanguage.Content == "ENG")
                {
                    lbl_rankResult.Content = "Risque élevé de maladie cardiaque, d'hypertension, d'accident vasculaire cérébral, de diabète";
                }

                image_obese.Visibility = Visibility.Visible;
            }
        }

        //Action Buttun to change the language of the application
        //Action Buttun to change the language of the application
        private void Btn_ChangeLanguage_Click(object sender, RoutedEventArgs e)
        {
            //Check which language its been using and change it according with the option
            if ((String)btn_ChangeLanguage.Content == "ENG")
            {
                lbl_Title.Content = "Body Mass Index";
                lbl_weight.Content = "Weight: (Kg/pounds)";
                lbl_height.Content = "Height: (m/inches)";

                btn_Main.Content = "Show My Results";

                lbl_options.Content = "Which System:";

                radioButton_Imperial.Content = "Imperial";
                radioButton_Metric.Content = "Metric";

                lbl_result.Content = "Result:";
                lbl_rank.Content = "Singaporean Health Risk :";

                btn_ChangeLanguage.Content = "FR";

            }
            else if ((String)btn_ChangeLanguage.Content == "FR")
            {
                lbl_Title.Content = "Indice de Masse Corporrelle";
                lbl_weight.Content = "Poids: (Kg/pounds)";
                lbl_height.Content = "Taille: (m/inches)";

                btn_Main.Content = "Afficher Mes Résultats";

                lbl_options.Content = "Quelle Système:";

                radioButton_Imperial.Content = "Impérial";
                radioButton_Metric.Content = "Métrique";

                lbl_result.Content = "Résultats:";
                lbl_rank.Content = "Singaporean Health Risk :";

                btn_ChangeLanguage.Content = "ENG";
            }
        }
    }
}