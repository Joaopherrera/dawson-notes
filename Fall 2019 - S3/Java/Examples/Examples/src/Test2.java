public class Test2
{
    public static void main(String[] args)
    {
        int[] x = {100, 14, 16, 63, 763, 23, 5, 2678, 3100};

        int maxIndex = findMaxIndex(x);

        System.out.println(maxIndex);
    }

    // Method to find the Max value in the Counter[]
    public static int findMaxIndex(int[] arr)
    {
        int index = 0;
        for(int i = 1; i < arr.length; i++)
        {
            if(arr[i] > arr[index])
            {
                index = i;
            }
        }
        return index;
    }
}
