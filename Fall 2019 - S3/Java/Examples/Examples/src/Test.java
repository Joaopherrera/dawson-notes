import java.util.ArrayList;
import java.util.Collections;

public class Test
{
    public static void main(String[] args) {
        ArrayList<String> test = new ArrayList<String>();
        test.add("bob");
        test.add("jeff");
        test.add("elli");
        test.add("dobz");
        test.add("popz");
        test.add("cheese");
        test.add("cow");
        test.add("sheep");
        test.add("jeff");
        test.add("not");
        Collections.sort(test);

        for (String s: test)
        {
            System.out.println(s);
        }
        //System.out.println(binarySearch(test,"cow",10));

    }

    private static void bubbleSort(String[] arrayToSort) {
        String switchVar;
        int flag = 0;
        while(flag == 0) {
            flag = 1;
            for(int i = 0 ; i < arrayToSort.length-1; i++) {
                if(!(arrayToSort[i].compareTo(arrayToSort[i+1]) < 0 || arrayToSort[i].compareTo(arrayToSort[i+1]) < 0)) {
                    switchVar = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i+1];
                    arrayToSort[i+1]=switchVar;
                    flag = 0;
                }
            }
        }
    }

    public static boolean binarySearch(String[] language, String target, int size)
    {
        int start = 0;
        int end = size - 1;
        int middle = 1 + (start + end) / 2;

        while (start <= end)
        {
            int result = language[middle].compareTo(target);
            if (result > 0)
            {
                start = middle + 1;
            }
            else if (result < 0)
            {
                end = middle - 1;
            }
            else
            {
                return true;
            }
            middle = (start + end) / 2;
        }
        return false;
    }

    public static void sortArray(String[] arr)
    {
        String temp;
        int flag = 0;

        for (int i = 0; i < arr.length; i++)
        {
            for (int j = i + 1; j < arr.length; j++)
            {
                if (arr[j] == null)
                {
                    flag = 1;
                    break;
                }
                if (arr[i].compareTo(arr[j]) > 0)
                {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
            if (flag == 1)
            {
                break;
            }
        }
    }
}
