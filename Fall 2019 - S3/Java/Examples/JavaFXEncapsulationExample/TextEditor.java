import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class TextEditor extends Application {
  public void start(Stage stage) {
    //container and layout
    Group root = new Group();
    
    VBox mainPane = new VBox();
    
    root.getChildren().add(mainPane);
    
    MyToolBar toolBar = new MyToolBar();
    TextPane editor = new TextPane();
    mainPane.getChildren().add(toolBar);
    mainPane.getChildren().add(editor);
    
    // We want to set the action of the save() button to take the text and save it
    toolBar.setLoadAction(new EventHandler<ActionEvent>() {
    	public void handle(ActionEvent e) {
    		try {
    		String fromFile = FileUtilities.load(toolBar.getPath());
    		editor.setText(fromFile);
    		}
    		catch (IOException e2) {
    			Alert a = createInvalidFilePathAlert(toolBar.getPath());
    			a.showAndWait();
    		}
    	}
    });
    toolBar.setSaveAction(new EventHandler<ActionEvent>() {
    	public void handle(ActionEvent e) {
    		try {
    		String fileName = toolBar.getPath();
    		String textToSave = editor.getText();
    		FileUtilities.save(fileName, textToSave);
    		}
    		catch (IOException e2) {
    			Alert a = createInvalidFilePathAlert(toolBar.getPath());
    			a.showAndWait();		
    		}
    	}
    	
    });
    
   
    
//scene is associated with container, dimensions
    Scene scene = new Scene(root, 400, 300);
    scene.setFill(Color.WHITE);
    
	stage.setScene(scene); 
	stage.show(); 
    
  }
 public static void main(String[] args ) {
Application.launch(args);
}
  
  public Alert createInvalidFilePathAlert(String path) {
	  Alert alert = new Alert(AlertType.INFORMATION);
      alert.setTitle("IllegalFilePath");

      alert.setContentText(path + " is an invalid file path");
	  return alert;
  }
}