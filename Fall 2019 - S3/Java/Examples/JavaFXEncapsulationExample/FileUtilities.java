import java.io.IOException;
import java.nio.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class FileUtilities {
	public static void save(String file, String content) throws IOException{
		Path path = Paths.get(file);
		Files.write(path, content.getBytes());
	}
	
	public static String load(String file) throws IOException{
		Path path = Paths.get(file);
		byte[] fileAsBytes = Files.readAllBytes(path);
		return new String(fileAsBytes);
	}
}
