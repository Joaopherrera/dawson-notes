import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;


public class MyToolBar extends GridPane{
  private TextField location;
  private Button load;
  private Button save;
  public MyToolBar() {
    this.load = new Button("Load");
    this.save = new Button("Save");
    this.location = new TextField();
    this.setWidth(100);
    this.setHeight(100);
  //  this.getChildren().addAll(load,save);
    
   this.add(load, 0, 0, 1, 1);
   this.add(save, 1, 0, 1, 1);
   this.add(location, 0, 1, 2, 1);
  }
  
  
  public String getPath() {
	  return this.location.getText();
  }
  
  public void setLoadAction(EventHandler<ActionEvent> e) {
	  this.load.setOnAction(e);
  }
  
  public void setSaveAction(EventHandler<ActionEvent> e) {
	  this.save.setOnAction(e);
  }
}