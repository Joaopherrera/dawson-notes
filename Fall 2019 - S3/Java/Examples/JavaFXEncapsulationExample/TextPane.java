import javafx.scene.*;
import javafx.scene.control.*;


public class TextPane extends Group {
 private TextArea textArea = new TextArea();
 
 public TextPane() {
  this.getChildren().add(this.textArea); 
 }
  
 
 public String getText() {
	 return this.textArea.getText();
 }
  
 public void setText(String text) {
	 this.textArea.setText(text);
 }
 
}