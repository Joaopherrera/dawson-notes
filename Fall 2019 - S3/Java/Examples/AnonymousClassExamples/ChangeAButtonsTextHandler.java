import javafx.event.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;

public class ChangeAButtonsTextHandler implements EventHandler<ActionEvent>{
  private Button button;
  
  public ChangeAButtonsTextHandler(Button b) {
    this.button = b;
  }
  public void handle(ActionEvent e) {
    button.setText("A different thing");
  }
}