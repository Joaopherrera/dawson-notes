import javafx.event.*;
import javafx.scene.*;
import javafx.scene.paint.*;

public class FlipHandler implements EventHandler<ActionEvent> {
  private int numClicked = 0;
  private Scene scene;
  
  public FlipHandler(Scene scene) {
   this.scene = scene; 
  }
  
  public void handle(ActionEvent e) {
    if (numClicked % 2 == 0) {
     scene.setFill(Color.RED); 
    }
    else {
     scene.setFill(Color.GREEN); 
    }
    numClicked++;
  }
  
}