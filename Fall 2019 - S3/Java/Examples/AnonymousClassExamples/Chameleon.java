import javafx.application.Application;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import java.util.Random;
import javafx.beans.*;
import javafx.beans.value.*;
import javafx.event.*;

public class Chameleon extends Application {
@Override
public void start(Stage stage) {
//container and layout
Group root = new Group();
//scene is associated with container, dimensions
Scene scene = new Scene(root, 400, 300);
scene.setFill(Color.WHITE);

VBox vbox = new VBox();
root.getChildren().add(vbox);

Button turnGreen = new Button("Green");
turnGreen.setOnAction(new TurnFixedHandler(scene, Color.GREEN));

Button turnRed = new Button("Red");
turnRed.setOnAction(new TurnFixedHandler(scene, Color.RED));

Button turnRandom = new Button("Random per program");

Random r = new Random();

turnRandom.setOnAction(new TurnFixedHandler(scene, Color.rgb(r.nextInt(256), r.nextInt(256), r.nextInt(256))));

Button turnRandom2 = new Button("Random");
turnRandom2.setOnAction (new TurnRandomHandler(scene));

Button flip = new Button("Reverse");
flip.setOnAction(new FlipHandler(scene));

vbox.getChildren().add(turnGreen);
vbox.getChildren().add(turnRed);
vbox.getChildren().add(turnRandom);
vbox.getChildren().add(turnRandom2);
vbox.getChildren().add(flip);

VBox vbox2 = new VBox();
Button b1 = new Button("Original text");
Button b2 = new Button("Click me to change button 1");
b2.setOnAction(new ChangeAButtonsTextHandler(b1));
Button b3 = new Button("Change the text of ALL the  buttons");


b3.setOnAction(
               new EventHandler<ActionEvent>() {
  public void handle(ActionEvent e) {
   b1.setText("Changed");
   b2.setText("Different");
   b3.setLayoutX(200);
   scene.setFill(Color.PURPLE);
   vbox.getChildren().add(new Button("Magic"));
  }
               
}
               );
Button b4 = new Button("From anonymous in a different method");
b4.setOnAction(generateAction(b1,b2, b3, scene, vbox));

vbox2.getChildren().addAll(b1, b2, b3, b4);
root.getChildren().add(vbox2);
vbox2.setLayoutX(200);


TextField tf = new TextField("You've clicked 0 times");
Button b5 = new Button("Click me to see something magical!");

tf.textProperty().addListener( 
  new ChangeListener<String>() {
    public void changed(ObservableValue<? extends String> s, String old, String newV) {
      System.out.println(old + " " + newV);
    }
  }
);

b5.setOnAction( new EventHandler<ActionEvent>() {
  private int i = 0;
  public void handle(ActionEvent e) {
    i++;
    tf.setText(tf.getText() + " " + i);
  
  }
}
               );
vbox2.getChildren().addAll(tf, b5);

//associate scene to stage and show
stage.setTitle("My JavaFX Application");
stage.setScene(scene);
stage.show();
}
public static void main(String[] args ) {
Application.launch(args);
}

public EventHandler<ActionEvent> generateAction(Button b1, Button b2, Button b3, Scene scene, VBox vbox) {
  return new EventHandler<ActionEvent>() {
  public void handle(ActionEvent e) {
   b1.setText("Changed");
   b2.setText("Different");
   b3.setLayoutX(200);
   scene.setFill(Color.PURPLE);
   vbox.getChildren().add(new Button("Magic"));
  }
  };
}

}