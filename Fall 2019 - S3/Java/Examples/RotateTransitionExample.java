import javafx.animation.RotateTransition; 
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import static javafx.application.Application.launch; 
import javafx.scene.Group; 
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color; 
import javafx.scene.shape.Polygon; 
import javafx.stage.Stage; 
import javafx.util.Duration; 

// This example is heavily inspired by https://www.tutorialspoint.com/javafx/javafx_animations.htm
public class RotateTransitionExample extends Application { 
   @Override 
   public void start(Stage stage) {      
      //Creating a hexagon 
      Polygon hexagon = new Polygon();        
      
      //Adding coordinates to the hexagon 
      hexagon.getPoints().addAll(new Double[]{        
         200.0, 50.0, 
         400.0, 50.0, 
         450.0, 150.0,          
         400.0, 250.0, 
         200.0, 250.0,                   
         150.0, 150.0, 
      }); 
      //Setting the fill color for the hexagon 
      hexagon.setFill(Color.BLUE); 
       
      //Creating a rotate transition    
      RotateTransition rotateTransition = new RotateTransition(); 
      
      //Setting the duration for the transition 
      rotateTransition.setDuration(Duration.millis(1000)); 
      
      //Setting the node for the transition 
      rotateTransition.setNode(hexagon);       
      
      //Setting the angle of the rotation 
      rotateTransition.setByAngle(360); 
      
      //Setting the cycle count for the transition 
      rotateTransition.setCycleCount(50); 
      
      //Setting auto reverse value to false 
      rotateTransition.setAutoReverse(false); 
      
      //Playing the animation 
      //rotateTransition.play(); 
        
      Button playButton = new Button("Play");
      playButton.setOnAction(
    		  new EventHandler<ActionEvent>() {
    			  public void handle(ActionEvent e) {
    				  rotateTransition.play();
    			  }
    		  }
    		  );
      
      Button pauseButton = new Button("Pause");
      
      pauseButton.setOnAction(
    		  new EventHandler<ActionEvent>() {
    			  public void handle(ActionEvent e) {
    				  rotateTransition.pause();
    			  }
    		  }
    		  );
      
      Label buttonChanging = new Label("The below button will change between pause/play");
      Button changeButton = new Button("Play");
      
      changeButton.setOnAction(
    		  new EventHandler<ActionEvent>() {
    			  boolean isPlay = true; // store whether to be play or pause
    			  
    			  public void handle(ActionEvent e) {
    				  if (isPlay) {
    					  rotateTransition.play();
    					  // change button's display text
    					  changeButton.setText("Pause");
    				  }
    				  else {
    					  rotateTransition.pause();
    					  changeButton.setText("Play");
    					  
    				  }
    				  // flip play vs not play
    				  isPlay = !isPlay;
    			  }
    		  }
    		  );
      
      
      VBox vbox = new VBox();
      vbox.getChildren().addAll(hexagon, playButton, pauseButton, buttonChanging, changeButton);
      
      //Creating a Group object   
      Group root = new Group(vbox); 
         
      //Creating a scene object 
      Scene scene = new Scene(root, 600, 300);   
      
      //Setting title to the Stage 
      stage.setTitle("Rotate transition example "); 
         
      //Adding scene to the stage 
      stage.setScene(scene); 
         
      //Displaying the contents of the stage 
      stage.show(); 
   }      
   public static void main(String args[]){ 
      Application.launch(args); 
   } 
} 