public static void sortPoints(Point[] points) {
	/*
		for i = 0 to points.length - 1
			1. minSpot = Find smallest spot from i to end
			2. Swap points[i] with points[minSpot]

	*/
	/* 4 5 10 3 2 100 3 10 -1 3 5 1 */
	for (int i = 0; i < points.length; i++) {
		int smallestLocation = findMinimum(points, i);
		swap(points, i, smallestLocation);
	}
}

public static int findMinimum(Point[] points, int start) {
	// find where the minimum is
		//double smallest = points[start];
		int smallestIndex = start;
		
		for (int j = start + 1; j < points.length; j++) {
		//	double point1AsValue = points[j].getX() + points[j].getY();
		//	double point2AsValue = points[smallestIndex].getX() + points[smallestIndex].getY();
		//	if (points1AsValue < points2AsValue )) {
		if (points[j].compareTo(points[smallestIndex]) < 0)
			//smallest = points[j].getX();
				smallestIndex = j;
			}
		}
		
		return smallestIndex;
}

public static void swap(Point[] points, int position1, int position2) {
	Point temp = points[position1];
	points[position1] = points[position2];
	points[position2] = temp;
}




public class Point implements Comparable<Point> {
	private double x;
	private double y;


	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return this.x;
	}
	public double getY() { return this.y; }
	
	public double getMagnitude() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	
	public int compareTo(Point other) {
		// return -1 if "this" is less than "other"
		// return 1 if "this" is bigger than "other"
		// return 0 otherwise
		if (this.x < other.x) { return -1; }
		else if (this.x > other.x) { return 1;
		else { return 0; }
		
		// doesn't work since x is a double, which is primitive
		// return this.x.compareTo(other.x);
		
		/*if (this.getMagnitude() < other.getMagnitude()) {
			return -1;
		}
		else if (this.getMagnitude() > other.getMagnitude()) {
			return 1;
		}
		else {return 0; }*/
		
		/* return this.getMagnitude() - other.getMagnitude() */
		
		
	}
}


// other code:
Point[] points = new Point[500];
///......fill out the array

Arrays.sort(points);



// Comparator to sort the points by the X coordinate:

public class PointXComparator implements Comparator<Point> {
	
	public int compare(Point one, Point two) {
		if (one.getX() < two.getX() ) { return -1; }
		else if (one.getX() > two.getX()) { return 1; }
		else { return 0;}
	}
	
}

public class PointYComparator implements Comparator<Point> {
		public int compare(Point one, Point two) {
			return one.getY() - two.getY();
		}
}


Point[] points = new Point[500];
// .... fill out the array

Scanner reader = new Scanner(System.in);
System.out.println("Enter 1 to order by x, 2 to order by y");
int choice = reader.nextInt();
Comparator<Point> comparator;

if (choice == 1) {
	comparator = new PointXComparator();
	
}
else if (choice == 2) {
	comparator = new PointYComparator();

}
else if (choice = 3) {
	comparator = new MagnitudeComparator();
}

Arrays.sort(points, comparator);
print(points);

inside Arrays.sort:

if (comparator.compareTo(array[i], array[j])) 
