import javafx.event.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import java.util.Random;
public class TurnRandomHandler implements EventHandler<ActionEvent> {
  
  private Scene scene;
  private Random r = new Random();
  public TurnRandomHandler(Scene scene) {
    this.scene = scene;
   
  }
  
  public void handle(ActionEvent e) {
    
    this.scene.setFill(Color.rgb(r.nextInt(256), r.nextInt(256), r.nextInt(256)));
  }
  
}