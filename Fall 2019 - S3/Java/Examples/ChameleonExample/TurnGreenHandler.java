import javafx.event.*;
import javafx.scene.*;
import javafx.scene.paint.*;
public class TurnFixedHandler implements EventHandler<ActionEvent> {
  
  private Scene scene;
  private Color color;
  public TurnGreenHandler(Scene scene, Color color) {
    this.scene = scene;
    this.color = color;
  }
  
  public void handle(ActionEvent e) {
    this.scene.setFill(color);
  }
  
}