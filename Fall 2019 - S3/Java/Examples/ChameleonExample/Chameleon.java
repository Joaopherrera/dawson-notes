import javafx.application.Application;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import java.util.Random;

public class Chameleon extends Application
{

    @Override
    public void start(Stage stage) 
    {
        //container and layout
        Group root = new Group();
        //scene is associated with container, dimensions
        Scene scene = new Scene(root, 400, 300);
        scene.setFill(Color.WHITE);

        VBox vbox = new VBox();
        root.getChildren().add(vbox);

        Button turnGreen = new Button("Green");
        turnGreen.setOnAction(new TurnFixedHandler(scene, Color.GREEN));

        Button turnRed = new Button("Red");
        turnRed.setOnAction(new TurnFixedHandler(scene, Color.RED));

        Button turnRandom = new Button("Random per program");

        Random r = new Random();

        turnRandom.setOnAction(new TurnFixedHandler(scene, Color.rgb(r.nextInt(256), r.nextInt(256), r.nextInt(256))));

        Button turnRandom2 = new Button("Random");
        turnRandom2.setOnAction (new TurnRandomHandler(scene));
        vbox.getChildren().add(turnGreen);
        vbox.getChildren().add(turnRed);
        vbox.getChildren().add(turnRandom);
        vbox.getChildren().add(turnRandom2);

        //associate scene to stage and show
        stage.setTitle("My JavaFX Application");
        stage.setScene(scene);
        stage.show();
    }
    
    public static void main(String[] args)
    {
        Application.launch(args);
    }
}