public class LinearAlgebraTest
{
    public static void main(String[] args)
    {
    	Point3D p1 = new Point3D(2, 5, 7);
        Point3D p2 = new Point3D(1, 4, 6);
        Point3D p3 = new Point3D(5, 4, 4);
        Point3D p4 = new Point3D(3, 0, 3);
        Point3D p5 = new Point3D(6, 5, 2);

        Point3D[] points = {p1, p2, p3, p4, p5};
        
//        for(int i = 0; i < points.length; i++)
//        {
//            System.out.println(points[i].getX() + " ");
//        }
//        System.out.println("");
//
//        LinearAlgebra.sortByXCoordinate(points);
//
//        LinearAlgebra.genericSort(points, new Point3DXSorter());
//
//        for(int i = 0; i < points.length; i++)
//        {
//            System.out.println(points[i].getX() + " ");
//        }

        int result = pow(8);
        System.out.println(result);
    }

    public static int pow(int n)
    {
        if (n == 0)
        {
            return 1;
        }
        else if(n == 1)
        {
            return 2;
        }
        return pow(n-1)*2;
    }
}
