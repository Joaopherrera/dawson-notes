public class Point3DXSorter implements IPoint3DSorter
{
    public int compareTo(Point3D one, Point3D two)
    {
        if(one.getX() < two.getX())
        {
            return -1;
        }
        else if(one.getX() > two.getX())
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
