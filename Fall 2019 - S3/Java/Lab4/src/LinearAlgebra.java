import java.awt.*;

public class LinearAlgebra
{
    /** Method to sort the Points3D array using Selection Sort **/
    public static void sortByXCoordinate(Point3D[] points)
    {
        for(int i = 0; i < points.length; i++)
        {
            int min = findMinX(points,i);

            Point3D temp = points[i];
            points[i] = points[min];
            points[min] = temp;
        }
    }

    public static void sortByYCoordinate(Point3D[] points)
    {
        for(int i = 0; i < points.length; i++)
        {
            int min = findMinY(points,i);

            Point3D temp = points[i];
            points[i] = points[min];
            points[min] = temp;
        }
    }

    public static void genericSort(Point3D[] points, IPoint3DSorter sorter)
    {

    }

    /** --> HELPER METHODS <-- **/

    /** Method to find the smallest number inside the array **/
    public static int findMinX(Point3D[] points, int start)
    {
        int min = start;
        for(int i = start + 1; i < points.length; i++)
        {
            if(points[i].getX() < points[min].getX())
            {
                min = i;
            }
        }
        return min;
    }

    public static int findMinY(Point3D[] points, int start)
    {
        int min = start;
        for(int i = start + 1; i < points.length; i++)
        {
            if(points[i].getY() < points[min].getY())
            {
                min = i;
            }
        }
        return min;
    }
}
