public interface IPoint3DSorter
{
    int compareTo(Point3D one, Point3D two);
}
