public class Point3D
{
    private int x;
    private int y;
    private int z;

    public Point3D(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /** Getters **/
    public int getX()
    {
        return this.x;
    }
    public int getY()
    {
        return this.y;
    }
    public int getZ()
    {
        return this.z;
    }

    public double magnitude()
    {
        double m;
        m = Math.sqrt((Math.pow(x, 2.0) + Math.pow(y, 2.0) + Math.pow(z, 2.0)));
        return m;
    }

}
