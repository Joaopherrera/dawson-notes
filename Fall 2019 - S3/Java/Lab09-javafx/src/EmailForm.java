import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class EmailForm 
{
	private TextField emailTextField;
	private Text actionTarget;
	
	public EmailForm()
	{
		emailTextField = new TextField();
		actionTarget = new Text();
	}
	
	public void start(Stage stage)
	{
		//Set Window's Title
		stage.setTitle("Get Email");
		
		//Invoke helper method which return elements in a layout
		GridPane root = createUserInterface();
		
		//instantiate the Scene, indicating the root layout and dimensions
		Scene scene = new Scene(root, 350, 275);
		
		//set the scene in the stage and display
		stage.setScene(scene);
		stage.show();
	}
	
	private GridPane createUserInterface() 
	{
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));
		
		Text scenetitle = new Text("Welcome");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);
		Label email = new Label("Enter email address:");
		grid.add(email, 0, 1);
		grid.add(emailTextField, 1, 1);
		
		Button btn = new Button("Sign in");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn); grid.add(hbBtn, 1, 4);
		
		actionTarget.setId("actiontarget"); 
		grid.add(actionTarget, 0, 6, 2, 1);
		
		btn.setOnAction(this::signInButtonHandler);
		
		return grid;
	}
	
	private void signInButtonHandler(ActionEvent e) 
	{
		actionTarget.setText("Welcome " + this.emailTextField.getText());
	}
}
