import javafx.application.Application;
import javafx.stage.*;


public class Main extends Application
{
	private EmailForm gui;
	
	@Override 
	public void start(Stage stage) 
	{ 
		gui.start(stage);
	} 
	
	@Override
	public void init()
	{
		gui = new EmailForm();
	}
	
	public static void main(String[] args) 
	{ 
		Application.launch(args);
	}
}
