int x = 3;
double y = x;


Car v; 

Interface: Never create an object of the base type

Class/Inheritance: Create an object of the base type

public interface IVehicle {
	void turn();
	void go();
	void stop();
}


public abstract class Vehicle implements IVehicle {
	public void turn() {
		// concrete code
	}

	public abstract void go();
	public abstract void stop();

}

public class Bicycle extends Vehicle {
	public void go() {
		S.o.p("GOOO");
	}

	public void stop() {
		S.o.p("Stop");
	}
	
	public void ringBell() {
		S.o.p("riing");
	}
}

public class RoadBicycle extends Bicycle {

	public RoadBicycle(int numberGears) {
	
	}

	public void go() {
		super.go();
		super.ringBell();
		S.o.p("Going very fast!");
	}
	
	public void race() {
		S.o.p("Race");
	}
}

super();


public class SpecializedRoadBicycle extends RoadBicycle {
	public SpecializedRoadBicycle(int speed) {
		this(speed, 30);
		
		// could add some code here
	}
	
	
	public SpecializedRoadBicycle(int speed, int numberGears) {
		super(numberGears);
		// do 100 different things
	}
	
	public void go() {
		super.go();
			// do some more stuff
	}

	public void stop() {
		super.stop();
		super.speedUp(); -->compiler error
	}

	
}





Bicycle y = new Bicycle();

if (y instanceof Vehicle) {

}

IVehicle v = new Vehicle(); // Vehicle is abstract, can't create--->compiler

Vehicle v = new Bicycle();
IVehicle v2 = v;
v2.stop() ---> call stop in the bicycle class

Vehicle v3 = new Bicycle();
if (userEntersTheNumberOne) {
	v3 = new RoadBicycle();
}
v3.go(); ----> "Going very fast" version
v3.stop(); --->  "Stop"

v3.race(); ---> Compiler error

if (v3 instanceof RoadBicycle) {
	((RoadBicycle)v3).race();

}


Vehicle v4 = new Bicycle();
((RoadBicycle)v4).race();

((String)v4).length();


RoadBicycle r = new RoadBicycle();
((Bike)r).race(); ---> compiler error, because Bikes don't have race()

A)((Bike)r).stop();

B)Bike b2 = (Bike)r;
b2.stop();


((Bike)r).go(); ---> "Going very fast"
r.go() ---> "Going very fast"


RoadBicycle r = new RoadBicycle();
r.super.go()---> no such syntax

// runtime exception
Bicycle b = (RoadBicycle)new Bicycle();

same as:
Bicycle b2 = new Bicycle();
Bicycle b = (RoadBicycle) b2;

Bicycle b = b2; // fine

RoadBicycle r2 = (RoadBicycle)new Bicycle();



Runtime:
N = size of the array

Selection Sort : O(N*N) ---> 10000 * 10000 
MergeSort: O(N*Log(N)) ----> 10000 * 13
Linear search : O(N)
Merge: O(N)


Log_2(10000) ---> 2^what power = 10000  2^13 = 10000
Log_2(20000) ---->


// given 2 *sorted* Lists, returns 1 sorted list
// {1,2,3,10,20} vs {2, 4, 6, 10, 30}
// 1, 2, 2, 3,4
public List<Integer> merge(ArrayList<Integer> list1, ArrayList<Integer> list2) {
	// does not use recursion
	int pos1 = 0;
	int pos2 = 0;
	
	List<Integer> merged = new ArrayList<Integer>(list1.size() + list2.size());
	
	
	while (pos1 < list1.size() || pos2 < list2.size() ) {
		if (pos1 >= list1.size()) {
			merged.add(list2.get(pos2);
			pos2++;
		}
		else if (pos2 >= list2.size()) {
			merged.add(list1.get(pos1));
			pos1++;
		}
		else if (list1.get(pos1) < list2.get(pos2)) {
			merged.add(list1.get(pos1));
			pos1++;
		}
		else {
			merged.add(list2.get(pos2));
			pos2++;
		}
	
	}
	return merged;
}

public List<Integer> mergeSort(List<Integer> list) {
	if (list.size() <= 1) {
		return list;
	}

	// create an ArrayList of the first half of the list
	List<Integer> firstHalf = list.sublist(0, list.size() / 2);
	List<Integer> firstHalfSorted = mergeSort(firstHalf);
	ArrayList<Integer> firstHalfSortedAsArrayList = new ArrayList<Integer>(firstHalfSorted);
	
	List<Integer> secondHalf = list.sublist(list.size() / 2, list.size());
	List<Integer> secondHalfSorted = mergeSort(secondHalf);
	ArrayList<Integer> secondHalfSortedAsArrayList = new ArrayList<Integer>(secondHalfSorted);
	
	return merge((firstHalfSortedAsArrayList, secondHalfSortedAsArrayList);
}

What we know:
it works on list.size = ????
1
2
3


mergeSort(16)
mergesort(8) + mergeSort(8)
mergeSort(4) + mergeSort(4) + megeSort(4), Mergesort(4)


// Generic merge method:
//x.compareTo(y) < 0 if x < y

public <T extends Comparable> List<T> merge(ArrayList<T> list1, ArrayList<T> list2) {
	// does not use recursion
	int pos1 = 0;
	int pos2 = 0;
	
	List<T> merged = new ArrayList<T>(list1.size() + list2.size());
	
	
	while (pos1 < list1.size() || pos2 < list2.size() ) {
		if (pos1 >= list1.size()) {
			merged.add(list2.get(pos2);
			pos2++;
		}
		else if (pos2 >= list2.size()) {
			merged.add(list1.get(pos1));
			pos1++;
		}
		
		else if (list1.get(pos1).compareTo(list2.get(pos2)) < 0) {
		//else if (list1.get(pos1) < list2.get(pos2)) {
			merged.add(list1.get(pos1));
			pos1++;
		}
		else {
			merged.add(list2.get(pos2));
			pos2++;
		}
	
	}
	return merged;
}


public <T extends Comparable> List<T> mergeSort(List<T> list) {
	if (list.size() <= 1) {
		return list;
	}

	// create an ArrayList of the first half of the list
	List<T> firstHalf = list.sublist(0, list.size() / 2);
	List<T> firstHalfSorted = mergeSort(firstHalf);
	ArrayList<T> firstHalfSortedAsArrayList = new ArrayList<T>(firstHalfSorted);
	
	List<T> secondHalf = list.sublist(list.size() / 2, list.size());
	List<T> secondHalfSorted = mergeSort(secondHalf);
	ArrayList<T> secondHalfSortedAsArrayList = new ArrayList<T>(secondHalfSorted);
	
	return merge((firstHalfSortedAsArrayList, secondHalfSortedAsArrayList);
}







public <T> List<T> merge(ArrayList<T> list1, ArrayList<T> list2, Comparator<T> comparator) {
	// does not use recursion
	int pos1 = 0;
	int pos2 = 0;
	
	List<T> merged = new ArrayList<T>(list1.size() + list2.size());
	
	
	while (pos1 < list1.size() || pos2 < list2.size() ) {
		if (pos1 >= list1.size()) {
			merged.add(list2.get(pos2);
			pos2++;
		}
		else if (pos2 >= list2.size()) {
			merged.add(list1.get(pos1));
			pos1++;
		}
		
		else if (comprator.compare(list1.get(pos1),list2.get(pos2)) < 0) {
		//else if (list1.get(pos1) < list2.get(pos2)) {
			merged.add(list1.get(pos1));
			pos1++;
		}
		else {
			merged.add(list2.get(pos2));
			pos2++;
		}
	
	}
	return merged;
}


// Cat extends Animal
ArrayList<Animal> foo = new ArrayList<Cat>(); // does not compiler

ArrayList<Animal> animals = new ArrayList<Animal>();
animals.add(new Cat());


public <T extends Animal> void print(ArrayList<T> animals) {

//public void print(Animal a) {

public void print(ArrayList<? extends Animal> animals ) {
	//animals.get(0) ---> Animal
	//animals.get(0).meow() ---> compiler error!
	for (Animal a : animals) {
		if (a instanceof Cat) {
			S.o.p(a.getName();
		}
	}

}


print(new ArrayList<Cat>()) ; // compile with the ? version

Animal[] animals = new Cat[10];
animals[0] = new Dog();

Collection : interface

Set : interface
List : interface


HashSet implemented Set
ArrayList implemented List
LinkedList implement List

Map
HashMap

(this.name + this.price).hashCode();


printStars(int n ) {
	if (n > 0) {
		S.o.p("*");
		S.o.p("*");
		printStars(n-2);
		printStars(n-1);
		printStars(n-2);
	}
}

n = -1 : nothing
n = 0 : nothing
n = 1: print 1 star, print 1 star, ps(-1), ps(0), ps(-1) ---> prints 2 stars
n = 2 : print 1 star, print 1 star, ps(0), ps(1), ps(0)---> print 4 stars
n  = 3: print 1 star, print 1 star, ps(1), ps(2), ps(1) --> 2 + 2 + 4 + 2 = 10 stars


10 + 24




public <t extends collection> thing(t thingone, t thingtwo,extends comparable)