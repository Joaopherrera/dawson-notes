import javax.swing.*;
import javax.swing.plaf.synth.SynthTextAreaUI;
import java.sql.*;
import java.util.ArrayList;

public class JLServices
{
    public static void main(String[] args)
    {
        try
        {
            Connection connection = getConnection();

            String isbnExample = "1059831198";

            printBook(createBookFromISBN(isbnExample, connection));

            findBookByTitle("MICKEY", connection);

            calculatePercentProfit(isbnExample, connection);

            findMostProfitableBooks(2.50, connection);

        }
        catch (SQLException sqlException)
        {
            System.out.println("Something went wrong!");
            System.out.println(sqlException.getMessage());
        }

    }


    //Method for question 1
    public static Connection getConnection() throws SQLException
    {
        String username = "SYSTEM";
        String password = "Nh3206ea?";

        Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE",username,password);

        return connection;
    }

    //Method for question Number 3*
    public static Book createBookFromISBN(String isbn, Connection connection) throws SQLException
    {
        System.out.println("----- Question Number 3 -----");

        String query = "SELECT * FROM Books WHERE ISBN = " + isbn;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();

        resultSet.next();

        Book book = new Book(resultSet.getString("isbn"),resultSet.getString("title"),resultSet.getString("PubDate"),resultSet.getString("pubId"), getPublishers(resultSet.getString("isbn"),connection),resultSet.getString("cost"),resultSet.getString("retail"),resultSet.getString("discount"),resultSet.getString("category"), createAuthors(resultSet.getString("isbn"),connection));



        preparedStatement.close();
        return book;
    }

    //Method for question Number 5*
    public static void findBookByTitle(String input, Connection connection) throws SQLException
    {
        System.out.println("----- Question Number 5 -----");

        ArrayList<Book> Books = new ArrayList<>();

        String query = "SELECT * FROM Books WHERE Books.Title LIKE '" + "%" + input + "%" + "'";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery(query);

        while(resultSet.next())
        {
            Book newBook = new Book(resultSet.getString("isbn"), resultSet.getString("title"), resultSet.getString("PubDate"), resultSet.getString("pubId"), getPublishers(resultSet.getString("isbn"),connection), resultSet.getString("cost"), resultSet.getString("retail"), resultSet.getString("discount"), resultSet.getString("category"), createAuthors(resultSet.getString("isbn"),connection));
            Books.add(newBook);
        }

        System.out.println("Number of books: " + Books.size());

        //Loop to print all of the books
        for(Book book : Books)
        {
            printBook(book);
        }

        preparedStatement.close();
    }

    //Method for question Number 8*
    public static double calculatePercentProfit(String isbn, Connection connection) throws SQLException
    {
        System.out.println("----- Question Number 8 Part 1 -----");

        String query = "SELECT * FROM Books WHERE Books.ISBN =" + isbn;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery(query);

        resultSet.next();
        Book book = new Book(resultSet.getString("isbn"),resultSet.getString("title"),resultSet.getString("pubDate"), getPublishers(resultSet.getString("isbn"),connection), resultSet.getString("pubId"),resultSet.getString("cost"),resultSet.getString("retail"),resultSet.getString("discount"),resultSet.getString("category"),createAuthors(resultSet.getString("isbn"),connection));

        double percentage = ((Double.parseDouble(book.retail)-Double.parseDouble(book.cost))/(Double.parseDouble(book.cost)*100));

        System.out.println("The percent profit is:" + percentage);

        preparedStatement.close();
        return percentage;
    }

    public static void findMostProfitableBooks(double percentage, Connection connection) throws SQLException
    {
        System.out.println("----- Question Number 8 Part 2 -----");

        ArrayList<Book> mostProfitable = new ArrayList<>();

        String query = "SELECT * FROM Books";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery(query);

        while(resultSet.next())
        {
            if(calculatePercentProfit(resultSet.getString("ISBN"), connection) > percentage)
            {
                mostProfitable.add(new Book(resultSet.getString("isbn"),resultSet.getString("title"),resultSet.getString("pubDate"), getPublishers(resultSet.getString("isbn"),connection), resultSet.getString("pubId"),resultSet.getString("cost"),resultSet.getString("retail"),resultSet.getString("discount"),resultSet.getString("category"),createAuthors(resultSet.getString("isbn"),connection)));
            }
        }

        preparedStatement.close();
    }

    private static ArrayList<Author> createAuthors(String isbn, Connection connection) throws SQLException
    {
        ArrayList<Author> authors = new ArrayList<>();

        String query = "Select * From Author join BOOKAUTHOR on Author.AuthorID = BOOKAUTHOR.AuthorID where BOOKAUTHOR.ISBN = " + isbn ;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery(query);

        while(resultSet.next())
        {
            Author newAuthor = new Author(resultSet.getString("Lname"),resultSet.getString("Fname"),resultSet.getString("AuthorID"));
            authors.add(newAuthor);
        }

        preparedStatement.close();
        return authors;
    }

    private static String getPublishers(String isbn,Connection connection) throws SQLException
    {
        String publishers = "";
        String query = "Select * From Publisher join Books on Books.PubID = Publisher.PubID where Books.ISBN = " + isbn;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery(query);

        while(resultSet.next())
        {
            publishers = publishers + " " + resultSet.getString("Name");
        }

        preparedStatement.close();
        return publishers;
    }

    //this will print every field of the book object (the fields are public to save some time)
    private static void printBook(Book book)
    {
        System.out.println("Title: " + book.title);
        System.out.println("Isbn: " + book.ISBN);
        System.out.println("Retail: " + book.retail);
        book.printAuthors();
        System.out.println("Publisher: " + book.publisher);
        System.out.println(" ");
    }
}
