import java.util.ArrayList;

public class Book
{
    public String ISBN;
    public String title;
    public String pubDate;
    public String pubID;
    public String publisher;
    public String cost;
    public String retail;
    public String discount;
    public String category;
    public ArrayList<Author> authors;



    public Book(String ISBN, String title, String pubDate, String pubID, String publisher, String cost, String retail, String discount, String category, ArrayList<Author> authors)
    {
        this.ISBN = ISBN;
        this.title = title;
        this.pubDate = pubDate;
        this.pubID = pubID;
        this.publisher = publisher;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
        this.authors = authors;
    }

    public void printAuthors()
    {
        for (Author author: this.authors)
        {
            System.out.println("Author Name :" + author.Fname + " " + author.Lname);
        }
    }
}
