public class Author
{
    public String authorID;
    public String Fname;
    public String Lname;

    public Author(String fname, String lname, String authorID)
    {
        this.authorID = authorID;
        this.Fname = fname;
        this.Lname = lname;
    }
}
