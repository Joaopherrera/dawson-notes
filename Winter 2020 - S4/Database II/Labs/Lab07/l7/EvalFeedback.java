import java.sql.*;
import java.util.Scanner;

public class EvalFeedback {

	String username;
	String password;
	Connection conn;


	public EvalFeedback (String username, String password) {
		this.username = username;
		this.password = password;
		
	}
	
	public Connection getConn() throws SQLException {
		if (conn == null) {
			String url = "jdbc:oracle:thin:@198.168.52.73:1522/pdborad12c.dawsoncollege.qc.ca";
			this.conn = DriverManager.getConnection(url, username, password);
		}
		
		return conn;

	}


	public static void main(String[] args) throws SQLException {
		
		
		String  username = args[0];
		String password = args[1];
		EvalFeedback feedback = new EvalFeedback(username, password);
		
		EvalLogin login = new EvalLogin(feedback.getConn());
		
		Scanner reader = new Scanner(System.in); 
		System.out.println("Enter 1 to create new user or 2 to login and view results");
		int choice = reader.nextInt();
		if (choice == 1)
		{
			login.newUser();
		}else if (choice == 2){
			String user = login.login();
			if (user == null){
				return;
			}
			System.out.println("Your user info is: ");
			Statement stmt = feedback.getConn().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM students WHERE username = \'" + user + "\'");
			rs.next();
			System.out.println("username: " + rs.getString(1));
			System.out.println("password: " + rs.getString(3));
			System.out.println("grade: " + rs.getString(2));
			stmt.close();
						
		}
		


	}
	
	
	public void getFeedback() {
		
		
	}
	

}
