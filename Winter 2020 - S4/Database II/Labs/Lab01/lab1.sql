DECLARE
    var VARCHAR2(10) := 'Hello';
BEGIN
    DBMS_OUTPUT.PUT_LINE(var);
END;

-- Practice PL/SQL Questions --
-- 1 --
DECLARE
    rPrice NUMBER(5,2);
    rISBN VARCHAR2(10);
BEGIN
    SELECT Retail, ISBN INTO rPrice, rISBN FROM Books
    WHERE ISBN = '1059831198';

    DBMS_OUTPUT.PUT_LINE(rISBN || ' and ' || rPrice);
END;

-- 2 --
DECLARE
    vOrder# NUMBER(4) := '1021';
    vCustomer# NUMBER(4) := '1003';
    vOrderDate DATE := '23-JUN-20';
    vShipDate DATE := '27-JUN-20';
    vShipStreet VARCHAR2(18) := '1201 ORANGE AVE';
    vShipCity VARCHAR2(15) := 'SP';
    vShipState VARCHAR2(2) := 'SP';
    vShipZip VARCHAR2(5) := '97635';
    vShipCost NUMBER(4,2) := 30.50;
BEGIN
    INSERT INTO Orders VALUES (vOrder#, vCustomer#, vOrderDate, vShipDate, vShipStreet, vShipCity, vShipState, vShipZip, vShipCost);
    DBMS_OUTPUT.PUT_LINE('Record Created.');
END;

-- 3 --
DECLARE
    resultCategory VARCHAR2(10);
    resultRetail NUMBER(5,2);
    bookISBN VARCHAR2(10) := '1059831198';
BEGIN
    SELECT Category, Retail INTO resultCategory, resultRetail FROM Books
    WHERE ISBN = bookISBN;

    IF resultCategory = 'COMPUTER' THEN
        DBMS_OUTPUT.PUT_LINE('ISBN ' || bookISBN || '. Category: ' || resultCategory || '. Price During Sale: ' || (resultRetail-(resultRetail*0.7)));

    ELSIF resultCategory = 'FITNESS' THEN
        DBMS_OUTPUT.PUT_LINE('ISBN ' || bookISBN || '. Category: ' || resultCategory || '. Price During Sale: ' || (resultRetail-(resultRetail*0.6)));

    ELSIF resultCategory = 'BUSINESS' THEN
        DBMS_OUTPUT.PUT_LINE('ISBN ' || bookISBN || '. Category: ' || resultCategory || '. Price During Sale: ' || (resultRetail-(resultRetail*0.8)));

    ELSE
        DBMS_OUTPUT.PUT_LINE('ISBN ' || bookISBN || '. Category: ' || resultCategory || '. Price During Sale: ' || (resultRetail-(resultRetail*0.9)));
    END IF;
END;

-- 4 --
DROP TABLE Roots;
CREATE TABLE Roots(
    n NUMBER(4,0),
    rootn NUMBER(10,3)
);

BEGIN
    FOR num IN 1 .. 1000 LOOP
        INSERT INTO Roots VALUES (num, SQRT(num));
    END LOOP;
END;

-- Graded Questions --
-- 1 --
DECLARE
    lower NUMBER(3,0) := 0;
    upper NUMBER(3,0) := 10;
    vCount NUMBER(3,0);
BEGIN
    WHILE upper <= 100
    LOOP
        SELECT COUNT(*) INTO vCOUNT FROM Books WHERE Retail BETWEEN lower AND upper;
        DBMS_OUTPUT.PUT_LINE('Between ' || lower || ' and ' || upper || ': ' || vCount);
        lower := lower + 10;
        upper := upper + 10;
    END LOOP;
END;

-- 2 --
DECLARE
    vReferredPoints NUMBER(4);
    vOrdersPoints NUMBER(4);
    vPoints NUMBER(4);
    vCustomer# NUMBER(4) := 1001;
    vCustomerName VARCHAR2(10);
BEGIN
    WHILE vCustomer# <= 1020 LOOP
        SELECT COUNT(*) INTO vReferredPoints FROM Customers WHERE vCustomer# = Referred;
        SELECT SUM(OrderItems.Quantity) INTO vOrdersPoints FROM Orders
        INNER JOIN ORDERITEMS ON Orders.ORDER# = ORDERITEMS.ORDER#
        WHERE vCustomer# = Orders.Customer#;

        IF vOrdersPoints IS NULL THEN
            vOrdersPoints := 0;
        END IF;

        vReferredPoints := vReferredPoints * 500;
        vOrdersPoints := vOrdersPoints * 100;
        vPoints := vReferredPoints + vOrdersPoints;

        SELECT FirstName INTO vCustomerName FROM Customers WHERE vCustomer# = Customer#;
        DBMS_OUTPUT.PUT_LINE(vCustomerName || '. Number of points: ' || vPoints);

        IF vPoints > 0 AND vPoints < 1000 THEN
            DBMS_OUTPUT.PUT_LINE('BRONZE TIER');

        ELSIF vPoints > 1001 AND vPoints < 2000 THEN
            DBMS_OUTPUT.PUT_LINE('SILVER TIER');

        ELSIF vPoints > 2000 THEN
            DBMS_OUTPUT.PUT_LINE('GOLD TIER');
        END IF;

        DBMS_OUTPUT.PUT_LINE('-----------------------------------------------------------------------');

        vCustomer# := vCustomer# + 1;
    END LOOP;
END;