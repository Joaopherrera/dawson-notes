-- LAB 02 --

-- Practice Questions --
-- 1. --
DECLARE
    TYPE numberarray IS VARRAY(0) OF NUMBER;
    multiples numberarray := numberarray();
    sum NUMBER(7) := 0;
    counter NUMBER(3) := 0;
BEGIN
    FOR num IN 1 .. 1000 LOOP
         IF MOD(num,3) = 0 OR MOD(num,5)= 0 THEN
             multiples.EXTEND;
             multiples(counter) := num;
             counter := counter + 1;
             sum := sum + num;
         END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Total Sum: ' || sum);
END;

-- 2. --

-- 3. --

--Graded Questions --
-- 4. --

-- 5. --