-- Lab 05 --
-- 1 --


-- 2 (GRADED) --
CREATE OR REPLACE PACKAGE analytics AS
        TYPE booksPurchased IS VARRAY(1) OF NUMBER;
        FUNCTION book_purchasers(bookIsbn IN VARCHAR2) RETURN booksPurchased;
END analytics;

-- 4 (GRADED) --
CREATE OR REPLACE TRIGGER toUpperCaseTitle BEFORE INSERT OR UPDATE
ON Books
FOR EACH ROW
    BEGIN
        :New.Title := UPPER(:New.Title);
    END;

-- 7 (GRADED) --
CREATE OR REPLACE TRIGGER removeChildrenBook20 AFTER INSERT
ON Books
FOR EACH ROW
DECLARE
    OVERPRICED_KIDS_BOOK EXCEPTION;
    PRAGMA EXCEPTION_INIT(OVERPRICED_KIDS_BOOK, -20001);
BEGIN
    IF (:NEW.Category = 'CHILDREN') AND (:NEW.Cost > 20.00 ) THEN
        RAISE_APPLICATION_ERROR(-20001, 'OVERPRICED_KIDS_BOOKS');
    END IF;
END;
