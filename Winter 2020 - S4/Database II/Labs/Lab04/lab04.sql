-- Lab 04 --
-- Practice Questions --
-- 1. --

-- 2. --

-- 3. --

-- Graded Questions --
-- 1. --
CREATE OR REPLACE FUNCTION GetAuthorName(visbn IN VARCHAR2)
RETURN VARCHAR2
AS
    returnName VARCHAR2(30);
    NO_AUTHORS EXCEPTION;
    PRAGMA EXCEPTION_INIT(NO_AUTHORS, -20001);
    TOO_MANY_AUTHORS EXCEPTION;
    PRAGMA EXCEPTION_INIT (TOO_MANY_AUTHORS, -20002);
BEGIN
    SELECT (Fname || ' ' || Lname) INTO returnName FROM Author
    INNER JOIN BookAuthor ON AUTHOR.AUTHORID = BOOKAUTHOR.AUTHORID
    INNER JOIN Books ON BOOKAUTHOR.ISBN = BOOKS.ISBN
    WHERE Books.ISBN = visbn;

    RETURN returnName;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20001, 'NO_AUTHOR');
        returnName := 'No author found';
        RETURN returnName;

    WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR(-20002, 'TOO_MANY_AUTHORS');
        returnName := 'Too many authors';
        return returnName;
END;

BEGIN
    --Test to normal case
    DBMS_OUTPUT.PUT_LINE(GetAuthorName('9959789321'));

    --Test for more than one author
    --DBMS_OUTPUT.PUT_LINE(GetAuthorName('1059831198'));

    --Test for no authors
    --DBMS_OUTPUT.PUT_LINE(GetAuthorName('1059831111'));

    DBMS_OUTPUT.PUT_LINE('---------------------------------------------');
END;

-- 2. --
CREATE OR REPLACE PROCEDURE DisplayISBNandNames
AS
    CURSOR bookInfo IS SELECT Title, ISBN FROM Books;
    bookName Books.Title%TYPE;
    bookISBN Books.ISBN%TYPE;
    bookAuthorName VARCHAR2(30);
    errorNum NUMBER(5);
BEGIN
    DBMS_OUTPUT.PUT_LINE('Start.....');
    DBMS_OUTPUT.PUT_LINE('---------------------------------------------------');

    OPEN bookInfo;
    FOR row IN (SELECT Title, ISBN FROM Books) LOOP
        BEGIN
            FETCH bookInfo INTO bookName, bookISBN;
            bookAuthorName := GetAuthorName(bookISBN);
            DBMS_OUTPUT.PUT_LINE('Book Title: ' || bookName);
            DBMS_OUTPUT.PUT_LINE('Book Author: ' || bookAuthorName);

            --bookAuthorName := GetAuthorName(row.ISBN);
            --DBMS_OUTPUT.PUT_LINE('Book Title: ' || row.TITLE);
            --DBMS_OUTPUT.PUT_LINE('Book Author: ' || bookAuthorName);

            EXCEPTION
                WHEN OTHERS THEN
                    errorNum := SQLCODE;
                    IF errorNum = -20002 THEN
                        DBMS_OUTPUT.PUT_LINE('Book Title: ' || bookName);
                        DBMS_OUTPUT.PUT_LINE('Multiple Authors!');
                    END IF;
        END;

        DBMS_OUTPUT.PUT_LINE('---------------------------------------------------');
    END LOOP;

    CLOSE bookInfo;
    DBMS_OUTPUT.PUT_LINE('End.....');
END;

BEGIN
    DisplayISBNandNames;
END;