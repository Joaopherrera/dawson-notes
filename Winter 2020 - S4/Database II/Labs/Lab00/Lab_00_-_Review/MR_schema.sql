-- Table: Customer
CREATE TABLE Customer (
    cust_id varchar2(10)  NOT NULL,
    name varchar2(100)  NOT NULL,
    phone varchar2(10)  NOT NULL,
    CONSTRAINT Customer_pk PRIMARY KEY (cust_id)
) ;

-- Table: Employee
CREATE TABLE Employee (
    emp_id varchar2(10)  NOT NULL,
    name varchar2(100)  NOT NULL,
    manager_id varchar2(10)  NULL,
    CONSTRAINT Employee_pk PRIMARY KEY (emp_id)
) ;

-- Table: Movie
CREATE TABLE Movie (
    movie_id varchar2(10)  NOT NULL,
    title varchar2(100)  NOT NULL,
    category varchar2(100)  NOT NULL,
    qty_available number(2,0)  NOT NULL,
    rental_cost number(3,2)  NOT NULL,
    late_fee number(3,2)  NOT NULL,
    CONSTRAINT movie_id PRIMARY KEY (movie_id)
) ;

-- Table: Rental
CREATE TABLE Rental (
    rental_id varchar2(10)  NOT NULL,
    movie_id varchar2(10)  NOT NULL,
    emp_id varchar2(10)  NOT NULL,
    cust_id varchar2(10)  NOT NULL,
    due_date date  NOT NULL,
    returned char(1)  NOT NULL,
    CONSTRAINT Rental_pk PRIMARY KEY (rental_id)
) ;

-- foreign keys
-- Reference: Employee_Employee (table: Employee)
ALTER TABLE Employee ADD CONSTRAINT Employee_Employee
    FOREIGN KEY (manager_id)
    REFERENCES Employee (emp_id);

-- Reference: Rental_Customer (table: Rental)
ALTER TABLE Rental ADD CONSTRAINT Rental_Customer
    FOREIGN KEY (cust_id)
    REFERENCES Customer (cust_id);

-- Reference: Rental_Employee (table: Rental)
ALTER TABLE Rental ADD CONSTRAINT Rental_Employee
    FOREIGN KEY (emp_id)
    REFERENCES Employee (emp_id);

-- Reference: Rental_Movie (table: Rental)
ALTER TABLE Rental ADD CONSTRAINT Rental_Movie
    FOREIGN KEY (movie_id)
    REFERENCES Movie (movie_id);

COMMIT;
