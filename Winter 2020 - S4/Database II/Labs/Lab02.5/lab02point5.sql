-- LAB 02.5 --

-- Practice Questions --
-- 1. --
DECLARE
    TYPE numberarray IS VARRAY(0) OF NUMBER;
    multiples numberarray := numberarray();
    summm NUMBER(7) := 0;
    counter NUMBER(3) := 1;
BEGIN
    FOR num IN 1 .. 1000 LOOP
         IF MOD(num,3) = 0 OR MOD(num,5)= 0 THEN
             multiples.EXTEND;
             multiples(counter) := num;
             counter := counter + 1;
             summm := summm + num;
         END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Total Sum: ' || summm);
END;

-- 2 --
DECLARE
    TYPE assArray IS TABLE OF NUMBER(3,1)
    INDEX BY VARCHAR2(20);
    students assArray := assArray();
    average NUMBER(5,2) := 0;
    stud VARCHAR2(20);
BEGIN
    students('John') := 75;
    students('Alex') := 80;
    students('Bob') := 90;
    students('Charlie') := 65;
    students('Juliette') := 50;

    stud := students.FIRST;

    WHILE stud IS NOT NULL LOOP
        average := average + students(stud);
        stud := students.NEXT(stud);
    END LOOP;

    average := average / students.COUNT;
    stud := students.FIRST;

    WHILE stud IS NOT NULL LOOP
        IF (students(stud) > average) THEN
            DBMS_OUTPUT.PUT_LINE(stud || ' is better than the average.');
        END IF;
        stud := students.NEXT(stud);
    END LOOP;
END;

-- 3 --
DECLARE
    TYPE orderArray IS VARRAY(4) OF NUMBER;

    
BEGIN

END;

-- 4 --


-- 5 --


-- 6a --


-- 6b --


-- 7 --


--> GRADED QUESTIONS <--
-- 8 --
DECLARE
    TYPE isbnArr IS VARRAY(20) OF VARCHAR2(10);
    booksArr isbnArr := isbnArr();
    i PLS_INTEGER := 1;
    currentDiscount NUMBER(5,2);
    actualDiscount NUMBER(5,2);
BEGIN
    SELECT ISBN BULK COLLECT INTO booksArr FROM ORDERITEMS GROUP BY ISBN HAVING (SUM(QUANTITY) > 2);

    FOR i IN 1 .. booksArr.COUNT LOOP
        SELECT Discount INTO currentDiscount FROM Books WHERE ISBN = booksArr(i);
        SELECT (Retail * 0.1) INTO actualDiscount FROM BOOKS WHERE ISBN = booksArr(i);

        IF (currentDiscount IS NULL) THEN
            UPDATE Books SET Discount = (Retail * 0.1) WHERE ISBN = booksArr(i);
            DBMS_OUTPUT.PUT_LINE('NULL replaced for ISBN ' || booksArr(i));
        ELSIF (currentDiscount < actualDiscount) THEN
            UPDATE Books SET Discount = (Retail * 0.1) WHERE ISBN = booksArr(i);
            DBMS_OUTPUT.PUT_LINE('Updated Discount for ISBN ' || booksArr(i));
        END IF;
    END LOOP;
END;
---------------------------------------------------------------------------------------
-- 9 --
CREATE OR REPLACE PROCEDURE cancel_orders(customerID IN NUMBER)
AS
BEGIN
    DELETE FROM ORDERITEMS WHERE ORDER# = (SELECT ORDER# FROM ORDERS WHERE CUSTOMER# = customerID AND ORDERS.SHIPDATE IS NULL);
    DELETE FROM ORDERS WHERE ORDERS.Customer# = customerID AND ORDERS.ShipDate IS NULL;
    DBMS_OUTPUT.PUT_LINE('Order Deleted.');
END;

BEGIN
    cancel_orders(1017);
END;
---------------------------------------------------------------------------------------
-- 10 --
CREATE OR REPLACE FUNCTION BOOK_OF_THE_MONTH(month IN DATE)
RETURN VARCHAR2
AS
    returnBook VARCHAR2(20);
    firstDayOfMonth DATE := TRUNC(month, 'MONTH');
    firstOfNextMonth DATE;
BEGIN
    firstOfNextMonth := ADD_MONTHS(firstDayOfMonth, 1);

    SELECT Title INTO returnBook FROM OrderItems
    INNER JOIN Orders USING (ORDER#)
    INNER JOIN Books USING (ISBN)
    WHERE ORDERDATE BETWEEN firstDayOfMonth AND firstOfNextMonth
    GROUP BY ISBN, Title HAVING SUM(QUANTITY) = (
        SELECT SUM(QUANTITY) FROM ORDERITEMS
        INNER JOIN ORDERS USING (ORDER#)
        INNER JOIN BOOKS USING (ISBN)
        WHERE ORDERDATE BETWEEN firstDayOfMonth AND firstOfNextMonth GROUP BY ISBN
        );
    RETURN returnBook;
END;

DECLARE
    returned VARCHAR2(10);
BEGIN
    returned := BOOK_OF_THE_MONTH('2003-04-10');
    DBMS_OUTPUT.PUT_LINE(returned);
END;




