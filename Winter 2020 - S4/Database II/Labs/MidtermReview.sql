--- LAB01 ---
--- PRACTICE QUESTIONS ---
// --- 1. --- //
DECLARE
    vISBN  Books.ISBN%TYPE := 1059831198;
    vTitle Books.Title%TYPE;
    vPrice Books.Retail%TYPE;
BEGIN
    SELECT Title, Retail INTO vTitle, vPrice FROM BOOKS WHERE ISBN = vISBN;
    DBMS_OUTPUT.PUT_LINE('Title: ' || vTitle || ' Price: ' || vPrice);
END;

// --- 2. --- //
DECLARE


BEGIN

END;

// --- 3. --- //
DECLARE
    vISBN BOOKS.ISBN%TYPE := 1059831198;
    vPrice BOOKS.Retail%TYPE;
    vCategory BOOKS.Category%TYPE;
BEGIN
    SELECT Retail, Category INTO vPrice, vCategory FROM BOOKS WHERE ISBN = vISBN;

    IF vCategory = 'COMPUTER' THEN
        DBMS_OUTPUT.PUT_LINE(vPrice *  0.70);
    ELSIF vCategory = 'FITNESS' THEN
        DBMS_OUTPUT.PUT_LINE(vPrice * 0.60);
    ELSIF vCategory = 'BUSINESS' THEN
        DBMS_OUTPUT.PUT_LINE(vPrice * 0.80);
    ELSE
        DBMS_OUTPUT.PUT_LINE(vPrice * 0.90);
    END IF;
END;

// --- 4. --- //
DROP TABLE Roots;
CREATE TABLE Roots
(
    N NUMBER(6),
    RootN NUMBER(15)
);

DECLARE

BEGIN
    FOR i IN 1 .. 1000 LOOP
       INSERT INTO Roots VALUES (i, SQRT(i));
    END LOOP;
END;

// --- GRADED QUESTIONS --- //
// --- 1. --- //
DECLARE
    counter NUMBER(3) := 0;
    lower NUMBER(3) := 0;
    upper NUMBER(3) := 10;
BEGIN
    WHILE upper <= 100 LOOP
        SELECT COUNT(*) INTO counter FROM Books WHERE Retail > lower AND Retail < upper;
        DBMS_OUTPUT.PUT_LINE('Between ' || upper || ' and ' || lower || ': ' || counter);
        upper := upper + 10;
        lower := lower + 10;
    END LOOP;
END;

// --- 2. --- //



------------------------------------------------
// --- LAB 2.5 --- //
// --- PRACTICE QUESTIONS --- //
// --- 1. --- //
DECLARE
    TYPE multiples IS VARRAY(1) OF NUMBER;
    array multiples := multiples();
    sumAll NUMBER(5) := 0;
    length NUMBER(5) := 0;
BEGIN
    FOR i IN 0 .. 1000 LOOP
        IF MOD(i,3) = 0 OR MOD(i,5) = 0 THEN
            array.EXTEND;
            array(length) := i;
            length := length + 1;
            sumAll := sumAll + i;
        END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE(sumAll);
END;
// --- 2. --- //
DECLARE
    TYPE studArray IS TABLE OF NUMBER
    INDEX BY VARCHAR2(20);
    
BEGIN

END;
// --- 3. --- //

// --- 4. --- //

// --- 5. --- //

// --- 6.A --- //

// --- 6.B --- //

// --- 7 --- //