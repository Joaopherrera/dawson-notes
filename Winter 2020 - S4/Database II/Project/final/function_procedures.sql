--- STORED PROCEDURE TO CREATE CUSTOMERS ---
CREATE OR REPLACE PROCEDURE createCustomer(vUsername IN VARCHAR2, vSalt IN VARCHAR2, vHash IN RAW, vName IN VARCHAR2, vEmail IN VARCHAR2, vPhone# IN VARCHAR2, vAddress IN VARCHAR2, vCountry IN VARCHAR2, vDate_of_Birth IN VARCHAR2)
AS
BEGIN
    INSERT INTO CUSTOMERS VALUES (CUSTOMERPK_SEQ.nextval, vUsername, vSalt, vHash, vName, vEmail, vPhone#, vAddress, UPPER(vCountry), TO_DATE(vDate_of_Birth));
END;
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
--- INSERT/DELETE ITEMS INTO/FROM THE CART ---
CREATE OR REPLACE PROCEDURE updateCart(vCustomerID IN VARCHAR2, vCD_ID IN VARCHAR2, vQuantity IN NUMBER, vOption IN NUMBER)
AS
BEGIN
    IF vOption = 1 THEN
        INSERT INTO CART VALUES (vCustomerID, vCD_ID, vQuantity);
    ELSIF vOption = 2 THEN
        DELETE FROM CART WHERE CD_ID = vCD_ID AND CUSTOMER_ID = vCustomerID;
    END IF;
END;
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
--- PROCEDURE BEING CALLED IN THE after_insert_orderItems TRIGGER TO UPDATE THE STOCK AND POPULARITY ---
CREATE OR REPLACE PROCEDURE updateStockAndPop(vCD_ID IN VARCHAR2, vQuantity IN NUMBER)
AS
BEGIN
    UPDATE CD SET AVAILABLE = (Available - vQuantity), SOLD = (Sold + 1) WHERE CD_ID = vCD_ID;
END;

-- TRIGGER TO EMPTY THE CART AFTER A PURCHASE --
CREATE OR REPLACE TRIGGER after_insert_ORDERITEMS AFTER INSERT
ON ORDERITEMS
FOR EACH ROW
BEGIN
    updateStockAndPop(:NEW.CD_ID, :New.Quantity);
END;
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
--- PROCEDURE TO INSERT ROWS INTO OREDERITEMS ---
CREATE OR REPLACE PROCEDURE insertOrderItems(vOrderID IN VARCHAR2, vCustomerID IN VARCHAR2)
AS
    CURSOR cartInfo IS SELECT CD_ID, Quantity FROM CART WHERE CUSTOMER_ID = vCustomerID;
    vCD_ID CART.CD_ID%TYPE;
    vQuantity CART.Quantity%TYPE;
    vPrice CD.Price%TYPE;
BEGIN
    OPEN cartInfo;
    FOR row IN (SELECT CD_ID, Quantity FROM CART WHERE CUSTOMER_ID = vCustomerID) LOOP
        BEGIN
            FETCH cartInfo INTO vCD_ID, vQuantity;

            SELECT Price INTO vPrice FROM CD WHERE CD_ID = vCD_ID;

            INSERT INTO ORDERITEMS VALUES (vOrderID, vCD_ID, vQuantity, vPrice);
        END;
    END LOOP;
END;
--- PROCEDURE TO EMPTY THE CART ---
CREATE OR REPLACE PROCEDURE emptyCart(vCustomerID IN VARCHAR2)
AS
BEGIN
    DELETE FROM CART WHERE CUSTOMER_ID = vCustomerID;
END;
--- PROCEDURE TO PLACE AN ORDER, THEN INSERT INTO ORDERITEMS AND EMPTY THE CART ---
CREATE OR REPLACE PROCEDURE placeOrder(vCustomerID IN VARCHAR2, vFinalCost IN NUMBER)
AS
    customerAddress CUSTOMERS.Address%TYPE;
    customerCountry CUSTOMERS.Country%TYPE;
    vShipCost ORDERS.ShipCost%TYPE;
    vPromotion ORDERS.Promotion_ID%TYPE;
    promotionDiscount PROMOTIONS.Percentage%TYPE;
    vTotalCost ORDERS.TotalCost%TYPE;
    vOrderDate ORDERS.OrderDate%TYPE;
    vDOB CUSTOMERS.Date_Of_Birth%TYPE;
BEGIN
    SELECT Address, Country, DATE_OF_BIRTH INTO customerAddress, customerCountry, vDOB FROM CUSTOMERS WHERE CUSTOMER_ID = vCustomerID;
    vOrderDate := CURRENT_DATE;

    -- To apply the birth day promotion --
    IF  vDOB = CURRENT_DATE THEN
        vPromotion := 'P0007';
        SELECT PERCENTAGE INTO promotionDiscount FROM PROMOTIONS WHERE PROMOTION_ID = vPromotion;
    ELSE
        vPromotion := NULL;
        promotionDiscount := 0;
    END IF;

    -- To check the country to apply shipping cost, tax and calculate final price --
    IF customerCountry = 'CANADA' THEN
        vShipCost := vFinalCost * 0.13;
        vTotalCost := (vFinalCost * 1.15 + vShipCost) - (vFinalCost * 1.15 + vShipCost) * promotionDiscount;
    ELSE
        vShipCost := vFinalCost * 0.23;
        vTotalCost := (vFinalCost + vShipCost)  - (vFinalCost * 1.15 + vShipCost) * promotionDiscount;
    END IF;

    INSERT INTO ORDERS VALUES (ORDERSPK_SEQ.nextval, vCustomerID, vOrderDate, NULL, customerAddress, customerCountry, vShipCost, vPromotion, vTotalCost);

    insertOrderItems(ORDERSPK_SEQ.currval, vCustomerID);
    emptyCart(vCustomerID);
END;
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------

