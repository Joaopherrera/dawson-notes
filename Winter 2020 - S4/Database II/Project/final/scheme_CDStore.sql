--> DROP ALL THE TABLES <--
DROP TABLE CD CASCADE CONSTRAINTS;
DROP TABLE CUSTOMERS CASCADE CONSTRAINTS;
DROP TABLE ARTISTS CASCADE CONSTRAINTS;
DROP TABLE ORDERS CASCADE CONSTRAINTS;
DROP TABLE CD_SUPPLIERS CASCADE CONSTRAINTS;
DROP TABLE CD_ARTISTS CASCADE CONSTRAINTS;
DROP TABLE ORDERITEMS CASCADE CONSTRAINTS;
DROP TABLE PROMOTIONS CASCADE CONSTRAINTS;
DROP TABLE SUPPLIERS CASCADE CONSTRAINTS;
DROP TABLE CART CASCADE CONSTRAINTS;
DROP SEQUENCE CustomerPK_seq;
DROP SEQUENCE OrdersPK_seq;
--DROP INDEX Index_Username;
--DROP INDEX Index_Password;
----------------------------------------------------------------------
--> TABLE CD <--
CREATE TABLE CD (
    CD_ID VARCHAR2(5),
    Title VARCHAR2(50) NOT NULL,
    Category VARCHAR2(15) NOT NULL,
    Record_Label VARCHAR2(30) NOT NULL,
    Price NUMBER(4,2) NOT NULL,
    Description VARCHAR2(1000) NOT NULL,
    Release_Date DATE NOT NULL,
    Num_of_Tracks NUMBER(3) NOT NULL,
    Available NUMBER(4) NOT NULL,
    Sold NUMBER(4) NOT NULL,

    -- CONSTRAINTS --
    CONSTRAINT CD_pk PRIMARY KEY (CD_ID)
);
----------------------------------------------------------------------
--> TABLE CUSTOMERS <--
CREATE TABLE CUSTOMERS (
    Customer_ID VARCHAR2(5),
    Username VARCHAR2(20) NOT NULL,
    Salt VARCHAR2(32) NOT NULL,
    HashedPassword RAW(256) NOT NULL,
    Name VARCHAR2(20) NOT NULL,
    Email VARCHAR2(30),
    Phone# VARCHAR2(10),
    Address VARCHAR2(30) NOT NULL,
    Country VARCHAR2(10) NOT NULL,
    Date_of_Birth DATE NOT NULL,

    -- CONSTRAINTS --
    CONSTRAINT CUSTOMERS_pk PRIMARY KEY (Customer_ID)
);
----------------------------------------------------------------------
--> TABLE ARTISTS <--
CREATE TABLE ARTISTS (
    Artist_ID VARCHAR2(5),
    Name VARCHAR2(25) NOT NULL,

    -- CONSTRAINTS --
    CONSTRAINT ARTISTS_pk PRIMARY KEY (Artist_ID)
) ;
----------------------------------------------------------------------
--> TABLE CD_ARTISTS <--
CREATE TABLE CD_ARTISTS (
    CD_ID VARCHAR2(5),
    Artist_ID VARCHAR2(5)  NOT NULL,

    -- CONSTRAINTS --
    CONSTRAINT CD_ARTISTS_pk PRIMARY KEY (CD_ID, Artist_ID),
    CONSTRAINT CD_ARTISTSCD_fk FOREIGN KEY (CD_ID) REFERENCES CD (CD_ID),
    CONSTRAINT ARTISTS_CDARTISTS_fk FOREIGN KEY (Artist_ID) REFERENCES ARTISTS (Artist_ID)
) ;
----------------------------------------------------------------------
--> TABLE PROMOTIONS <--
CREATE TABLE PROMOTIONS (
    Promotion_ID VARCHAR2(5),
    Type VARCHAR2(15) NOT NULL,
    Percentage NUMBER(2) NOT NULL,

    -- CONSTRAINTS --
    CONSTRAINT PROMOTIONS_pk PRIMARY KEY (Promotion_ID)
) ;
----------------------------------------------------------------------
--> TABLE ORDERS <--
CREATE TABLE ORDERS (
    Order_ID VARCHAR2(5),
    Customer_ID VARCHAR2(5) NOT NULL,
    OrderDate DATE NOT NULL,
    ShipDate DATE,
    ShipAddress VARCHAR2(30) NOT NULL,
    ShipCountry VARCHAR2(10) NOT NULL,
    ShipCost NUMBER(4,2) NOT NULL,
    Promotion_ID VARCHAR2(5),
    TotalCost NUMBER(5,2),

    -- CONSTRAINTS --
    CONSTRAINT ORDERS_pk PRIMARY KEY (Order_ID),
    CONSTRAINT ORDERS_CUSTOMERS_fk FOREIGN KEY (Customer_ID) REFERENCES CUSTOMERS (Customer_ID),
    CONSTRAINT PROMOTIONS_ORDERS_fk FOREIGN KEY (Promotion_ID) REFERENCES PROMOTIONS (Promotion_ID)
) ;
----------------------------------------------------------------------
--> TABLE SUPPLIERS <--
CREATE TABLE SUPPLIERS (
    Supplier_ID VARCHAR2(5),
    Name VARCHAR2(20) NOT NULL,

    -- CONSTRAINTS --
    CONSTRAINT SUPPLIERS_pk PRIMARY KEY (Supplier_ID)
) ;
----------------------------------------------------------------------
--> TABLE CD_SUPPLIERS <--
CREATE TABLE CD_SUPPLIERS (
    CD_ID VARCHAR2(5),
    Supplier_ID VARCHAR2(5) NOT NULL,

    -- CONSTRAINTS --
    CONSTRAINT CDSUPPLIERS_pk PRIMARY KEY (CD_ID, Supplier_ID),
    CONSTRAINT CDSUPPLIERS_CD_fk FOREIGN KEY (CD_ID) REFERENCES CD (CD_ID),
    CONSTRAINT CDSUPPLIERS_SUPPLIERS_fk FOREIGN KEY (Supplier_ID) REFERENCES SUPPLIERS (Supplier_ID)
) ;
----------------------------------------------------------------------
--> TABLE ORDERITEMS <--
CREATE TABLE ORDERITEMS (
    Order_ID VARCHAR2(5),
    CD_ID VARCHAR2(5) NOT NULL,
    Quantity NUMBER(2) NOT NULL,
    Price NUMBER(4,2),

    -- CONSTRAINTS --
    CONSTRAINT ORDERITEMS_pk PRIMARY KEY (Order_ID, CD_ID),
    CONSTRAINT ORDERITEMS_ORDERS_fk FOREIGN KEY (Order_ID) REFERENCES ORDERS (Order_ID),
    CONSTRAINT ORDERITEM_CD_fk FOREIGN KEY (CD_ID) REFERENCES CD (CD_ID)
) ;
----------------------------------------------------------------------
--> TABLE CART <--
CREATE TABLE CART (
    Customer_ID VARCHAR2(5),
    CD_ID VARCHAR2(5),
    Quantity NUMBER(2)  NOT NULL,

    -- CONSTRAINTS --
    CONSTRAINT CART_pk PRIMARY KEY (Customer_ID,CD_ID),
    CONSTRAINT CD_CART_fk FOREIGN KEY (CD_ID) REFERENCES CD (CD_ID),
    CONSTRAINT CUSTOMERS_CART_fk FOREIGN KEY (Customer_ID) REFERENCES CUSTOMERS (Customer_ID)
) ;
----------------------------------------------------------------------

--> SEQUENCE FOR AUTO INCREMENT PRIMARY KEY <--
CREATE SEQUENCE CustomerPK_seq
MINVALUE 0
START WITH 0
INCREMENT BY 1;

CREATE SEQUENCE OrdersPK_seq
MINVALUE 0
START WITH 0
INCREMENT BY 1;

--> CREATE INDEX FOR THE CD AND CUSTOMER TABLE <--
CREATE INDEX Index_Username ON CUSTOMERS(Username);
CREATE INDEX Index_Password ON CUSTOMERS(HASHEDPASSWORD);