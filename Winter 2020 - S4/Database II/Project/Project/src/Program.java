import java.sql.*;
import java.util.InputMismatchException;

public class Program
{
    public static Connection connection;
    public static String currentUser;
    private String category = "";
    private static String keyword = "";

    //Method to establish the connection
    public void startConnection()
    {
        try
        {
            String username = "A1835031";
            String password = "daw420cs";

            this.connection = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.73:1522/pdborad12c.dawsoncollege.qc.ca",username,password);
            //this.connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE",username,password);
            System.out.println("Connected");
        }
        catch (SQLException e) { System.out.println("Connection Error"); }
    }

    //Method to run the application it self. If has the loop for the login and then it calls the mainMenu()
    public void runApp()
    {
        //To start the connection
        startConnection();
        System.out.println("---------------------------------");
        System.out.println("Welcome to the Super CD Store.");

        //Loop to keep asking for the user to login or create an account
        boolean flag = false;
        do { flag = loginMenu(); }
        while (!flag);
        //Menu to run the app itself
        mainMenu();
    }

    //Method to take care of the login menu.
    private boolean loginMenu()
    {
        boolean bool = false;
        System.out.println("Choose one of the following options:");
        System.out.println("1. Login");
        System.out.println("2. Create an account");

        int choice = Utilities.getInputFromUser();

        switch (choice)
        {
            case 1: //call the Login method
                try { bool = login(); }
                catch (SQLException e) { System.out.println("Failed To login. Unsername or Password Invalid.");}
                break;

            case 2: //Create an account
                try{ bool = newUser(); }
                catch (SQLException e ) { System.out.println("Something went wrong, Please try again.   Error: " + e.getMessage());}
                break;
        }
        return bool;
    }

     //Method to login and validate login information
    private boolean login() throws SQLException
    {
        //Getting credentials from the user
        System.out.println("Enter a username ");
        String username = Utilities.reader.next();

        System.out.println("Enter a password ");
        String password = Utilities.reader.next();

        //Query to get the salt
        String query = "SELECT Salt FROM CUSTOMERS WHERE Username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,username);

        ResultSet rs = preparedStatement.executeQuery();

        String salt = "";
        rs.next();
        salt = rs.getString("Salt");


        //Getting the hash from the user's salt and the provided password
        byte[] hashpassword = Utilities.hash(password, salt);

        //Query to validate if the credentials are correct
        String sql = "SELECT * FROM CUSTOMERS WHERE USERNAME = ? AND HashedPassword = ?";
        PreparedStatement preparedStatement2 = connection.prepareStatement(sql);
        preparedStatement2.setString(1,username);
        preparedStatement2.setBytes(2,hashpassword);

        ResultSet rs2 = preparedStatement2.executeQuery();

        if (rs2.next())
        {
            System.out.println("Login as " + username + " successful!");
            currentUser = rs2.getString("Customer_ID");
            return true;
        }
        return false;
    }

    //Method to create a new user
    private boolean newUser() throws SQLException
    {
        Utilities.reader.nextLine();
        System.out.println("Enter a username:");
        String username = Utilities.reader.nextLine();

        System.out.println("Enter a password:");
        String password = Utilities.reader.nextLine();
        String salt = Utilities.getSalt();
        byte[] hashpassword = Utilities.hash(password, salt);

        System.out.println("Please enter your name: ");
        String name = Utilities.reader.nextLine();

        System.out.println("Please enter your email:");
        String email = Utilities.reader.nextLine();

        System.out.println("Please enter your phone number: (without the '-' )");
        String phone = Utilities.reader.nextLine();

        System.out.println("Please enter your address:");
        String address = Utilities.reader.nextLine();

        System.out.println("Please enter your country:");
        String country = Utilities.reader.nextLine();

        System.out.println("Please enter your date of birth (yy-MMM-dd):");
        String bdate = Utilities.reader.nextLine();

        //Making the callable statement to create the customers
        String sql = "{ call createCustomer(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
        CallableStatement callableStatement = connection.prepareCall(sql);

        callableStatement.setString(1, username);
        callableStatement.setString(2, salt);
        callableStatement.setBytes(3, hashpassword);
        callableStatement.setString(4, name);
        callableStatement.setString(5,email);
        callableStatement.setString(6, phone);
        callableStatement.setString(7, address);
        callableStatement.setString(8, country);
        callableStatement.setString(9, bdate);

        callableStatement.execute();

        System.out.println("User created!");
        return true;
    }

    //Method to handle the main menu. It loops infinitely until the user logs out
    private void mainMenu()
    {
        boolean flag = true;

        while(flag)
        {
            System.out.println("---------------------------------");
            System.out.println("Please choose one of the following options: ");
            System.out.println("1. See all products");
            System.out.println("2. Filter per category");
            System.out.println("3. Search using keyword");
            System.out.println("------");
            System.out.println("4. My Cart");
            System.out.println("5. Precede to Checkout");
            System.out.println("6. See my orders");
            System.out.println("7. Logout");

            System.out.println("");
            System.out.println("Your choice: ");
            int choice = Utilities.getInputFromUser();


            switch (choice)
            {
                case 1: //Option to see all the items
                    option1();
                    break;

                case 2: //Option to filter all items by category
                    option2();
                    break;

                case 3: //Option to search using a keyword
                    option3();
                    break;

                case 4: //Option to see cart and the sub menu for it
                    Cart.cartOptions();
                    break;

                case 5: //Option to place an order from you cart
                    Checkout.checkout();
                    break;

                case 6: //Option to see the user's orders
                    Order.optionOrders();
                    break;

                case 7: //Option to logout and end the application
                    flag = false;
                    System.out.println("Loging out .....");
                    System.out.println("Thank you for choosing the Super CD Store.");
                    break;

                default:
                    System.out.println("Please choose a valid option.");
            }
        }
    }

    //Method for sub-menu for option 1 -- 1. See all products
    private void option1()
    {
        int displayOptions = Utilities.printCD();
        boolean flag = true;

        while (flag)
        {
            System.out.println("---------------------------------");
            System.out.println("What do you want to do next:");
            System.out.println("1. See the list again");
            System.out.println("2. See an Item's description");
            System.out.println("3. Add an item to your cart");
            System.out.println("4. See cart");
            System.out.println("5. Go back");

            System.out.println("");
            System.out.println("Your choice: ");
            int choice = Utilities.getInputFromUser();

            switch (choice) {
                case 1:
                    displayOptions = Utilities.printCD();
                    break;

                case 2:
                    printDescription(displayOptions);
                    break;

                case 3:
                    Cart.addToCart(displayOptions);
                    break;

                case 4:
                    Cart.cartOptions();
                    break;

                case 5:
                    flag = false;
            }
        }
    }

    //Method to print the description of a given cd
    private void printDescription(int displayOption)
    {
        System.out.println("Which item do you want to see the description? ");
        System.out.println("Your choice: ");
        int choice = Utilities.getInputFromUser();

        String cdquery = "";

        switch (displayOption)
        {
            case 1: //nothing
                cdquery = "SELECT * FROM CD";
                break;

            case 2: //alphabetically
                cdquery = "SELECT * FROM CD ORDER BY TITLE ASC";
                break;

            case 3: //Popularity
                cdquery = "SELECT * FROM CD ORDER BY SOLD DESC";
                break;
        }

        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(cdquery);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                if (choice == index)
                {
                    String cd = rs.getString("CD_ID");

                    String query = "SELECT * FROM CD WHERE CD_ID = ?";
                    PreparedStatement preparedStatement1 = connection.prepareStatement(query);
                    preparedStatement1.setString(1, cd);

                    ResultSet cdInfo = preparedStatement1.executeQuery();

                    cdInfo.next();
                    System.out.println("Title: " + cdInfo.getString("Title") + " | Number Of Tracks: " + cdInfo.getInt("Num_Of_Tracks"));
                    System.out.println(cdInfo.getString("Description"));

                    break;
                }
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cd. (From: Program Line 314)");
            System.out.println(e.getMessage());
        }
    }

    //-----------------------------------------------------------------------------------//

    //Set of menus for option 2 - Filter per category
    private void option2()
    {
        int displayOptions = printByCategory();
        boolean flag = true;

        while (flag)
        {
            System.out.println("---------------------------------");
            System.out.println("What do you want to do next:");
            System.out.println("1. See the list again");
            System.out.println("2. See an Item's description");
            System.out.println("3. Add an item to your cart");
            System.out.println("4. See cart");
            System.out.println("5. Go back");

            System.out.println("");
            System.out.println("Your choice: ");
            int choice = Utilities.getInputFromUser();

            switch (choice)
            {
                case 1:
                    displayOptions = printByCategory();
                    break;

                case 2:
                    printDescription(displayOptions, category);
                    break;

                case 3:
                    Cart.addToCart(displayOptions, category);
                    break;

                case 4:
                    Cart.cartOptions();
                    break;

                case 5:
                    flag = false;
            }
        }
    }

    //Method to print all the books on a certain category
    private int printByCategory()
    {
        try
        {
            String query = "SELECT DISTINCT Category FROM CD ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                System.out.println(index + ". " + rs.getString("Category"));
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cds. (From: Program Line 382)");
            System.out.println(e.getMessage());
        }

        System.out.println("Which Category do you want to see:");
        int choiceCategory = Utilities.getInputFromUser();

        try
        {
            String query = "SELECT DISTINCT Category FROM CD ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                if (choiceCategory == index)
                {
                    category = rs.getString("Category");
                    break;
                }
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cds. (From: Program Line 407)");
            System.out.println(e.getMessage());
        }

        int choice = -1;
        boolean flag = false;
        String cdquery = "SELECT * FROM CD WHERE CATEGORY = ?";

        while(!flag)
        {
            System.out.println("Filter results by: ");
            System.out.println("1. Nothing");
            System.out.println("2. Title");
            System.out.println("3. Popularity");

            System.out.println("Your choice:");
            choice = Utilities.getInputFromUser();

            switch (choice)
            {
                case 1:
                    cdquery = "SELECT * FROM CD WHERE CATEGORY = ?";
                    flag = true;
                    break;

                case 2:
                    cdquery = "SELECT * FROM CD WHERE CATEGORY = ? ORDER BY TITLE ASC";
                    flag = true;
                    break;

                case 3:
                    cdquery = "SELECT * FROM CD WHERE CATEGORY = ? ORDER BY SOLD DESC";
                    flag = true;
                    break;

                default:
                    System.out.println("Please enter a valid option.");
            }
        }

        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(cdquery);
            preparedStatement.setString(1, category);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                String artistQuery = "SELECT ARTISTS.NAME FROM ARTISTS INNER JOIN CD_ARTISTS ON ARTISTS.ARTIST_ID = CD_ARTISTS.ARTIST_ID WHERE CD_ARTISTS.CD_ID = ?";
                PreparedStatement preparedStatement1 = connection.prepareStatement(artistQuery);
                preparedStatement1.setString(1, rs.getString("CD_ID"));

                ResultSet rsArtistName = preparedStatement1.executeQuery();
                rsArtistName.next();

                System.out.println(index + ". " + rs.getString("Title") + " | By: " + rsArtistName.getString("Name") + " | Price: " + rs.getDouble("Price") + " | Stock: " + rs.getString("Available"));
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cds. (From: Program Line 468)");
            System.out.println(e.getMessage());
        }
        return choiceCategory;
    }

    //Method to print the description for a given cd (category)
    private void printDescription(int displayOption, String category)
    {
        System.out.println("Which item do you want to see the description? ");
        System.out.println("Your choice: ");
        int choice = Utilities.getInputFromUser();

        String cdquery = "";

        switch (displayOption)
        {
            case 1:
                cdquery = "SELECT * FROM CD WHERE CATEGORY = ?";
                break;

            case 2:
                cdquery = "SELECT * FROM CD WHERE CATEGORY = ? ORDER BY TITLE ASC";
                break;

            case 3:
                cdquery = "SELECT * FROM CD WHERE CATEGORY = ? ORDER BY SOLD DESC";
                break;

            default:
                System.out.println("Please enter a valid option.");
        }

        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(cdquery);
            preparedStatement.setString(1, category);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                if (choice == index)
                {
                    String cd = rs.getString("CD_ID");

                    String query = "SELECT * FROM CD WHERE CD_ID = ?";
                    PreparedStatement preparedStatement1 = connection.prepareStatement(query);
                    preparedStatement1.setString(1, cd);

                    ResultSet cdInfo = preparedStatement1.executeQuery();

                    cdInfo.next();
                    System.out.println("Title: " + cdInfo.getString("Title") + " | Number Of Tracks: " + cdInfo.getInt("Num_Of_Tracks"));
                    System.out.println(cdInfo.getString("Description"));

                    break;
                }
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cd. (From: Program Line 529)");
            System.out.println(e.getMessage());
        }
    }

    //-----------------------------------------------------------------------------------//

    //Method for sub-menu for option 1 -- 1. See all products
    private void option3()
    {
        int displayOptions = printSearchResults();
        boolean flag = true;

        while (flag)
        {
            System.out.println("---------------------------------");
            System.out.println("What do you want to do next:");
            System.out.println("1. See the list again");
            System.out.println("2. See an Item's description");
            System.out.println("3. Add an item to your cart");
            System.out.println("4. See cart");
            System.out.println("5. Go back");

            System.out.println("");
            System.out.println("Your choice: ");
            int choice = Utilities.getInputFromUser();

            switch (choice) {
                case 1:
                    displayOptions = printSearchResults();
                    break;

                case 2:
                    printDescriptionSearch(displayOptions, keyword);
                    break;

                case 3:
                    Cart.addToCartSearch(displayOptions, keyword);
                    break;

                case 4:
                    Cart.cartOptions();
                    break;

                case 5:
                    flag = false;
            }
        }
    }

    //Method to print the description of a given cd
    private void printDescriptionSearch(int displayOption, String keyword)
    {
        System.out.println("Which item do you want to see the description? ");
        System.out.println("Your choice: ");
        int choice = Utilities.getInputFromUser();

        String cdquery = "";

        switch (displayOption)
        {
            case 1:
                cdquery = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ?";
                break;

            case 2:
                cdquery = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ? ORDER BY TITLE ASC";
                break;

            case 3:
                cdquery = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ? ORDER BY SOLD DESC";
                break;

            default:
                System.out.println("Please enter a valid option.");
        }

        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(cdquery);
            preparedStatement.setString(1, "% " + keyword + " %");
            preparedStatement.setString(2, "% " + keyword + " %");
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                if (choice == index)
                {
                    String cd = rs.getString("CD_ID");

                    String query = "SELECT * FROM CD WHERE CD_ID = ?";
                    PreparedStatement preparedStatement1 = connection.prepareStatement(query);
                    preparedStatement1.setString(1, cd);

                    ResultSet cdInfo = preparedStatement1.executeQuery();

                    cdInfo.next();
                    System.out.println("Title: " + cdInfo.getString("Title") + " | Number Of Tracks: " + cdInfo.getInt("Num_Of_Tracks"));
                    System.out.println(cdInfo.getString("Description"));

                    break;
                }
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cd. (From: Program Line 637)");
            System.out.println(e.getMessage());
        }
    }

    //Method to print all the results from the search
    public static int printSearchResults()
    {
        System.out.println("Enter the keyword:");

        String input = Utilities.reader.next();
        keyword = input.toUpperCase();

        int choice = -1;
        boolean flag = false;
        String query = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ?";

        while(!flag)
        {
            System.out.println("Filter results by: ");
            System.out.println("1. Nothing");
            System.out.println("2. Title");
            System.out.println("3. Popularity");

            System.out.println("Your choice:");
            choice = Utilities.getInputFromUser();

            switch (choice)
            {
                case 1:
                    query = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ?";
                    flag = true;
                    break;

                case 2:
                    query = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ? ORDER BY TITLE ASC";
                    flag = true;
                    break;

                case 3:
                    query = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ? ORDER BY SOLD DESC";
                    flag = true;
                    break;

                default:
                    System.out.println("Please enter a valid option.");
            }
        }

        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, "% " + keyword + " %");
            preparedStatement.setString(2, "% " + keyword + " %");
            ResultSet rs = preparedStatement.executeQuery();

            int index = 1;

            if (!rs.next())
            {
                System.out.println("No results.");
                return choice;
            }

            do
            {
                String artistQuery = "SELECT ARTISTS.NAME FROM ARTISTS INNER JOIN CD_ARTISTS ON ARTISTS.ARTIST_ID = CD_ARTISTS.ARTIST_ID WHERE CD_ARTISTS.CD_ID = ?";
                PreparedStatement preparedStatement1 = connection.prepareStatement(artistQuery);
                preparedStatement1.setString(1, rs.getString("CD_ID"));

                ResultSet rsArtistName = preparedStatement1.executeQuery();
                rsArtistName.next();

                System.out.println(index + ". " + rs.getString("Title") + " | By: " + rsArtistName.getString("Name") + " | Price: " + rs.getDouble("Price") + " | Stock: " + rs.getString("Available"));
                index++;
            }while(rs.next());
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying get the results. (From: Program Line 707)");
            System.out.println(e.getMessage());
        }
        return choice;
    }
}
