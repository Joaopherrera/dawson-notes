import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Checkout
{
    public static double subtotal = 0;

    //Method to do the checkout, display all the info and price
    public static void checkout()
    {
        boolean flag = true;
        while(flag)
        {
            double totalCost = 0.0;

            System.out.println("Checking out....");
            //If statement to check if the cart is empty.
            //If it is it will skip all of this and display cart empty
            if(!Cart.seeCart())
            {
                System.out.println("-----");
                //If they live in canada, apply tax and less shipping
                if (customerCountry().equals("CANADA"))
                {
                    System.out.println("Tax: " + subtotal *.15);
                    System.out.println("Shipping Cost: " + subtotal *.13);
                    totalCost = subtotal + (subtotal *.15) + (subtotal * .13);
                }
                else //if it's not canada, no tax and higer shipping
                {
                    System.out.println("Tax: 0.00");
                    System.out.println("Shipping Cost: " + subtotal *.23);
                    totalCost = subtotal + (subtotal * .23);
                }
                System.out.println("-----");
                System.out.println("Total: " + totalCost);
                System.out.println("---------------------");

                System.out.println("1. Place Order");
                System.out.println("2. Continue Shopping");
                System.out.println("");
                System.out.println("Your choice: ");
                int choice = Utilities.getInputFromUser();

                switch (choice)
                {
                    //option to place the order
                    case 1:
                        try
                        {
                            String sql = "{ call placeOrder(?, ?)}";
                            CallableStatement callableStatement = Program.connection.prepareCall(sql);
                            callableStatement.setString(1, Program.currentUser);
                            callableStatement.setDouble(2, subtotal);

                            callableStatement.execute();
                            System.out.println("Order placed.");
                            System.out.println("Thank you for buying with us.");
                        }
                        catch (SQLException e)
                        {
                            System.out.println("Problem placing your order. (From: Checkout Line 62). " + e.getMessage());
                        }
                        flag = false;
                        break;
                    //Option to go back to keep shopping
                    case 2:
                        System.out.println("Going back to the menu...");
                        flag = false;
                        break;

                    default:
                        System.out.println("Please choose a valid option.");
                }
            }
            flag = false;
        }
        subtotal = 0;
    }

    //Helper method to find the customer's country
    public static String customerCountry()
    {
        String country = "";
        try
        {
            String query = "SELECT Country FROM CUSTOMERS WHERE CUSTOMER_ID = ?";
            PreparedStatement preparedStatement = Program.connection.prepareStatement(query);
            preparedStatement.setString(1, Program.currentUser);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            country = rs.getString("Country");
        }
        catch (SQLException e) { System.out.println("Something went wrong when trying to access the cds. (From: Checkout Line 93)" + e.getMessage()); }
        return country;
    }
}
