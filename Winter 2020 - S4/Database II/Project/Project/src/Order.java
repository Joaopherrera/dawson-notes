import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Order
{
    //Method to items for a given order and print them
    public static void getOrderItems(String orderID)
    {
        try
        {
            String query = "SELECT * FROM ORDERITEMS WHERE ORDER_ID = ?";
            PreparedStatement preparedStatement = Program.connection.prepareStatement(query);
            preparedStatement.setString(1, orderID);
            ResultSet rs = preparedStatement.executeQuery();

            System.out.println("Items: ");
            while(rs.next())
            {
                String cdInfoQuery = "SELECT Title FROM CD WHERE CD_ID = ?";
                PreparedStatement preparedStatement1 = Program.connection.prepareStatement(cdInfoQuery);
                preparedStatement1.setString(1, rs.getString("CD_ID"));

                ResultSet rsCdInfo = preparedStatement1.executeQuery();
                rsCdInfo.next();

                System.out.println("CD: " + rsCdInfo.getString("Title") + " | Quantity: " + rs.getInt("Quantity") + " | Price: " + rs.getDouble("Price"));
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access ordersItems. (From: Order Line 31)");
            System.out.println(e.getMessage());
        }
    }

    //Method to print all the unshippedOrders
    public static void seeUnshippedOrders()
    {
        try
        {
            String query = "SELECT * FROM ORDERS WHERE CUSTOMER_ID = ? AND SHIPDATE IS NULL ORDER BY ORDERDATE DESC";
            PreparedStatement preparedStatement = Program.connection.prepareStatement(query);
            preparedStatement.setString(1, Program.currentUser);
            ResultSet rs = preparedStatement.executeQuery();

            //If rs.next() is null, there's no unshipped Orders, so it ends there with a message.
            if (!rs.next())
            {
                System.out.println("You have no unshiped orders.");
                System.out.println("");
                return;
            }
            //If they user has unshipped Orders they will be displayed here
            do
            {
                System.out.println("---------------------------------");
                System.out.println("-- Your Unshipped Orders --");
                System.out.println("|-----------------------------|");
                System.out.println("Order Number: " + rs.getString("Order_ID"));
                System.out.println("Order Date: " + rs.getDate("OrderDate"));
                System.out.println("Ship Address: " + rs.getString("ShipAddress") + ", " + rs.getString("ShipCountry"));
                System.out.println("Ship Cost: " + rs.getDouble("ShipCost"));
                System.out.println("Total Cost: " + rs.getDouble("TotalCost"));

                getOrderItems(rs.getString("Order_ID"));
            }while(rs.next());
        }
        catch (SQLException e) { System.out.println("Something went wrong when trying to access your orders. (From: Order Line 69)" + e.getMessage()); }
    }

    //Method to print all the user's orders
    private static void seeOrders()
    {
        try
        {
            String query = "SELECT * FROM ORDERS WHERE CUSTOMER_ID = ? AND SHIPDATE IS NOT NULL ORDER BY ORDERDATE DESC";
            PreparedStatement preparedStatement = Program.connection.prepareStatement(query);
            preparedStatement.setString(1, Program.currentUser);
            ResultSet rs = preparedStatement.executeQuery();

            //If rs.next() is null, there's no Orders, so it ends there with a message.
            if (!rs.next())
            {
                System.out.println("You have no orders.");
                System.out.println("");
                return;
            }
            //If they user has Orders they will be displayed here
            do
            {
                System.out.println("---------------------------------");
                System.out.println("-- Your Orders --");
                System.out.println("|-----------------------------|");
                System.out.println("Order Number: " + rs.getString("Order_ID"));
                System.out.println("Order Date: " + rs.getDate("OrderDate") + " | Ship Date: " + rs.getDate("ShipDate"));
                System.out.println("Ship Address: " + rs.getString("ShipAddress") + ", " + rs.getString("ShipCountry"));
                System.out.println("Ship Cost: " + rs.getDouble("ShipCost"));
                System.out.println("Total Cost: " + rs.getDouble("TotalCost"));

                getOrderItems(rs.getString("Order_ID"));
            } while(rs.next());
        }
        catch (SQLException e) { System.out.println("Something went wrong when trying to access your orders. (From: Order Line 95)" + e.getMessage()); }
    }

    //Method to take care of the order options
    public static void optionOrders()
    {
        boolean flag = true;
        while(flag)
        {
            System.out.println("Which orders would you like to see?");
            System.out.println("1. My orders");
            System.out.println("2. My Unshipped Orders");
            System.out.println("3. Go back");

            System.out.println("");
            System.out.println("Your choice: ");
            int choice = Utilities.reader.nextInt();

            switch (choice)
            {
                case 1:
                    seeOrders();
                    break;

                case 2:
                    seeUnshippedOrders();
                    break;

                case 3:
                    flag = false;
                    break;

                default:
                    System.out.println("Please enter a valid option.");
            }
        }

    }
}
