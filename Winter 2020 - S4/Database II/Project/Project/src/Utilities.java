import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Utilities
{
    public static Scanner reader = new Scanner(System.in);
    private static SecureRandom random = new SecureRandom();

    //Creates a randomly generated String
    public static String getSalt(){
        return new BigInteger(140, random).toString(32);
    }

    //Takes a password and a salt a performs a one way hashing on them, returning an array of bytes.
    public static byte[] hash(String password, String salt){
        try{
            SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );

			/*When defining the keyspec, in addition to passing in the password and salt, we also pass in
			a number of iterations (1024) and a key size (256). The number of iterations, 1024, is the
			number of times we perform our hashing function on the input. Normally, you could increase security
			further by using a different number of iterations for each user (in the same way you use a different
			salt for each user) and storing that number of iterations. Here, we just use a constant number of
			iterations. The key size is the number of bits we want in the output hash*/
            PBEKeySpec spec = new PBEKeySpec( password.toCharArray(), salt.getBytes(), 1024, 256 );

            SecretKey key = skf.generateSecret( spec );
            byte[] hash = key.getEncoded( );
            return hash;
        }catch( NoSuchAlgorithmException | InvalidKeySpecException e ) {
            throw new RuntimeException(e);
        }
    }

    //Method to get the input from the user and make sure the user enters a proper option
    public static int getInputFromUser()
    {
        int choice;
        while(true)
        {
            String input = reader.next();
            if(!(input.matches("[0-9]+")))
            {
                System.out.println("Invalid Input, please try again.");
                System.out.println("Your choice:");
            }
            else
            {
                choice = Integer.parseInt(input);
                break;
            }
        }
        return choice;
    }

    //Huge method to insert sample data properly in the database
    //It will not be run again, only if needed to recreate sample users
    public static void createSampleUsers() throws SQLException
    {
        //Insert for user 1
        String username = "JOJOXX23";
        String salt = getSalt();
        byte[] hashpassword = hash("1234567890", salt);
        String name = "JOJO";
        String email = "JOJOTHEBEST@EMAIL.COM";
        String phone = "5145556666";
        String address = "123 A STREET";
        String country = "CANADA";
        String bdate = "00-MAR-16";

        String sql = "{ call createCustomer(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
        CallableStatement callableStatement = Program.connection.prepareCall(sql);

        callableStatement.setString(1, username);
        callableStatement.setString(2, salt);
        callableStatement.setBytes(3, hashpassword);
        callableStatement.setString(4, name);
        callableStatement.setString(5,email);
        callableStatement.setString(6, phone);
        callableStatement.setString(7, address);
        callableStatement.setString(8, country);
        callableStatement.setString(9, bdate);

        callableStatement.execute();

        System.out.println("User 1 created!");
        System.out.println("-----------------------------------");

        //Insert for user 2
        username = "BOB123";
        salt = getSalt();
        hashpassword = hash("0987654321", salt);
        name = "BOB";
        email = "BOBTHETRASH@EMAIL.COM";
        phone = "5145268976";
        address = "974 NOWHERE STREET";
        country = "US";
        bdate = "89-MAY-24";

        callableStatement.setString(1, username);
        callableStatement.setString(2, salt);
        callableStatement.setBytes(3, hashpassword);
        callableStatement.setString(4, name);
        callableStatement.setString(5,email);
        callableStatement.setString(6, phone);
        callableStatement.setString(7, address);
        callableStatement.setString(8, country);
        callableStatement.setString(9, bdate);

        callableStatement.execute();

        System.out.println("User 2 created!");
        System.out.println("-----------------------------------");


        //Insert for user 3
        username = "EAT-652";
        salt = getSalt();
        hashpassword = hash("UisndaibdJSN224", salt);
        name = "EATAN";
        email = "YEETTER@EMAIL.COM";
        phone = "5146240989";
        address = "2 BLOB STREET";
        country = "CANADA";
        bdate = "02-SEP-21";

        callableStatement.setString(1, username);
        callableStatement.setString(2, salt);
        callableStatement.setBytes(3, hashpassword);
        callableStatement.setString(4, name);
        callableStatement.setString(5,email);
        callableStatement.setString(6, phone);
        callableStatement.setString(7, address);
        callableStatement.setString(8, country);
        callableStatement.setString(9, bdate);

        callableStatement.execute();

        System.out.println("User 3 created!");
        System.out.println("-----------------------------------");


        //Insert for user 4
        username = "MYNANEISADA";
        salt = getSalt();
        hashpassword = hash("NOTMYPASSWORD123", salt);
        name = "ADA";
        email = "LITTLEADA@EMAIL.COM";
        phone = "5144327689";
        address = "98 ELM STREET";
        country = "CANADA";
        bdate = "99-OCT-30";

        callableStatement.setString(1, username);
        callableStatement.setString(2, salt);
        callableStatement.setBytes(3, hashpassword);
        callableStatement.setString(4, name);
        callableStatement.setString(5,email);
        callableStatement.setString(6, phone);
        callableStatement.setString(7, address);
        callableStatement.setString(8, country);
        callableStatement.setString(9, bdate);

        callableStatement.execute();

        System.out.println("User 4 created!");
        System.out.println("-----------------------------------");


        //Insert for user 5
        username = "JOWAW";
        salt = getSalt();
        hashpassword = hash("hgfvJCFRYGKH12?", salt);
        name = "JOAO";
        email = "JOWAW@EMAIL.COM";
        phone = "5143216745";
        address = "2543 MEOW STREET";
        country = "CANADA";
        bdate = "00-MAR-02";

        callableStatement.setString(1, username);
        callableStatement.setString(2, salt);
        callableStatement.setBytes(3, hashpassword);
        callableStatement.setString(4, name);
        callableStatement.setString(5,email);
        callableStatement.setString(6, phone);
        callableStatement.setString(7, address);
        callableStatement.setString(8, country);
        callableStatement.setString(9, bdate);

        callableStatement.execute();

        System.out.println("User 5 created!");
        System.out.println("-----------------------------------");
    }

    //Method to print all the cds
    public static int printCD()
    {
        int choice = -1;
        boolean flag = false;
        String cdquery = "SELECT * FROM CD";

        while(!flag)
        {
            System.out.println("Filter results by: ");
            System.out.println("1. Nothing");
            System.out.println("2. Title");
            System.out.println("3. Popularity");

            System.out.println("Your choice:");
            choice = Utilities.getInputFromUser();

            switch (choice)
            {
                case 1:
                    cdquery = "SELECT * FROM CD";
                    flag = true;
                    break;

                case 2:
                    cdquery = "SELECT * FROM CD ORDER BY TITLE ASC";
                    flag = true;
                    break;

                case 3:
                    cdquery = "SELECT * FROM CD ORDER BY SOLD DESC";
                    flag = true;
                    break;

                default:
                    System.out.println("Please enter a valid option.");
            }
        }

        try
        {
            PreparedStatement preparedStatement = Program.connection.prepareStatement(cdquery);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                String artistQuery = "SELECT ARTISTS.NAME FROM ARTISTS INNER JOIN CD_ARTISTS ON ARTISTS.ARTIST_ID = CD_ARTISTS.ARTIST_ID WHERE CD_ARTISTS.CD_ID = ?";
                PreparedStatement preparedStatement1 = Program.connection.prepareStatement(artistQuery);
                preparedStatement1.setString(1, rs.getString("CD_ID"));

                ResultSet rsArtistName = preparedStatement1.executeQuery();
                rsArtistName.next();

                System.out.println(index + ". " + rs.getString("Title") + " | By: " + rsArtistName.getString("Name") + " | Price: " + rs.getDouble("Price") + " | Stock: " + rs.getString("Available"));
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cds. (From Utilities Line 246)");
            System.out.println(e.getMessage());
        }
        return choice;
    }
}
