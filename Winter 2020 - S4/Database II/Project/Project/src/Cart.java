import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Cart
{
    //Method to print all the items in the cart
    public static boolean seeCart()
    {
        boolean cartEmpty = true;
        try
        {
            Checkout.subtotal = 0;
            //Query to get all the items from the cart
            String query = "SELECT * FROM CART WHERE Customer_ID = ?";
            PreparedStatement preparedStatement = Program.connection.prepareStatement(query);
            preparedStatement.setString(1, Program.currentUser);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;

            //If there's no results, it displays a message
            if (!rs.next())
            {
                System.out.println("Cart Empty.");
                return true;
            }
            System.out.println("Your cart:");
            //Loop to go through the resultSet and print the items in the cart
            do
            {
                cartEmpty = false;
                String cdInfoQuery = "SELECT Title, Price FROM CD WHERE CD_ID = ?";
                PreparedStatement preparedStatement1 = Program.connection.prepareStatement(cdInfoQuery);
                preparedStatement1.setString(1, rs.getString("CD_ID"));

                ResultSet rsCdInfo = preparedStatement1.executeQuery();
                rsCdInfo.next();

                System.out.println("Item " + index + ". " + rsCdInfo.getString("Title") + " | Quantity: " + rs.getInt("Quantity") + " | Price: " + rsCdInfo.getDouble("Price"));
                //Calculating the subtotal
                Checkout.subtotal += rsCdInfo.getDouble("Price") * rs.getInt("Quantity");

                index++;
            } while (rs.next());
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the Cart. (From: Cart Line 44)");
            System.out.println(e.getMessage());
        }
        System.out.println("-----");
        System.out.println("Subtotal: " + Checkout.subtotal);

        return cartEmpty;
    }

    //Method to display the options to edit their cart
    public static int cartOptions()
        {
        int choice = -1;
        boolean flag = true;
        if (!seeCart())
        {
            while(flag)
            {
                System.out.println("1. Remove an Item");
                System.out.println("2. Remove All Items");
                System.out.println("3. Go back");

                System.out.println("");
                System.out.println("Your choice: ");
                choice = Utilities.getInputFromUser();

                //Option 1 - Remove an item from the cart
                if (choice == 1)
                {
                    try
                    {
                        System.out.println("Which item would you like to remove?");
                        seeCart();
                        System.out.println("Your choice: ");
                        int itemToBeRemoved = Utilities.getInputFromUser();

                        String cd = findCDinCart(itemToBeRemoved);

                        String sql = "{ call updateCart(?, ?, ?, ?)}";
                        CallableStatement callableStatement = Program.connection.prepareCall(sql);

                        callableStatement.setString(1, Program.currentUser);
                        callableStatement.setString(2, cd);
                        callableStatement.setInt(3, 0);
                        callableStatement.setInt(4, 2);

                        callableStatement.execute();

                        System.out.println("Item Removed");
                    }
                    catch (SQLException e) { System.out.println("Problem removing that item. (From: Cart 90.) " + e.getMessage()); }
                }
                //Option 2 - Empty the cart
                else if (choice == 2)
                {
                    try
                    {
                        String sql = "{ call emptyCart(?)}";
                        CallableStatement callableStatement = Program.connection.prepareCall(sql);
                        callableStatement.setString(1, Program.currentUser);
                        callableStatement.execute();

                        System.out.println("Cart empty now");
                        flag = false;
                    }
                    catch (SQLException e) { System.out.println("Problem romving that item. (From: Cart Line 106). " + e.getMessage()); }
                }
                else if (choice == 3)
                {
                    flag = false;
                }
            }
        }
        return choice;
    }

    //Method that takes input the user's choice (int) of which item to remove
    //and return the CD_ID of that item so it can be removed
    private static String findCDinCart(int choice)
    {
        String cd = "";
        try
        {
            String query = "SELECT * FROM CART WHERE Customer_ID = ?";
            PreparedStatement preparedStatement = Program.connection.prepareStatement(query);
            preparedStatement.setString(1, Program.currentUser);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;

            while(rs.next())
            {
                if (choice == index)
                {
                    cd = rs.getString("CD_ID");
                    break;
                }
                else { index++; }
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the Cart. (From: Cart Line 138)");
            System.out.println(e.getMessage());
        }

        return cd;
    }

    //Method to add an item to the cart
    public static void addToCart(int displayOption)
    {
        System.out.println("Which item do you want to add to your cart? ");
        System.out.println("Your choice: ");
        int choice = Utilities.getInputFromUser();

        System.out.println("Quantity: ");
        int quantity = Utilities.getInputFromUser();

        String cdquery = "";

        switch (displayOption)
        {
            case 1:
                cdquery = "SELECT * FROM CD";
                break;

            case 2:
                cdquery = "SELECT * FROM CD ORDER BY TITLE ASC";
                break;

            case 3:
                cdquery = "SELECT * FROM CD ORDER BY SOLD DESC";
                break;
        }

        try
        {
            PreparedStatement preparedStatement = Program.connection.prepareStatement(cdquery);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                if (choice == index)
                {
                    String cd = rs.getString("CD_ID");

                    String query = "INSERT INTO CART VALUES(?, ?, ?)";
                    PreparedStatement preparedStatement1 = Program.connection.prepareStatement(query);
                    preparedStatement1.setString(1, Program.currentUser);
                    preparedStatement1.setString(2, cd);
                    preparedStatement1.setInt(3, quantity);

                    preparedStatement1.executeQuery();


                    break;
                }
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cds. (From Cart Line 198)");
            System.out.println(e.getMessage());
        }
    }

    //Method to add an item to the cart (Category version)
    public static void addToCart(int displayOption, String category)
    {
        System.out.println("Which item do you want to add to your cart? ");
        System.out.println("Your choice: ");
        int choice = Utilities.getInputFromUser();

        System.out.println("Quantity: ");
        int quantity = Utilities.getInputFromUser();

        String cdquery = "";

        switch (displayOption)
        {
            case 1:
                cdquery = "SELECT * FROM CD WHERE CATEGORY = ?";
                break;

            case 2:
                cdquery = "SELECT * FROM CD WHERE CATEGORY = ? ORDER BY TITLE ASC";
                break;

            case 3:
                cdquery = "SELECT * FROM CD WHERE CATEGORY = ? ORDER BY SOLD DESC";
                break;

            default:
                System.out.println("Please enter a valid option.");
        }

        try
        {
            PreparedStatement preparedStatement = Program.connection.prepareStatement(cdquery);
            preparedStatement.setString(1, category);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                if (choice == index)
                {
                    String cd = rs.getString("CD_ID");

                    String query = "INSERT INTO CART VALUES(?, ?, ?)";
                    PreparedStatement preparedStatement1 = Program.connection.prepareStatement(query);
                    preparedStatement1.setString(1, Program.currentUser);
                    preparedStatement1.setString(2, cd);
                    preparedStatement1.setInt(3, quantity);

                    preparedStatement1.executeQuery();

                    break;
                }
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cds. (From Cart Line 260)");
            System.out.println(e.getMessage());
        }
    }

    //Method to add an item to the cart (Search version)
    public static void addToCartSearch(int displayOption, String keyword)
    {
        System.out.println("Which item do you want to add to your cart? ");
        System.out.println("Your choice: ");
        int choice = Utilities.getInputFromUser();

        System.out.println("Quantity: ");
        int quantity = Utilities.getInputFromUser();

        String cdquery = "";

        switch (displayOption)
        {
            case 1:
                cdquery = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ?";
                break;

            case 2:
                cdquery = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ? ORDER BY TITLE ASC";
                break;

            case 3:
                cdquery = "SELECT * FROM CD WHERE TITLE LIKE ? OR DESCRIPTION LIKE ? ORDER BY SOLD DESC";
                break;
        }

        try
        {
            PreparedStatement preparedStatement = Program.connection.prepareStatement(cdquery);
            preparedStatement.setString(1, "% " + keyword + " %");
            preparedStatement.setString(2, "% " + keyword + " %");
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while(rs.next())
            {
                if (choice == index)
                {
                    String cd = rs.getString("CD_ID");

                    String query = "INSERT INTO CART VALUES(?, ?, ?)";
                    PreparedStatement preparedStatement1 = Program.connection.prepareStatement(query);
                    preparedStatement1.setString(1, Program.currentUser);
                    preparedStatement1.setString(2, cd);
                    preparedStatement1.setInt(3, quantity);

                    preparedStatement1.executeQuery();

                    break;
                }
                index++;
            }
        }
        catch (SQLException e)
        {
            System.out.println("Something went wrong when trying to access the cds. (From: Cart Line 320)");
            System.out.println(e.getMessage());
        }
    }
}
