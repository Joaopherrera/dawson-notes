*** Hello and thank you for using this application. ***
By using this application you agree with out terms and conditions.

INSTRUCTIONS: 
Once the application started, you will be prompted with two options:
	1. LogIn
	2. Create an User

If you enter wrong credentials or the user doens't exists, you will be asked to try again, or create an user.
If you don't have an user, you cannot access the application. Create a user just requires some information. 
*CAUTION* If you don't follow the proper date format shown in the menu, you will be requiered to start over again.

Once you have your user, you can acess the application. 
You may choose one of the following option:

	1. See all products -> It will display all the items order by one of the following: default, alphabeticaly or popularity
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	2. Filter per category -> You can choose a category and see all the results order by one of the following: default, alphabeticaly or popularity      
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	3. Search using keyword -> You can enter a keyword and order by one of the following: default, alphabeticaly or popularity			     
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------------------------------------
		For all those options you will have a submenu with the following options:

			1. See the list again -> It will ask the user to choose the options to display the results again.
			-------------------------------------------------------------------------------------------------------------------------------------
			2. See an item's description -> It will ask the user which item to display the description, then it will display it.
			-------------------------------------------------------------------------------------------------------------------------------------
			3. Add an item to your cart -> It will ask the user which item and the quantity to be added to their cart.
			-------------------------------------------------------------------------------------------------------------------------------------
			4. See cart ->  It will print your cart with the item's name, quantity, price per cd and the subtotal.
					It will have the following options as well:
						1. Remove an Item -> It will ask the user which item to be removed from their cart
						2. Remove all itmes -> It will empty your cart
						3. Go back -> Go back to the previous menu
				If your cart is empty, it will display "Cart Emtpy" and go back to the menu.
			-------------------------------------------------------------------------------------------------------------------------------------
			5. Go back -> Go previous menu
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	4. My Cart -> It will print your cart with the item's name, quantity, price per cd and the subtotal.
				It will have the following options as well:
					1. Remove an Item -> It will ask the user which item to be removed from their cart
					2. Remove all itmes -> It will empty your cart
					3. Go back -> Go back to the previous menu
		      If your cart is empty, it will display "Cart Emtpy" and go back to the menu.
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	5. Precede to Checkout -> It will display the cart with all its information. In addition, it will display the tax, shipping cost and final price.
				  	It will have the following options:
				  		1. Place Order -> It will place the order, empty the cart and display a message.
						2. Continue Shopping -> It will go back to the previous menu so the user can keep browsing for items.	
				  If your cart is empty, it will display "Cart Emtpy" and go back to the menu.
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	6. See My Orders -> It will have the following options:
				1. My orders -> It will show all the user's orders (if the user doesn't have orders, a message will be displayed).
				2. My Unshipped Orders -> It will show all the user's unshipped orders (if the user doesn't have unshipped  orders, a message will be displayed).
				3. Go back -> Go back to the previous menu.
	-----------------------------------------------------------------------------------------------------------------------------------------------------
	7. Logout -> Logs out of the application. Ends the application.
	-----------------------------------------------------------------------------------------------------------------------------------------------------

For all of the menus that the user needs to enter a number, it will keep asking for input until it receives a valid otion.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--> DATABASE SECTION <--
Inside the folder, I have the following SQL files:
	- scheme_CDStore.sql -> It contains all the table defitions to create all the tables, as well as the indexes and auto-incrementing primary keys
	- data_CDStore.sql -> It contains all the INSERTs to populate the tables with sample data.
	- function_procedures.sql -> It contains all the procedures to INSERT and UPDATE the database. 

** IMPORTANT ** 
If for some reason it is required to drop the tables and repopulate them with the sample data, inside *DATA_CDSTORE.SQL*, execute the INSERTs up to the ARTISTS table, 
then go into the java application inside the Main Class, remove the comments of:
	
	//        try {
	//            Utilities.createSampleUsers();
	//        }
	//        catch (SQLException e)
	//        {System.out.println(e.getMessage());}

and run *JUST THAT*, then go back on SQL and run the rest of the script.
OTRHERWISE IT WILL GIVE ERRORS BECAUSE THE CUSTOMERS DON'T EXIST YET.

It's set up this way to ensure that the sample users have their password secure (with hash and salt).
