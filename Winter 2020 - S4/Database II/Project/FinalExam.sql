-- Question 1 --
CREATE OR REPLACE PROCEDURE makereservation(vBook_id IN VARCHAR2, vPat_id IN VARCHAR2)
AS
    numOfBooks NUMBER(5);
    numOfReservation NUMBER(5);
BEGIN
    SELECT COUNT(*) INTO numOfBooks FROM LOAN WHERE vPat_id = pat_id AND Returned = '0';
    SELECT COUNT(*) INTO numOfReservation FROM Reservation WHERE vPat_id = pat_id;

    IF numOfReservation = 0 THEN
            INSERT INTO RESERVATION VALUES(CURRENT_DATE, vPat_id, vBook_id);
    END IF;
END;

-- Question 2 --
CREATE OR REPLACE FUNCTION reservationposition(vPat_id IN VARCHAR2, vBook_id IN VARCHAR)
RETURN NUMBER
AS
    numberOfPeople NUMBER(4);
    Pat Patron.part_id%TYPE;
    PatDate DATE;
    CURSOR allReservation IS SELECT * FROM Reservations;
    tempResDate DATE;

BEGIN
    numberOfPeople := 0;

    SELECT COUNT(*) INTO Pat FROM Reservation WHERE vPat_id = pat_id;
    IF Pat = 0 THEN
        SELECT COUNT(*) INTO numberOfPeople FROM Reservation WHERE book_id = vBook_id;
        RETURN numberOfPeople;
    END IF;

    SELECT Reserved_on INTO PatDate FROM Reservation WHERE vPat_id = pat_id;

    OPEN allReservation;
    LOOP
        FETCH allReservation INTO tempResDate;
        EXIT WHEN allReservation%NOTFOUND;
            IF tempResDate < PatDate THEN
                numberOfPeople := numberOfPeople + 1;
            END IF;
    END LOOP;

    RETURN numberOfPeople;
END;

-- Question 3 --
CREATE OR REPLACE PACKAGE patron_mgmt
AS
    TYPE tOverdueArray IS TABLE OF NUMBER(3,0) INDEX BY patron.pat_id%TYPE;
    invalid_loan EXCEPTION;
    FUNCTION overdue RETURN tOverdueArray;
END patron_mgmt;

CREATE OR REPLACE PACKAGE BODY patron_mgmt AS
    FUNCTION overdue
    RETURN tOverdueArray
    IS
        CURSOR allLoans IS SELECT pat_id, COUNT(*) AS isOverdue FROM LOAN
            WHERE due_date < CURRENT_DATE AND returned = '0'
            GROUP BY pat_id;
        currentRow allLoans%ROWTYPE;
        overdueArray tOverdueArray;
    BEGIN
        OPEN allLoans;
        LOOP
            FETCH allLoans INTO currentRow;
            EXIT WHEN allLoans%NOTFOUND;
            overdueArray(currentRow.pat_id) := currentRow.isOverdue
        END LOOP;
        CLOSE allLoans;
        RETURN overdueArray;
    END;

END patron_mgmt;

-- Question 4 --
CREATE OR REPLACE PROCEDURE lendbook(vBook_id IN Book.book_id%TYPE, vPat_id IN Patron.pat_id%TYPE, remainingBooks OUT NUMBER)
AS
    numOfBooks NUMBER(4);
BEGIN
    SELECT COUNT(*) INTO numOfBooks FROM LOAN WHERE vPat_id = pat_id AND Returned = '0';

    IF numOfBooks < 10 THEN
        INSERT INTO LOAN(due_date, returned, book_id, pat_id) VALUES (CURRENT_DATE + 14, '0', vBook_id, vPat_id);
        UPDATE Book SET qty_available = qty_available - 1 WHERE book_id  = vBook_id;
        SELECT qty_available INTO remainingBooks FROM Book WHERE book_id = vBook_id;
    END IF;
END;

-- Question 5 --
CREATE OR REPLACE TRIGGER confirmloan BEFORE INSERT
ON LOAN
FOR EACH ROW
DECLARE
    queue NUMBER;
    available NUMBER;
BEGIN
    SELECT qty_available INTO available FROM Book
    WHERE book_id = :NEW.book_id;

    queue := reservationposition(:NEW.par_id, :NEW.book_id);

    IF(available < que) THEN
        RAISE invalid_loan;
    END IF;
END;





