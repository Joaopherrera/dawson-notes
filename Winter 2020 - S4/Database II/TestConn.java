import java.sql.*;

public class TestConn {

 public static void main(String[] args) throws SQLException
 {
  String  username = args[0];
  String password = args[1];
  String url = "jdbc:oracle:thin:@198.168.52.73:1522/pdborad12c.dawsoncollege.qc.ca";
    
  Connection conn = DriverManager.getConnection(url, username, password);

  Statement stmt = conn.createStatement();
  ResultSet rs = stmt.executeQuery("SELECT * FROM books");
  while(rs.next()) {
   System.out.println(rs.getString("title"));
  }
  
  
  stmt.close();
  
  System.out.println("Connection made");
 
 }
 
}
