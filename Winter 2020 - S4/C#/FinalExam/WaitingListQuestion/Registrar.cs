﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitingListQuestion
{
    class Registrar
    {

        public Registrar(WaitingList waitingList)
        {
            waitingList.NewStudentAdded += CheckAvailable;
        }

        public void CheckAvailable(IStudent student)
        {
            if(student is PartTimeStudent)
            {
                Console.WriteLine("StudentID: " + student.StudentId + " Graduating Year: " + student.GraduatingYear);
            }
        }
    }
}
