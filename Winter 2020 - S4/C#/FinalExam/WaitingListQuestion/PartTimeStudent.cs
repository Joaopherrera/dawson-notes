﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitingListQuestion
{
    class PartTimeStudent : IStudent
    {
        Student student;
        public int StudentId { get; }
        public string LastName { get; }
        public int GraduatingYear { get; }
        
        public PartTimeStudent(Student student)
        {
            this.student = student;
            this.StudentId = student.StudentId;
            this.LastName = student.LastName;
            this.GraduatingYear = student.GraduatingYear + 2;
        }

    }
}
