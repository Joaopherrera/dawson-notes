﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitingListQuestion
{
    public class Student : IStudent
    {
        public int StudentId { get; }
        public string LastName { get; }
        public int GraduatingYear { get; }

        public Student(int StudentId, string LastName, int GraduatingYear)
        {
            this.StudentId = StudentId;
            this.LastName = LastName;
            this.GraduatingYear = GraduatingYear;
        }
    }
}
