﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitingListQuestion
{
    public interface IStudent
    {
        int StudentId { get; }
        string LastName { get; }
        int GraduatingYear { get; }
    }
}
