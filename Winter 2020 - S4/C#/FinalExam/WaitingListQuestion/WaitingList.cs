﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitingListQuestion
{
    public delegate void DelNewStudent(IStudent student);
    public delegate bool DelFindStudent(IStudent student);

    class WaitingList
    {
        List<IStudent> waitList = new List<IStudent>();
        public event DelNewStudent NewStudentAdded;

        public void onEventAddStudent(IStudent student)
        {
            NewStudentAdded?.Invoke(student);
        }

        public void AddStudent(IStudent student)
        {
            waitList.Add(student);
            onEventAddStudent(student);
        }

        public List<IStudent> FindAll(DelFindStudent del)
        {
            List<IStudent> newList = new List<IStudent>();
            foreach (var student in waitList)
            {
                if(del(student))
                {
                    newList.Add(student);
                }
            }

            var query = from student in newList
                           orderby student.GraduatingYear ascending
                           select student;

            List<IStudent> studentsByGradYear = query.ToList();
            return studentsByGradYear;
        }
    }
}
