﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitingListQuestion
{
    class Program
    {
        static void Main(string[] args)
        {
            WaitingList waitingList = new WaitingList();

            Registrar registrar = new Registrar(waitingList);

            Student s1 = new Student(1, "bob", 2020);
            Student s2 = new Student(2, "max", 2021);
            PartTimeStudent ps1 = new PartTimeStudent(s2);
            Student s3 = new Student(3, "john", 2021);
            PartTimeStudent ps2 = new PartTimeStudent(s3);

            waitingList.AddStudent(s1);
            waitingList.AddStudent(ps1);
            waitingList.AddStudent(ps2);

            Console.WriteLine("Please enter first letter of last name: ");
            string input = Console.ReadLine();
            
            List<IStudent> list = waitingList.FindAll((IStudent s) => s.LastName[0].ToString().Equals(input));

            foreach (var item in list)
            {
                Console.WriteLine(item.StudentId);
            }

            Console.ReadKey();
        }
    }
}
