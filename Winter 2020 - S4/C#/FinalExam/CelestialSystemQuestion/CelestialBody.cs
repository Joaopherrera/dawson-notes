﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelestialSystemQuestion
{
    public class CelestialBody : ICelestialEntity
    {
        public string Name { get; }

        public CelestialBody(string name)
        {
            this.Name = name;
        }

        public void Print()
        {
            Console.WriteLine(this.Name);
        }

        public int CountBodies()
        {
            return 1;
        }

        public int CountBodies(Filter f)
        {
            if(f(this))
            {
                return 1;
            }
            return 0;
        }
    }
}
