﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelestialSystemQuestion
{
    public delegate bool Filter(ICelestialEntity celestialBody);
    public interface ICelestialEntity
    {
        string Name { get; }
        
        void Print();
        int CountBodies();
        int CountBodies(Filter f);
    }
}
