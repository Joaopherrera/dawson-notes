﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelestialSystemQuestion
{
    class Program
    {
        static void Main(string[] args)
        {
            CelestialBody earth = new CelestialBody("Earth");
            
            CelestialBody sun = new CelestialBody("Sun");
            
            List<ICelestialEntity> celestialEntities = new List<ICelestialEntity>{ earth, sun };
            
            System solarSystem = new System(celestialEntities, "Solar System");
            
            CelestialBody sirius = new CelestialBody("Sirius");
            
            List<ICelestialEntity> celestialEntities1 = new List<ICelestialEntity> { solarSystem, sirius };

            System milkyWay = new System(celestialEntities1, "Milky Way");

            milkyWay.Print();

            int result = milkyWay.CountBodies((ICelestialEntity celestial) => celestial.Name.Length < 5);

            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
