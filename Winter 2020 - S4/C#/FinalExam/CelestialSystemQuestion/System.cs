﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelestialSystemQuestion
{
    public class System : ICelestialEntity
    {
        private List<ICelestialEntity> celestialEntities;
        public string Name { get; }

        public System(List<ICelestialEntity> celestialEntities, string name)
        {
            this.celestialEntities = celestialEntities;
            this.Name = name;
        }

        public void Print()
        {
            Console.WriteLine(this.Name);
            foreach (var item in this.celestialEntities)
            {
                item.Print();
            }
        }

        public int CountBodies()
        {
            int count = 0;
            foreach (var item in this.celestialEntities)
            {
                if (item is CelestialBody)
                {
                    count += item.CountBodies();
                }
            }
            return count;
        }

        public int CountBodies(Filter f)
        {
            int count = 0;
            foreach (var item in this.celestialEntities)
            {
                count += item.CountBodies(f);
            }
            return count;
        }
    }
}