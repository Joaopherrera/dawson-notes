﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppointmentsandCustomers.Models;

namespace AppointmentsandCustomers.Controllers
{
    public class HomeController : Controller
    {
        private final2Entities db = new final2Entities();

        // GET: Home
        public ActionResult Index()
        {
            var query = from a in db.Appointments
                        where a.TypeOfStyle.StartsWith("b")
                        orderby a.Price
                        orderby a.TypeOfStyle
                        select a;

            return View(query);
        }

        // GET: Home/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
