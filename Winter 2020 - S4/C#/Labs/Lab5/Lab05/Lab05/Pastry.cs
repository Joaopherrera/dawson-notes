﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    class Pastry : IBakedGood
    {
        public string Name
        {
            get
            {
                return "Pastry";
            }
        }

        public double Price
        {
            get
            {
                return 5;
            }
        }
    }
}
