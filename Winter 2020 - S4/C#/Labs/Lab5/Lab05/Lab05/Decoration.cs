﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    public abstract class Decoration
    {
        private IBakedGood bakedGood;

        public Decoration(IBakedGood bakedGood)
        {
            this.bakedGood = bakedGood;
        }
    }
}
