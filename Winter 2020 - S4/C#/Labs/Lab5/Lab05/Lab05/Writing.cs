﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    public class Writing : Decoration
    {
        private IBakedGood bakedGood;

        public Writing(IBakedGood bakedGood) : base(bakedGood)
        {
            this.bakedGood = bakedGood;
        }

        public string Name
        {
            get { return "Congratulations " + bakedGood.Name; }
        }

        public double Price
        {
            get { return bakedGood.Price; }
        }
    }
}
