﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    public class Cake : IBakedGood
    {
        public string Name 
        {
            get    
            {
                return "Cake";
            }
        }

        public double Price
        {
            get
            {
                return 20;
            }
        }
    }
}
