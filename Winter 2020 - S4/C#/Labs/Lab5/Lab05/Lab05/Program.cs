﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    class Program
    {
        static void Main(string[] args)
        {
            Cake cake = new Cake();

            Sprinkles cakeWithSprinkles = new Sprinkles(cake);
            Writing cakeWithWriting = new Writing(cake);

            Writing cakeWithSprinklesandWriting = new Writing(cakeWithSprinkles as IBakedGood);
            
            Console.WriteLine(cakeWithSprinklesandWriting.Name + " :" + cakeWithSprinklesandWriting.Price);


        }
    }
}
