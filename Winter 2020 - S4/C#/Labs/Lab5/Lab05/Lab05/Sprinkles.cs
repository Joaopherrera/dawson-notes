﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    public class Sprinkles : Decoration
    {
        private IBakedGood bakedGood;
        
        public Sprinkles(IBakedGood bakedGood) : base(bakedGood)
        {
            this.bakedGood = bakedGood;
        }

        public string Name
        {
            get { return bakedGood.Name; }
        }

        public double Price
        {
            get { return (bakedGood.Price * 1.05); }
        }
    }
}
