﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntList
{
	// Delegate types
	public delegate bool IntPredicate(int x);
	public delegate void IntAction(int x);

	// Integer lists with Act and Filter operations.
	// An IntList containing the element 7 9 13 may be constructed as 
	// new IntList(7, 9, 13) due to the params modifier.

	class IntList
	{
		List<int> list;

		public IntList(params int[] elements)
		{
			list = elements.ToList();
		}

		public void Act(IntAction f)
		{
			foreach (var item in this.list)
			{
				f(item);
			}
		}

		public IntList Filter(IntPredicate p)
		{
			List<int> tempList = new List<int>();

			foreach (var item in this.list)
			{
				if(p(item))
				{
					tempList.Add(item);
				}
			}
			IntList newList = new IntList(tempList.ToArray());
			return newList;
		}
	}

	class MyTest
	{
		public static void Main(String[] args)
		{
			IntList list = new IntList(2, 6, 8, 1, 56);

			list.Act(Console.WriteLine);

			Console.WriteLine("---------");

			IntList newList = list.Filter(x => x % 2 == 0);
			newList.Act(Console.WriteLine);

			Console.WriteLine("---------");

			IntList newList2 = list.Filter(x => x >= 25);
			newList2.Act(Console.WriteLine);

			Console.ReadKey();
		}



	}
}
	