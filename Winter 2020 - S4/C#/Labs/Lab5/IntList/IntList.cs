	// Delegate types
	public delegate bool IntPredicate(int x);
	public delegate void IntAction(int x);

	// Integer lists with Act and Filter operations.
	// An IntList containing the element 7 9 13 may be constructed as 
	// new IntList(7, 9, 13) due to the params modifier.

	class IntList
	{
		List<int> list;

		public IntList(params int[] elements)
		{

		}

		public void Act(IntAction f)
		{

		}

		public IntList Filter(IntPredicate p)
		{

		}
	}

	class MyTest
	{
		public static void Main(String[] args)
		{


			Console.ReadKey();

		}

	}