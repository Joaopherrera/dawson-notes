﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    class Shape : GraphicComponent
    {

        public Shape(string name)
        {
            this.Name = name;
        }

        public override void Display()
        {
            Console.WriteLine("I am a: " + this.Name);
        }
    }
}
