﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    class Graphic : GraphicComponent
    {
        private List<GraphicComponent> components = new List<GraphicComponent>();
        
        public Graphic(string name)
        {
            this.Name = name;
        }

        public override void Display()
        {
            Console.WriteLine("I am a: " + this.Name);
            foreach (var comp in components)
            {
                comp.Display();
            }
        }

        public void Add(GraphicComponent gc)
        {
            components.Add(gc);
        }

        public bool Remove(GraphicComponent gc)
        {
            return components.Remove(gc);
        }
    }
}
