﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab04
{
    class Program
    {
        static void Main(string[] args)
        {
            Graphic house = new Graphic("house");
            Shape triangle = new Shape("triangle roof");
            Graphic square = new Graphic("square");
            Graphic rectangle = new Graphic("Door rectangle");
            Shape circle = new Shape("Knob circle");
            Shape windowL = new Shape("Left Window");
            Shape windowR = new Shape("Right Window");

            house.Add(triangle);
            house.Add(square);
            square.Add(rectangle);
            square.Add(windowL);
            square.Add(windowR);
            rectangle.Add(circle);

            house.Display();
            Console.ReadKey();
        }

        public static void PrintPath(string path)
        {
            string fileName = Path.GetFileName(path);
            string[] files = Directory.GetFiles(path);
            string[] dirs = Directory.GetDirectories(path);

            Console.WriteLine(fileName);
            foreach (var file in files)
            {
                Console.WriteLine("     |_" + file);
            }

            foreach (var dir in dirs)
            {
                Console.WriteLine("|_" + dir);
                PrintPath(dir);
            }
        }
    }
}
