﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public abstract class GraphicComponent
    {
        private string name;
        
        public string Name
        {
            get { return name; }

            protected set { name = value; }
        }

        public abstract void Display();
    }
}
