﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace KNearestNeighbour
{
    public class KNN
    {
        private FourDPoint[] trainingData;
        private int k;
        private int numLabels;

        public KNN (string filename, int k)
        {
            loadData(filename);
            this.k = k;
            //figure out how many possible labels (categories) there are in the training set
            this.numLabels = findNumLabels();
        }

        private void loadData (string filename)
        {
            //reads the file, skipping the first line
            //for each subsequent line, create an array of 4 doubles, plus read the string

            //Hint: System.IO.File class has a method ReadAllLines(string path) that returns an array of strings
            // The string's Split method in C# is similar to Java: Split takes a params char[], which means
            // that you can pass it an array or one or more comma-separated chars (i.e. like varargs … in Java).
            // In the provided file, the separator is ‘,‘. To convert form string to double, use the double.Parse
            // or double.TryParse method
            // Note that you need to instantiate this.trainingData array, and instanitate and assign a FourDPoint
            // into the training data array for every line in the file (ignoring the first line)
            
            //TODO
            throw new NotImplementedException();
        }

        private int findNumLabels()
        {
            //First, go through trainingData and find all the unique labels
            string[] labels = new string[100]; //larger array than needed
            int count = 0;
            for (int i = 0; i < this.trainingData.Length; i++)
            {
                string current = trainingData[i].Label;
                if (Array.IndexOf(labels, current) < 0)  // not found
                {
                    labels[count] = current;
                    count++;
                }
            }

            //return count of unique labels
            return count;
        }

        /// <summary>
        /// Uses the k-nearest-neighbour algorithm to predict the label of a given point.
        /// </summary>
        /// <param name="point"></param>
        /// <returns>string of the predicted label</returns>
        public string PredictLabel(double[] point)
        {
            FourDPoint tbc = new FourDPoint(point, "");
            //TODO
            //First make sure that you go through all the traning points and calculate the distance with the point to be
            //classified (complete the private method below)
            fillDistances(tbc);

            //Then get the k nearest FourDPoints (complete the private method below)
            FourDPoint[] closest = getKNearest();

            //Then count the votes
            return this.getPrediction(closest);
        }

        private void fillDistances(FourDPoint tbc)
        {
            //go through all the traning points and calculate the distance with the point to be classified. 
            //TODO
            throw new NotImplementedException();
        }

        private FourDPoint[] getKNearest()
        {
            //go through the traning data and return an array with the k smallest FourDPoints
            //TODO
            throw new NotImplementedException();
        }

        private string getPrediction(FourDPoint[] closest)
        {
            //initialize two parallel arrays (or one array of tuples)
            string[] labels = new string[this.numLabels];
            int[] votes = new int[this.numLabels];
            int count = 0; //counter of different labels within the k closest 

            //go through the closest points and count occurences
            for (int i = 0; i < closest.Length; i++)
            {
                string current = closest[i].Label;
                int found = Array.IndexOf(labels, current);
                if ( found < 0)  // not found, first vote
                {
                    labels[count] = current;
                    votes[count] = 1;
                    count++;
                }
                else //already has 1 vote
                {
                    votes[found]++;
                }
            }

            //Find the index of the highest number of votes (choose first in case of tie)
            //TODO

            //return labels[indexHighest];
            throw new NotImplementedException();
        }
    }
}
