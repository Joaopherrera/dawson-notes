﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNearestNeighbour
{
    /// <summary>
    /// Represents a datapoint with 4 numeric features (or dimensions) and a label (or category). Can be compared
    /// with another FourDPoint based on distance to new point to be classified
    /// </summary>
    public class FourDPoint
    {
        /// <summary>
        /// Point property is 4 dimension array with the features
        /// </summary>
        public double[] Point {get; private set; }

        /// <summary>
        /// Label property is a String representing the category
        /// </summary>
        public string Label { get; private set; }

        /// <summary>
        /// Distance property saves the distance to the new datapoint to be classified
        /// </summary>
        public double Distance { get; private set; }

        public FourDPoint(double[] point, string label)
        {
            this.Point = point;
            this.Label = label;
        }

        //Note the new shortcut syntax in C# 7.1
        /*
          public FourDPoint(double[] point, string label) => (this.Point, this.Label) = (point, label);
        */

        /// <summary>
        /// Calculates the Euclididean distance between this and the other 4DPoint, and saves as Distance property
        /// </summary>
        /// <param name="toBeClassified">The new 4DPoint to be classified</param>
        public void CalcEuclideanDistance( FourDPoint toBeClassified)
        {
            //TODO
            throw new NotImplementedException();
        }
    }
}
