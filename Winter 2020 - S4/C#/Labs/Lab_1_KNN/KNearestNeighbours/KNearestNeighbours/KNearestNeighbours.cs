﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KNearestNeighbour;

namespace KNearestNeighbours
{
    class KNearestNeighbours
    {
        static void Main(string[] args)
        {
            KNN knn = new KNN("..//iris.csv", 3);
            double[] numbers = { 2.2, 3.6, 5.1, 2.5 };
            String result = knn.PredictLabel(numbers);
            System.Diagnostics.Debug.WriteLine(result);
        }
    }
}
