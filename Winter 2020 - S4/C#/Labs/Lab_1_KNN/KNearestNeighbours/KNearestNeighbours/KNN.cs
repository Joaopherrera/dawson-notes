﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace KNearestNeighbour
{
    public class KNN
    {
        private FourDPoint[] trainingData;
        private int k;
        private int numLabels;

        public KNN (string filename, int k)
        {
            loadData(filename);
            this.k = k;
            //figure out how many possible labels (categories) there are in the training set
            this.numLabels = findNumLabels();
        }

        private void loadData (string filename)
        {
            //reads the file, skipping the first line
            //for each subsequent line, create an array of 4 doubles, plus read the string

            //Hint: System.IO.File class has a method ReadAllLines(string path) that returns an array of strings
            // The string's Split method in C# is similar to Java: Split takes a params char[], which means
            // that you can pass it an array or one or more comma-separated chars (i.e. like varargs … in Java).
            // In the provided file, the separator is ‘,‘. To convert form string to double, use the double.Parse
            // or double.TryParse method
            // Note that you need to instantiate this.trainingData array, and instanitate and assign a FourDPoint
            // into the training data array for every line in the file (ignoring the first line)

            string[] allLines = System.IO.File.ReadAllLines(filename);

            FourDPoint[] dummie = new FourDPoint[150];
            for (int i = 1; i < allLines.Length-1; i++)
            {
                string[] sentence = allLines[i].Split(',');

                double[] numbers = new double[4];
                for (int j = 0; j < 3; j++)
                {
                    numbers[j] = double.Parse(sentence[j]);
                }
                dummie[i - 1] = new FourDPoint(numbers, sentence[4]);
            }

            trainingData = dummie;
        }

        private int findNumLabels()
        {
            //First, go through trainingData and find all the unique labels
            string[] labels = new string[150]; //larger array than needed
            int count = 0;
            for (int i = 0; i < this.trainingData.Length - 1; i++)
            {
                string current = trainingData[i].Label;
                if (Array.IndexOf(labels, current) < 0)  // not found
                {
                    labels[count] = current;
                    count++;
                }
            }

            //return count of unique labels
            return count;
        }

        /// <summary>
        /// Uses the k-nearest-neighbour algorithm to predict the label of a given point.
        /// </summary>
        /// <param name="point"></param>
        /// <returns>string of the predicted label</returns>
        public string PredictLabel(double[] point)
        {
            FourDPoint tbc = new FourDPoint(point, "");

            //First make sure that you go through all the traning points and calculate the distance with the point to be
            //classified (complete the private method below)
            fillDistances(tbc);

            //Then get the k nearest FourDPoints (complete the private method below)
            FourDPoint[] closest = getKNearest();

            //Then count the votes
            return this.getPrediction(closest);
        }

        private void fillDistances(FourDPoint tbc)
        {
            //go through all the traning points and calculate the distance with the point to be classified. 
            for (int i = 0; i < trainingData.Length -1; i++)
            {
                trainingData[i].CalcEuclideanDistance(tbc);
            }
        }

        private FourDPoint[] getKNearest()
        {
            //go through the traning data and return an array with the k smallest FourDPoints
            Sort();
            FourDPoint[] topK = new FourDPoint[k];
            for (int i = 0; i < k - 1; i++)
            {
                topK[i] = trainingData[i];
            }
            return topK;
        }

        private string getPrediction(FourDPoint[] closest)
        {
            //initialize two parallel arrays (or one array of tuples)
            string[] labels = new string[this.numLabels];
            int[] votes = new int[this.numLabels];
            int count = 0; //counter of different labels within the k closest 

            //go through the closest points and count occurences
            for (int i = 0; i < closest.Length - 1; i++)
            {
                string current = closest[i].Label;
                int found = Array.IndexOf(labels, current);
                if ( found < 0)  // not found, first vote
                {
                    labels[count] = current;
                    votes[count] = 1;
                    count++;
                }
                else //already has 1 vote
                {
                    votes[found]++;
                }
            }

            //Find the index of the highest number of votes (choose first in case of tie)
            int indexHighest = votes[0];
            for (int i = 1; i < votes.Length - 1; i++)
            {
                if (votes[indexHighest] < votes[i])
                {
                    indexHighest = i;
                }
            }

            return labels[indexHighest];
        }
        
        public void Sort()
        {
            int smallest;
            for (int i = 0; i < trainingData.Length - 1; i++)
            {
                smallest = i;

                for (int index = i + 1; index < trainingData.Length - 1; index++)
                {
                    if (trainingData[index].Distance < trainingData[smallest].Distance)
                    {
                        smallest = index;
                    }
                }
                Swap(i, smallest);
            }

        }
        public void Swap(int first, int second)
        {
            FourDPoint temp = trainingData[first];
            trainingData[first] = trainingData[second];
            trainingData[second] = temp;
        }
    }
}
