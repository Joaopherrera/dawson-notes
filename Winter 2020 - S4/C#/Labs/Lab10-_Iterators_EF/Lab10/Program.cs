﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab10
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var db = new db1835031DbContext())
            {
                //addAddress(db);
                //changeAddress(db);
                deleteAddress(db);
            }
        }

        static void addAddress(db1835031DbContext db)
        {
            var address1 = new Address();
            address1.FirstName = "John";
            address1.LastName = "Johnson";
            address1.Email = "johnjohn@email.com";
            address1.PhoneNumber = "123123123";

            var address2 = new Address();
            address2.FirstName = "Mike";
            address2.LastName = "Mikeson";
            address2.Email = "mikemike@email.com";
            address2.PhoneNumber = "74563677";

            var address3 = new Address();
            address3.FirstName = "Roger";
            address3.LastName = "Rogerson";
            address3.Email = "rogerroger@email.com";
            address3.PhoneNumber = "3246755421";
            
            //db.Addresses.Add(address1);
            db.Addresses.Add(address2);
            db.Addresses.Add(address3);


            db.SaveChanges();
        }

        static void changeAddress(db1835031DbContext db)
        {
            Address address = (from a in db.Addresses where a.FirstName.Contains("M") orderby a.FirstName select a).FirstOrDefault();

            if(address != null)
            {
                address.LastName = "notMikesonAnymore";
                db.SaveChanges();
            }
        }

        static void deleteAddress(db1835031DbContext db)
        {
            Address address = (from a in db.Addresses where a.FirstName.Contains("M") orderby a.FirstName select a).FirstOrDefault();

            if (address != null)
            {
                db.Addresses.Remove(address);
                db.SaveChanges();
            }
        }
    }
}
