﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03
{
    public class Fraction
    {
        private int num;
        private int denom;

        public Fraction(int num, int denom)
        {
            int gcd = Program.GDC(num, denom);

            this.num = (num / gcd);
            this.denom = (denom / gcd);
        }

        public static Fraction operator + (Fraction a, Fraction b)
        {
            int num, denom;
            num = a.num * b.denom + b.num * a.denom;
            denom = a.denom * b.denom;
            return new Fraction(num, denom);
        }

        public static Fraction operator - (Fraction a, Fraction b)
        {
            int num, denom;
            num = a.num * b.denom - b.num * a.denom;
            denom = a.denom * b.denom;
            return new Fraction(num, denom);
        }

        public static Fraction operator * (Fraction a, Fraction b)
        {
            return new Fraction((a.num * b.num), (a.denom * b.denom));
        }

        public static Fraction operator / (Fraction a, Fraction b)
        {
            int num = a.num * b.denom;
            int denom = a.denom * b.num;
            return new Fraction(num, denom);
        }

        public static bool operator == (Fraction a, Fraction b)
        {
            if (Object.ReferenceEquals(null, a))
            {
                return Object.ReferenceEquals(null, b);
            }
            
            if (Object.ReferenceEquals(null, b))
            {
                return false;
            }
            
            return (a.num == b.num && a.denom == b.denom);
        }

        public static bool operator != (Fraction a, Fraction b)
        {
            if (Object.ReferenceEquals(null, a))
            {
                return !(Object.ReferenceEquals(null, b));
            }

            if (Object.ReferenceEquals(null, b))
            {
                return true;
            }

            return !(a.num == b.num && a.denom == b.denom);
        }

        public static implicit operator Fraction (int x)
        {
            return new Fraction(x, 1);
        }

        public static explicit operator int (Fraction a)
        {
            return (a.num / a.denom);
        }

        public override bool Equals(object obj)
        {
            // If parameter is null return false.
            if (Object.ReferenceEquals(null, obj))
            { 
                return false;
            }
            if (Object.ReferenceEquals(this, obj))
            {
                return true;
            }

            // check if same type in inheritance hierarchy
            if (obj is Fraction)
            {
                Fraction f = obj as Fraction; //same as (Fraction)obj;
                return (this == f);
            }
            return false;
        }
    }
}
