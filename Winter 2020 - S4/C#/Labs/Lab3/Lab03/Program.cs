﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Console.WriteLine(GDC(45, 12));
        }

        public static int GDC(int n, int m)
        {
            int remainder;
            if (n < 0 || m < 0)
            {
                throw new ArgumentException("One of the variables is less than zero.");
            }

            if (n > m)
            {
                int temp = n;
                n = m;
                m = temp;
            }

            while (m > 0)
            {
                remainder = n % m;
                n = m;
                m = remainder;
            }

            return n;
        }
    }
}
