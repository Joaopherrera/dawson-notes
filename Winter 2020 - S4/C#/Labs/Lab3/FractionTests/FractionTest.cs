﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab03;

namespace Lab03Test
{
    [TestClass]
    public class FractionTest
    {
        [TestMethod]
        public void SumTest()
        {
            //Arrange
            Fraction a = new Fraction(1,2);
            Fraction b = new Fraction(2, 3);

            Fraction expected = new Fraction(7,6);
            //Act
            Fraction result = a + b;

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MinusTest()
        {
            //Arrange
            Fraction a = new Fraction(2, 3);
            Fraction b = new Fraction(1, 2);

            Fraction expected = new Fraction(1, 6);
            
            //Act
            Fraction result = a - b;

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MultTest()
        {
            //Arrange
            Fraction a = new Fraction(2, 3);
            Fraction b = new Fraction(1, 2);
            
            Fraction expected = new Fraction(2, 6);

            //Act
            Fraction result = a * b;

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DivsTest()
        {
            //Arrange
            Fraction a = new Fraction(2, 3);
            Fraction b = new Fraction(2, 5);

            Fraction expected = new Fraction(10, 6);

            //Act
            Fraction result = a / b;

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void ImplicitTest()
        {
            //Arrange
            Fraction a = new Fraction(27, 3);
            int expected = 9;

            //Act
            int result = (int)a;

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void ExplicitTest()
        {
            //Arrange
            Fraction expected = new Fraction(3, 1);

            //Act
            Fraction a = 3;

            //Assert
            Assert.AreEqual(expected, a);
        }
    }
}
