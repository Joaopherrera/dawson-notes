﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab07
{
    public delegate void TickHandler(int count);

    class Metronome
    {
        public event TickHandler Tick;
        private int sleep;
        private int totalTicks;

        public Metronome(int sleep, int totalTicks)
        {
            this.sleep = sleep;
            this.totalTicks = totalTicks;
            Start();
        }

        public void Start()
        {

        }
    }
}
