﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab07
{
    public delegate void HREventHandler(String name, string employeeID);

    class HR
    {
        public event HREventHandler NewEmployeeEvent;
        
        public void RegisterEmployee(string name, string employeeId)
        {
            // if this was a real application we would do lots of HR related stuff here 
            // raise the event that the new employee is fired here   
            NewEmployeeEvent?.Invoke(name, employeeId);
        }
    }

    class Admin
    {
        public Admin(HR hr)
        {
            //subscribe to the event
            hr.NewEmployeeEvent += PrintNewEmployee;
        }
        
        public void PrintNewEmployee(string name, string employeeID)
        {
            Console.WriteLine(name + " " + employeeID);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            HR hr = new HR();
            Admin admin = new Admin(hr);
            hr.RegisterEmployee("Joe Kerr", "a112233");
            Console.ReadLine();
        }
    }
}
