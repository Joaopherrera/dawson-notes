﻿using Lab11.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lab11.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Address> results;
            using(var db = new db1835031Entities())
            {
                var query = from address in db.Addresses
                            orderby address.LastName, address.FirstName descending
                            select address;

                results = query.ToList();
            }

            return View(results);
        }
    }
}