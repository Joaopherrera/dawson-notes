﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Linq1();
            Linq2();
            Linq3();
            Linq4();
			
			Console.ReadKey();
        }

        public static void Linq1()
        {
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            // add LINQ query to find all elements of the array that are even.

            Console.WriteLine("even numbers:");
            // add the code that will write result of the query to the console

        }

        public static void Linq2()
        {
            string[] words = { "cherry", "apple", "blueberry" };

            // add LINQ query that sorts the words alphabetically

            Console.WriteLine("The alphabetically sorted list of words:");
            // add the code that will write result of the query to the console


            // add LINQ query that sorts the words according to their length

            Console.WriteLine("The sorted list of words (by length):");
            // add the code that will write result of the query to the console
        }

        public static void Linq3()
        {
            string[] digits = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

            // add LINQ query that uses a compound orderby to sort a list of digits, first by length of their name, and then alphabetically 

            Console.WriteLine("Compound sorted digits:");
            // add the code that will write result of the query to the console
        }

        public static void Linq4()
        {
            Program part3 = new Program();

        }

        private List<Student> students = new List<Student>
        {
            new Student {First="Svetlana", Last="Omelchenko", ID=111, Scores= new List<int> {97, 92, 81, 60}},
            new Student {First="Claire", Last="O'Donnell", ID=112, Scores= new List<int> {75, 84, 91, 39}},
            new Student {First="Sven", Last="Mortensen", ID=113, Scores= new List<int> {88, 94, 65, 91}},
            new Student {First="Cesar", Last="Garcia", ID=114, Scores= new List<int> {97, 89, 85, 82}},
            new Student {First="Debra", Last="Garcia", ID=115, Scores= new List<int> {35, 72, 91, 70}},
            new Student {First="Fadi", Last="Fakhouri", ID=116, Scores= new List<int> {99, 86, 90, 94}},
            new Student {First="Hanying", Last="Feng", ID=117, Scores= new List<int> {93, 92, 80, 87}},
            new Student {First="Hugo", Last="Garcia", ID=118, Scores= new List<int> {92, 90, 83, 78}},
            new Student {First="Lance", Last="Tucker", ID=119, Scores= new List<int> {68, 79, 88, 92}},
            new Student {First="Terry", Last="Adams", ID=120, Scores= new List<int> {99, 82, 81, 79}},
            new Student {First="Eugene", Last="Zabokritski", ID=121, Scores= new List<int> {96, 85, 91, 60}},
            new Student {First="Michael", Last="Tucker", ID=122, Scores= new List<int> {94, 92, 91, 91}}
        };
    }

    public class Student
    {
        public string First { get; set; }
        public string Last { get; set; }
        public int ID { get; set; }
        public List<int> Scores;
    }
}

