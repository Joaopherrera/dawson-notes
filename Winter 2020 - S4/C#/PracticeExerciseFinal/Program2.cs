﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeExerciseFinal
{
    public delegate bool Predicate(Album a);
    
    public class Store
    {
        private List<Album> albums;

        public Store()
        {
            albums = new List<Album>();
        }

        public void Add(Album a)
        {
            albums.Add(a);
        }

        public List<Album> FindAll(Predicate p)
        {
            List<Album> newList = new List<Album>();
            foreach (var item in albums)
            {
                if (p(item))
                    newList.Add(item);
            }
            return newList;
        }
    }

    public class Album
    {
        public string Title { get; set; }
        public Artist artist { get; set; }
        public int Year { get; set; }
    }

    public class Artist
    {
        public string Name { get; set; }
        public bool Active { get; set; }
    }

    public class Program2
    {
        static void Main(String[] args)
        {
            Artist prince = new Artist { Name = "Prince", Active = false };
            Album pr = new Album { Title = "Purlple Rain", artist = prince, Year = 1984 };
            Store playlist80s = new Store();
            playlist80s.Add(pr);
            playlist80s.FindAll((Album a) => a.artist.Active);
        }
    }

}
