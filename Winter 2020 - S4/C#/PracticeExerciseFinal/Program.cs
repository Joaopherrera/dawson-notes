﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeExerciseFinal
{
    public delegate void PartHandler(Part p);

    public class Factory
    {
        public event PartHandler PartCreated;

        public Part Create()
        {
            Part p = new Part();
            PartCreated?.Invoke(p);
            return p;
        }
    }

    public class Inventory
    {
        public List<Part> parts;

        public Inventory(Factory f)
        {
            parts = new List<Part>();
            f.PartCreated += (Part p) => parts.Add(p);
        }
    }

    public class Part
    {}

    class Program
    {
        static void Main(string[] args)
        {
            Factory f = new Factory();
            Inventory i = new Inventory(f);

            f.Create();

            Console.WriteLine(i.parts[0]);
            Console.ReadKey();
        }
    }
}
