"use strict";

/* Use an object literal to declare a global name space to hold any global variables
  In this object literal, also define an array that lists all the filenames of the supplied images: 
"cat.jpg", "doge.jpg", etc. .
*/
var myImageGallery = {
		imageIndex : 0,
		idList:[],
		altTextArr:[],
		imageFilesList : ["cat.jpg","doge.jpg","dolphin.jpg","leopard.jpg",
		"mouse.jpg","parrot.jpg","polarbear.jpg","tortoise.jpg","owl.jpg"],
		imageFilesElements:[],
};

var slideShowSection = null; 


// create a function updateImageIndex(step) with the following specs
//. step is the index value of the array 
function updateImageIndex(step)
{	//normally step is -1 (prev) prev +1 (next)
	var offset= myImageGallery.imageIndex + step
	//check if you are at the beginning and you go prev
	if(offset <0) myImageGallery.imageIndex=myImageGallery.imageFilesList.length-1;
	else	//check if you are at the end and you go next
		if(offset>  myImageGallery.imageFilesList.length-1) myImageGallery.imageIndex=0;
	else
		myImageGallery.imageIndex=offset;
	//--------------------------------
	// now replace the old image with the new onerror
	var oldImage = slideShowSection.children.slideImage;
  slideShowSection.replaceChild
	( myImageGallery.imageFilesElements[myImageGallery.imageIndex], 
				oldImage);
}

function previous() { updateImageIndex(-1);}
function next() { updateImageIndex(1); }
function first() { updateImageIndex(1000);}
function last() { updateImageIndex(-1000);}


function preloadImages() {
	var imageFilesElements =[];
  imageFilesElements =  myImageGallery.imageFilesList.map(function(imageFileName) {
    var element = new Image();
    element.id = "slideImage";
    element.alt = "spider slideshow";
    element.src = "images/" + imageFileName;
    return element;
  });
return imageFilesElements;
  }

myImageGallery.imageFilesElements = preloadImages();

// initialization
document.addEventListener("DOMContentLoaded", function () {
  // set up slide show event handlers
   slideShowSection = document.getElementById("slideSection");
  
  var prevButton = document.getElementById("prev");	prevButton.addEventListener("click", previous);
  var nextButton = document.getElementById("next");	nextButton.addEventListener("click", next);

  var firstButton = document.getElementById("first");	firstButton.addEventListener("click", first);
  var lastButton = document.getElementById("last");	lastButton.addEventListener("click", last);

});