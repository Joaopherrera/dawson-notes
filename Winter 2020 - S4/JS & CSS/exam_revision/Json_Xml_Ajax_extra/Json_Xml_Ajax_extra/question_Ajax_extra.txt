

 Ajax: text versus JSON versus XML

 ## setup
in you sonic JS423, create a folder called Week15
Create a sub-folder called Json_Xml_Ajax_extra
copy all the files in  Json_Xml_Ajax_extra

[you can always  test Run on a local server, you can use the Atom Live Server plugin (already installed in labs). 
You'll be able to navigate to a URL like the following in any browser: http://127.0.0.1:3000/...


## Tasks

1.  Modify the JS file ajaxAddress.js to change the language of the form labels based
    on XML (language.xml file) requested from the server instead of a text file. 
	
1.  Modify the JS file ajaxAddress.js to change the language of the form labels based
    on JSON (language.json file) requested from the server.
