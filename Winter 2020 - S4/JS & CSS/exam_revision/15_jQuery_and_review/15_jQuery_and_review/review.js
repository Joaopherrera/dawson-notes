"use strict";

// TODO remove this comment to enable eslint
/* eslint-disable no-unused-vars */

var g = {
  // used for slide show
  imageData: [
    {
      "albumId": 4,
      "id": 151,
      "title": "0 possimus dolor minima provident ipsam",
      "url": "http://placehold.it/600/1d2ad4",
      "thumbnailUrl": "http://placehold.it/150/1d2ad4"
    },
    {
      "albumId": 4,
      "id": 152,
      "title": "1 et accusantium enim pariatur eum nihil fugit",
      "url": "http://placehold.it/600/a01c5b",
      "thumbnailUrl": "http://placehold.it/150/a01c5b"
    },
    {
      "albumId": 4,
      "id": 153,
      "title": "2 eum laborum in sunt ea",
      "url": "http://placehold.it/600/9da52c",
      "thumbnailUrl": "http://placehold.it/150/9da52c"
    },
    {
      "albumId": 4,
      "id": 154,
      "title": "3 dolorum ipsam odit",
      "url": "http://placehold.it/600/7f330f",
      "thumbnailUrl": "http://placehold.it/150/7f330f"
    },
    {
      "albumId": 4,
      "id": 155,
      "title": "4 occaecati sed earum ab ut vel quibusdam perferendis nihil",
      "url": "http://placehold.it/600/877cd8",
      "thumbnailUrl": "http://placehold.it/150/877cd8"
    },
    {
      "albumId": 4,
      "id": 156,
      "title": "5 sed quia accusantium nemo placeat dolor ut",
      "url": "http://placehold.it/600/11af10",
      "thumbnailUrl": "http://placehold.it/150/11af10"
    }
  ],

  init: function () {
    // slide show (TODO)
    g.showButton = document.getElementById("showBtn");
    g.imageWrapper = document.getElementById("wrapper");



    // welcome greeting (TODO)
    g.guestEl = document.querySelector("#welcome a");



    // ajax queries (TODO)
    g.queryButtons = document.querySelectorAll(".query");


  },

  // TODO feel free to add as many other functions
  // as you like to the namespace

  displayEmails: function (data) {
    // TODO
    return;
  },

  displayError: function (request) {
    // TODO
    return;
  },

  getRequestHandler: function(url) {
    // TODO
    return;
  },

  updateCookieObj: function () {
    g.allCookies = {};
    // TODO
  }

};

document.addEventListener("DOMContentLoaded", g.init);
