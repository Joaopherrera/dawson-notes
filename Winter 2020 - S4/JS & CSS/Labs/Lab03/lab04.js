//--- 1 ---//
console.log('----------------- 1 -----------------')

function checkType(element)
{
    if ((typeof element) === "undefined")
    {
        return undefined;
    }
    else if ((typeof element) == null)
    {
        return null;
    }
    else if ((typeof element) === "number")
    {
        return "number";
    }
    else if ((typeof element) === "string")
    {
        return "string";
    }
    else if ((typeof element) === "array")
    {
        return "array";
    }
    else if ((typeof element) === "object")
    {
        return  "object";
    }
}
var myVar = 'example';
console.log(checkType(myVar));

//--- 2 ---//
console.log('----------------- 2 -----------------')

function arrayFirstElems(array, number)
{
    if (number < 0)
    {
        return 'Number less than 0.';
    }

    if( typeof array == array || array === "string")
    {
        return 'Not an array or string.';
    }

    if (isNaN(number))
    {
        return 'Not an number.';
    }

    var newArray = new Array(number);

    for (var i = 0; i < number; i++)
    {
        newArray[i] = array[i];
    }

    return newArray;
}

arr1= [1,2,3,4,5,6];
arr2 =['a', [2,5,4], [33], 'f',2.5];

var test1 = arrayFirstElems ('hello',3);
var test2 = arrayFirstElems (arr1,-4);
var test3 = arrayFirstElems (arr1,4);
var test4 = arrayFirstElems (arr2,3);

console.log('Test 1: ' + test1);
console.log('Test 2: ' + test2);
console.log('Test 3: ' + test3);
console.log('Test 4: ' + test4);

//--- 3 ---//
console.log('----------------- 3 -----------------')

function arrayLastElems(array, number)
{
    if (number < 0)
    {
        return 'Number less than 0.';
    }

    if( typeof array == array || array === "string")
    {
        return 'Not an array or string.';
    }

    if (isNaN(number))
    {
        return 'Not a number';
    }

    var newArray = new Array(number);

    for (var i = 0; i < number; i++)
    {
        newArray[i] = array[(array.length - 1)- i];
    }

    return newArray;
}

var test1 = arrayLastElems ('hello',3);
var test2 = arrayLastElems (arr1,-4);
var test3 = arrayLastElems (arr1,4);
var test4 = arrayLastElems (arr2,3);

console.log('Test 1: ' + test1);
console.log('Test 2: ' + test2);
console.log('Test 3: ' + test3);
console.log('Test 4: ' + test4);

//-- 4 --//
console.log('----------------- 4 -----------------')

function vectSumSquare(vect)
{
    var sum = 0;

    for (var i = 0; i < vect.length ; i++)
    {
        sum = sum + Math.pow(vect[i],2);
    }
    return sum;
}

var v =[3,4];
console.log(vectSumSquare(v));

//-- 5 --//
console.log('----------------- 5 -----------------')

function Book(authorName, title, numbPages, price)
{
    this.authorName = authorName;
    this.title = title;
    this.numbPages = numbPages;
    this.price = price;
}
var book1 = new Book('smith', 'into to java', 120, 59.9);
var book2 = new Book('sara', 'into to javascript', 100,49.9);
var book3 = new Book('james', 'basic of AI', 300,79.9);
var book4 = new Book('ali', 'coding with C++', 250,19.9);
var book5 = new Book('baker', 'physics', 280,39.9);

bookArray = [book1, book2, book3, book4, book5];

function sortBook_author(a, b)
{
    if (a.authorName > b.authorName)
    {
        return -1;
    }
    if (b.authorName > a.authorName)
    {
        return 1;
    }
    return 0;
}

function sortBook_title(a, b)
{
    if (a.title > b.title)
    {
        return -1;
    }
    if (b.title > a.title)
    {
        return 1;
    }
    return 0;
}

function sortBook_price(a, b)
{
    return a.price - b.price;
}

function sortBook_numberPages(a, b)
{
    return a.numbPages - b.numbPages;
}
console.log(bookArray);
console.log('------------')
bookArray.sort(sortBook_price);

console.log(bookArray);


function sortBook_authorDESC(a, b)
{
    if (a.authorName > b.authorName)
    {
        return 1;
    }
    if (b.authorName > a.authorName)
    {
        return -1;
    }
    return 0;
}

function sortBook_titleDESC(a, b)
{
    if (a.title > b.title)
    {
        return 1;
    }
    if (b.title > a.title)
    {
        return -1;
    }
    return 0;
}

function sortBook_priceDESC(a, b)
{
    return b.price - a.price;
}

function sortBook_numberPagesDESC(a, b)
{
    return b.numbPages - a.numbPages;
}