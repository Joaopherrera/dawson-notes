document.addEventListener('DOMContentLoaded', process);

//Global Variables
var animalImagesArr;
var currentImageIndex = 0;

function preload()
{
    //Preload for the Animals
    var animalimg1 = document.getElementById('animalImg1');
    animalimg1.src = "images/animals/cat.PNG";

    var animalimg2 = document.getElementById('animalImg2');
    animalimg2.src = "images/animals/dog.PNG";
    animalimg2.style.display = "none";

    var animalimg3 = document.getElementById('animalImg3');
    animalimg3.src = "images/animals/duck.PNG";
    animalimg3.style.display = "none";

    var animalimg4 = document.getElementById('animalImg4');
    animalimg4.src = "images/animals/lion.PNG";
    animalimg4.style.display = "none";

    var animalimg5 = document.getElementById('animalImg5');
    animalimg5.src = "images/animals/ostrich.PNG";
    animalimg5.style.display = "none";

    var animalimg6 = document.getElementById('animalImg6');
    animalimg6.src = "images/animals/panda.JPG";
    animalimg6.style.display = "none";

    var animalimg7 = document.getElementById('animalImg7');
    animalimg7.src = "images/animals/polarbear.JPG";
    animalimg7.style.display = "none";

    var animalimg8 = document.getElementById('animalImg8');
    animalimg8.src = "images/animals/zebra.PNG";
    animalimg8.style.display = "none";

    animalImagesArr = [animalimg1, animalimg2, animalimg3, animalimg4, animalimg5, animalimg6, animalimg7, animalimg8];

    //Preload for the Insects
    //TODO - Preload for the Insects
}

function imageViewer()
{
    var target = event.target;

    switch (target)
    {
        case document.getElementById('btn-first'):
            currentImageIndex = 0;

            break;

        case document.getElementById('btn-previous'):
            currentImageIndex = currentImageIndex - 1;
            break;

        case document.getElementById('btn-next'):
            currentImageIndex = currentImageIndex + 1;
            break;

        case document.getElementById('btn-last'):
            currentImageIndex = 7;
            break;
    }

    imageGallery();

    if (currentImageIndex == 8)
    {
        currentImageIndex = 0;
        animalImagesArr[0].style.display = "block";
    }
    if (currentImageIndex == -1)
    {
        currentImageIndex = 8;
        animalImagesArr[0].style.display = "block";
    }
}

function imageGallery()
{
    for (var i = 0; i < animalImagesArr.length; i++)
    {
        if (i == currentImageIndex)
        {
            animalImagesArr[i].style.display = "block";
        }
        else
        {
            animalImagesArr[i].style.display = "none";;
        }
    }
}

function process()
{
    console.log('Starts...');
    preload();

    document.getElementById('btn-first').addEventListener('click', imageViewer);
    document.getElementById('btn-previous').addEventListener('click', imageViewer);
    document.getElementById('btn-next').addEventListener('click', imageViewer);
    document.getElementById('btn-last').addEventListener('click', imageViewer);

    console.log('End...');
}
