// test.js
/*
- start executing javasript once the whole DOM  content is loaded
*/
document.addEventListener('DOMContentLoaded',init);
/*
in test.js, write the following functions
- init or main : fireup once the DOM is loaded.
.call intiTheGForm
. and register rememberChanges to user, comment and semester.
. register  forgetChanges to forgetBnt
. find the adequate event you need to attach to user, comment, semester and forgetBnt
 */
function init()
{
    initTheForm();
    document.getElementById('regByn').addEventListener('click',rememberChanges);
    document.getElementById('forgetBtn').addEventListener('click',forgetChanges);

}

/*
-initTheForm
.set user, semester and comment with data that may have been saved in
the local storage
*/
function initTheForm()
{
    if(localStorage.length == 0)
    {
        document.getElementById('user_name').innerText = '';
        document.getElementById('user_name').innerText = '';
    }
    else
    {
        document.getElementById('user_name').innerText = localStorage.getItem('name');
        document.getElementById('user_name').innerText = localStorage.getItem('comments');
    }
}
/*
- rememberChanges
.save data entered in user, semester and comment in local storage
.for semester you will need to use getOptionChosen defined below.
.log message 'no autosave used for this elem' if the elem is not user/sem/comment.
*/
function rememberChanges()
{
    localStorage.setItem('name',document.getElementById('user_name').innerText);
    localStorage.setItem('semester',getOptionsChosen() );
    localStorage.setItem('comments',document.getElementById('comments').innerText);

    if (event.target == document.getElementById('use_name') || event.target == document.getElementById('comments'))
    {
        console.log('No autosave used for this element');
    }
}

/*
-forgetChanges
.use clear and test it then comment it,
.test another alternative:  use a loop on localStorage and remove item by item
*/
function forgetChanges()
{
    document.localStorage.clear();
}
/*
-getOptionChosen:
.receives an array and use forEach to determine the index of the checked option
.return the position of the checked  option.
*/
function getOptionsChosen()
{
    var options = document.getElementsByName('semester');

    for(var i = 0; i < options.length; i++)
    {
        if(options[i].checked)
        {
            return i;
        }
    }
}

/*
notice that submit button is disabled
if we click forget, all data that were saved in local storage are removed.
test by entering data, closing browser, reopening browser and display the page (without forget, and with forget)
*/
