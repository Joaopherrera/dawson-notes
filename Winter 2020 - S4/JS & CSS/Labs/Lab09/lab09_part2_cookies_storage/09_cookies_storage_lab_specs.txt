Web Storage, Cookies 420-423 JavaScript Lab 9
Cookies & Web Storage


## Running a local server

Up to now we've been debugging/viewing our web apps by simply opening HTML files in various
browsers. Opening a local file in a browser does not use HTTP

Since cookies are meant to be used with HTTP, you must debug your cookie JavaScript
code by starting a local web server that hosts the source files you're working on. This means you
have to start some sort of program in your OS that responds to HTTP requests (that's what a web
server does).

In our computer labs, the easiest way to do this is with the Atom Live Server plugin (already
installed). Open your 423-study directory in Atom, then do Packages > atom-live-server > start.
This starts a web server that listens to HTTP requests on port 3000 (by default). It serves all
the files at the root of your project directory, so you'll be able to navigate to a URL like
the following in any browser:

```
http://127.0.0.1:3000/exercises/09_storage/lab9.html
```

Spec

Edit the html/js files provided alongside these instructions.

### Cookies

_In real life, we should only use cookies in the context of communicating with server-side code,
but here we will practice interacting with cookies on the client side only just for the sake of
practice._

Use a cookie to detect whether the user has visited this page before:

*   Rather than using the helper methods shown in class, write the cookie-manipulation code yourself
    from scratch specifically for this situation to practice for the final exam.
	create an empty object called _cookie{},  and assign helper functions such as:
   createCookie, deleteCookie, updateCookie, removeAllCookies, getAllCookies.
   _cookie{ f1:function (), f2:function (params), ...};
* we want to recognze the user in the future even of they entered data but never hit on submit. 
-> all fields (except passwords) , whenever they have been changed, you store a cookie for each one of them.
the expiry should be set to (initially 60seconds- for your testing) 15 days.
 - The cookies should be able to recognize the visiting user whenver he/she revisits any page of 
 our website (sub-domains included).
Whenever a user revisit the page:
 - a welcome message is displayed on top with the user name and his city.
 - all fields that he/she filled last time should be redisplayed (drop down menu should be set properly)
    The role of the cookie is simply to detect whether our
    site has been visited by this user before. 
	*   Remember that cookies can easily be modified by the user, so use validation and/or    
        `encodeURIComponent`/`decodeURIComponent` when reading in cookie data.
*   If user has previously visited, display "Welcome back! Name from City" at the top of the body. 
When the webpage loads you should verify if this particular cookie exists for the site. 
The welcome message should consist of the following elements inserted into the top of the body by 
your JavaScript.
    Use `document.body.prepend` method:

    ```HTML
    <header><p>Welcome back!</p></header>
    ```

*   If the user hasn't visited before (or they visited more than 20 days ago, so the
    cookie has expired), you can just make the header empty:

    ```HTML
    <header></header>
    ```

*   Every time the user visits the expiration date of the cookies should be push forward (i.e.
    the existing cookie, if any, should be replaced with one that expires 30 days from today)

Does it work?

*   Look at "Storage" in dev tools to find your cookie. Verify the name, value, expiration date.
*   Close your HTML doc in the browser then open it again to check that the welcome message
    displays, and that all his/her information are displayed properly.
*   Use dev tools to manually delete the cookie and check that when the doc is reloaded the welcome
    message is no longer displayed.

### Part2 Local storage
* remove all cookies in part 1, and use a single cookie for the last name only and 
only to show the welcome message.

All other data will be remebered usimng localStorage, as explained below:
Use `localStorage` to remember the data (excpet passwords) entered by the user into the registration form.

*   When the web page loads you should verify form data is available in local storage for the site.
    If yes, you will need to load the data from local storage into the form fields.

*   You have to decide how to represent the form data in local storage. Some options are: a
    key-value for each form field? Or one (long)key-value for the whole form where the value is a string
    or object literal for all the fields? Or...?

*   Whenever a form field changes you must update (or add) the data in `localStorage`. If the data
    is invalid, don't store it in local storage.

*   As with cookies, local storage is easy to tamper with outside of your application, so when
    reading in data from local storage include some defensive steps and validation. Don't assume
    that the data will be well-formed or valid.

*   Add a "Forget" button at the top of the page that resets the form fields 
    and deletes all the saved form data from local storage. (this is more than reset)

*   When the user switches between "Canada" and "USA", the state/province and zip/postalcode fields
    change. If there's a relevant value in localStorage 
	(e.g. user switches to Canada and there's a province value in localStorage already) 
	then update the value. 
	
*   You might have to refactor some of the starter JavaScript code -- feel free to do so!

Does it work?

*   If the user starts filling the form, leaves the page without submitting, and then returns
    later, the values they entered should still be there (unless they were invalid).
*   Use the browser's dev tools to verify that data is being stored.
*   Use the browser's dev tools to change/delete data in local storage. Does your web app
    deal with these manual changes gracefully when the page is reloaded?

## Submission
show to your instructor
