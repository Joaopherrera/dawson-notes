"use strict";

var g = {
  /* eslint-disable no-useless-escape */
  isNotName: /[$-/:-?{-~!"^_`\[\]0-9]/,
  isEmail: /^[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}$/,
  isPostal: /^[a-zA-Z]\d[a-zA-Z][ ]?\d[a-zA-Z]\d$/,
  isZip: /^[0-9]{5}$/,
  isPhone: /^\d\d\d-\d\d\d-\d\d\d\d$/,
  // Using a rough regex; will combine it with string-based checks
  isLikeDate: /^\d\d\d\d-\d*\d-\d*\d$/
};

function getValue(id) {
  var field = document.getElementById(id);
  if (field && field.value) {
    return field.value;
  }
}

g.validationFunctions  = {
  firstname: getValidName,
  lastname: getValidName,
  email: getValidEmail,
  birthdate: getValidBirthDate,
  phoneNumber: getValidPhone,
  city: getValidCity,
  country: getValidCountry,
  province: getValidProvince,
  postalcode: getValidPostalCode,
  state: getValidState,
  zipcode: getValidZipCode
};

function getValidName(name) {
  if (name) {
    if (name.length < 2) {
      return;
    }
    if (g.isNotName.test(name)) {
      return;
    }
    return true;
  }
}

function getValidEmail(email) {
  if (email) {
    if (email.length < 5) {
      return;
    }
    if (g.isEmail.test(email)) {
      return true;
    }
  }
}

function getValidBirthDate(date) {
  // optional
  if (date === undefined) {
    return true;
  }
  if (g.isLikeDate.test(date)) {
    // parse into an array of numbers
    var values = date.split("-").map(function (i) {
      return parseInt(i);
    });
    // use Date object to check that values correspond to an existing date (e.g. not 30/02/2000)
    // note that Date constuctor expects a 0-based index for month (e.g. March is 2)
    var dateObject = new Date(values[0], values[1] - 1, values[2]);
    // when you create a Date object with weird value like "14th month", the adjacent value
    // is adjusted, so we would get back different data for date, month and year than what is
    // in values if that's the case.
    if (dateObject.getDate() !== values[2]) {
      return;
    }
    if (dateObject.getMonth() !== values[1] - 1) {
      return;
    }
    if (dateObject.getFullYear() !== values[0]) {
      return;
    }
    if (values[0] < 1900 || values[0] > (new Date()).getFullYear()) {
      return;
    }
    return true;
  }
}

function getValidPhone(phone) {
  // optional
  if (phone === undefined) {
    return true;
  }
  if (g.isPhone.test(phone)) {
    return true;
  }
}

function getValidCity(city) {
  // optional
  if (city === undefined) {
    return true;
  }
  return getValidName(city);
}

function getValidPostalCode(code) {
  // optional
  if (code === undefined) {
    return true;
  }
  if (code.length > 5 && g.isPostal.test(code)) {
    return true;
  }
}

function getValidZipCode(code) {
  // optional
  if (code === undefined) {
    return true;
  }
  if (code.length === 5 && g.isZip.test(code)) {
    return true;
  }
}

function getValidCountry(country) {
  // optional
  if (country === undefined) {
    return true;
  }
  var choices = document.getElementById("country").querySelectorAll("option");
  for (var i = 0; i < choices.length; i++) {
    if (country === choices[i].value) {
      return true;
    }
  }
}

function getValidProvince(province) {
  // optional
  if (province === undefined) {
    return true;
  }
  var choices = [
    "Nunavut", "Alberta", "Saskatchewan", "Yukon", "Manitoba", "British Columbia",
    "Ontario", "Quebec", "Prince Edward Island", "Newfoundland and Labrador",
    "Northwest Territories", "Nova Scotia", "New Brunswick"
  ];
  if (choices.indexOf(province) >= 0) {
    return true;
  }
}

function getValidState(state) {
  // optional
  if (state === undefined) {
    return true;
  }
  var choices = [
    "Alabama",
    "Alaska",
    "Arizona",
    "Arkansas",
    "California",
    "Colorado",
    "Connecticut",
    "Delaware",
    "Florida",
    "Georgia",
    "Hawaii",
    "Idaho",
    "Illinois",
    "Indiana",
    "Iowa",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Maine",
    "Maryland",
    "Massachusetts",
    "Michigan",
    "Minnesota",
    "Mississippi",
    "Missouri",
    "Montana",
    "Nebraska",
    "Nevada",
    "New Hampshire",
    "New Jersey",
    "New Mexico",
    "New York",
    "North Carolina",
    "North Dakota",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Rhode Island",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Vermont",
    "Virginia",
    "Washington",
    "West Virginia",
    "Wisconsin",
    "Wyoming",
    "District of Columbia",
    "Puerto Rico",
    "Guam",
    "American Samoa",
    "U.S. Virgin Islands",
    "Northern Mariana Islands"
  ];
  if (choices.indexOf(state) >= 0) {
    return true;
  }
}

function updateRedBorder(element, off) {
  if (off) {
    element.style.border = "";
  } else {
    element.style.border = "solid 1px red";
  }
}

function changeHandler(e) {
  validateOne(e.target);
}

function validateOne(el) {
  var value = getValue(el.id);
  var isValid = g.validationFunctions[el.id];
  if (typeof isValid === "function") {
    var valid = isValid(value);
    updateRedBorder(el, valid);
    if (valid && value !== undefined) {
      localStorage.setItem(el.id, value);
    }
    return valid;
  } else if (isValid === undefined) {
    // we don't have a validation function for this element, so we never mark it as invalid
    return true;
  }
}

function validateAll(e) {
  var valid = true;
  var allFields = g.fieldSet.querySelectorAll("input, select");
  for (var i = 0; i < allFields.length; i++) {
    if (allFields[i].id) {
      valid = valid && validateOne(allFields[i]);
    }
  }
  if (!valid) {
    e.preventDefault();
    g.errorMessage.style.display = "block";
    setTimeout(function () {
      g.errorMessage.style = "";
    }, 10000);
  }
}

function updateCountryFields(e) {
  if (e.target.value === "Canada") {
    changeLabel(g.countryDiv.children[0], g.countryDiv.children[1], "Province");
    changeLabel(g.countryDiv.children[2], g.countryDiv.children[3], "Postal Code");
  } else if (e.target.value === "USA") {
    changeLabel(g.countryDiv.children[0], g.countryDiv.children[1], "State");
    changeLabel(g.countryDiv.children[2], g.countryDiv.children[3], "Zip Code");
  }
}

function changeLabel(label, input, newLabelString) {
  var id = newLabelString.toLowerCase().replace(/\s/g, "");
  label.textContent = newLabelString;
  label.setAttribute("for", id);
  input.setAttribute("name", id);
  input.setAttribute("id", id);
}

function initializeWelcome() {
  // TODO -- put your cookie solution here
  // find the cookie
  // if cookie found, set up the welcome message
  // otherwise clear the header
}

document.addEventListener("DOMContentLoaded", function () {
  initializeWelcome();
  document.getElementById("theForm").addEventListener("submit", validateAll);
  document.getElementById("country").addEventListener("change", updateCountryFields);
  document.getElementById("theForm").addEventListener("change", changeHandler);
  g.countryDiv = document.getElementById("countryFields");
  g.errorMessage = document.getElementById("errorMessage");
});
