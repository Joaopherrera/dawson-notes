//--------------------  file.js---------------------//

//Joao Pedro Morais Herrera - ID 1835031

// 0.    You may use a global object (namespace) to hold filenames, their contents, …to keep track of the 3 text files
// and/or other pertinent variables .
var testQuestions = [];


// 1.    your code can only run once the whole DOM has been loaded.
document.addEventListener('DOMContentLoaded', init);

// 2.    use a function called init that acts like your main program entry, and in which
// you call other functions, do your initializations, register event listeners, etc.
function init()
{
    cachingQuestionFiles();

    question6_setMenu();
}


/* 3. write a function called getQuestionsFromServer that receives a filename and uses AJAX to return the (text) content of that file.
 (The function will be used for each of the 3 text filenames to return the test questions that are stored in that text file)
*/
function getQuestionsFromServer(filename)
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function ()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            return this.responseText;
        }
    }
    xmlhttp.open("GET", filename, true);
    xmlhttp.send();
}

/* 4.    All the three test questions files must be cached (preferably in an array) before
the user starts interacting with the application.
*/
function cachingQuestionFiles()
{
    testQuestions = [
        getQuestionsFromServer('coding.txt'),
        getQuestionsFromServer('multipleChoice.txt'),
        getQuestionsFromServer('shortAnswers.txt')];
}


/* 5.    Dynamically create a div and set its id to 'questionId' (creation should be done once).
This div will be attached dynamically to one of the three sections and, later on, its content is to be set with the right test questions.
*/
function createDiv()
{
    var questionDiv = document.createElement('div');
    questionDiv.id = 'questionId';
}

/* 6.    Function question6_setMenu - Setting the mainMenu:
a.    Modify the properties of the elements inside the <nav> element in order to make the 4 li elements display horizontally.
b.    each li element gets its background toggled when the mouse enter/exit it (see point7)
c.    each li element gets its corresponding section toggled when it is clicked (see point8)
*/
function question6_setMenu()
{
    var thisNav = document.getElementsByTagName('nav');
    [].slice.call(thisNav.getElementsByTagName('li').forEach(function (li)
    {
        li.style.display = 'inline-block';
        li.addEventListener('mouseenter', function () { toggleBackground(li, 'enter'); });
        li.addEventListener('mouseleave', function () { toggleBackground(li, 'leave'); });
        li.addEventListener('click', function() { toggleSections();});
    }));
}

/*7.    Function toggleBackground : Whenever the mouse enters/exits any li element event handler toggleBackground is triggered.
 When mouse is inside li element, change the background of the element to red and its text color to white,
 and when outside change the background back to white and text color to black. 
*/
function toggleBackground(li, option)
{
    if(option == 'enter')
    {
        li.style.backgroundColor = "red";
        li.style.color = "white";
    }
    else if(option == 'leave')
    {
        li.style.backgroundColor = "white";
        li.style.color = "black";
    }
}

/*8.    Function toggleSections: Whenever user clicks on any li element, event handler toggleSections  is triggered.
When an <li> element is clicked display the section element that has the same name as the text content of the li element,
and hide all the rest.
For example, if <li>Multipe Choice</li> is clicked, then show the section with id= multipleChoice’.
Hide all of the other section elements in such a way that they do not take any space on the page.
*/
function toggleSections()
{

}

/*9.    Function displayQuestionList: Whenever one of the three sections are displayed (other than the Home section),
 you must load the comma separated list of questions into the div identified by ‘questionId’.
 This div must then be attached to the section that is active. Make sure you do not attach the div more than once.
a.    Do not use innerHTML to write the questions into the div identified by ‘questionId’.
b.    You may use a global object (namespace) to hold filenames, their contents, …to keep track of the 3 text files .
*/
function displayQuestionList(sectionsArr)
{

}
