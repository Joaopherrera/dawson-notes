

//var myJsonText = `[{ "userId":"psmith", "mark":75.5, "grade":"A"},
//								{ "userId":"hsara", "mark":55.5, "grade":"F"},
//							{ "userId":"klim", "mark":65.5, "grade":"C"}]`;

document.addEventListener('DOMContentLoaded', main);
//-----------------
function main()
{
	 var jsObj = null;//JSON.parse(myString);
	 document.getElementById('loadBtn').addEventListener('click',
														function()
														{  xmlObj = new XMLHttpRequest();
															url = "server_data/employees.json";
															xmlObj.open('GET', url, true);
															xmlObj.send();
															xmlObj.addEventListener('readystatechange',
																					function() {
																						if(this.readyState==4  && this.status==200)
																						{
																							theObject = JSON.parse(this.responseText);
																							processMyArrayObjects(theObject.employees.employee);
																						}
																					});
														}
													);
}
//-----------------
function processMyArrayObjects(arrObjects)
{
//keys=['name', 'age', 'prog'];
//arrObjects = [{name:"sara", age:24, prog:"CS"},
          //{name:"dara", age:42, prog:"SC"}  ];

theKeys = Object.keys(arrObjects[0]);

	createTable(theKeys, arrObjects);
}
//--------
function createTable(theHeading, theRows)
{
	var myTable = document.createElement('table');
	//step1  - add row for headings
	var tr =null, th=null, td=null;
	tr = document.createElement('tr');
  theHeading.forEach(function(elem) {
		 th = document.createElement('th');
		 th.appendChild(document.createTextNode(elem));
		 tr.appendChild(th);
	});
	myTable.appendChild(tr);

	//step2  -  add row for data
	//(only  for an array of objects (no array inside each object))
//----------------
//[{key1:val1, key2:val2}, {}, {}]
  theRows.forEach(function(obj)
	{
		tr = document.createElement('tr');
			for (key in obj)
			{
				td = document.createElement('td');
	 		 td.appendChild(document.createTextNode(obj[key]));
	 		 tr.appendChild(td);
			}
				myTable.appendChild(tr);

	});

//---- add table to body
	document.body.appendChild(myTable);

}
//------------------------
