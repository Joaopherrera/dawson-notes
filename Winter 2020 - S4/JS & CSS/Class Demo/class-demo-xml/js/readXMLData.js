document.addEventListener('DOMContentLoaded', main);

function main()
{
var hardCodedText, parser, myXMLDocTree;
var hardCodedText = "<announcements>"+
"	<announcement>"+
"		<to> my JS students </to>"+
"		<from> me of course </from>"+
"		<subject>graded lab </subject>"+
"		<date> "+
"			<day>9</day>"+
"			<mon>04</mon>"+
"			<year> 2019</year>"+
"		</date>"+
"	</announcement>"+
"		<announcement>"+
"		<to>  JS pple </to>"+
"		<from> prof</from>"+
"		<subject>final exam </subject>"+
"		<date> "+
"			<day>23</day>"+
"			<mon>05</mon>"+
"			<year> 2019</year>"+
"		</date>"+
"	</announcement>"+
"</announcements>";
//--------------
parser = new DOMParser();
myXMLDocTree = parser.parseFromString(hardCodedText,"text/xml");

var myElementsArr = myXMLDocTree.getElementsByTagName("announcements")[0].children;
createMyDynamicTable(myElementsArr);
//-----------------
function createMyDynamicTable(myElementsArr) {
  var myTable = document.createElement("TABLE");
  myTable.setAttribute("id", "myTable");
  document.body.appendChild(myTable);
//------1st table row with th (table headings
  var tr = document.createElement("TR");
    var th1	= document.createElement("TH");  th1.appendChild(document.createTextNode('to'));
	var th2	= document.createElement("TH");  th2.appendChild(document.createTextNode('from'));
	var th3	= document.createElement("TH");  th3.appendChild(document.createTextNode('subject'));
	var th4	= document.createElement("TH");  th4.appendChild(document.createTextNode('date'));
tr.append(th1, th2, th3, th4);
  //tr.appendChild(th1);tr.appendChild(th2);tr.appendChild(th3);tr.appendChild(th4);
	myTable.appendChild(tr);
  //------END 1st table row with th (table headings
  // loop through myElementsArr to get the elements data
  for (var i=0; i<myElementsArr.length; i++)
  {
	tr = document.createElement("TR");
	var annouceDataArr = myElementsArr[i].children;
	var td1	= document.createElement("TD");  td1.appendChild(document.createTextNode(annouceDataArr[0].innerHTML));//to
	var td2	= document.createElement("td");  td2.appendChild(document.createTextNode(annouceDataArr[1].innerHTML)); //'from'
	var td3	= document.createElement("tD");  td3.appendChild(document.createTextNode(annouceDataArr[2].innerHTML)); //subject'

	var td4	= document.createElement("Td");
		//td4.appendChild(document.createTextNode(annouceDataArr[3].innerHTML)); //'date'
	//or
	 var dateDataArr = annouceDataArr[3].children;
	 var dateString = dateDataArr [0].innerHTML+"-"+dateDataArr [1].innerHTML+"-"+dateDataArr [2].innerHTML;
		td4.appendChild(document.createTextNode(dateString)); //'date'
    tr.append(td1, td2, td3, td4);
  //tr.appendChild(td1);tr.appendChild(td2);tr.appendChild(td3);tr.appendChild(td4);
	myTable.appendChild(tr);
	}//end for
}
//--------------
}//main
