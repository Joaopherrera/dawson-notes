document.addEventListener('DOMContentLoaded', main);

function main()
{
var  myXMLDocTree;
//--------------
document.querySelector('button').addEventListener('click', xmlAjaxPopulateTable);
function xmlAjaxPopulateTable()
{
var xmlHttpObj = new XMLHttpRequest();
var url ="../xmlData/employees.xml";
xmlHttpObj.open('GET', url, true);
xmlHttpObj.send();

xmlHttpObj.addEventListener('readystatechange',
  function(){
     if(this.readyState==4  && this.status==200)
     {
       var myEmployeesArr = this.responseXML.documentElement.children;
       createMyDynamicTable(myEmployeesArr);
     }
  } );

}

//-----------------
function createMyDynamicTable(myEmployeesArr) {
  var myTable = document.createElement("TABLE");
  myTable.setAttribute("id", "myTable");
  document.body.appendChild(myTable);
//------1st table row with th (table headings
  var tr = document.createElement("TR");

 var employeeRecord = myEmployeesArr[0].children;
 for(var i=0; i< employeeRecord.length; i++)
 {
   var th1	= document.createElement("TH");
       th1.appendChild(document.createTextNode(employeeRecord[i].nodeName));
 	     tr.appendChild(th1);
 }
  myTable.appendChild(tr);
  //------END 1st table row with th (table headings
  // loop through myEmployeesArr to get the elements data
  for (var i=0; i<myEmployeesArr.length; i++)
  {
	tr = document.createElement("TR");
	 employeeRecord= myEmployeesArr[i].children;
   for (var j=0; j<employeeRecord.length; j++)
   {
     var td1	= document.createElement("TD");
     td1.appendChild(document.createTextNode(employeeRecord[j].innerHTML));//to
   tr.appendChild(td1);
   }
   myTable.appendChild(tr);
	}//end for
}
//--------------
}//main
