$(function(){

		setUI();
		initEvents();
		}
	);

//set the UI
function setUI()
{
$("input[type='submit']").addClass('disabledBtn');

$('label').addClass('col-25');
$('input[type=text]').addClass('col-75');
$('input[type=password]').addClass('col-75');

}
//-------------
function disableSubmit() {
	$("input[type='submit']").removeClass('enableBtn')
	$("input[type='submit']").addClass('disabledBtn')
}

function enableSubmit() {
	$("input[type='submit']").removeClass('disabledBtn')
	$("input[type='submit']").addClass('enableBtn')
}
//----
// set evenets
function initEvents()
{
//checValidnameInput('#lname', lnameErrDiv)

	$('#fname').blur(function() {
			checValidnameInput('#fname', 'fnameErrDiv', 6, 'your fname must be >=6');
		});
	$('#lname').blur(function() {
			checValidnameInput('#lname', 'lnameErrDiv', 10, 'your Lname must be >=10');

																});
	 $('#password2').blur(function() {
		  validatePasswords();
					});

}
//---------
// check if form complete inm order to enable button

function checValidnameInput(nameId, nameErrDiv, maxLen, errMsg)
{
	var nameId = $(nameId);
	if(nameId.val().length<maxLen)
	{
		if(($('#'+nameErrDiv).length)==0)
	nameId.parent().append('<div id="'+nameErrDiv+'" class="errorDiv" > '+errMsg+'</div>');
//or nameId.before('<div id="'+nameErrDiv+'" class="errorDiv" id="fnameError">helleo</div>');
return 1; //there is an error
	}
	else $('#'+nameErrDiv).remove();
return 0; //there is NO an error
}
//-- validate Passwords
function  validatePasswords(){
	if($('#password1').val()!== $('#password2').val())
	{
		$('#password1').parent().append('<div id="passErrDiv" class="errorDiv">pass do not match</div>');
			$('#password1').focus();
return 1;
	}
	else $('#passErrDiv').remove();
return 0;
		;
}
//------------------
// // --- hide/show all paragraphs
function hideShowAllP()
{
$("button").click(function(){
	$("p").toggle();
});
}
//---------------
function hideShowAllNote()
{
$("button").click(function(){
	$(".note").toggle();
});
}
