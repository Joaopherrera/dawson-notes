
function random(number) {
  return Math.floor(Math.random()*(number+1));
}


function setInput() 
{  
var data = document.getElementById('someId').value;
if (data === 'NEW VAL') 
	 document.getElementById('someId').value = "OLD VALUE";
else document.getElementById('someId').value = "NEW VAL";
}
function changeBGD() {
  var rndCol = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
  document.body.style.backgroundColor = rndCol;
}

function init()
{
var btn = document.querySelector('button');

btn.addEventListener('click', changeBGD);
btn.addEventListener('click', setInput);
}
document.addEventListener('DOMContentLoaded', init);

