document.addEventListener('DOMContentLoaded', main);
function main()
var txtArea1 = document.getElementById('txtArea1');
{
var btn = document.querySelector("[type=button]");

btn.addEventListener('click', ajaxTextAreaUpdate);
//------------------------------
 function ajaxTextAreaUpdate()
 {
/*  for checking how asynchronous really works
 var index=1;
var clkId = setInterval(function() { console.log('updating index:'+ (index++));} , 1);
var xmlHttpObj = createXMLHttpRequestObject(); //new XMLHttpRequest();
*/
xmlHttpObj.addEventListener('readystatechange',
    function() {
      if((xmlHttpObj.readyState==4) && (xmlHttpObj.status==200) ) //ready(received) and ok
       {
         txtArea1.value= xmlHttpObj.response; // responseText  response
         //clearInterval(clkId);
       }
    }
  );
  xmlHttpObj.open('GET', 'myFile.xml', true); //true->asynchronous   false-> synchronous
  xmlHttpObj.send();
}// ajaxTextAreaUpdate
//---------------------------------
function createXMLHttpRequestObject()
{
  if(XMLHttpRequest) {return new XMLHttpRequest();}
  else {
      return new ActiveXObject('Microsoft.XMLHTTP');
  }
}
}//main
