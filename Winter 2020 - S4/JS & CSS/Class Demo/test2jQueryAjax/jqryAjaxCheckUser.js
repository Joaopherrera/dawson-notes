var xmlhttp=null;
document.addEventListener('DOMContentLoaded', init);
function init ()
{document.querySelector('#userId').addEventListener('blur', checkUserInServer);}

function checkUserInServer() {
	var user_id = document.getElementById('userId').value;
	//xmlhttp = createXMLHttpRequest();

   var request= $.ajax(
   {
	   url:"userIdExist.php", //test (debug) with a wrong name for the server (php)file  to see the error (404 doesnt exist)
								//test (debug) with an error inside  the server (php)file  to see the error (500 )
	   data:{userId:user_id},
	   type:"GET",
	   success: function(data, response, r) {
								$("#userId_feedback").test(data);
								if (data=="0")	$("#userId_feedback").css("border","1px solid green");
								else 			$("#userId_feedback").css("border","1px solid red");
								},
	   error: function(r, errorType){
		   console.log(r.status);
	   }

   }
   );

   /*xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("userId_feedback").innerHTML =
      this.responseText;
    }
  };
  //for GET
  xmlhttp.open("GET", "userIdExist.php?userId=" + user_id, true);
  xmlhttp.send();
  //For POST
  xmlhttp.open("POST", "userIdExist.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send("userId="+user_id);
  */
}
//-------------
//for com[atibility with old IEs
function createXMLHttpRequest()
{ if (window.XMLHttpRequest) {   // code for modern browsers
   xhttp = new XMLHttpRequest(); }
    else {    // code for old IE browser   
   xhttp = new ActiveXObject("Microsoft.XMLHTTP");
   }
   return xhttp;
}
