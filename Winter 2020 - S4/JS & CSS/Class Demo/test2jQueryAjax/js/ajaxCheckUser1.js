var xmlhttp=null;
document.addEventListener('DOMContentLoaded', init);
function init ()
{document.querySelector('#userId').addEventListener('blur', checkUserInServer);}

function checkUserInServer() {
	var user_id = document.getElementById('userId').value;
	xmlhttp = createXMLHttpRequest();

  xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("userId_feedback").innerHTML =
      this.responseText+' -> js';
    }
  };
  //for GET
  xmlhttp.open("GET", "userIdExist.php?userId=" + user_id, true);
  xmlhttp.send();
  /*For POST
  xmlhttp.open("POST", "userIdExist.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send("userId="+user_id);
  */
}
//-------------
//for com[atibility with old IEs
function createXMLHttpRequest()
{ if (window.XMLHttpRequest) {   // code for modern browsers
   xhttp = new XMLHttpRequest(); }
    else {    // code for old IE browser   
   xhttp = new ActiveXObject("Microsoft.XMLHTTP");
   }
   return xhttp;
}
